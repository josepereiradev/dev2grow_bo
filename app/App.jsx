/*! testeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
 *
 * Angle - Bootstrap Admin App + ReactJS
 *
 * Version: 3.7
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, hashHistory, useRouterHistory, IndexRoute } from 'react-router';
import { createHistory } from 'history';
import { HashRouter } from 'react-router-dom'

import initTranslation from './components/Common/localize';
import initLoadThemes from './components/Common/load-themes';

import Base from './components/Layout/Base';
import BasePage from './components/Layout/BasePage';


import Dashboard from './components/Dashboard/Dashboard';


import Login from './components/Pages/Login';
import Register from './components/Pages/Register';
import Recover from './components/Pages/Recover';
import Lock from './components/Pages/Lock';
import NotFound from './components/Pages/NotFound';
import Error500 from './components/Pages/Error500';
import Maintenance from './components/Pages/Maintenance';

import Companies from './components/S2GPages/Company/Companies';
import AddCompany from './components/S2GPages/Company/AddCompany';
import EditCompany from './components/S2GPages/Company/EditCompany';

import AddCustomerCompany from './components/S2GPages/CustomersCompanies/AddCustomerCompany';
import EditCustomerCompany from './components/S2GPages/CustomersCompanies/EditCustomerCompany';

import Customers from './components/S2GPages/Customer/Customers';
import AddCustomer from './components/S2GPages/Customer/AddCustomer';
import EditCustomer from './components/S2GPages/Customer/EditCustomer';
import ViewCustomer from './components/S2GPages/Customer/CustomerDetails';

import Users from './components/S2GPages/Users/Users';
import AddUser from './components/S2GPages/Users/AddUser';
import EditUser from './components/S2GPages/Users/EditUser';

import Contacts from './components/S2GPages/Contacts/Contacts';
import AddContact from './components/S2GPages/Contacts/AddContact';
import EditContact from './components/S2GPages/Contacts/EditContact';
import ContactTypes from './components/S2GPages/Contacts/ContactType/ContactTypes';
import AddContactType from './components/S2GPages/Contacts/ContactType/AddContactType';
import EditContactType from './components/S2GPages/Contacts/ContactType/EditContactType';

import Contracts from './components/S2GPages/Contracts/Contracts';
import AddContract from './components/S2GPages/Contracts/AddContract';
import AddContractFromList from './components/S2GPages/Contracts/AddContractFromList';
import EditContract from './components/S2GPages/Contracts/EditContract';
import ContractDetails from './components/S2GPages/Contracts/ContractDetails';
import ContractTypes from './components/S2GPages/Contracts/ContractType/ContractTypes';
import AddContractType from './components/S2GPages/Contracts/ContractType/AddContractType';
import EditContractType from './components/S2GPages/Contracts/ContractType/EditContractType';
import ContractState from './components/S2GPages/Contracts/ContractState/ContractStates';
import AddContractState from './components/S2GPages/Contracts/ContractState/AddContractState';
import EditContractState from './components/S2GPages/Contracts/ContractState/EditContractState';

import AddAddendum from './components/S2GPages/Addenda/AddAddendum';
import EditAddendum from './components/S2GPages/Addenda/EditAddendum';

import Projects from './components/S2GPages/Projects/Projects';
import AddProjectFromList from './components/S2GPages/Projects/AddProjectFromList';
import AddProject from './components/S2GPages/Projects/AddProject';
import EditProject from './components/S2GPages/Projects/EditProject';
import ProjectDetails from './components/S2GPages/Projects/ProjectDetails';
import ProjectTypes from './components/S2GPages/Projects/ProjectType/ProjectTypes';
import AddProjectType from './components/S2GPages/Projects/ProjectType/AddProjectType';
import EditProjectType from './components/S2GPages/Projects/ProjectType/EditProjectType';
import ProjectState from './components/S2GPages/Projects/ProjectStates/ProjectStates';
import AddProjectState from './components/S2GPages/Projects/ProjectStates/AddProjectState';
import EditProjectState from './components/S2GPages/Projects/ProjectStates/EditProjectState';

import Processes from './components/S2GPages/Processes/Processes';
import AddProcessFromList from './components/S2GPages/Processes/AddProcessFromList';
import AddProcess from './components/S2GPages/Processes/AddProcess';
import EditProcess from './components/S2GPages/Processes/EditProcess';
import ProcessDetails from './components/S2GPages/Processes/ProcessDetails';
import ProcessTypes from './components/S2GPages/Processes/ProcessTypes/ProcessTypes';
import AddProcessType from './components/S2GPages/Processes/ProcessTypes/AddProcessType';
import EditProcessType from './components/S2GPages/Processes/ProcessTypes/EditProcessType';
import ProcessState from './components/S2GPages/Processes/ProcessStates/ProcessStates';
import AddProcessState from './components/S2GPages/Processes/ProcessStates/AddProcessState';
import EditProcessState from './components/S2GPages/Processes/ProcessStates/EditProcessState';

import Interventions from './components/S2GPages/Interventions/Interventions';
import AddIntervention from './components/S2GPages/Interventions/AddIntervention';
import AddInterventionFromList from './components/S2GPages/Interventions/AddInterventionFromList';
import EditIntervention from './components/S2GPages/Interventions/EditIntervention';
import InterventionDetails from './components/S2GPages/Interventions/InterventionDetails';
import InterventionTypes from './components/S2GPages/Interventions/InterventionType/InterventionTypes';
import AddInterventionType from './components/S2GPages/Interventions/InterventionType/AddInterventionType';
import EditInterventionType from './components/S2GPages/Interventions/InterventionType/EditInterventionType';
import InterventionMeans from './components/S2GPages/Interventions/InterventionMeans/InterventionMeans';
import AddInterventionMean from './components/S2GPages/Interventions/InterventionMeans/AddInterventionMean';
import EditInterventionMean from './components/S2GPages/Interventions/InterventionMeans/EditInterventionMean';

import Templates from './components/S2GPages/Templates/Templates';
import EditTemplates from './components/S2GPages/Templates/EditTemplates';

import Functions from './components/S2GPages/Users/UserFunction/Functions';
import AddFunction from './components/S2GPages/Users/UserFunction/AddFunction';
import EditFunction from './components/S2GPages/Users/UserFunction/EditFunction';



// Application Styles
import './styles/bootstrap.scss';
import './styles/app.scss'


// Init translation system
initTranslation();
// Init css loader (for themes)
initLoadThemes();

// Disable warning "Synchronous XMLHttpRequest on the main thread is deprecated.."
$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
    options.async = true;
});

// specify basename below if running in a subdirectory or set as "/" if app runs in root
const appHistory = useRouterHistory(createHistory)({
    basename: WP_BASE_HREF
})


function requireAuth(nextState, replace) {
	$('#loader').show();
    if (!sessionStorage.getItem('user')) {
        replace({
			pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
		});
    }
}

function disableLoader(nextState, replace) {
	$('#loader').hide();
}

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={Base}>
            {/*Dashboard*/}
            <IndexRoute component={Dashboard} onEnter={requireAuth} />

            <Route path="companies" component={Companies} onEnter={requireAuth} />
            <Route path="companies/create" component={AddCompany} onEnter={requireAuth} />
            <Route path="companies/edit/:id" component={EditCompany} onEnter={requireAuth} />

            <Route path="customersCompanies/create/:id" component={AddCustomerCompany} onEnter={requireAuth} />
            <Route path="customersCompanies/edit/:id" component={EditCustomerCompany} onEnter={requireAuth} />

            <Route path="customers" component={Customers} onEnter={requireAuth} />
            <Route path="customers/create" component={AddCustomer} onEnter={requireAuth} />
            <Route path="customers/edit/:id" component={EditCustomer} onEnter={requireAuth} />
            <Route path="customers/view/:id/:tabNumber" component={ViewCustomer} onEnter={requireAuth} />

            <Route path="users" component={Users} onEnter={requireAuth} />
            <Route path="users/create" component={AddUser} onEnter={requireAuth} />
            <Route path="users/edit/:id" component={EditUser} onEnter={requireAuth} />
            <Route path="functions" component={Functions} onEnter={requireAuth} />
            <Route path="functions/create" component={AddFunction} onEnter={requireAuth} />
            <Route path="functions/edit/:id" component={EditFunction} onEnter={requireAuth} />


            <Route path="contacts" component={Contacts} onEnter={requireAuth} />
            <Route path="contacts/create/:idEntity" component={AddContact} onEnter={requireAuth} />
            <Route path="contacts/edit/:idEntity/:id" component={EditContact} onEnter={requireAuth} />
            <Route path="contactTypes" component={ContactTypes} onEnter={requireAuth} />
            <Route path="contactTypes/create" component={AddContactType} onEnter={requireAuth} />
            <Route path="contactTypes/edit/:id" component={EditContactType} onEnter={requireAuth} />

            <Route path="contracts" component={Contracts} onEnter={requireAuth} />
            <Route path="contracts/view/:idEntity/:id" component={ContractDetails} onEnter={requireAuth} />
            <Route path="contracts/create" component={AddContractFromList} onEnter={requireAuth} />
            <Route path="contracts/create/:idEntity" component={AddContract} onEnter={requireAuth} />
            <Route path="contracts/create/:idEntity/:idContract" component={AddContract} onEnter={requireAuth} />
            <Route path="contracts/edit/:idEntity/:id" component={EditContract} onEnter={requireAuth} />
            <Route path="contractTypes" component={ContractTypes} onEnter={requireAuth} />
            <Route path="contractTypes/create" component={AddContractType} onEnter={requireAuth} />
            <Route path="contractTypes/edit/:id" component={EditContractType} onEnter={requireAuth} />
            <Route path="contractStates" component={ContractState} onEnter={requireAuth} />
            <Route path="contractStates/create" component={AddContractState} onEnter={requireAuth} />
            <Route path="contractStates/edit/:id" component={EditContractState} onEnter={requireAuth} />
 
            <Route path="addenda/create/:idEntity/:idContract" component={AddAddendum} onEnter={requireAuth} />
            <Route path="addenda/edit/:idEntity/:idContract/:id" component={EditAddendum} onEnter={requireAuth} />

            <Route path="projects" component={Projects} onEnter={requireAuth} />
            <Route path="projects/view/:idEntity/:id" component={ProjectDetails} onEnter={requireAuth} />
            <Route path="projects/create" component={AddProjectFromList} onEnter={requireAuth} />
            <Route path="projects/create/:idEntity" component={AddProject} onEnter={requireAuth} />
            <Route path="projects/edit/:idEntity/:id" component={EditProject} onEnter={requireAuth} />
            <Route path="projectTypes" component={ProjectTypes} onEnter={requireAuth} />
            <Route path="projectTypes/create" component={AddProjectType} onEnter={requireAuth} />
            <Route path="projectTypes/edit/:id" component={EditProjectType} onEnter={requireAuth} />
            <Route path="projectStates" component={ProjectState} onEnter={requireAuth} />
            <Route path="projectStates/create" component={AddProjectState} onEnter={requireAuth} />
            <Route path="projectStates/edit/:id" component={EditProjectState} onEnter={requireAuth} />

            <Route path="processes" component={Processes} onEnter={requireAuth} />
            <Route path="processes/create" component={AddProcessFromList} onEnter={requireAuth} />
            <Route path="processes/create/:idEntity" component={AddProcess} onEnter={requireAuth} />
            <Route path="processes/view/:idEntity/:id" component={ProcessDetails} onEnter={requireAuth} />
            <Route path="processes/edit/:idEntity/:id" component={EditProcess} onEnter={requireAuth} />
            <Route path="processTypes" component={ProcessTypes} onEnter={requireAuth} />
            <Route path="processTypes/create" component={AddProcessType} onEnter={requireAuth} />
            <Route path="processTypes/edit/:id" component={EditProcessType} onEnter={requireAuth} />
            <Route path="processStates" component={ProcessState} onEnter={requireAuth} />
            <Route path="processStates/create" component={AddProcessState} onEnter={requireAuth} />
            <Route path="processStates/edit/:id" component={EditProcessState} onEnter={requireAuth} />

            <Route path="interventions/create/:idEntity/:idProcess" component={AddIntervention} onEnter={requireAuth} />
            <Route path="interventions/create/:idEntity/:idContractProject/:noData" component={AddIntervention} onEnter={requireAuth} />
            <Route path="interventions/create/:page" component={AddInterventionFromList} onEnter={requireAuth} />
            <Route path="interventions/create" component={AddInterventionFromList} onEnter={requireAuth} />
            <Route path="interventions" component={Interventions} onEnter={requireAuth} />
            <Route path="interventions/view/:idEntity/:idProcess/:id" component={InterventionDetails} onEnter={requireAuth} />
            <Route path="interventions/view/:id" component={InterventionDetails} onEnter={requireAuth} />
            <Route path="interventions/edit/:idEntity/:idProcess/:id" component={EditIntervention} onEnter={requireAuth} />
            <Route path="interventions/edit/:idEntity/:idProcess/:id/:fromProcess" component={EditIntervention} onEnter={requireAuth} />
            <Route path="interventionTypes" component={InterventionTypes} onEnter={requireAuth} />
            <Route path="interventionTypes/create" component={AddInterventionType} onEnter={requireAuth} />
            <Route path="interventionTypes/edit/:id" component={EditInterventionType} onEnter={requireAuth} />
            <Route path="interventionMeans" component={InterventionMeans} onEnter={requireAuth} />
            <Route path="interventionMeans/create" component={AddInterventionMean} onEnter={requireAuth} />
            <Route path="interventionMeans/edit/:id" component={EditInterventionMean} onEnter={requireAuth} />

            <Route path="templates" component={Templates} onEnter={requireAuth} />
            <Route path="templates/edit/:id" component={EditTemplates} onEnter={requireAuth} />
        </Route>



        {/*Pages*/}
        <Route path="/" component={BasePage} onEnter={disableLoader}>
            <Route path="login" component={Login} />

           
            <Route path="register" component={Register} />
            <Route path="recover" component={Recover} />
            <Route path="lock" component={Lock} />
            <Route path="notfound" component={NotFound} />
            <Route path="error500" component={Error500} />
            <Route path="maintenance" component={Maintenance} />
        </Route>

        {/* Not found handler */}
        <Route path="*" component={NotFound}  onEnter={disableLoader}/>

    </Router>
,
    document.getElementById('app')
);



// Auto close sidebar on route changes
hashHistory.listen(function (ev) {
    $('body').removeClass('aside-toggled');
});
