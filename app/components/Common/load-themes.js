// LOAD THEMES
// -----------------------------------


const themeE = require('!raw!sass!../../styles/themes/theme-e.scss');


export default () => {

    let styleTag;
    let defaultTheme = 'E';

    $(document).on('click', '[data-load-theme]', function(e) {
        createStyle();
       injectStylesheet(themeE);
    });


    function createStyle() {
        // remove if exists
        var el = document.getElementById('appthemes');
        if (el) el.parentNode.removeChild(el);
        // create
        const head = document.head || document.getElementsByTagName('head')[0];
        styleTag = document.createElement('style');
        styleTag.type = 'text/css';
        styleTag.id = 'appthemes';
        head.appendChild(styleTag);
    }

    function injectStylesheet(css) {
        styleTag.innerHTML = css;
    }

}