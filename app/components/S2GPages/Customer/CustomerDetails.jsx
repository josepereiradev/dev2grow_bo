import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Tab,
    Tabs,
    Table,
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class CustomerDetails extends React.Component {

    constructor() {
        super();
        this.state = {
            nif: "",
            qrCode: "",
            taxName: "",
            abrevName: "",
            selectedAccountManager: "",
            accountManagerName: "",
            customerContacts: [],
            customersContracts: [],

            entityProjects: [],
            entityProcesses: [],
            contacts: [],
            contractTypes: [],
            contractStates: [],
            projectStates: [],
            contactTypes: [],
            projectTypes: [],
            processTypes: [],
            processStates: [],
            contracts: [],
            projects: [],
            processes: [],
            interventions: [],
            users: [],
            associatedUsers: [],

            customerCompanies: [],
            companies: [],

            key: 1
        };

    }
    handleSelect(key) {
        this.setState({key});
    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;

        this.handleSelect(parseInt(this.props.params.tabNumber));
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + id)
            .then(function (result) {

                th.setState({
                    nif: result.data.nif,
                    qrCode: result.data.qrCode,
                    taxName: result.data.taxName,
                    abrevName: result.data.abrevName,
                    selectedAccountManager: result.data.accountManager,
                    customersContracts: result.data.contracts,
                    entityProjects: result.data.projects,
                    customerContacts: result.data.contacts,
                    entityProcesses: result.data.processes,
                    associatedUsers: result.data.associatedUsers
                });

            })

        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({users: result.data});

                th
                    .state
                    .users
                    .forEach((item, i) => {
                        if (th.state.selectedAccountManager == item._id) {
                            th.setState({accountManagerName: item.name})
                        }

                    })
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/entityAndCompanies/")
            .then(function (result) {
                th.setState({customerCompanies: result.data});

            })
        this.serverRequest = axios
            .get(__APIURL__ + "/companies/")
            .then(function (result) {
                th.setState({companies: result.data});

            })

        //parte de tratamento dos contactos
        this.serverRequest = axios
            .get(__APIURL__ + "/contacts/",)
            .then(function (result) {

                if (result.data.message !== 'NO_RESULTS') {
                    th.setState({contacts: result.data});
                }

                setTimeout(function () {
                    var dtContacts = $('.dtContacts').dataTable({
                        'retrieve': true, 'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'responsive': true, // https://datatables.net/extensions/responsive/examples/
                        // Text translation options Note the required keywords between underscores (e.g
                        // _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs.keyup(function () {
                        dtContacts.fnFilter(this.value, columnInputs.index(this));
                    });
                })
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/contactTypes/",)
            .then(function (result) {
                if (result.data.message !== 'NO_RESULTS') {
                    th.setState({contactTypes: result.data});
                }
            })

        //parte de tratamento dos contratos
        this.serverRequest = axios
            .get(__APIURL__ + "/contracts/",)
            .then(function (result) {

                if (result.data.message !== 'NO_RESULTS') {
                    th.setState({contracts: result.data});
                }
                setTimeout(function () {
                    var dtContracts = $('.dtContracts').dataTable({
                        'retrieve': true, 'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'responsive': true, // https://datatables.net/extensions/responsive/examples/
                        // Text translation options Note the required keywords between underscores (e.g
                        // _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs.keyup(function () {
                        dtContracts.fnFilter(this.value, columnInputs.index(this));
                    });
                })
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/contractTypes/",)
            .then(function (result) {
                th.setState({contractTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contractStates/",)
            .then(function (result) {
                th.setState({contractStates: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/projectStates/",)
            .then(function (result) {
                th.setState({projectStates: result.data});
            })

        //parte de tratamento dos projetos
        this.serverRequest = axios
            .get(__APIURL__ + "/projects/",)
            .then(function (result) {

                if (result.data.message !== 'NO_RESULTS') {

                    th.setState({projects: result.data});
                }
                setTimeout(function () {
                    var dtProjects = $('.dtProjects').dataTable({
                        'retrieve': true, 'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'responsive': true, // https://datatables.net/extensions/responsive/examples/
                        // Text translation options Note the required keywords between underscores (e.g
                        // _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs.keyup(function () {
                        dtProjects.fnFilter(this.value, columnInputs.index(this));
                    });
                })
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/projectTypes/",)
            .then(function (result) {
                th.setState({projectTypes: result.data});
            })

        //parte de tratamento dos processos
        this.serverRequest = axios
            .get(__APIURL__ + "/processes/",)
            .then(function (result) {
                if (result.data.message !== 'NO_RESULTS') {
                    th.setState({processes: result.data});
                }
                setTimeout(function () {
                    var dtProcesses = $('.dtProcesses').dataTable({
                        'retrieve': true, 'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'responsive': true, // https://datatables.net/extensions/responsive/examples/
                        // Text translation options Note the required keywords between underscores (e.g
                        // _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs.keyup(function () {
                        dtProcesses.fnFilter(this.value, columnInputs.index(this));
                    });
                })
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/processTypes/",)
            .then(function (result) {
                th.setState({processTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processStates/",)
            .then(function (result) {
                th.setState({processStates: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/interventions/",)
            .then(function (result) {
                th.setState({interventions: result.data});
            })

    }
    view(id, collection) {
        this
            .props
            .router
            .push('/' + collection + '/view/' + this.props.params.id + '/' + id);
    }

    create(collection) {
        this
            .props
            .router
            .push('/' + collection + '/create/' + this.props.params.id);
    }

    edit(id, collection) {
        this
            .props
            .router
            .push('/' + collection + '/edit/' + this.props.params.id + '/' + id);
    }

    // delete(name, id, collection) {     swal({         title: "Tem a certeza que
    // quer eliminar?",         text: "Após eliminar não poderá recuperar o
    // registo!",         type: "warning",         showCancelButton: true,
    // confirmButtonColor: "#DD6B55",         confirmButtonText: "Sim",
    // closeOnConfirm: false,         closeOnCancel: false     }, (isConfirm) => {
    // if (!isConfirm) {             swal("Cancelled", "Your imaginary file is safe
    // :)", "error");         }     }) }

    delete(name, id, collection) {

        const {
            nif,
            qrCode,
            taxName,
            abrevName,
            selectedAccountManager,
            customerContacts,
            customersContracts,
            entityProjects,
            entityProcesses,
            associatedUsers
        } = this.state;
        var headers = new Headers({'Content-Type': 'application/json'});
        var updateEntity = JSON.parse(JSON.stringify({
            "nif": nif,
            "qrCode": qrCode,
            "taxName": taxName,
            "abrevName": abrevName,
            "accountManager": selectedAccountManager,
            "contacts": customerContacts,
            "projects": entityProjects,
            "processes": entityProcesses,
            "contracts": customersContracts,
            "associatedUsers": associatedUsers
        }));
        var idEntity = this.props.params.id;

        var manipulateCollection = []
        switch (collection) {
            case 'contacts':
                manipulateCollection = this.state.customerContacts;
                break;
            case 'contracts':
                manipulateCollection = this.state.customersContracts;
                break;
            case 'projects':
                manipulateCollection = this.state.entityProjects;
                break;
            case 'processes':
                manipulateCollection = this.state.entityProcesses;
                break;
        }

        var self = this;
        var canDelete = true;

        if (collection == "processes") {
            self
                .state
                .processes
                .forEach(process => {
                    if (process._id == id) {
                        if (process.interventions.length != 0) {
                            canDelete = false;
                        }
                    }
                })
        } else if (collection == "contracts") {

            self
                .state
                .contracts
                .forEach(contract => {
                    if (contract._id == id) {
                        if (contract.addenda.length != 0) {
                            canDelete = false;

                        }
                    }
                })
            if(!self.state.interventions.error){
                 self
                .state
                .interventions
                .forEach(interv => {
                    if (interv.contractToCharge == id) {
                        canDelete = false;
                    }
                })
            }
           
        } else if (collection == "contacts") {

            if (!self.state.processes.error) {
                self
                    .state
                    .processes
                    .forEach(process => {
                        if (process.requestedBy == id) {
                            canDelete = false;
                        } else {
                            process
                                .notify
                                .forEach(contactToNot => {
                                    if (contactToNot == id) {
                                        canDelete = false;
                                    }
                                })
                        }
                    })
            }

            if (!self.state.interventions.error) {
                self
                    .state
                    .interventions
                    .forEach(interv => {
                        if (interv.requestedBy == id) {
                            canDelete = false;
                        } else {
                            interv
                                .notify
                                .forEach(contactToNot => {
                                    if (contactToNot == id) {
                                        canDelete = false;
                                    }
                                })
                        }
                    })
            }

        } else if (collection = "projects") {
                 
            if (!self.state.interventions.error) {
                self
                    .state
                    .interventions
                    .forEach(interv => {
                        if (interv.projectToCharge == id) {
                            canDelete = false;
                        }
                    })
            }

        }
 
        if (canDelete) {
            swal({
                title: "Tem a certeza que quer eliminar?",
                text: "Após eliminar não poderá recuperar o registo!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {

                    axios
                        .delete(__APIURL__ + '/' + collection + '/' + id)
                        .then(function (response) {
                            if (response.status == 200) {
                                manipulateCollection.splice(manipulateCollection.indexOf(id), 1);
                                var updateEntity;
                                switch (collection) {
                                    case 'contacts':
                                        updateEntity = JSON.parse(JSON.stringify({
                                            "nif": nif,
                                            "qrCode": qrCode,
                                            "taxName": taxName,
                                            "abrevName": abrevName,
                                            "accountManager": selectedAccountManager,
                                            "contacts": manipulateCollection,
                                            "projects": entityProjects,
                                            "processes": entityProcesses,
                                            "contracts": customersContracts,
                                            "associatedUsers": associatedUsers
                                        }));
                                        var newContacts = []
                                        self
                                            .state
                                            .contacts
                                            .forEach(contact => {
                                                if (contact._id != id) {
                                                    newContacts.push(contact);
                                                }
                                            });
                                        self.setState({contacts: newContacts});

                                        break;
                                    case 'contracts':
                                        updateEntity = JSON.parse(JSON.stringify({
                                            "nif": nif,
                                            "qrCode": qrCode,
                                            "taxName": taxName,
                                            "abrevName": abrevName,
                                            "accountManager": selectedAccountManager,
                                            "contacts": customerContacts,
                                            "projects": entityProjects,
                                            "processes": entityProcesses,
                                            "contracts": manipulateCollection,
                                            "associatedUsers": associatedUsers
                                        }));
                                        var newContracts = []
                                        self
                                            .state
                                            .contracts
                                            .forEach(contract => {
                                                if (contract._id != id) {
                                                    newContracts.push(contract);
                                                }
                                            });
                                        self.setState({contracts: newContracts});

                                        break;
                                    case 'projects':
                                        updateEntity = JSON.parse(JSON.stringify({
                                            "nif": nif,
                                            "qrCode": qrCode,
                                            "taxName": taxName,
                                            "abrevName": abrevName,
                                            "accountManager": selectedAccountManager,
                                            "contacts": customerContacts,
                                            "projects": manipulateCollection,
                                            "processes": entityProcesses,
                                            "contracts": customersContracts,
                                            "associatedUsers": associatedUsers
                                        }));
                                        var newProjects = []
                                        self
                                            .state
                                            .projects
                                            .forEach(project => {
                                                if (project._id != id) {
                                                    newProjects.push(project);
                                                }
                                            });
                                        self.setState({projects: newProjects});

                                        break;
                                    case 'processes':
                                        updateEntity = JSON.parse(JSON.stringify({
                                            "nif": nif,
                                            "qrCode": qrCode,
                                            "taxName": taxName,
                                            "abrevName": abrevName,
                                            "accountManager": selectedAccountManager,
                                            "contacts": customerContacts,
                                            "projects": entityProjects,
                                            "processes": manipulateCollection,
                                            "contracts": customersContracts,
                                            "associatedUsers": associatedUsers
                                        }));
                                        var newProcesses = []
                                        self
                                            .state
                                            .processes
                                            .forEach(process => {
                                                if (process._id != id) {
                                                    newProcesses.push(process);
                                                }
                                            });
                                        self.setState({processes: newProcesses});

                                        break;
                                }

                                self.serverRequest = axios
                                    .put(__APIURL__ + '/entities/' + idEntity, updateEntity, {headers: self.headers})
                                    .then(function (response2) {})
                                    .catch(function (error) {
                                        console.log(error);
                                    });

                                if (response.status == 200) {
                                    swal("Eliminado!", name + " foi eliminado.", "success");

                                    self.forceUpdate();
                                } else {
                                    // TODO: HANDLE IT
                                }

                            }

                        })
                        .catch(function (error) {
                            console.log(error);
                            // TODO: HANDLE IT
                        });

                }

            });
        } else {
            swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
        }

    }

    renderContactsTable() {
        var contactRows = []
        var contactT = "";
        var activeState;

        //Popular a linha de registos dos contactos
        this
            .state
            .contacts
            .map(contact => {
                if (this.state.customerContacts != undefined) {
                    this
                        .state
                        .customerContacts
                        .forEach((ec, i) => {

                            if (contact._id === ec) {
                                //trata de um contacto só
                                this
                                    .state
                                    .contactTypes
                                    .forEach((contactType, j) => {
                                        //para esse contacto verifica o nome da descrição do tipo de contacto
                                        if (contact.contactType == contactType._id) {
                                            contactT = contactType.description;
                                        }
                                    })

                                if (contact.active == true) {
                                    activeState = <span className="label label-success">Activo</span>
                                } else {
                                    activeState = <span className="label label-default">Inactivo</span>
                                }

                                //para esse contacto faz o push para contact rows
                                contactRows.push(
                                    <tr key={contact._id} className="active">
                                        <td className="displayTableCell">{contactT}</td>
                                        <td className="displayTableCell">{contact.name}</td>
                                        <td className="displayTableCell">{contact.telephone}</td>
                                        <td className="displayTableCell">{contact.mobilePhone}</td>
                                        <td className="displayTableCell">{contact.email}</td>
                                        <td className="displayTableCell">{activeState}</td>
                                        <td className="displayTableCell">
                                            <button
                                                onClick={() => this.edit(contact._id, 'contacts')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-pencil"></i>
                                            </button>
                                        </td>
                                        <td className="displayTableCell">
                                            <button
                                                onClick={() => this.delete(contact.name, contact._id, 'contacts')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                );
                            }
                        })
                    return contact;
                }

            })

        return contactRows;
    };

    renderContractsTable() {
        var contractRows = []
        var contractT = "";
        var contractS = "";

        //Popular a linha de registos dos contactos
        this
            .state
            .contracts
            .map(contract => {
                if (this.state.customersContracts != undefined) {
                    this
                        .state
                        .customersContracts
                        .forEach((ec, i) => {
                            if (contract._id == ec) {
                                //trata de um contacto só
                                this
                                    .state
                                    .contractTypes
                                    .forEach((contractType, j) => {
                                        //para esse contrato verifica o nome da descrição do tipo de contrato
                                        if (contract.contractType == contractType._id) {
                                            contractT = contractType.description;
                                        }
                                    })
                                this
                                    .state
                                    .contractStates
                                    .forEach((contractState, j) => {

                                        //para esse contrato verifica o nome da descrição do tipo de contrato
                                        if (contract.state == contractState._id) {
                                            contractS = <span className={contractState.stateLabelColor}>{contractState.stateName}</span>
                                        }
                                    })

                                console.log(contract.contractCode)
                                var a = contract
                                    .hours
                                    .split(':');
                                    console.log(a)
                                var minutesContractHours = (+ a[0]) * 60 + (+ a[1]);

                                var b = contract
                                    .availableHours
                                    .split(':');
                                    console.log(b)
                                var minutesAvailableContractHours = (+ b[0]) * 60 + (+ b[1]);

                                if (minutesContractHours * 0.2 > minutesAvailableContractHours) {
                                    var availableHours = <td
                                        className="displayTableCell"
                                        style={{
                                        color: "red",
                                        fontWeight: "bold",
                                        textAlign: "center"
                                    }}>{contract.availableHours}</td>
                                } else {
                                    var availableHours = <td
                                        className="displayTableCell"
                                        style={{
                                        textAlign: "center"
                                    }}>{contract.availableHours}</td>
                                }
                                //para esse contacto faz o push para contact rows
                                contractRows.push(
                                    <tr key={contract._id} className="active">
                                        <td
                                            className="displayTableCell"
                                            style={{
                                            textAlign: "center"
                                        }}>{contract.contractCode}</td>
                                        <td
                                            className="displayTableCell"
                                            style={{
                                            textAlign: "center"
                                        }}>{contractT}</td>
                                        <td
                                            className="displayTableCell"
                                            style={{
                                            whiteSpace: 'nowrap',
                                            textAlign: "center"
                                        }}>{new Date(contract.beginDate).toLocaleDateString()}</td>
                                        <td
                                            className="displayTableCell"
                                            style={{
                                            whiteSpace: 'nowrap',
                                            textAlign: 'center'
                                        }}>{new Date(contract.endDate).toLocaleDateString()}</td>
                                        <td
                                            className="displayTableCell"
                                            style={{
                                            whiteSpace: 'nowrap',
                                            textAlign: "center"
                                        }}>{contract.lastRenewal
                                                ? new Date(contract.lastRenewal).toLocaleDateString()
                                                : "-"}</td>
                                        {/*<td  className="displayTableCell">{contract.notifyRenewal}</td>*/}
                                        <td
                                            className="displayTableCell"
                                            style={{
                                            textAlign: "center"
                                        }}>{contract.hours}</td>
                                        {availableHours}
                                        <td
                                            className="displayTableCell"
                                            style={{
                                            textAlign: "center"
                                        }}>{contract.exhaustedHours}</td>

                                        <td
                                            className="displayTableCell"
                                            style={{
                                            textAlign: "center"
                                        }}>{contractS}</td>
                                        <td
                                            className="displayTableCell"
                                            style={{
                                            textAlign: "center"
                                        }}>
                                            <button
                                                onClick={() => this.view(contract._id, 'contracts')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-eye"></i>
                                            </button>
                                        </td>
                                        <td className="displayTableCell">
                                            <button
                                                onClick={() => this.edit(contract._id, 'contracts')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-pencil"></i>
                                            </button>
                                        </td>
                                        <td className="displayTableCell">
                                            <button
                                                onClick={() => this.delete(contract.contractCode, contract._id, 'contracts')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                );
                            }
                        })
                    return contract;
                }

            })

        return contractRows;
    };

    renderProjectsTable() {
        var projectRows = []
        var projectT = "";
        var projectS = "";
        var blockExHours;
        // var notifyExHours; Popular a linha de registos dos projetos
        this
            .state
            .projects
            .map(project => {
                if (this.state.entityProjects != undefined) {
                    this
                        .state
                        .entityProjects
                        .forEach((ep, i) => {
                            if (project._id == ep) {
                                //trata de um projeto só
                                this
                                    .state
                                    .projectTypes
                                    .forEach((projectType, j) => {

                                        if (project.projectType == projectType._id) {
                                            projectT = projectType.description;
                                        }
                                    })
                                this
                                    .state
                                    .projectStates
                                    .forEach((projectState, j) => {

                                        if (project.state == projectState._id) {
                                            projectS = <span className={projectState.stateLabelColor}>{projectState.stateName}</span>
                                        }
                                    })

                                if (project.blockExhaustedHours == true) {
                                    blockExHours = <em className="fa fa-check"></em>
                                } else {
                                    blockExHours = <em className="fa fa-close"></em>
                                }
                                // if (project.notifyExhaustedHours == true) {     notifyExHours = <em
                                // className="fa fa-check"></em> } else {     notifyExHours = <em className="fa
                                // fa-close"></em> } para esse projeto faz o push para contact rows
                                projectRows.push(
                                    <tr key={project._id} className="active">
                                        <td className="displayTableCell tdAlignCenter">{project.projectCode}</td>
                                        <td className="displayTableCell tdAlignCenter">{projectT}</td>
                                        <td
                                            className="tdAlignCenter"
                                            style={{
                                            whiteSpace: 'nowrap'
                                        }}>{new Date(project.beginDate).toLocaleDateString()}</td>
                                        <td
                                            className="tdAlignCenter"
                                            style={{
                                            whiteSpace: 'nowrap'
                                        }}>{project.endDate != null
                                                ? new Date(project.endDate).toLocaleDateString()
                                                : "-"}</td>
                                        <td className="displayTableCell tdAlignCenter">{project.hours}</td>
                                        {/* <td className="displayTableCell tdAlignCenter">{project.availableHours}</td> */}
                                        <td className="displayTableCell tdAlignCenter">{project.exhaustedHours}</td>
                                        {/* <td className="displayTableCell tdAlignCenter">{notifyExHours}</td> */}
                                        <td className="displayTableCell tdAlignCenter">{blockExHours}</td>
                                        <td className="displayTableCell tdAlignCenter">{projectS}</td>
                                        <td className="displayTableCell tdAlignCenter">
                                            <button
                                                onClick={() => this.view(project._id, 'projects')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-eye"></i>
                                            </button>
                                        </td>
                                        <td className="displayTableCell tdAlignCenter">
                                            <button
                                                onClick={() => this.edit(project._id, 'projects')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-pencil"></i>
                                            </button>
                                        </td>
                                        <td className="displayTableCell tdAlignCenter">
                                            <button
                                                onClick={() => this.delete(project.projectCode, project._id, 'projects')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                );
                            }
                        })
                    return project;
                }

            })

        return projectRows;
    };

    renderProcessesTable() {
        var processRows = []
        var processT = "";
        var processS = "";
        var processRequester = "";
        var processCreator = "";
        //Popular a linha de registos dos contactos
        this
            .state
            .processes
            .map(process => {

                if (this.state.entityProcesses != undefined) {
                    this
                        .state
                        .entityProcesses
                        .forEach((ep, i) => {
                            if (process._id == ep) {

                                //trata de um processo só
                                this
                                    .state
                                    .processTypes
                                    .forEach((processType, j) => {
                                        //para esse processo verifica o nome da descrição do tipo de contrato
                                        if (process.processType == processType._id) {
                                            processT = processType.description;
                                        }
                                    })
                                this
                                    .state
                                    .processStates
                                    .forEach((processState, j) => {

                                        //para esse processo verifica o nome da descrição do tipo de contrato
                                        if (process.state == processState._id) {

                                            processS = <span className={processState.stateLabelColor}>{processState.stateName}</span>
                                        }
                                    })
                                this
                                    .state
                                    .contacts
                                    .forEach((requester, j) => {

                                        //para esse processo verifica o nome da descrição do tipo de contrato
                                        if (process.requestedBy == requester._id) {
                                            processRequester = requester.name;
                                        }
                                    })
                                this
                                    .state
                                    .users
                                    .forEach((creator, j) => {

                                        //para esse processo verifica o nome da descrição do tipo de contrato
                                        if (process.createdBy == creator._id) {
                                            processCreator = creator.name;
                                        }
                                    })

                                //para esse processo faz o push para contact rows
                                processRows.push(
                                    <tr key={process._id} className="active">
                                        <td className="displayTableCell">{process.processNumber}</td>
                                        <td className="displayTableCell">{processT}</td>
                                        <td
                                            style={{
                                            whiteSpace: 'nowrap'
                                        }}>{new Date(process.date).toLocaleDateString()}</td>
                                        <td className="displayTableCell">{process.duration}</td>
                                        <td className="displayTableCell">{processRequester}</td>
                                        <td className="displayTableCell">{processCreator}</td>
                                        <td className="displayTableCell">{processS}</td>
                                        <td className="displayTableCell">
                                            <button
                                                onClick={() => this.view(process._id, 'processes')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-eye"></i>
                                            </button>
                                        </td>
                                        <td className="displayTableCell">
                                            <button
                                                onClick={() => this.edit(process._id, 'processes')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-pencil"></i>
                                            </button>
                                        </td>
                                        <td className="displayTableCell">
                                            <button
                                                onClick={() => this.delete(process.processNumber, process._id, 'processes')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                );
                            }
                        })
                    return process;
                }

            })

        return processRows;
    };

    renderCompaniesTable() {
        var self = this;
        var customerCompaniesRows = [];

        if (!this.state.customerCompanies.error) {
            this
                .state
                .customerCompanies
                .forEach(custComp => {

                    if (custComp.entity == this.props.params.id) {

                        this
                            .state
                            .companies
                            .forEach(company => {
                                if (company._id == custComp.company) {
                                    customerCompaniesRows.push(
                                        <tr key={custComp._id} className="active">
                                            <td className="displayTableCell">{company.name}</td>
                                            <td className="displayTableCell">{custComp.primaveraCode}</td>

                                            <td className="displayTableCell">
                                                <button
                                                    onClick={() => this.props.router.push('customersCompanies/edit/' + custComp._id)}
                                                    type="button"
                                                    id="lineBtns"
                                                    className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                    <i className="icon-pencil"></i>
                                                </button>
                                            </td>
                                            <td className="displayTableCell">
                                                <button
                                                    onClick={() => deleteCompany(custComp._id)}
                                                    type="button"
                                                    id="lineBtns"
                                                    className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                    <i className="icon-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    );

                                }

                            });

                    }
                });
            return customerCompaniesRows;
        }

        function deleteCompany(id) {
            swal({
                title: "Tem a certeza que quer eliminar?",
                text: "Após eliminar não poderá recuperar o registo!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                closeOnConfirm: true
            }, function () {
                axios
                    .delete(__APIURL__ + '/entityAndCompanies/' + id)
                    .then(function (response) {
                        swal("Eliminado!", name + " foi eliminado.", "success");
                        var newCustCompArray = [];
                        self
                            .state
                            .customerCompanies
                            .forEach(custCompany => {
                                if (custCompany._id != id) {
                                    newCustCompArray.push(custCompany);
                                }
                            });
                        self.setState({customerCompanies: newCustCompArray});
                    });
            });
        }

    };

    render() {

        const css = css;
        var tableContacts = [];
        var tableContracts = [];
        var tableProjects = [];
        var tableProcesses = [];
        var tableCompanies = [];

        // if (this.state.customerContacts.length !== 0) {  //if there is no contacts

        tableContacts.push(
            <Table className="dtContacts">
                <thead>
                    <tr className="active">
                        <th className="displayTableCell">Tipo de contacto</th>
                        <th className="displayTableCell">Nome</th>
                        <th className="displayTableCell">Telefone</th>
                        <th className="displayTableCell">Telemóvel</th>
                        <th className="displayTableCell">E-mail</th>
                        <th className="displayTableCell">Activo</th>
                        <th className="displayTableCell"></th>
                        <th className="displayTableCell"></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderContactsTable()}
                </tbody>
            </Table>

        );

        /*   } else {

            tableContacts.push(
                <Col lg={12}>

                    <b>Esta entidade não tem contactos</b>
                </Col>
            )
        }*/

        // if (this.state.customersContracts[0] != null) {
        tableContracts.push(
            <Table className="dtContracts">
                <thead>
                    <tr className="active">
                        <th className="displayTableCell">Código do contrato</th>
                        <th className="displayTableCell">Tipo de contrato</th>
                        <th className="displayTableCell">Data de início</th>
                        <th className="displayTableCell">Data de fim</th>
                        <th className="displayTableCell">Última renovação</th>
                        {/*<th  className="displayTableCell">Notificar renovação</th>*/}
                        <th className="displayTableCell">Horas</th>
                        <th className="displayTableCell">Horas disponíveis</th>
                        <th className="displayTableCell">Horas consumidas</th>
                        {/*<th  className="displayTableCell">Por mês</th>*/}
                        {/*<th  className="displayTableCell">Notificar horas consumidas</th>*/}
                        {/*<th  className="displayTableCell">Bloquear horas consumidas</th>*/}
                        <th className="displayTableCell">Estado</th>
                        <th className="displayTableCell"></th>
                        <th className="displayTableCell"></th>
                        <th className="displayTableCell"></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderContractsTable()}
                </tbody>
            </Table>

        );

        /*   } else {
            tableContracts.push(
                <Col lg={12}>
                    <b>Esta entidade não tem contratos</b>
                </Col>
            )
        }*/

        //if (this.state.entityProjects[0] != null) {
        tableProjects.push(
            <Table className="dtProjects">
                <thead>
                    <tr className="active">
                        <th className="displayTableCell tdAlignCenter">Código do projeto</th>
                        <th className="displayTableCell tdAlignCenter">Tipo de projeto</th>
                        <th className="displayTableCell tdAlignCenter">Data de início</th>
                        <th className="displayTableCell tdAlignCenter">Data de fim</th>
                        <th className="displayTableCell tdAlignCenter">Horas</th>
                        {/* <th className="displayTableCell tdAlignCenter">Horas disponíveis</th> */}
                        <th className="displayTableCell tdAlignCenter">Horas consumidas</th>
                        {/* <th className="displayTableCell tdAlignCenter">Notificar horas consumidas</th> */}
                        <th className="displayTableCell tdAlignCenter">Bloquear horas consumidas</th>
                        <th className="displayTableCell tdAlignCenter">Estado</th>
                        <th className="displayTableCell tdAlignCenter"></th>
                        <th className="displayTableCell tdAlignCenter"></th>
                        <th className="displayTableCell tdAlignCenter"></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderProjectsTable()}
                </tbody>
            </Table>

        );

        /* } else {
            tableProjects.push(
                <Col lg={12}>
                    <b>Esta entidade não tem projetos</b>
                </Col>
            )
        }*/

        // tratamentos dos processos console.log("processos de", this.state.taxName,
        // ":", this.state.entityProcesses); if (this.state.entityProcesses[0] != null)
        // {
        tableProcesses.push(
            <Table className="dtProcesses">
                <thead>
                    <tr className="active">
                        <th className="displayTableCell">Código do processo</th>
                        <th className="displayTableCell">Tipo de processo</th>
                        <th className="displayTableCell">Data</th>
                        <th className="displayTableCell">Duração</th>
                        <th className="displayTableCell">Pedido por</th>
                        <th className="displayTableCell">Criado por</th>
                        <th className="displayTableCell">Estado</th>
                        <th className="displayTableCell"></th>
                        <th className="displayTableCell"></th>
                        <th className="displayTableCell"></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderProcessesTable()}
                </tbody>
            </Table>

        );

        /*  } else {
            tableProcesses.push(
                <Col lg={12}>
                    <b>Esta entidade não tem processos</b>
                </Col>
            )
        }*/

        tableCompanies.push(
            <Table className="dtProcesses">
                <thead>
                    <tr className="active">
                        <th className="displayTableCell">Nome</th>
                        <th className="displayTableCell">Código</th>
                        <th className="displayTableCell"></th>
                        <th className="displayTableCell"></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderCompaniesTable()}
                </tbody>
            </Table>

        );

        return (
            <ContentWrapper>
                <h3>Cliente
                </h3 >
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Detalhes do cliente</div>
                            </div>
                            <Col lg={10}>
                                <div className="panel-body">
                                    <div className="form-group">
                                        <label className="control-label">NIF:&nbsp;</label>{this.state.nif}

                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">QR code:&nbsp;</label>{this.state.qrCode}

                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Nome fiscal:&nbsp;</label>{this.state.taxName}

                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Nome abreviado:&nbsp;</label>{this.state.abrevName}

                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Gestor de conta:&nbsp;</label>{this.state.accountManagerName}

                                    </div>
                                    <div className="form-group">
                                        <button onClick={() => this.props.router.push('/customers')} className="btn ">Voltar atrás</button>

                                    </div>
                                </div>
                            </Col>

                            {/* END panel */}
                            {/* START tabs */}
                            <Tabs
                                activeKey={this.state.key}
                                onSelect={this
                                .handleSelect
                                .bind(this)}
                                justified
                                id="tabID">
                                <Tab eventKey={1} title="Contactos">
                                    <button
                                        onClick={() => this.create('contacts')}
                                        type="button"
                                        id="addBtn"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                        <i className="icon-plus"></i>
                                    </button>
                                    <Row>
                                        {/* data table de contactos */}
                                        {tableContacts}
                                    </Row>

                                </Tab>
                                <Tab eventKey={2} title="Contratos">
                                    <button
                                        onClick={() => this.create('contracts')}
                                        type="button"
                                        id="addBtn"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                        <i className="icon-plus"></i>
                                    </button>
                                    <Row>

                                        {/* data table de contratos */}
                                        {tableContracts}
                                    </Row>
                                </Tab>
                                <Tab eventKey={3} title="Projetos">
                                    {/* data table de projetos */}
                                    <button
                                        onClick={() => this.create('projects')}
                                        type="button"
                                        id="addBtn"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                        <i className="icon-plus"></i>
                                    </button>
                                    <Row>
                                        {/* data table de projectos */}
                                        {tableProjects}
                                    </Row>
                                </Tab>
                                <Tab eventKey={4} title="Processos">
                                    {/* data table de processos */}
                                    <button
                                        onClick={() => this.create('processes')}
                                        type="button"
                                        id="addBtn"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                        <i className="icon-plus"></i>
                                    </button>
                                    <Row>
                                        {tableProcesses}
                                    </Row>
                                </Tab>
                                <Tab eventKey={5} title="Empresas">
                                    {/* data table de processos */}
                                    <button
                                        onClick={() => this.create('customersCompanies')}
                                        type="button"
                                        id="addBtn"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                        <i className="icon-plus"></i>
                                    </button>
                                    <Row>
                                        {tableCompanies}
                                    </Row>
                                </Tab>
                            </Tabs>
                            {/* END tabs */}

                        </div>
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
}

export default CustomerDetails;