import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class EditCustomer extends React.Component {

    constructor() {
        super();
        this.state = {
            nif: "",
            qrCode: "",
            taxName: "",
            abrevName: "",
            selectedAccountManager: "",
            users: [],
            otherData: [],
            //companies: [],
           // selectedCompany: "",
            entityAndCompaniesData: []

        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleSubmit(e) {
        const {
            nif,
            qrCode,
            taxName,
            abrevName,
            selectedAccountManager,
            otherData,
           // selectedCompany,
            entityAndCompaniesData
        } = this.state;
        var headers = new Headers({'Content-Type': 'application/json'});
        var updateEntity = JSON.parse(JSON.stringify({
            "nif": nif,
            "qrCode": qrCode,
            "taxName": taxName,
            "abrevName": abrevName,
            "accountManager": selectedAccountManager,
            "contacts": otherData.contacts,
            "projects": otherData.projects,
            "processes": otherData.processes,
            "contracts": otherData.contracts
        }));
        var id = this.props.params.id;
        var self = this;

        e.preventDefault();
        console.log(updateEntity);
        this.serverRequest = axios
            .put(__APIURL__ + '/entities/' + id, updateEntity, {headers: this.headers})
            .then(function (response) {

              //  var updateCompanyEntityRelation = JSON.parse(JSON.stringify({"entity": response.data.obj._id, "company": selectedCompany, "primaveraCode": entityAndCompaniesData.primaveraCode}));
                self
                            .props
                            .router
                            .push('/customers');
              /*  axios
                    .put(__APIURL__ + '/entityAndCompanies/'+entityAndCompaniesData._id, updateCompanyEntityRelation, {headers: self.headers})
                    .then(function (response) {
                        console.log(response);

                        
                    })*/
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + id)
            .then(function (result) {
                th.setState({
                    nif: result.data.nif,
                    qrCode: result.data.qrCode,
                    taxName: result.data.taxName,
                    abrevName: result.data.abrevName,
                    selectedAccountManager: result.data.accountManager,
                    otherData: result.data
                });
            })

this.serverRequest = axios
            .get(__APIURL__ + "/entityAndCompanies/")
            .then(function (result) {
                for(var i = 0; i < result.data.length; i++){
                    console.log(result.data[i])
                    if(result.data[i].entity == th.props.params.id){
                th.setState({entityAndCompaniesData: result.data[i]});
                th.setState({selectedCompany: result.data[i].company});
                    }
                }
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({users: result.data});
            })
             this.serverRequest = axios
            .get(__APIURL__ + "/companies/")
            .then(function (result) {
                th.setState({companies: result.data});
            })
    }

    render() {
        var accountManagersList = [];
        //var companiesList = [];

     if(this.state.users[0] != undefined)
   this
            .state
            .users
            .forEach((item, i) => {
                if (item.internalUser == true) 

                    accountManagersList.push(
                        <option key={i.toString()} value={item._id}>{item.name}</option>
                    );
                }
            )
            
    /*if(this.state.companies[0] != undefined)
    this
            .state
            .companies
            .forEach((item, i) => {
                companiesList.push(
                    <option key={i.toString()} value={item._id}>{item.name}</option>
                );
            })*/
        return (
            <ContentWrapper>
                <h3>Cliente
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar cliente</div>
                            </div>
                             <div className="panel-body">
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">NIF*</label>
                                            <FormControl
                                                type="text"
                                                name="nif"
                                                required="required"
                                                className="form-control"
                                                value={this.state.nif}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'nif')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">QR code*</label>
                                            <FormControl
                                                type="text"
                                                name="qrCode"
                                                required="required"
                                                className="form-control"
                                                value={this.state.qrCode}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'qrCode')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Nome fiscal*</label>
                                            <FormControl
                                                type="text"
                                                name="taxName"
                                                required="required"
                                                className="form-control"
                                                value={this.state.taxName}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'taxName')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Nome abreviado*</label>
                                            <FormControl
                                                type="text"
                                                name="abrevName"
                                                required="required"
                                                className="form-control"
                                                value={this.state.abrevName}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'abrevName')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Gestor de conta*</label>
                                            <FormControl
                                                id="select2-1"
                                                componentClass="select"
                                                name="selectedAccountManager"
                                                className="form-control m-b"
                                                value={this.state.selectedAccountManager}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedAccountManager')}>
                                                {accountManagersList}
                                            </FormControl>
                                        </div>
                                       {/*} <div className="col-sm-6">
                                            <label className="control-label">Empresa*</label>
                                            <FormControl
                                                id="select2-1-company"
                                                componentClass="select"
                                                name="selectedCompany"
                                                className="form-control m-b"
                                                value={this.state.selectedCompany}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedCompany')}>
                                                {companiesList}

                                            </FormControl>
                                        </div>*/}
                                    </Row>
                                </div>
                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={1}>

                                        <button
                                            onClick={() => this.props.router.push('/customers')}
                                            type="button"
                                            className="btn ">Voltar</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }

    componentDidMount() {

        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepicker1').datetimepicker({
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            });
            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1').select2({theme: 'bootstrap'});
            $('#select2-1').on('change', function () {
                self.setState({
                    selectedAccountManager: $(this).val()
                });
            });
     $('#select2-1-company').select2({theme: 'bootstrap'});
            $('#select2-1-company').on('change', function () {
                self.setState({
                    selectedCompany: $(this).val()
                });
            });    
    }
    }
}

export default EditCustomer;
