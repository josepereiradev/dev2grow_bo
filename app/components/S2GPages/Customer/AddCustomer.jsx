import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {Router, Route, Link, History, withRouter} from 'react-router';
import {browserHistory} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class AddCustomer extends React.Component {
    constructor() {
        super();
        this.state = {
            nif: "",
            qrCode: "",
            taxName: "",
            abrevName: "",
            selectedAccountManager: "",
            users: [],
            companies: [],
            selectedCompany: ""

        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleSubmit(e) {
        const {
            nif,
            qrCode,
            taxName,
            abrevName,
            selectedAccountManager,
            selectedCompany
        } = this.state;
        console.log(nif, qrCode, taxName, abrevName, selectedAccountManager, selectedCompany);
        // if (nif != "" && qrCode != "" && taxName != "" && abrevName != "" &&
        // accountManager != "") {
        var headers = new Headers({'Content-Type': 'application/json'});
        var updateEntity = JSON.parse(JSON.stringify({"nif": nif, "qrCode": qrCode, "taxName": taxName, "abrevName": abrevName, "accountManager": selectedAccountManager}));
        var self = this;

        e.preventDefault();
        if (updateEntity.nif == "" || updateEntity.qrCode == "" || updateEntity.taxName == "" || updateEntity.abrevName == "" || updateEntity.accountManager == "") {} else {
            this.serverRequest = axios
                .post(__APIURL__ + '/entities/', updateEntity, {headers: this.headers})
                .then(function (response) {
                    console.log(response);
                  /*     var newCompanyEntityRelation = JSON.parse(JSON.stringify({"entity": response.data.obj._id, "company": selectedCompany}));
                console.log(newCompanyEntityRelation);
                axios
                    .post(__APIURL__ + '/entityAndCompanies/', newCompanyEntityRelation, {headers: self.headers})
                    .then(function (response) {
                        console.log(response);
                    });*/
                    self
                        .props
                        .router
                        .push('/customers');
                    //                  })
                
                })
                .catch(function (error) {
                    console.log(error);
                });
            //  }
        }

    }

    componentWillMount() {

        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({users: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/companies/")
            .then(function (result) {
                th.setState({companies: result.data});
            })
    }

    render() {
        var accountManagersList = [];
        var companiesList = [];
        if (this.state.users[0] != undefined) 
            this.state.users.forEach((item, i) => {
                if (item.internalUser == true) 
                    if (i == 0) {
                        accountManagersList.push(
                            <option disabled value key={-1}>
                                -- Escolha uma opção --
                            </option>
                        );
                    }
                if (item.internalUser) {
                    accountManagersList.push(
                        <option key={i.toString()} value={item._id}>{item.name}</option>
                    );
                }

            })

        if (this.state.companies[0] != undefined) 
            this.state.companies.forEach((item, i) => {
                if (i == 0) {
                    companiesList.push(
                        <option disabled value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                companiesList.push(
                    <option key={i.toString()} value={item._id}>{item.name}</option>
                );
            })
        return (
            <ContentWrapper>
                <h3>Clientes
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar um novo cliente</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">NIF*</label>
                                            <FormControl
                                                type="text"
                                                name="nif"
                                                required="required"
                                                className="form-control"
                                                value={this.state.nif}
                                                onChange={this.handleChange}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">QR code*</label>
                                            <FormControl
                                                type="text"
                                                name="qrCode"
                                                required="required"
                                                className="form-control"
                                                value={this.state.qrCode}
                                                onChange={this.handleChange}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Nome fiscal*</label>
                                            <FormControl
                                                type="text"
                                                name="taxName"
                                                required="required"
                                                className="form-control"
                                                value={this.state.taxName}
                                                onChange={this.handleChange}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Nome abreviado*</label>
                                            <FormControl
                                                type="text"
                                                name="abrevName"
                                                required="required"
                                                className="form-control"
                                                value={this.state.abrevName}
                                                onChange={this.handleChange}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Gestor de conta*</label>
                                            <FormControl
                                                id="select2-1"
                                                componentClass="select"
                                                name="selectedAccountManager"
                                                required="required"
                                                className="form-control m-b"
                                                value={this.state.selectedAccountManager}
                                                onChange={this.handleChange}>
                                                {accountManagersList}
                                            </FormControl>
                                        </div>
                                      {/*}  <div className="col-sm-6">
                                            <label className="control-label">Empresa*</label>
                                            <FormControl
                                                id="select2-1-company"
                                                componentClass="select"
                                                name="selectedCompany"
                                                required="required"
                                                className="form-control m-b"
                                                value={this.state.selectedCompany}
                                                onChange={this.handleChange}>
                                                {companiesList}

                                            </FormControl>
                                        </div>*/}
                                    </Row>
                                </div>
                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={1}>

                                        <button
                                            onClick={() => this.props.router.push('/customers')}
                                            type="button"
                                            className="btn ">Voltar</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }

    componentDidMount() {

        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepicker1').datetimepicker({
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            });
            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1')
                .select2({theme: 'bootstrap'})
                .on('change', this.handleChange);
            // $('#select2-1').on('change', function () {     self.setState({
            // selectedAccountManager: $(this).val()     }); });
            $('#select2-1-company')
                .select2({theme: 'bootstrap'})
                .on('change', this.handleChange);
            // $('#select2-1-company').on('change', function () {     self.setState({
            // selectedCompany: $(this).val()     }); });
        }

    }
}

export default AddCustomer;
