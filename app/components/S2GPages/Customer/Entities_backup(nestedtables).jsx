import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';

class Entities extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            contacts: [],
            contactTypes: [],
            accountManager: [],
            collapsedContacts: true,
            collapsedContactsIcon: "fa fa-chevron-down"
        }
        this.toggleChevron = this
            .toggleChevron
            .bind(this);
    }

    componentWillMount() {

        this.getDataFromBD();

        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/users/",)
            .then(function (result) {
                th.setState({accountManager: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/contacts/",)
            .then(function (result) {
                th.setState({contacts: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/contactTypes/",)
            .then(function (result) {
                th.setState({contactTypes: result.data});
           
           // later... not working with nested tables.
        // data//            this.loadDataTableFeatures();
          /*  setTimeout(function () {
                    var datatable = $('.datatable').dataTable({
                        'retrieve': true, 'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'responsive': true, // https://datatables.net/extensions/responsive/examples/
                        // Text translation options Note the required keywords between underscores (e.g
                        // _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs.keyup(function () {
                        datatable.fnFilter(this.value, columnInputs.index(this));
                    });

                    var datatable2 = $('.datatable2').dataTable({
                        'retrieve': true, 'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'responsive': true, // https://datatables.net/extensions/responsive/examples/
                        // Text translation options Note the required keywords between underscores (e.g
                        // _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs.keyup(function () {
                        datatable2.fnFilter(this.value, columnInputs.index(this));
                    });

                }.bind(this), 500);*/
            }).catch(function (error) {
                console.log(error);
            });

    }
   
        
        

    
    getDataFromBD() {
        axios
            .get(__APIURL__ + "/entities/")
            .then(res => {
                if(res.data.message != "NO_RESULTS"){                 const data = res
                    .data
                    .map(obj => obj);
                this.setState({data})

                
          }      })
            .catch(function (error) {
                console.log(error);
            });
    }

    create() {
        this
            .props
            .router
            .push('/entities/create');
    }

    edit(id) {
        this
            .props
            .router
            .push('/entities/edit/' + id);
    }

    delete(name, id) {
        var self = this;
        swal({
            title: "Tem a certeza que quer eliminar?",
            text: "Após eliminar não poderá recuperar o registo!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            closeOnConfirm: false
        }, function () {
            axios
                .delete(__APIURL__ + "/entities/" + id)
                .then(function (response) {
                    if (response.status == 200) {
                        swal("Eliminado!", name + " foi eliminado.", "success");
                        self.getDataFromBD();
                    } else {
                        // TODO: HANDLE IT
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    // TODO: HANDLE IT
                });
        });
    }

    toggleChevron(e) {
        if (this.state.collapsedContacts == true) {
            this.setState({collapsedContactsIcon: "fa fa-chevron-down", collapsedContacts: false})
        } else {
            this.setState({collapsedContactsIcon: "fa fa-chevron-right", collapsedContacts: true})
        }
    }

    renderContactsTable(entityContacts) {

        var contactRows = []
        var contactT = "";
        //Popular a linha de registos dos contactos
        this
            .state
            .contacts
            .map(contact => {

                entityContacts.forEach((ec, i) => {
                    if (contact._id == ec) {
                        //trata de um contacto só
                        this
                            .state
                            .contactTypes
                            .forEach((contactType, j) => {
                                //para esse contacto verifica o nome da descrição do tipo de contacto
                                if (contact.contactType == contactType._id) {
                                    contactT = contactType.description;
                                }
                            })

                        //para esse contacto faz o push para contact rows
                        contactRows.push(
                            <tr key={contact._id} className="active">
                                <td>{contactT}</td>
                                <td>{contact.name}</td>
                                <td>{contact.telephone}</td>
                                <td>{contact.mobilePhone}</td>
                                <td>{contact.email}</td>
                                <td>{contact.notifyProcesses}</td>
                                <td>{contact.active}</td>
                                <td>
                                    <button
                                        onClick={() => this.edit(data._id)}
                                        type="button"
                                        id="lineBtns"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                        <i className="icon-pencil"></i>
                                    </button>
                                </td>
                                <td>
                                    <button
                                        onClick={() => this.delete(data.name, data._id)}
                                        type="button"
                                        id="lineBtns"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                        <i className="icon-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        );

                    }
                })
            })

        return contactRows;
    };

    render() {
        const css = css;
        var rows = [];
        var tableContacts = [];
        var hasNotContacts = true;

        this
            .state
            .data
            .map((data, a) => {
                this
                    .state
                    .accountManager
                    .forEach((user, i) => {
                        if (data.accountManager == user._id) {
                            console.log("contactos de", data.taxName, ":", data.contacts[0]);
                            if (data.contacts[0] !== undefined) {
                                this.hasNotContacts = false;
                                
                                // imprime esta tabela sempre que inserir um registo de entidade novo e que essa
                                // entidade tenha contactos
                                tableContacts.push(
                                    <ContentWrapper>
                                        <b>Contactos ({data.contacts.length})</b>
                                        <Table className="">
                                            <thead>
                                                <tr className="active">
                                                    <th>Tipo de contacto</th>
                                                    <th>Nome</th>
                                                    <th>Telefone</th>
                                                    <th>Telemóvel</th>
                                                    <th>E-mail</th>
                                                    <th>Notificar</th>
                                                    <th>Activo</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.renderContactsTable(data.contacts)}
                                            </tbody>
                                        </Table>
                                    </ContentWrapper>

                                );

                            }

                            rows.push(

                                <tr key={data._id}>
                                    <td >
                                        { <i
                                            name={a}
                                            ref="icon"
                                            hidden={this.hasNotContacts}
                                            name={a}
                                            onClick={this
                                            .toggleChevron
                                            .bind(this, a)}
                                            className={this.state.collapsedContactsIcon}></i>}
                                    </td>
                                    <td>{data._id}</td>
                                    <td>{data.nif}</td>
                                    <td>{data.qrCode}</td>
                                    <td>{data.taxName}</td>
                                    <td>{data.abrevName}</td>
                                    <td>{user.name}
                                    </td>
                                    <td>
                                        <button
                                            onClick={() => this.edit(data._id)}
                                            type="button"
                                            id="lineBtns"
                                            className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                            <i className="icon-pencil"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <button
                                            onClick={() => this.delete(data.taxName, data._id)}
                                            type="button"
                                            id="lineBtns"
                                            className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                            <i className="icon-trash"></i>
                                        </button>
                                    </td>

                                </tr>

                            );
                            rows.push(
                                <td colSpan="9" hidden={this.state.collapsedContacts} id={a}>{tableContacts}</td>

                            )
                            tableContacts = [];
                            this.hasNotContacts = true;
                        }
                    })
            });

        return (
            <ContentWrapper>
                <h3>Entidades
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <button
                    onClick={() => this.create()}
                    type="button"
                    id="addBtn"
                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                    <i className="icon-plus"></i>
                </button>
                <Table className="" responsive hover>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nº da entidade</th>
                            <th>NIF</th>
                            <th>QR code</th>
                            <th>Nome fiscal</th>
                            <th>Nome abreviado</th>
                            <th>Gestor de conta</th>

                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}</tbody>
                </Table>
            </ContentWrapper>
        );
    }

}

export default Entities;
