import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';

class Customers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            contacts: [],
            contactTypes: [],
            accountManager: [],
            customerCompanies: []
        }

    }

    componentWillMount() {

        this.getDataFromBD();

        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/users/",)
            .then(function (result) {
                th.setState({accountManager: result.data});
            })
    }

    getDataFromBD() {

        axios
            .get(__APIURL__ + "/entities/")
            .then(res => {

                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({data})
                    setTimeout(function () {
                        var dtEntities = $('#dtEntities').dataTable({
                            'pageLength': 100,
                            'retrieve': true, 'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'responsive': true, // https://datatables.net/extensions/responsive/examples/
                            // Text translation options Note the required keywords between underscores (e.g
                            // _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            // Datatable Buttons setup
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                {
                                    extend: 'copy',
                                    className: 'btn-sm',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'csv',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'excel',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'pdf',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'print',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs.keyup(function () {
                            dtEntities.fnFilter(this.value, columnInputs.index(this));
                        });
                    }.bind(this), 500);
                } else {
                    this.setState({data: []}); //just to update the component because has no data to display
                }
            })
            .catch(function (error) {
                console.log(error);
            });

               axios
            .get(__APIURL__ + "/entityAndCompanies")
            .then(res => {
                this.setState({customerCompanies: res.data})
            })
    }

    create() {
        this
            .props
            .router
            .push('/customers/create');
    }

    edit(id) {
        this
            .props
            .router
            .push('/customers/edit/' + id);
    }
    view(id) {
        this
            .props
            .router
            .push('/customers/view/' + id + "/" + 1); //por default vai para a aba contactos
    }

    delete(name, id) {
        var self = this;
        var cust = "";
        var onCompany = false;
        this
            .state
            .data
            .forEach(customer => {
                if (customer._id == id) {
                    cust = customer;
                }
            })
            this.state.customerCompanies.forEach(customerC =>{
                if(customerC.entity == id){
                    onCompany = true;
                }
            })
           
        if (cust.processes.length == 0 && cust.projects.length == 0 && cust.contracts.length == 0 && cust.contacts.length == 0 && !onCompany) {
            swal({
                title: "Tem a certeza que quer eliminar?",
                text: "Após eliminar não poderá recuperar o registo!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                closeOnConfirm: false
            }, function () {

                axios
                    .delete(__APIURL__ + "/entities/" + id)
                    .then(function (response) {
                        if (response.status == 200) {
                            swal("Eliminado!", name + " foi eliminado.", "success");
                            self.getDataFromBD();
                        } else {
                            // TODO: HANDLE IT
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        // TODO: HANDLE IT
                    });
            });
        } else {
            swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
        }

    }

    render() {
        const css = css;
        var rows = [];
        var tableContacts = [];
        var hasNotContacts = true;
        var accountManagerName = "";
        this
            .state
            .data
            .map((data, a) => {

                this
                    .state
                    .accountManager
                    .forEach((user, i) => {
                        if (data.accountManager == user._id) {
                            accountManagerName = user.name;
                        }
                    })
                rows.push(

                    <tr key={data._id}>

                        <td>{data.nif}</td>
                        <td>{data.qrCode}</td>
                        <td>{data.taxName}</td>
                        <td>{data.abrevName}</td>
                        <td>{accountManagerName}
                        </td>
                        <td>
                            <button
                                onClick={() => this.view(data._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-eye"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.edit(data._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(data.taxName, data._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>

                    </tr>

                );

            });

        return (
            <ContentWrapper>
                <h3>Clientes
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <button
                    onClick={() => this.create()}
                    type="button"
                    id="addBtn"
                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                    <i className="icon-plus"></i>
                </button>
                <Row>
                    <Col lg={12}>
                        <Panel>
                            <Table id="dtEntities" responsive hover>
                                <thead>
                                    <tr>
                                        <th>NIF</th>
                                        <th>QR code</th>
                                        <th>Nome fiscal</th>
                                        <th>Nome abreviado</th>
                                        <th>Gestor de conta</th>

                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {rows}</tbody>
                                {/* <tfoot>

                        <tr>
                            <th>
                            </th>
                            <th style={{width:"20%"}}>
                                <input
                                    type="text"
                                    name="filter_nif"
                                    placeholder="Filtrar NIF"
                                    className="form-control input-sm datatable_input_col_search"/>
                            </th>
                            <th style={{width:"20%"}}>
                                <input
                                    type="text"
                                    name="filter_qrcode"
                                    placeholder="Filtrar QR Code"
                                    className="form-control input-sm datatable_input_col_search"/>
                            </th>
                            <th style={{width:"20%"}}>
                                <input
                                    type="text"
                                    name="filter_tax_name"
                                    placeholder="Filtrar Nome fiscal"
                                    className="form-control input-sm datatable_input_col_search"/>
                            </th>
                            <th style={{width:"20%"}}>
                                <input
                                    type="text"
                                    name="filter_abrev_name"
                                    placeholder="Filtrar Nome abreviado"
                                    className="form-control input-sm datatable_input_col_search"/>
                            </th>
                            <th style={{width:"20%"}}>
                                <input
                                    type="text"
                                    name="filter_acc_man"
                                    placeholder="Filtrar Gestor de conta"
                                    className="form-control input-sm datatable_input_col_search"/>
                            </th>
                             <tr></tr>
                        <tr></tr>
                        </tr>

                        </tfoot>*/}
                            </Table>
                        </Panel>
                    </Col>
                </Row>

            </ContentWrapper>
        );
    }

}

export default Customers;
