import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';
import moment from 'moment';

class Interventions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            processInterventions: [],
            interventionTypes: [],
            interventionMeans: [],
            contracts: [],
            projects: [],
            users: [],
            contacts: [],
            processes: [],
            entities: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/interventionTypes/",)
            .then(function (result) {
                th.setState({interventionTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/interventionMeans/",)
            .then(function (result) {
                th.setState({interventionMeans: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processes/",)
            .then(function (result) {
                th.setState({processes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projects/",)
            .then(function (result) {
                th.setState({projects: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contracts/",)
            .then(function (result) {
                th.setState({contracts: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/",)
            .then(function (result) {
                th.setState({entities: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/users/",)
            .then(function (result) {
                th.setState({users: result.data});
            })

    }
    // Retira x tempo (intervencao) ao tmepo do processo  (para quando se elimina
    // intervenções)
    calcUpdatedProcessTime(processTime, interventionTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(interventionTime.substring(0, interventionTime.indexOf(':')));
        var minutesInterv = parseInt(interventionTime.split(":").pop());

        var hoursProcess = parseInt(processTime.substring(0, processTime.indexOf(':')));
        var minutesProcess = parseInt(processTime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }
    calcSumBetweenA_B(ATime, BTime) {
        //ATime: '10:00' BTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesInterv + minutesProcess;
        var updatedHours = hoursInterv + hoursProcess;

        if (updatedMinutes >= 60) {
            updatedMinutes = updatedMinutes - 60;
            updatedHours++;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }
    calcDifBetweenA_B(ATime, BTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    getDataFromBD() {

        axios
            .get(__APIURL__ + "/interventions/")
            .then(res => {
                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);

                    this.setState({data})
                    setTimeout(function () {
                        $.fn.dataTableExt.oSort["customIntervention-desc"] = function(x, y) {
                            return moment(x, 'DD-MM-YYYY').valueOf() < moment(y, 'DD-MM-YYYY').valueOf();
                        };
                          
                        $.fn.dataTableExt.oSort["customIntervention-asc"] = function(x, y) {
                            return moment(x, 'DD-MM-YYYY').valueOf() > moment(y, 'DD-MM-YYYY').valueOf();
                        }

                        var dtInterventions = $('#dtInterventions').dataTable({
                            'pageLength': 100,
                            'retrieve': true, 'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'responsive': true, // https://datatables.net/extensions/responsive/examples/
                            // Text translation options Note the required keywords between underscores (e.g
                            // _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            "aoColumnDefs": [{
                                "sType": "customIntervention",
                                "bSortable": true,
                                "aTargets": [4]
                            }],
                            // Datatable Buttons setup
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                {
                                    extend: 'copy',
                                    className: 'btn-sm',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'csv',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Intervenções',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'excel',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Intervenções',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'pdf',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Intervenções',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'print',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Intervenções',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs.keyup(function () {
                            dtInterventions.fnFilter(this.value, columnInputs.index(this));
                        });
                    }.bind(this), 500);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    view(id) {
        this
            .props
            .router
            .push('/interventions/view/' + id);
    }

    create() {
        this
            .props
            .router
            .push('/interventions/create/1');
    }

    edit(id, entityParent, processParent) {
        var customerID;
        var processID;

        this
            .state
            .processes
            .forEach(process => {
                process.interventions.forEach(intervID =>{
                    if(intervID == id){
                        processID = process._id;
                        customerID = process.entity;
                    }
                })
            });

        this
            .props
            .router
            .push('/interventions/edit/' + customerID + '/' + processID + '/' + id);
    }

    delete(name, id) {
        var self = this;
        swal({
            title: "Tem a certeza que quer eliminar?",
            text: "Após eliminar não poderá recuperar o registo!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            closeOnConfirm: true
        }, function () {
            axios
                .get(__APIURL__ + "/interventions/" + id)
                .then(function (intervention) {

                    self
                        .state
                        .processes
                        .forEach(process => {

                            process
                                .interventions
                                .forEach(intervID => {
                                    if (intervID == id) {

                                        var updateProcess = {
                                            processNumber: process.processNumber,
                                            date: (process.date == null
                                                ? moment().format()
                                                : process.date),
                                            description: process.description,
                                            entity: process.entity,
                                            createdBy: process.createdBy,
                                            requestedBy: process.requestedBy,
                                            notify: process.notify,
                                            //contract: process.contract, project: process.project,
                                            processType: process.processType,
                                            state: process.state,
                                            duration: process.duration,
                                            interventions: process.interventions
                                        };
                                        if (!intervention.data.toBeValidated) {
                                            updateProcess.duration = self.calcUpdatedProcessTime(process.duration, moment(intervention.data.timeToCharge).format("HH:mm"));
                                        }

                                        var newInterventionsList = [];

                                        updateProcess
                                            .interventions
                                            .forEach(interv => {
                                                if (interv != id) {
                                                    newInterventionsList.push(interv);
                                                }
                                            })
                                        updateProcess.interventions = newInterventionsList;

                                        axios
                                            .put(__APIURL__ + '/processes/' + process._id, updateProcess, {headers: self.headers})
                                            .then(function (response) {
                                                axios
                                                    .delete(__APIURL__ + "/interventions/" + id)
                                                    .then(function (response) {
                                                        var interventionsLeft = [];

                                                        // if (!intervention.data.toBeValidated) { caso nao tenha sido validada, nao
                                                        // deve descontar nos contratos

                                                        self
                                                            .state
                                                            .data
                                                            .forEach(interv => {
                                                                if (interv._id != id) { //todas as que nao sejam para eliminar continuam
                                                                    interventionsLeft.push(interv);
                                                                } else { //a que é para eliminar

                                                                    if (interv.contractToCharge != null) {
                                                                        self
                                                                            .state
                                                                            .contracts
                                                                            .forEach(contract => {
                                                                                if (contract._id == interv.contractToCharge && !intervention.data.toBeValidated) {

                                                                                    // console.log(self.calcSumBetweenA_B(contract.availableHours,
                                                                                    // intervention.data.timeToCharge));
                                                                                    // console.log(self.calcDifBetweenA_B(contract.exhaustedHours,
                                                                                    // intervention.data.timeToCharge));
                                                                                    var updateContract = JSON.parse(JSON.stringify({
                                                                                        "beginDate": (contract.beginDate == null
                                                                                            ? new Date()
                                                                                            : contract.beginDate),
                                                                                        "endDate": (contract.endDate == null
                                                                                            ? new Date()
                                                                                            : contract.endDate),
                                                                                        "lastRenewal": (contract.lastRenewal == null
                                                                                            ? null
                                                                                            : contract.lastRenewal),
                                                                                        "notifyRenewal": contract.notifyRenewal,
                                                                                        "hours": contract.hours,
                                                                                        "perMonth": contract.perMonth,
                                                                                        "notifyExhaustedHours": contract.notifyExhaustedHours,
                                                                                        "blockExhaustedHours": contract.blockExhaustedHours,
                                                                                        "contractType": contract.contractType,
                                                                                        "state": contract.state,
                                                                                        "addenda": contract.addenda,
                                                                                        "contractCode": contract.contractCode,
                                                                                        "availableHours": self.calcSumBetweenA_B(contract.availableHours, moment(intervention.data.timeToCharge).format("HH:mm")),
                                                                                        "exhaustedHours": self.calcDifBetweenA_B(contract.exhaustedHours, moment(intervention.data.timeToCharge).format("HH:mm")),
                                                                                        "observations": contract.observations
                                                                                    }));
                                                                                   
                                                                                    axios
                                                                                        .put(__APIURL__ + '/contracts/' + contract._id, updateContract, {headers: self.headers})
                                                                                        .then(function (response) {
                                                                                           

                                                                                        })
                                                                                        .catch(function (error) {
                                                                                            console.log(error);
                                                                                        });

                                                                                }
                                                                            });

                                                                    } else {
                                                                        if (interv.projectToCharge != null && !intervention.data.toBeValidated) {

                                                                            self
                                                                                .state
                                                                                .projects
                                                                                .forEach(project => {
                                                                                    if (project._id == interv.projectToCharge) {

                                                                                        // console.log(self.calcSumBetweenA_B(contract.availableHours,
                                                                                        // intervention.data.timeToCharge));
                                                                                        // console.log(self.calcDifBetweenA_B(contract.exhaustedHours,
                                                                                        // intervention.data.timeToCharge));
                                                                                        var updateProject = JSON.parse(JSON.stringify({
                                                                                            "beginDate": (project.beginDate == null
                                                                                                ? new Date()
                                                                                                : project.beginDate),
                                                                                            "endDate": (project.endDate == null
                                                                                                ? new Date()
                                                                                                : project.endDate),
                                                                                            "hours": project.hours,
                                                                                            "notifyExhaustedHours": project.notifyExhaustedHours,
                                                                                            "blockExhaustedHours": project.blockExhaustedHours,
                                                                                            "projectType": project.projectType,
                                                                                            "state": project.state,
                                                                                            "projectCode": project.projectCode,
                                                                                            "availableHours": project.availableHours,
                                                                                            "exhaustedHours": self.calcDifBetweenA_B(project.exhaustedHours, moment(intervention.data.timeToCharge).format("HH:mm"))
                                                                                        }));

                                                                                        axios
                                                                                            .put(__APIURL__ + '/projects/' + project._id, updateProject, {headers: self.headers})
                                                                                            .then(function (response) {
                                                                                               

                                                                                            })
                                                                                            .catch(function (error) {
                                                                                                console.log(error);
                                                                                            });

                                                                                    }
                                                                                });

                                                                        }
                                                                    }

                                                                }
                                                            });

                                                        //}

                                                        self.setState({
                                                            data: interventionsLeft || []
                                                        });

                                                        swal("Eliminado!", "Intervenção eliminada.", "success");

                                                    })
                                                    .catch(function () {
                                                        console.log(error);
                                                    });

                                            })
                                            .catch(function (error) {
                                                console.log(error);
                                            });

                                    }
                                });

                        })

                });

            /* th.serverRequest = axios
                .put(__APIURL__ + '/processes/' + selectedProcess._id, updateProcess, {headers: th.headers})
                .then(function (response) {

                    th
                        .props
                        .router
                        .push('/customers/view/' + selectedProcess.entity + "/4");

                })
                .catch(function (error) {
                    console.log(error);
                });*/
            // axios     .delete(__APIURL__ + "/interventions/" +
            // id)     .then(function (response) {         if (response.status == 200) {
            // swal("Eliminado!", name + " foi eliminado.", "success");
            // self.getDataFromBD();         } else {             // TODO: HANDLE IT } })
            // .catch(function (error) {         console.log(error);         //
            // TODO: HANDLE IT     });
        });
    }

    render() {
        const css = css;
        var th = this;
        var interventionRows = []
        var interventionT = "";
        var customer = "";
        var interventionM = "";
        var collaboratorName = "";
        var toCharge = "";
        var offerSymbol;
        var complaintSymbol;
        //   var validatedSymbol = <em className="fa fa-close"></em>;   var
        // terminateSymbol = <em className="fa fa-close"></em>; Popular a linha de
        // registos das intervenções
   
        this
            .state
            .data
            .forEach((intervention, i) => {

                th
                    .state
                    .interventionTypes
                    .forEach((intervT, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (intervention.interventionType == intervT._id) {

                            interventionT = intervT.description;
                        }
                    })
                th
                    .state
                    .interventionMeans
                    .forEach((intervM, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (intervention.means == intervM._id) {

                            interventionM = intervM.description;
                        }
                    })
                th
                    .state
                    .users
                    .forEach((collaborator, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (intervention.collaborator == collaborator._id) {

                            collaboratorName = collaborator.name;
                        }
                    })
                if (th.state.contracts.message != "NO_RESULTS") {
                    th
                        .state
                        .contracts
                        .forEach((c, j) => {
                            //para esse contacto verifica o nome da descrição do tipo de contacto
                            if (intervention.contractToCharge == c._id) {
                                th.state.entities.forEach(customerObj =>{
                                    customerObj.contracts.forEach(contrID =>{
                                        if(contrID == c._id){
                                            customer = customerObj.abrevName;
                                        }
                                    })
                                })
                                toCharge = c.contractCode;
                            }
                        })
                }

                if (th.state.projects.message != "NO_RESULTS") {
                    th
                        .state
                        .projects
                        .forEach((p, j) => {
                            //para esse contacto verifica o nome da descrição do tipo de contacto
                            if (intervention.projectToCharge == p._id) {
                                th.state.entities.forEach(customerObj =>{
                                    customerObj.projects.forEach(projtID =>{
                                        if(projtID == p._id){
                                            customer = customerObj.abrevName;
                                        }
                                    })
                                })
                                toCharge = p.projectCode;
                            }
                        })
                }

                if (intervention.offer == true) {
                    offerSymbol = <em className="fa fa-check"></em>
                } else {
                    offerSymbol = <em className="fa fa-close"></em>
                }
                if (intervention.complaint == true) {
                    complaintSymbol = <em className="fa fa-check"></em>
                } else {
                    complaintSymbol = <em className="fa fa-close"></em>;
                }
                var toBeValidated;
                if (intervention.toBeValidated) {
                    toBeValidated = <em className="fa fa-close"></em>
                } else {
                    toBeValidated = <em className="fa fa-check"></em>
                }
                //para esse contacto faz o push para contact rows
                interventionRows.push(
                    <tr key={intervention._id} className="active">
                        <td>{intervention.interventionNumber}</td>
                        <td>{interventionT}</td>
                        <td>{customer}</td>
                        <td>{collaboratorName}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{moment.utc(intervention.date).format('DD-MM-YYYY')}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(intervention.beginHour)
                                .toLocaleTimeString()
                                .substr(0, 5)}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(intervention.endHour)
                                .toLocaleTimeString()
                                .substr(0, 5)}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(intervention.totalTime)
                                .toLocaleTimeString()
                                .substr(0, 5)}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(intervention.timeToCharge)
                                .toLocaleTimeString()
                                .substr(0, 5)}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{toBeValidated}</td>
                        <td>

                            <button
                                onClick={() => this.view(intervention._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-eye"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.edit(intervention._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(intervention.interventionNumber, intervention._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                );

                //  }  })
            })

        return (
            <ContentWrapper>
                <h3>Todas as intervenções
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <Row>
                    <Col lg={12}>
                        <Panel>
                            <button
                                onClick={() => this.create()}
                                type="button"
                                id="addBtn"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                <i className="icon-plus"></i>
                            </button>
                            <Table id="dtInterventions" responsive hover>
                                <thead>
                                    <tr className="active">
                                        <th>Número de intervenção</th>
                                        <th>Tipo de intervenção</th>
                                        <th>Cliente</th>
                                        <th>Colaborador</th>
                                        <th>Data</th>
                                        <th>Hora de início</th>
                                        <th>Hora de fim</th>
                                        <th>Tempo real</th>
                                        <th>Tempo cobrado</th>
                                        <th>Validado</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {interventionRows}
                                </tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default Interventions;
