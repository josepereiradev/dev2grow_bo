import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import { Router, Route, Link, History, withRouter } from 'react-router';
import { browserHistory } from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class AddIntervention extends React.Component {
    constructor() {
        super();
        this.state = {
            //to go to db
            selectedEntity: "",
            selectedProcess: "",
            selectedContract: "",
            selectedProject: "",

            selectedInterventionType: "",
            selectedInterventionMean: null,
            collaborator: sessionStorage.getItem('user') ? JSON.parse(sessionStorage.getItem('user')).id : '',

            date: moment().format("DD-MM-YYYY"),
            beginHour: moment().format("HH:mm"),
            endHour: moment().format("HH:mm"),
            totalTime: '00:00',
            timeToCharge: '00:00',
            requestedBy: "",
            notifyThisInterv: [],
            report: "",
            selectedProcessState: "",
            toBeValidated: true,
            offer: false,
            complaint: false,

            //General Info
            interventionTypes: [],
            processesStates: [],
            contractStates: [],
            projectStates: [],
            processesTypes: [],
            processData: [],
            users: [],
            interventionMeans: [],
            projectToCharge: null,
            contractToCharge: null,
            terminateProcess: false,

            contractTypes: [],
            projectTypes: [],
            entities: [],
            processes: [], //processes of the customer passed by props
            toCharge: "",
            projects: [], //projects of the customer
            contracts: [], //contracts of the customer
            contacts: [],
            entityProcesses: [],
            processesOfContractsOrProjects: [],
            entityProjects: [],
            entityContracts: [],
            entityContacts: [],
            interventionToAssignToProcess: null,
            addingNewProcess: false,

            //NewProcessIfNotExists
            newProcessPage: false,
            processType: "",
            descriptionProcess: "",

            //Creating new contact
            addingNewContact: false,
            newContactName: "",
            newContactEmail: "",
            newContactPhone: "",
            newContactPhone2: ""
        };

        this.handleChange = this.handleChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);

        this.handleSubmitProcess = this.handleSubmitProcess.bind(this);

        this.handleSubmitNewContact = this.handleSubmitNewContact.bind(this);

        this.handleCheckbox = this.handleCheckbox.bind(this);

    }

    handleChange(name, e) {
        var th = this;
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

        /*if (name == "selectedEntity") {
            this.serverRequest = axios
                .get(__APIURL__ + "/entities/" + e.target.value)
                .then(function (result) {
                    th.setState({entityProcesses: result.data.processes, entityProjects: result.data.projects, entityContracts: result.data.contracts});
                })
        }*/

        // $('#select2-select2-1-contractOrProject-container').prop('title', '-- Escolha
        // uma opção --'); $("#select2-select2-1-contractOrProject-container").text("--
        // Escolha uma opção --"); //resets the value selected alert("af");

    }

    handleCheckbox(name, e) {

        if (e.target.name == "offer") {
            if (e.target.checked) {
                this.setState({
                    [e.target.name]: e.target.checked,
                    timeToCharge: moment("00:00", "HH:mm").format("HH:mm")
                });
            } else {
                if (this.state.selectedInterventionType != "") {
                    this
                        .state
                        .interventionTypes
                        .forEach(intType => {
                            if (intType._id == this.state.selectedInterventionType) {

                                if (moment(this.state.totalTime, "HH:mm").diff(moment(moment(intType.minimumTime).format("HH:mm"), "HH:mm")) < 0) {

                                    this.setState({
                                        [e.target.name]: e.target.checked,
                                        timeToCharge: moment(intType.minimumTime).format("HH:mm")
                                    });
                                    $('#datetimepickertime4').datetimepicker('date', moment(intType.minimumTime));
                                } else {
                                    var charge = this.calcNextTime(this.state.totalTime, moment(intType.minimumTime).format("HH:mm"), moment(intType.nextsTimes).format("HH:mm"));
                                    this.setState({
                                        [e.target.name]: e.target.checked,
                                        timeToCharge: charge
                                    });
                                    $('#datetimepickertime4').datetimepicker('date', charge);
                                }

                            }
                        });
                } else {
                    this.setState({
                        [e.target.name]: e.target.checked
                    });
                }

            }
        } else {
            this.setState({
                [e.target.name]: e.target.checked
            });
        }

        console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    /*validateWhatToCharge() {
        var th = this;
        if (this.state.contracts[0] != undefined)
            this.state.contracts.forEach((item, i) => {
                if (item._id == th.state.toCharge)
                    th.setState({contractToCharge: item._id});
                }
            )
        if (this.state.projects[0] != undefined)
            this.state.projects.forEach((item, i) => {
                if (item._id == th.state.toCharge)
                    th.setState({projectToCharge: item._id});
                }
            )

    }*/

    calcSumBetweenA_B(ATime, BTime) {
        //ATime: '10:00' BTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesInterv + minutesProcess;
        var updatedHours = hoursInterv + hoursProcess;

        if (updatedMinutes >= 60) {
            updatedMinutes = updatedMinutes - 60;
            updatedHours++;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    calcDifBetweenA_B(ATime, BTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    calcNextTime(totalTime, minimum, next) {

        //Transformar as horas (xx:xx) em segundos
        var a = totalTime.split(':');
        var numMinutesTotalTime = parseInt(a[0]) * 60 * 60 + parseInt(a[1]) * 60;

        var b = minimum.split(':');
        var numMinutesMinimumTime = parseInt(b[0]) * 60 * 60 + parseInt(b[1]) * 60;

        var c = next.split(':');
        var numMinutesNextTime = parseInt(c[0]) * 60 * 60 + parseInt(c[1]) * 60;

        //Subtrair os minutos
        var diff = numMinutesTotalTime - numMinutesMinimumTime;

        //Validação que verifica se atingiu o tempo minimo
        if (numMinutesTotalTime < numMinutesMinimumTime) {
            return minimum;
        }
        //Nova hora terá estes minutos
        var newTimeInMinutes = 0;
        if(numMinutesNextTime > 0){
            newTimeInMinutes = Math.ceil(diff / numMinutesNextTime) * numMinutesNextTime;
        }

        newTimeInMinutes = Number(newTimeInMinutes);
        var h = Math.floor(newTimeInMinutes / 3600);
        var m = Math.floor(newTimeInMinutes % 3600 / 60);

        var hDisplay = h > 0 && h < 10
            ? "0" + h
            : h;
        var mDisplay = m > 0 && m < 10
            ? "0" + m
            : m;

        var newTime = this.calcSumBetweenA_B(minimum, hDisplay + ":" + mDisplay);

        return newTime;

    }

    handleSubmit(e) {

        var th = this;

        e.preventDefault();
        const {
            selectedProcess,
            selectedEntity,
            contractToCharge,
            projectToCharge,
            selectedInterventionType,
            selectedInterventionMean,
            requestedBy,
            collaborator,
            date,
            beginHour,
            endHour,
            totalTime,
            timeToCharge,
            report,
            selectedProcessState,
            toBeValidated,
            offer,
            complaint
        } = this.state;

        var failed = false;

        if (this.state.selectedEntity == "") {
            swal({
                title: "Não selecionou um cliente",
                text: "Por favor, preencha os campos obrigatórios",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });
            failed = true;
        } else if (this.state.processType = "") {
            swal({
                title: "Nenhum tipo de processo selecionado",
                text: "Por favor, preencha os campos obrigatórios",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });
            failed = true;
        } else if (this.state.selectedProcessState == "") {
            swal({
                title: "Nenhum estado de processo selecionado",
                text: "Por favor, preencha os campos obrigatórios",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });
            failed = true;
        } else if (this.state.requestedBy == "") {
            swal({
                title: "Não especificou o campo Pedido Por",
                text: "Por favor, preencha os campos obrigatórios",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });
            failed = true;
        }

        if (!failed) {
            if (selectedProcess == "") {
                var headers = new Headers({ 'Content-Type': 'application/json' });
                var newIntervention = JSON.parse(JSON.stringify({
                    "process": selectedProcess._id,
                    "interventionType": selectedInterventionType,
                    "means": selectedInterventionMean,
                    "collaborator": collaborator,
                    "contractToCharge": contractToCharge
                        ? contractToCharge._id
                        : null,
                    "projectToCharge": projectToCharge
                        ? projectToCharge._id
                        : null,
                    "date": moment(date, "DD-MM-YYYY").format(),
                    "beginHour": moment(beginHour, "HH:mm").format(),
                    "endHour": moment(endHour, "HH:mm").format(),
                    "totalTime": moment(totalTime, "HH:mm").format(),
                    "timeToCharge": moment(timeToCharge, "HH:mm").format(),
                    "requestedBy": th.state.requestedBy,
                    "notify": th.state.notifyThisInterv,
                    "report": report,
                    "processState": selectedProcessState,
                    "toBeValidated": toBeValidated,
                    "offer": offer,
                    "complaint": complaint
                }));
                swal({
                    title: "Não selecionou um processo!",
                    text: "Deseja abrir um novo processo?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Abrir um novo processo",
                    cancelButtonText: "Voltar",
                    closeOnConfirm: true
                }, function (isConfirm) {
                    if (isConfirm) { //fazer aparecer o popup de abrir novo processo e de seguida criar intervenção
                        th.setState({ interventionToAssignToProcess: newIntervention, addingNewProcess: true });
                    }
                    // else {     var headers = new Headers({'Content-Type': 'application/json'});
                    // e.preventDefault();     this.serverRequest = axios
                    // .post(__APIURL__ + '/interventions/',
                    // newIntervention, {headers: this.headers})         .then(function () {     th
                    // .props                 .router .push('/interventions');  }) .catch(function
                    // (error) {     console.log(error);         }); }

                });
            } else {
                var headers = new Headers({ 'Content-Type': 'application/json' });
                var newIntervention = JSON.parse(JSON.stringify({
                    "process": selectedProcess._id,
                    "interventionType": selectedInterventionType,
                    "means": selectedInterventionMean,
                    "collaborator": collaborator,
                    "contractToCharge": contractToCharge
                        ? contractToCharge._id
                        : null,
                    "projectToCharge": projectToCharge
                        ? projectToCharge._id
                        : null,
                    "date": moment(date, "DD-MM-YYYY").format(),
                    "beginHour": moment(beginHour, "HH:mm").format(),
                    "endHour": moment(endHour, "HH:mm").format(),
                    "totalTime": moment(totalTime, "HH:mm").format(),
                    "timeToCharge": moment(timeToCharge, "HH:mm").format(),
                    "requestedBy": th.state.requestedBy,
                    "notify": th.state.notifyThisInterv,
                    "report": report,
                    "processState": selectedProcessState,
                    "toBeValidated": toBeValidated,
                    "offer": offer,
                    "complaint": complaint
                }));
                submitIntervention();
            }
        }

        function submitIntervention() {

            th.serverRequest = axios
                .post(__APIURL__ + '/interventions/', newIntervention, { headers: th.headers })
                .then(function (response) {

                    var idOfNewIntervention = response.data.id;
                    var intervNumber = response.data.obj.interventionNumber;
                    console.log(newIntervention);
                    if (response.status == 201) {
                        axios
                            .get(__APIURL__ + '/processStates/' + newIntervention.processState)
                            .then(function (result) {
                                var updateProcess = {
                                    processNumber: selectedProcess.processNumber,
                                    date: (selectedProcess.date == null
                                        ? moment().format()
                                        : selectedProcess.date),
                                    description: selectedProcess.description,
                                    entity: selectedProcess.entity,
                                    createdBy: selectedProcess.createdBy,
                                    requestedBy: selectedProcess.requestedBy,
                                    collaborator: collaborator,
                                    notify: selectedProcess.notify,
                                    processType: selectedProcess.processType,
                                    state: result.data._id,
                                    duration: selectedProcess.duration,
                                    interventions: selectedProcess.interventions
                                    //"contract": selectedProcess.contract, "project": selectedProcess.project,
                                };
                                if (!newIntervention.toBeValidated) {
                                    updateProcess.duration = th.calcSumBetweenA_B(updateProcess.duration, th.state.timeToCharge);
                                }

                                updateProcess
                                    .interventions
                                    .push(idOfNewIntervention);

                                th.serverRequest = axios
                                    .put(__APIURL__ + '/processes/' + selectedProcess._id, updateProcess, { headers: th.headers })
                                    .then(function (response) {

                                        if (contractToCharge != null) {

                                            var updateContract = JSON.parse(JSON.stringify({
                                                "beginDate": (contractToCharge.beginDate == null
                                                    ? new Date()
                                                    : contractToCharge.beginDate),
                                                "endDate": (contractToCharge.endDate == null
                                                    ? new Date()
                                                    : contractToCharge.endDate),
                                                "lastRenewal": (contractToCharge.lastRenewal == null
                                                    ? null
                                                    : contractToCharge.lastRenewal),
                                                "notifyRenewal": contractToCharge.notifyRenewal,
                                                "hours": contractToCharge.hours,
                                                "perMonth": contractToCharge.perMonth,
                                                "notifyExhaustedHours": contractToCharge.notifyExhaustedHours,
                                                "blockExhaustedHours": contractToCharge.blockExhaustedHours,
                                                "contractType": contractToCharge.contractType,
                                                "state": contractToCharge.state,
                                                "addenda": contractToCharge.addenda,
                                                "contractCode": contractToCharge.contractCode,
                                                "availableHours": contractToCharge.availableHours,
                                                "exhaustedHours": contractToCharge.exhaustedHours,
                                                "observations": contractToCharge.observations
                                            }));
                                            if (!newIntervention.toBeValidated) {
                                                updateContract.availableHours = th.calcDifBetweenA_B(updateContract.availableHours, moment(newIntervention.timeToCharge).format("HH:mm"));
                                                updateContract.exhaustedHours = th.calcSumBetweenA_B(updateContract.exhaustedHours, moment(newIntervention.timeToCharge).format("HH:mm"));
                                            }

                                            th.serverRequest = axios
                                                .put(__APIURL__ + '/contracts/' + contractToCharge._id, updateContract, { headers: th.headers })
                                                .then(function (response) {

                                                    swal({
                                                        title: "Confirmação de notificação por e-mail",
                                                        text: "Deseja notificar por e-mail os contactos selecionados?",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Sim",
                                                        cancelButtonText: "Não",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {
                                                        if (isConfirm) { //fazer aparecer o popup de abrir novo processo e de seguida criar intervenção
                                                            var objToSendToEmail = {};
                                                            th
                                                                .state
                                                                .entities
                                                                .forEach(cust => {
                                                                    if (cust._id == selectedProcess.entity) {
                                                                        objToSendToEmail.customer = cust.abrevName;
                                                                    }
                                                                });
                                                            th
                                                                .state
                                                                .interventionTypes
                                                                .forEach(intervType => {
                                                                    if (intervType._id == selectedInterventionType) {
                                                                        objToSendToEmail.interventionType = intervType.description;
                                                                    }
                                                                });
                                                            th
                                                                .state
                                                                .processesStates
                                                                .forEach(processState => {
                                                                    if (processState._id == newIntervention.processState) {
                                                                        objToSendToEmail.processState = processState.stateName;
                                                                    }
                                                                })
                                                            th
                                                                .state
                                                                .users
                                                                .forEach(users => {
                                                                    if (users._id == newIntervention.collaborator) {
                                                                        objToSendToEmail.collaborator = users.name;
                                                                    }
                                                                })
                                                            th
                                                                .state
                                                                .contacts
                                                                .forEach(contact => {

                                                                    if (contact._id == newIntervention.requestedBy) {
                                                                        objToSendToEmail.requestedBy = contact.email;
                                                                    }
                                                                })
                                                            var emailsToNotify = [];
                                                            newIntervention
                                                                .notify
                                                                .forEach(contactToNotify => {
                                                                    th
                                                                        .state
                                                                        .contacts
                                                                        .forEach(contact => {
                                                                            if (contactToNotify == contact._id) {
                                                                                emailsToNotify.push(contact.email);
                                                                            }
                                                                        })
                                                                })

                                                            objToSendToEmail.process = selectedProcess.processNumber;
                                                            objToSendToEmail.date = moment(newIntervention.date).format("DD/MM/YYYY");
                                                            objToSendToEmail.beginHour = moment(newIntervention.beginHour).format("HH:mm");
                                                            objToSendToEmail.endHour = moment(newIntervention.endHour).format("HH:mm");
                                                            objToSendToEmail.totalTime = moment(newIntervention.totalTime).format("HH:mm");
                                                            objToSendToEmail.timeToCharge = moment(newIntervention.timeToCharge).format("HH:mm");
                                                            objToSendToEmail.report = newIntervention.report;
                                                            objToSendToEmail.ticket = intervNumber;
                                                            objToSendToEmail.requestdescription = selectedProcess.description;
                                                            objToSendToEmail.notify = emailsToNotify;

                                                            axios
                                                                .post(__APIURL__ + "/services/send-intervention-email", objToSendToEmail, { headers: th.headers })
                                                                .then((result) => { }) // /////////////////////////////sending email /\
                                                            th
                                                                .props
                                                                .router
                                                                .push('/customers/view/' + selectedProcess.entity + "/4");
                                                        } else {
                                                            th
                                                                .props
                                                                .router
                                                                .push('/customers/view/' + selectedProcess.entity + "/4");
                                                        }

                                                    });

                                                    //Envio notificaçção para a app
                                                    var a = contractToCharge
                                                        .hours
                                                        .split(':');
                                                    var minutesContractHours = (+ a[0]) * 60 + (+ a[1]);

                                                    var b = contractToCharge
                                                        .availableHours
                                                        .split(':');
                                                    var minutesAvailableContractHours = (+ b[0]) * 60 + (+ b[1]);

                                                    if (minutesContractHours * 0.1 > minutesAvailableContractHours) {
                                                        var bodyForNotification = {
                                                            contract: response.data.obj,
                                                            usersToNotify: selectedEntity.associatedUsers
                                                        };
                                                        axios
                                                            .post(__APIURL__ + "/services/send-notification-exhausted-hour" +
                                                            "s",
                                                            bodyForNotification, { headers: th.headers })
                                                            .then(function (response) { })
                                                    }

                                                })
                                                .catch(function (error) {
                                                    console.log(error);
                                                });

                                        } else if (projectToCharge != null) {
                                            var updateProject = JSON.parse(JSON.stringify({
                                                "beginDate": (projectToCharge.beginDate == null
                                                    ? new Date()
                                                    : projectToCharge.beginDate),
                                                "endDate": (projectToCharge.endDate == null
                                                    ? null
                                                    : projectToCharge.endDate),
                                                "hours": projectToCharge.hours,
                                                "notifyExhaustedHours": projectToCharge.notifyExhaustedHours,
                                                "blockExhaustedHours": projectToCharge.blockExhaustedHours,
                                                "projectType": projectToCharge.projectType,
                                                "state": projectToCharge.state,
                                                "projectCode": projectToCharge.projectCode,
                                                "exhaustedHours": projectToCharge.exhaustedHours
                                            }));
                                            if (!newIntervention.toBeValidated) {
                                                updateProject.exhaustedHours = th.calcSumBetweenA_B(updateProject.exhaustedHours, th.state.timeToCharge);
                                            }

                                            th.serverRequest = axios
                                                .put(__APIURL__ + '/projects/' + projectToCharge._id, updateProject, { headers: th.headers })
                                                .then(function (response) {
                                                    swal({
                                                        title: "Confirmação de notificação por e-mail",
                                                        text: "Deseja notificar por e-mail os contactos selecionados?",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Sim",
                                                        cancelButtonText: "Não",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {
                                                        if (isConfirm) { //fazer aparecer o popup de abrir novo processo e de seguida criar intervenção
                                                            var objToSendToEmail = {};
                                                            th
                                                                .state
                                                                .entities
                                                                .forEach(cust => {
                                                                    if (cust._id == selectedProcess.entity) {
                                                                        objToSendToEmail.customer = cust.abrevName;
                                                                    }
                                                                });
                                                            th
                                                                .state
                                                                .interventionTypes
                                                                .forEach(intervType => {
                                                                    if (intervType._id == selectedInterventionType) {
                                                                        objToSendToEmail.interventionType = intervType.description;
                                                                    }
                                                                });
                                                            th
                                                                .state
                                                                .processesStates
                                                                .forEach(processState => {
                                                                    if (processState._id == newIntervention.processState) {
                                                                        objToSendToEmail.processState = processState.stateName;
                                                                    }
                                                                })
                                                            th
                                                                .state
                                                                .users
                                                                .forEach(users => {
                                                                    if (users._id == newIntervention.collaborator) {
                                                                        objToSendToEmail.collaborator = users.name;
                                                                    }
                                                                })
                                                            th
                                                                .state
                                                                .contacts
                                                                .forEach(contact => {

                                                                    if (contact._id == newIntervention.requestedBy) {
                                                                        objToSendToEmail.requestedBy = contact.email;
                                                                    }
                                                                })
                                                            var emailsToNotify = [];
                                                            newIntervention
                                                                .notify
                                                                .forEach(contactToNotify => {
                                                                    th
                                                                        .state
                                                                        .contacts
                                                                        .forEach(contact => {
                                                                            if (contactToNotify == contact._id) {
                                                                                emailsToNotify.push(contact.email);
                                                                            }
                                                                        })
                                                                })

                                                            objToSendToEmail.process = selectedProcess.processNumber;
                                                            objToSendToEmail.date = moment(newIntervention.date).format("DD/MM/YYYY");
                                                            objToSendToEmail.beginHour = moment(newIntervention.beginHour).format("HH:mm");
                                                            objToSendToEmail.endHour = moment(newIntervention.endHour).format("HH:mm");
                                                            objToSendToEmail.totalTime = moment(newIntervention.totalTime).format("HH:mm");
                                                            objToSendToEmail.timeToCharge = moment(newIntervention.timeToCharge).format("HH:mm");
                                                            objToSendToEmail.report = newIntervention.report;
                                                            objToSendToEmail.ticket = intervNumber;
                                                            objToSendToEmail.requestdescription = selectedProcess.description;
                                                            objToSendToEmail.notify = emailsToNotify;

                                                            axios
                                                                .post(__APIURL__ + "/services/send-intervention-email", objToSendToEmail, { headers: th.headers })
                                                                .then((result) => { }) // /////////////////////////////sending email /\
                                                            th
                                                                .props
                                                                .router
                                                                .push('/customers/view/' + selectedProcess.entity + "/4");
                                                        } else {
                                                            th
                                                                .props
                                                                .router
                                                                .push('/customers/view/' + selectedProcess.entity + "/4");
                                                        }

                                                    });
                                                })
                                                .catch(function (error) {
                                                    console.log(error);
                                                });
                                        } else {
                                            swal({
                                                title: "Confirmação de notificação por e-mail",
                                                text: "Deseja notificar por e-mail os contactos selecionados?",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Sim",
                                                cancelButtonText: "Não",
                                                closeOnConfirm: true
                                            }, function (isConfirm) {
                                                if (isConfirm) { //fazer aparecer o popup de abrir novo processo e de seguida criar intervenção
                                                    var objToSendToEmail = {};
                                                    th
                                                        .state
                                                        .entities
                                                        .forEach(cust => {
                                                            if (cust._id == selectedProcess.entity) {
                                                                objToSendToEmail.customer = cust.abrevName;
                                                            }
                                                        });
                                                    th
                                                        .state
                                                        .interventionTypes
                                                        .forEach(intervType => {
                                                            if (intervType._id == selectedInterventionType) {
                                                                objToSendToEmail.interventionType = intervType.description;
                                                            }
                                                        });
                                                    th
                                                        .state
                                                        .processesStates
                                                        .forEach(processState => {
                                                            if (processState._id == newIntervention.processState) {
                                                                objToSendToEmail.processState = processState.stateName;
                                                            }
                                                        })
                                                    th
                                                        .state
                                                        .users
                                                        .forEach(users => {
                                                            if (users._id == newIntervention.collaborator) {
                                                                objToSendToEmail.collaborator = users.name;
                                                            }
                                                        })
                                                    th
                                                        .state
                                                        .contacts
                                                        .forEach(contact => {

                                                            if (contact._id == newIntervention.requestedBy) {
                                                                objToSendToEmail.requestedBy = contact.email;
                                                            }
                                                        })
                                                    var emailsToNotify = [];
                                                    newIntervention
                                                        .notify
                                                        .forEach(contactToNotify => {
                                                            th
                                                                .state
                                                                .contacts
                                                                .forEach(contact => {
                                                                    if (contactToNotify == contact._id) {
                                                                        emailsToNotify.push(contact.email);
                                                                    }
                                                                })
                                                        })

                                                    objToSendToEmail.process = selectedProcess.processNumber;
                                                    objToSendToEmail.date = moment(newIntervention.date).format("DD/MM/YYYY");
                                                    objToSendToEmail.beginHour = moment(newIntervention.beginHour).format("HH:mm");
                                                    objToSendToEmail.endHour = moment(newIntervention.endHour).format("HH:mm");
                                                    objToSendToEmail.totalTime = moment(newIntervention.totalTime).format("HH:mm");
                                                    objToSendToEmail.timeToCharge = moment(newIntervention.timeToCharge).format("HH:mm");
                                                    objToSendToEmail.report = newIntervention.report;
                                                    objToSendToEmail.ticket = intervNumber;
                                                    objToSendToEmail.requestdescription = selectedProcess.description;
                                                    objToSendToEmail.notify = emailsToNotify;

                                                    axios
                                                        .post(__APIURL__ + "/services/send-intervention-email", objToSendToEmail, { headers: th.headers })
                                                        .then((result) => { }) // /////////////////////////////sending email /\
                                                    th
                                                        .props
                                                        .router
                                                        .push('/customers/view/' + selectedProcess.entity + "/4");
                                                } else {
                                                    th
                                                        .props
                                                        .router
                                                        .push('/customers/view/' + selectedProcess.entity + "/4");
                                                }

                                            });
                                        }

                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });

                            });
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

    }

    handleSubmitProcess(e) { //adiciona o processo e a intervenção
        var th = this;
        console.log(this.state);

        var interventionToAdd = this.state.interventionToAssignToProcess;

        const {
            selectedEntity,
            projectToCharge,
            contractToCharge,
            processType,
            selectedProcessState,
            date,
            requestedBy, //o requestedBy da intervenção vai ser o mesmo que o processo
            notifyThisInterv, //o notify do interv vai ser igual ao do processo
            descriptionProcess

        } = this.state;

        var project;
        var contract;
        if (projectToCharge != null) {
            project = projectToCharge._id;
            contract = null;
        } else {
            if (contractToCharge != null) {
                project = null;
                contract = contractToCharge._id;
            }
        }
        const processCreatedBy = sessionStorage.getItem('user')
            ? JSON
                .parse(sessionStorage.getItem('user'))
                .id
            : '';

        var headers = new Headers({ 'Content-Type': 'application/json' });
        var newProcess = JSON.parse(JSON.stringify({
            // "date": (date == null     ? new Date().toISOString()     : new
            // Date(date).toISOString()), "subject": subject, "description": description,
            // "entity": entityData._id, "createdBy": createdBy, "requestedBy": requestedBy,
            // "attachments": attachments, "notify": notify, //  "contract":
            // selectedContract, "processType": selectedProcessType, "state":
            // selectedProcessState
            "date": (date == null
                ? moment().format()
                : moment(date, "DD-MM-YYYY").format()),
            "description": descriptionProcess,
            "entity": selectedEntity._id,
            "createdBy": processCreatedBy,
            "requestedBy": requestedBy,
            "collaborator": interventionToAdd.collaborator,
            "notify": notifyThisInterv,
            "contract": contract,
            "project": project,
            "processType": processType,
            "duration": "00:00",
            "state": selectedProcessState,
            "interventions": []
        }));
        e.preventDefault();

        this.serverRequest = axios
            .post(__APIURL__ + '/interventions/', interventionToAdd, { headers: this.headers })
            .then(function (response) {
                if (response.status == 201) {
                    var idOfNewIntervention = response.data.id;
                    var intervNumber = response.data.obj.interventionNumber;

                    newProcess
                        .interventions
                        .push(idOfNewIntervention);

                    if (!interventionToAdd.toBeValidated) {

                        newProcess.duration = moment(interventionToAdd.timeToCharge).format("HH:mm");
                    }
                    axios
                        .post(__APIURL__ + '/processes/', newProcess, { headers: th.headers })
                        .then(function (response) {

                            var processInserted = response.data.obj;
                            th
                                .state
                                .entities
                                .forEach(cust => {
                                    if (cust._id == newProcess.entity) {
                                        var updatedCustomer = {
                                            "nif": cust.nif,
                                            "qrCode": cust.qrCode,
                                            "taxName": cust.taxName,
                                            "abrevName": cust.abrevName,
                                            "accountManager": cust.accountManager,
                                            "processes": cust.processes,
                                            "projects": cust.projects,
                                            "contracts": cust.contracts,
                                            "associatedUsers": cust.associatedUsers,
                                            "contacts": cust.contacts
                                        }

                                        updatedCustomer
                                            .processes
                                            .push(response.data.id);

                                        axios
                                            .put(__APIURL__ + '/entities/' + newProcess.entity, updatedCustomer, { headers: th.headers })
                                            .then(function (response) {

                                                var firstIfEntered = false;
                                                if (interventionToAdd.contractToCharge != null && !interventionToAdd.toBeValidated) {
                                                    firstIfEntered = true;
                                                    th
                                                        .state
                                                        .contracts
                                                        .forEach(contract => {
                                                            if (contract._id == interventionToAdd.contractToCharge) {

                                                                var updateContract = JSON.parse(JSON.stringify({
                                                                    "beginDate": (contract.beginDate == null
                                                                        ? new Date()
                                                                        : contract.beginDate),
                                                                    "endDate": (contract.endDate == null
                                                                        ? new Date()
                                                                        : contract.endDate),
                                                                    "lastRenewal": (contract.lastRenewal == null
                                                                        ? null
                                                                        : contract.lastRenewal),
                                                                    "notifyRenewal": contract.notifyRenewal,
                                                                    "hours": contract.hours,
                                                                    "perMonth": contract.perMonth,
                                                                    "notifyExhaustedHours": contract.notifyExhaustedHours,
                                                                    "blockExhaustedHours": contract.blockExhaustedHours,
                                                                    "contractType": contract.contractType,
                                                                    "state": contract.state,
                                                                    "addenda": contract.addenda,
                                                                    "contractCode": contract.contractCode,
                                                                    "availableHours": contract.availableHours,
                                                                    "exhaustedHours": contract.exhaustedHours,
                                                                    "observations": contract.observations
                                                                }));

                                                                updateContract.availableHours = th.calcDifBetweenA_B(updateContract.availableHours, moment(interventionToAdd.timeToCharge).format("HH:mm"));
                                                                updateContract.exhaustedHours = th.calcSumBetweenA_B(updateContract.exhaustedHours, moment(interventionToAdd.timeToCharge).format("HH:mm"));

                                                                /*  if (updateContract.availableHours.substring(0, 1) == "-") {
                                                    updateContract.availableHours = "00:00";
                                                }*/

                                                                th.serverRequest = axios
                                                                    .put(__APIURL__ + '/contracts/' + contractToCharge._id, updateContract, { headers: th.headers })
                                                                    .then(function (response) {

                                                                        swal({
                                                                            title: "Confirmação de notificação por e-mail",
                                                                            text: "Deseja notificar por e-mail os contactos selecionados?",
                                                                            type: "warning",
                                                                            showCancelButton: true,
                                                                            confirmButtonColor: "#DD6B55",
                                                                            confirmButtonText: "Sim",
                                                                            cancelButtonText: "Não",
                                                                            closeOnConfirm: true
                                                                        }, function (isConfirm) {
                                                                            if (isConfirm) { //fazer aparecer o popup de abrir novo processo e de seguida criar intervenção
                                                                                var objToSendToEmail = {};
                                                                                th
                                                                                    .state
                                                                                    .entities
                                                                                    .forEach(cust => {
                                                                                        if (cust._id == newProcess.entity) {
                                                                                            objToSendToEmail.customer = cust.abrevName;
                                                                                        }
                                                                                    });
                                                                                th
                                                                                    .state
                                                                                    .interventionTypes
                                                                                    .forEach(intervType => {
                                                                                        if (intervType._id == interventionToAdd.interventionType) {
                                                                                            objToSendToEmail.interventionType = intervType.description;
                                                                                        }
                                                                                    });
                                                                                th
                                                                                    .state
                                                                                    .processesStates
                                                                                    .forEach(processState => {
                                                                                        if (processState._id == interventionToAdd.processState) {
                                                                                            objToSendToEmail.processState = processState.stateName;
                                                                                        }
                                                                                    })
                                                                                th
                                                                                    .state
                                                                                    .users
                                                                                    .forEach(users => {
                                                                                        if (users._id == interventionToAdd.collaborator) {
                                                                                            objToSendToEmail.collaborator = users.name;
                                                                                        }
                                                                                    })
                                                                                th
                                                                                    .state
                                                                                    .contacts
                                                                                    .forEach(contact => {

                                                                                        if (contact._id == interventionToAdd.requestedBy) {
                                                                                            objToSendToEmail.requestedBy = contact.email;
                                                                                        }
                                                                                    })
                                                                                var emailsToNotify = [];
                                                                                interventionToAdd
                                                                                    .notify
                                                                                    .forEach(contactToNotify => {
                                                                                        th
                                                                                            .state
                                                                                            .contacts
                                                                                            .forEach(contact => {
                                                                                                if (contactToNotify == contact._id) {
                                                                                                    emailsToNotify.push(contact.email);
                                                                                                }
                                                                                            })
                                                                                    })

                                                                                objToSendToEmail.process = processInserted.processNumber;
                                                                                objToSendToEmail.date = moment(interventionToAdd.date).format("DD/MM/YYYY");
                                                                                objToSendToEmail.beginHour = moment(interventionToAdd.beginHour).format("HH:mm");
                                                                                objToSendToEmail.endHour = moment(interventionToAdd.endHour).format("HH:mm");
                                                                                objToSendToEmail.totalTime = moment(interventionToAdd.totalTime).format("HH:mm");
                                                                                objToSendToEmail.timeToCharge = moment(interventionToAdd.timeToCharge).format("HH:mm");
                                                                                objToSendToEmail.report = interventionToAdd.report;
                                                                                objToSendToEmail.ticket = intervNumber;
                                                                                objToSendToEmail.requestdescription = processInserted.description;
                                                                                objToSendToEmail.notify = emailsToNotify;
                                                                                axios
                                                                                    .post(__APIURL__ + "/services/send-intervention-email", objToSendToEmail, { headers: th.headers })
                                                                                    .then((result) => { })

                                                                                th
                                                                                    .props
                                                                                    .router
                                                                                    .push('/customers/view/' + newProcess.entity + "/4");
                                                                            } else {
                                                                                th
                                                                                    .props
                                                                                    .router
                                                                                    .push('/customers/view/' + newProcess.entity + "/4");
                                                                            }
                                                                            //Envio notificaçção para a app
                                                                            var a = contract
                                                                                .hours
                                                                                .split(':');
                                                                            var minutesContractHours = (+ a[0]) * 60 + (+ a[1]);

                                                                            var b = contract
                                                                                .availableHours
                                                                                .split(':');
                                                                            var minutesAvailableContractHours = (+ b[0]) * 60 + (+ b[1]);

                                                                            if (minutesContractHours * 0.1 > minutesAvailableContractHours) {
                                                                                var bodyForNotification = {
                                                                                    contract: response.data.obj,
                                                                                    usersToNotify: selectedEntity.associatedUsers
                                                                                };
                                                                                axios
                                                                                    .post(__APIURL__ + "/services/send-notification-exhausted-hour" +
                                                                                    "s",
                                                                                    bodyForNotification, { headers: th.headers })
                                                                                    .then(function (response) { })
                                                                            }

                                                                        })
                                                                    })
                                                                    .catch(function (error) {
                                                                        console.log(error);
                                                                    });
                                                            }

                                                        });
                                                }

                                                if (interventionToAdd.projectToCharge != null && !interventionToAdd.toBeValidated) {
                                                    firstIfEntered = true;
                                                    th
                                                        .state
                                                        .projects
                                                        .forEach(project => {
                                                            if (project._id == interventionToAdd.projectToCharge) {
                                                                var updateProject = JSON.parse(JSON.stringify({
                                                                    "beginDate": (project.beginDate == null
                                                                        ? new Date()
                                                                        : project.beginDate),
                                                                    "endDate": (project.endDate == null
                                                                        ? new Date()
                                                                        : project.endDate),
                                                                    "hours": project.hours,
                                                                    "notifyExhaustedHours": project.notifyExhaustedHours,
                                                                    "blockExhaustedHours": project.blockExhaustedHours,
                                                                    "projectType": project.projectType,
                                                                    "state": project.state,
                                                                    "projectCode": project.projectCode,
                                                                    "exhaustedHours": project.exhaustedHours
                                                                }));

                                                                updateProject.exhaustedHours = th.calcSumBetweenA_B(updateProject.exhaustedHours, th.state.timeToCharge);

                                                                th.serverRequest = axios
                                                                    .put(__APIURL__ + '/projects/' + project._id, updateProject, { headers: th.headers })
                                                                    .then(function (response) {

                                                                        swal({
                                                                            title: "Confirmação de notificação por e-mail",
                                                                            text: "Deseja notificar por e-mail os contactos selecionados?",
                                                                            type: "warning",
                                                                            showCancelButton: true,
                                                                            confirmButtonColor: "#DD6B55",
                                                                            confirmButtonText: "Sim",
                                                                            cancelButtonText: "Não",
                                                                            closeOnConfirm: true
                                                                        }, function (isConfirm) {
                                                                            if (isConfirm) { //fazer aparecer o popup de abrir novo processo e de seguida criar intervenção
                                                                                var objToSendToEmail = {};
                                                                                th
                                                                                    .state
                                                                                    .entities
                                                                                    .forEach(cust => {
                                                                                        if (cust._id == newProcess.entity) {
                                                                                            objToSendToEmail.customer = cust.abrevName;
                                                                                        }
                                                                                    });
                                                                                th
                                                                                    .state
                                                                                    .interventionTypes
                                                                                    .forEach(intervType => {
                                                                                        if (intervType._id == interventionToAdd.interventionType) {
                                                                                            objToSendToEmail.interventionType = intervType.description;
                                                                                        }
                                                                                    });
                                                                                th
                                                                                    .state
                                                                                    .processesStates
                                                                                    .forEach(processState => {
                                                                                        if (processState._id == interventionToAdd.processState) {
                                                                                            objToSendToEmail.processState = processState.stateName;
                                                                                        }
                                                                                    })
                                                                                th
                                                                                    .state
                                                                                    .users
                                                                                    .forEach(users => {
                                                                                        if (users._id == interventionToAdd.collaborator) {
                                                                                            objToSendToEmail.collaborator = users.name;
                                                                                        }
                                                                                    })
                                                                                th
                                                                                    .state
                                                                                    .contacts
                                                                                    .forEach(contact => {

                                                                                        if (contact._id == interventionToAdd.requestedBy) {
                                                                                            objToSendToEmail.requestedBy = contact.email;
                                                                                        }
                                                                                    })
                                                                                var emailsToNotify = [];
                                                                                interventionToAdd
                                                                                    .notify
                                                                                    .forEach(contactToNotify => {
                                                                                        th
                                                                                            .state
                                                                                            .contacts
                                                                                            .forEach(contact => {
                                                                                                if (contactToNotify == contact._id) {
                                                                                                    emailsToNotify.push(contact.email);
                                                                                                }
                                                                                            })
                                                                                    })

                                                                                objToSendToEmail.process = processInserted.processNumber;
                                                                                objToSendToEmail.date = moment(interventionToAdd.date).format("DD/MM/YYYY");
                                                                                objToSendToEmail.beginHour = moment(interventionToAdd.beginHour).format("HH:mm");
                                                                                objToSendToEmail.endHour = moment(interventionToAdd.endHour).format("HH:mm");
                                                                                objToSendToEmail.totalTime = moment(interventionToAdd.totalTime).format("HH:mm");
                                                                                objToSendToEmail.timeToCharge = moment(interventionToAdd.timeToCharge).format("HH:mm");
                                                                                objToSendToEmail.report = interventionToAdd.report;
                                                                                objToSendToEmail.ticket = intervNumber;
                                                                                objToSendToEmail.requestdescription = processInserted.description;
                                                                                objToSendToEmail.notify = emailsToNotify;
                                                                                axios
                                                                                    .post(__APIURL__ + "/services/send-intervention-email", objToSendToEmail, { headers: th.headers })
                                                                                    .then((result) => { })

                                                                                th
                                                                                    .props
                                                                                    .router
                                                                                    .push('/customers/view/' + newProcess.entity + "/4");
                                                                            } else {
                                                                                th
                                                                                    .props
                                                                                    .router
                                                                                    .push('/customers/view/' + newProcess.entity + "/4");
                                                                            }
                                                                        })

                                                                    })
                                                                    .catch(function (error) {
                                                                        console.log(error);
                                                                    });
                                                            }

                                                        });

                                                } else if (!firstIfEntered) {

                                                    th
                                                        .props
                                                        .router
                                                        .push('/customers/view/' + newProcess.entity + "/4");
                                                }
                                            });

                                    }
                                });

                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                }

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    handleSubmitNewContact(e) {
        e.preventDefault();
        var th = this;
        var newContact = JSON.parse(JSON.stringify({
            "name": this.state.newContactName, "contactType": "596f70582d3dda423a2f2490", //warning!!!!!!!!!!!!!!!!!!!!!
            "active": true,
            "notifyProcesses": true,
            "telephone": this.state.newContactPhone2,
            "mobilePhone": this.state.newContactPhone,
            "email": this.state.newContactEmail
        }));
        var selectedCustomer = this.state.selectedEntity;

        this.serverRequest = axios
            .post(__APIURL__ + '/contacts/', newContact, { headers: this.headers })
            .then(function (contactResponse) {

                var updateEntity = JSON.parse(JSON.stringify({
                    "nif": selectedCustomer.nif,
                    "qrCode": selectedCustomer.qrCode,
                    "taxName": selectedCustomer.taxName,
                    "abrevName": selectedCustomer.abrevName,
                    "accountManager": selectedCustomer.accountManager,
                    "contacts": selectedCustomer.contacts,
                    "projects": selectedCustomer.projects,
                    "processes": selectedCustomer.processes,
                    "contracts": selectedCustomer.contracts,
                    "associatedUsers": selectedCustomer.associatedUsers
                }));
                updateEntity
                    .contacts
                    .push(contactResponse.data.obj._id);

                th.serverRequest = axios
                    .put(__APIURL__ + '/entities/' + selectedCustomer._id, updateEntity, { headers: th.headers })
                    .then(function (response) {
                        var updatedAllContacts = th.state.contacts;
                        updatedAllContacts.push(contactResponse.data.obj);
                        th
                            .state
                            .selectedEntity
                            .contacts
                            .push(contactResponse.data.obj._id);
                        th.setState({ contacts: updatedAllContacts, addingNewContact: false });
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({ users: result.data });
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/interventionMeans/")
            .then(function (result) {
                th.setState({ interventionMeans: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/interventionTypes/")
            .then(function (result) {
                th.setState({ interventionTypes: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contractTypes/")
            .then(function (result) {
                th.setState({ contractTypes: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projectTypes/")
            .then(function (result) {
                th.setState({ projectTypes: result.data });
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + this.props.params.idEntity)
            .then(function (result) {
                var asyncCounter = 0;
                var selectedEntity = result.data;
                var projectsEntity = [];
                var contractsEntity = [];
                var contractToCharge = null;
                var projectToCharge = null;
                console.log(JSON.stringify(selectedEntity));
                //   console.log(this.props.params.idEntity);
                if (selectedEntity.projects.length != 0) {
                    th.serverRequest = axios
                        .get(__APIURL__ + "/projects/")
                        .then(function (result2) {

                            selectedEntity
                                .projects
                                .forEach(projectID => {
                                    result2
                                        .data
                                        .forEach(project => {

                                            if (project._id == projectID) {
                                                projectsEntity.push(project);
                                            }
                                            if (projectID == th.props.params.idContractProject) {
                                                projectToCharge = project;
                                            }

                                        });
                                });

                            asyncCounter++;
                            if (asyncCounter == 2) {
                                setStateNow()
                            }
                        })
                } else {
                    projectsEntity = null;
                    asyncCounter++;
                    if (asyncCounter == 2) {
                        setStateNow()
                    }
                }
                if (selectedEntity.contracts.length != 0) {
                    th.serverRequest = axios
                        .get(__APIURL__ + "/contracts/")
                        .then(function (result2) {
                            selectedEntity
                                .contracts
                                .forEach(contractID => {
                                    result2
                                        .data
                                        .forEach(contract => {

                                            if (contract._id == contractID) {
                                                contractsEntity.push(contract);
                                            }
                                            // console.log(contractID + "  "+ this.props.params.idContractProject)
                                            if (contract._id == th.props.params.idContractProject) {
                                                contractToCharge = contract;

                                            }

                                        });
                                });
                            asyncCounter++;
                            if (asyncCounter == 2) {
                                setStateNow()
                            }
                        })
                } else {
                    contractsEntity = null;
                    asyncCounter++;
                    if (asyncCounter == 2) {
                        setStateNow()
                    }
                }

                function setStateNow() {
                    if (th.props.params.idContractProject) {
                        th.setState({
                            selectedEntity: selectedEntity,
                            projects: projectsEntity,
                            contracts: contractsEntity,
                            contractToCharge: contractToCharge,
                            projectToCharge: projectToCharge,
                            toBeValidated: false
                        });
                    } else {
                        th.setState({ selectedEntity: selectedEntity, projects: projectsEntity, contracts: contractsEntity, contractToCharge: contractToCharge, projectToCharge: projectToCharge });
                    }

                }

            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/")
            .then(function (result) {
                th.setState({ entities: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processes")
            .then(function (result) {
                var processesOfEntity = [];
                var selectedProcess = "";
                var requestedBy = "";

                result
                    .data
                    .forEach(process => {
                        if (process.entity == th.props.params.idEntity) {
                            processesOfEntity.push(process);
                        }
                        if (process._id == th.props.params.idProcess) {

                            selectedProcess = process;
                            requestedBy = selectedProcess.requestedBy;

                        }
                    })
                th.setState({ processes: processesOfEntity, selectedProcess: selectedProcess, requestedBy: requestedBy })
                //   th.setState({processes: processesOfEntity, selectedProcess:
                // selectedProcess, requestedBy: selectedProcess.requestedBy});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processStates")
            .then(function (result) {
                th.setState({ processesStates: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contractStates")
            .then(function (result) {
                th.setState({ contractStates: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projectStates")
            .then(function (result) {
                th.setState({ projectStates: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contacts")
            .then(function (result) {
                th.setState({ contacts: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processTypes")
            .then(function (result) {
                th.setState({ processesTypes: result.data });
            })

        /*this.serverRequest = axios
            .get(__APIURL__ + "/processes/" + this.props.params.idProcess)
            .then(function (result1) {

                if (result1.data.contract == null && result1.data.project != null) {

                    th.serverRequest = axios
                        .get(__APIURL__ + "/projects/" + result1.data.project)
                        .then(function (result2) {
                            th.setState({selectedProcess: result1.data, selectedProject: result2.data});
                        })
                } else {
                    if (result1.data.contract != null && result1.data.project == null) {
                        th.serverRequest = axios
                            .get(__APIURL__ + "/contracts/" + result1.data.contract)
                            .then(function (result2) {
                                th.setState({selectedProcess: result1.data, selectedContract: result2.data});
                            })
                    }
                }
            })*/

    }

    componentDidMount() {

        var th = this;
        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider)
            $('[data-ui-slider]').bootstrapSlider();

        // CHOSEN -----------------------------------

        if ($.fn.chosen)
            $('.chosen-select').chosen();

        // MASKED -----------------------------------

        if ($.fn.inputmask)
            $('[data-masked]').inputmask();

        // FILESTYLE -----------------------------------

        if ($.fn.filestyle)
            $('.filestyle').filestyle();

        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg)
            $('.wysiwyg').wysiwyg();

        // Tags -----------------------------------
        if ($.fn.tagsinput)
            $("[data-role='tagsinput']").tagsinput()

        // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepicker').datetimepicker({
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                },
                format: 'DD-MM-YYYY'
            })
                .on('dp.change', function () {
                    let dateSelected = $('#datetimepicker')
                        .data("DateTimePicker")
                        .date();
                    th.setState({
                        date: dateSelected.format("DD-MM-YYYY")
                    });

                });
            // only time
            $('#datetimepickertime1')
                .datetimepicker({ format: 'HH:mm' })
                .on("dp.change", function (e) {
                    let dateSelected = $('#datetimepickertime1')
                        .data("DateTimePicker")
                        .date();
                    var begin = dateSelected.format("HH:mm"); //10:00 ex
                    var millisec = moment(th.state.endHour, 'HH:mm') - moment(begin, 'HH:mm');
                    if (th.state.selectedInterventionType != "") {
                        self
                            .state
                            .interventionTypes
                            .forEach(intType => {
                                if (intType._id == th.state.selectedInterventionType) {

                                    var newTimeToCharge = self.calcNextTime(moment(millisec).format("HH:mm"), moment(intType.minimumTime).format("HH:mm"), moment(intType.nextsTimes).format("HH:mm"));

                                    self.setState({
                                        beginHour: begin,
                                        totalTime: moment(millisec).format("HH:mm"),
                                        timeToCharge: newTimeToCharge
                                    });
                                }
                            });
                    } else {
                        self.setState({
                            beginHour: begin,
                            totalTime: moment(millisec).format("HH:mm")
                        });
                        console.log("Hora Inicial: " + th.state.beginHour);
                    }
                });
            $('#datetimepickertime2')
                .datetimepicker({ format: 'HH:mm' })
                .on("dp.change", function (e) {
                    let dateSelected = $('#datetimepickertime2')
                        .data("DateTimePicker")
                        .date();
                    var end = dateSelected.format("HH:mm"); //10:00 ex
                    var millisec = moment(end, 'HH:mm') - moment(th.state.beginHour, 'HH:mm');
                    if (th.state.selectedInterventionType != "") {
                        self
                            .state
                            .interventionTypes
                            .forEach(intType => {
                                if (intType._id == th.state.selectedInterventionType) {

                                    var newTimeToCharge = self.calcNextTime(moment(millisec).format("HH:mm"), moment(intType.minimumTime).format("HH:mm"), moment(intType.nextsTimes).format("HH:mm"));
                                    console.log(newTimeToCharge);
                                    self.setState({
                                        endHour: end,
                                        totalTime: moment(millisec).format("HH:mm"),
                                        timeToCharge: newTimeToCharge
                                    });
                                }
                            });
                    } else {
                        self.setState({
                            endHour: end,
                            totalTime: moment(millisec).format("HH:mm")
                        });
                        console.log("Hora Final: " + th.state.endHour);
                    }

                });

            $('#datetimepickertime4').datetimepicker({
                date: moment("00:00", "HH:mm"),
                format: 'HH:mm'
            })
                .on("dp.change", function (e) {
                    let dateSelected = $('#datetimepickertime4')
                        .data("DateTimePicker")
                        .date();
                    th.setState({
                        timeToCharge: moment(dateSelected, "HH:mm").format("HH:mm")
                    });
                    /*if (moment($('#datetimepickertime4').data("DateTimePicker").date()).format("HH:mm") != th.state.timeToCharge) {
                        if (th.state.selectedInterventionType != "") {
                            th
                                .state
                                .interventionTypes
                                .forEach(intType => {
                                    if (intType._id == th.state.selectedInterventionType) {
 
                                        if (moment(dateSelected).diff(moment(moment(intType.minimumTime).format("HH:mm"), "HH:mm")) < 0) {
                                            $('#datetimepickertime4').datetimepicker('date', moment(intType.minimumTime));
                                            th.setState({
                                                timeToCharge: moment(intType.minimumTime).format("HH:mm")
                                            });
                                        } else {
 
                                            console.log(dateSelected.diff(moment(th.state.timeToCharge, "HH:mm")));
                                            if (dateSelected.diff(moment(th.state.timeToCharge, "HH:mm")) > 0) {
                                                var nextTime = th.calcSumBetweenA_B(th.state.timeToCharge, moment(intType.nextsTimes).format("HH:mm"));
                                                $('#datetimepickertime4').datetimepicker('date', moment(nextTime, "HH:mm"));
                                                th.setState({timeToCharge: nextTime});
                                            } else {
                                                var previousTime = th.calcDifBetweenA_B(th.state.timeToCharge, moment(intType.nextsTimes).format("HH:mm"));
                                                $('#datetimepickertime4').datetimepicker('date', moment(previousTime, "HH:mm"));
                                                th.setState({timeToCharge: previousTime});
                                            }
 
                                        }
                                    }
                                });
                        } else {
                            th.setState({
                                timeToCharge: dateSelected.format("HH:mm")
                            });
                        }
 
                        console.log("Tempo a Cobrar: " + th.state.timeToCharge);
                    }*/

                });

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-processesStates')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        selectedProcessState: $(this).val()
                    });
                    console.log(self.state.selectedProcessState);
                });
            $('#select2-1-means').select2({ theme: 'bootstrap' });
            $('#select2-1-collaborator').select2({ theme: 'bootstrap' });

            $('#select2-1-customer')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    $('#projectOrContractList').prop('selected', 'true');

                    var cust;
                    var contracts = [];
                    var projects = [];
                    var processes = [];
                    self
                        .state
                        .entities
                        .forEach(customer => {
                            if (customer._id == $(this).val()) {

                                contracts = customer.contracts;
                                projects = customer.projects;
                                cust = customer;
                                if (self.state.processes.message != "NO_RESULTS") {
                                    customer
                                        .processes
                                        .forEach(processID => {
                                            console.log(self.state.processes);
                                            self
                                                .state
                                                .processes
                                                .forEach(process => {
                                                    if (process._id == processID) {
                                                        processes.push(process);
                                                    }
                                                });
                                        });
                                }

                            }
                        });

                    self.setState({ selectedEntity: cust, entityContracts: contracts, entityProjects: projects, entityProcesses: processes });

                    /*$('#select2-1-contractOrProject').off(); //impede que se criem multiplos onchange
                    $('#select2-1-contractOrProject') //isto tem de aqui estar senao o bootstrap faz override da nova informação e nao a mostra
                        .select2({theme: 'bootstrap'})
                        .on('change', function () {
                            $('#processesList').prop('selected', 'true');
 
                            var processosDoProjContr = [];
                            var contrCharge = null;
                            var projCharge = null;
 
                            if (self.state.contracts.message != "NO_RESULTS") {
                                self
                                    .state
                                    .contracts
                                    .forEach(contract => {
                                        if (contract._id == $(this).val()) {
                                            contrCharge = contract;
 
                                            if (self.state.processes.message != "NO_RESULTS") {
                                                self
                                                    .state
                                                    .processes
                                                    .forEach(process => {
                                                        self
                                                            .state
                                                            .entityProcesses
                                                            .forEach(entityProcessID => {
                                                                if (process._id == entityProcessID) {
                                                                    if (process.contract == $(this).val()) {
                                                                        processosDoProjContr.push(process);
                                                                    }
                                                                }
                                                            });
                                                    });
                                            }
                                            //Will now update the processes of this contract
 
                                        }
                                    });
                            }
 
                            if (self.state.projects.message != "NO_RESULTS") {
                                self
                                    .state
                                    .projects
                                    .forEach(project => {
                                        if (project._id == $(this).val()) {
                                            projCharge = project;
 
                                            //Will now update the processes of this contract
                                            if (self.state.processes.message != "NO_RESULTS") {
                                                self
                                                    .state
                                                    .processes
                                                    .forEach(process => {
                                                        self
                                                            .state
                                                            .entityProcesses
                                                            .forEach(entityProcessID => {
                                                                if (process._id == entityProcessID) {
                                                                    if (process.project == $(this).val()) {
                                                                        processosDoProjContr.push(process);
                                                                    }
                                                                }
                                                            });
                                                    });
                                            }
 
                                        }
                                    });
                            }
 
                            self.setState({contractToCharge: contrCharge, projectToCharge: projCharge, processesOfContractsOrProjects: processosDoProjContr, selectedProcess: ""});
 
                            $('#select2-1-process').off();
                            $('#select2-1-process')
                                .select2({theme: 'bootstrap'})
                                .on('change', function () {
                                    self
                                        .state
                                        .processes
                                        .forEach(process => {
                                            if (process._id == $(this).val()) {
                                                self.setState({selectedProcess: process, requestedBy: process.requestedBy, notifyThisInterv: process.notify});
                                            }
                                        });
 
                                });
 
                        });*/

                });

            $('#select2-1-contractOrProject') //isto tem de aqui estar senao o bootstrap faz override da nova informação e nao a mostra
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    $('#processesList').prop('selected', 'true');
                    if ($(this).val() == "") {
                        self.setState({ contractToCharge: null, projectToCharge: null, toBeValidated: true });
                    } else {

                        var contrCharge = null;
                        var projCharge = null;

                        if (self.state.contracts != null) {
                            self
                                .state
                                .contracts
                                .forEach(contract => {
                                    if (contract._id == $(this).val()) {
                                        contrCharge = contract;

                                    }
                                });
                        }

                        if (self.state.projects != null) {
                            self
                                .state
                                .projects
                                .forEach(project => {
                                    if (project._id == $(this).val()) {
                                        projCharge = project;

                                    }
                                });
                        }
                        console.log(contrCharge, projCharge)
                        self.setState({ contractToCharge: contrCharge, projectToCharge: projCharge, toBeValidated: false });
                    }

                });

            $('#select2-1-process')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {

                    if ($(this).val() == "") {
                        self.setState({ selectedProcess: "", requestedBy: "", notifyThisInterv: [] });
                    } else {
                        self
                            .state
                            .processes
                            .forEach(process => {

                                if (process._id == $(this).val()) {
                                    self.setState({ selectedProcess: process, requestedBy: process.requestedBy, notifyThisInterv: process.notify });

                                }
                            });
                    }

                });

            $('#select2-1-interventionType')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {

                    self
                        .state
                        .interventionTypes
                        .forEach(intType => {
                            if (intType._id == $(this).val()) {

                                if (moment(self.state.totalTime, "HH:mm").diff(moment(moment(intType.minimumTime).format("HH:mm"), "HH:mm")) < 0) {

                                    self.setState({
                                        selectedInterventionType: $(this).val(),
                                        timeToCharge: moment(intType.minimumTime).format("HH:mm")
                                    });
                                    $('#datetimepickertime4').datetimepicker('date', moment(intType.minimumTime));
                                } else {
                                    var charge = self.calcNextTime(self.state.totalTime, moment(intType.minimumTime).format("HH:mm"), moment(intType.nextsTimes).format("HH:mm"));
                                    self.setState({
                                        selectedInterventionType: $(this).val(),
                                        timeToCharge: charge
                                    });
                                    $('#datetimepickertime4').datetimepicker('date', charge);
                                }

                            }
                        });

                });

            $('#select2-1-means').on('change', function () {
                self.setState({
                    selectedInterventionMean: $(this).val()
                });
            });
            $('#select2-1-collaborator').on('change', function () {
                self.setState({
                    collaborator: $(this).val()
                });
            });
            $('#select2-1-requestedBy')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        requestedBy: $(this).val()
                    });
                    console.log("Pedido por: " + self.state.requestedBy);
                })
            $('#select2-1-notify')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        notifyThisInterv: $(this).val()
                    });
                    console.log("Notify: " + $(this).val());
                });

            $('#select2-1-notifyProcess')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        notifyThisInterv: $(this).val()
                    });
                    console.log("Notify: " + $(this).val());
                });

            $('#select2-1-processType')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    th.setState({
                        processType: $(this).val()
                    });
                    console.log(th.state.processType);
                });
            $('#select2-1-processes')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    th
                        .state
                        .processes
                        .forEach(proc => {
                            if (proc._id == $(this).val()) {
                                console.log(JSON.stringify(proc));
                                th.setState({ selectedProcess: proc, requestedBy: proc.requestedBy, notifyThisInterv: proc.notify });
                            }
                        })

                });

        }

    }

    render() {
        console.log(this.state);
        var th = this;
        var interventionTypesList = [];
        var interventionMeansList = [];
        var collaboratorList = [];
        var projectOrContractList = [];
        var contactsList = [];
        var contactsListForNotification = [];
        var projectsGroup = [];
        var contractsGroup = [];
        var entitiesList = [];
        var processesList = [];
        var processesStatesList = [];
        var processesTypesList = [];
        var isContractProjectSelected = false;
        var isCustomerSelected = false;
        var isProcessSelected = false;

        if (this.state.contacts[0] != undefined)
            this.state.contacts.forEach((item, i) => {
                if (this.state.selectedProcess == "") { //se não existir processo selecionado
                    if (i == 0) {
                        contactsList.push(
                            <option disabled selected value key={-1}>
                                -- Escolha uma opção --
                            </option>
                        );
                    }

                    if (this.state.selectedEntity != "") {
                        //se existir um cliente seleccionado
                        this
                            .state
                            .selectedEntity
                            .contacts
                            .forEach(customerContactID => {
                                if (customerContactID == item._id) {
                                    contactsList.push(
                                        <option key={i.toString()} value={item._id}>{item.name}</option>
                                    );
                                    contactsListForNotification.push(
                                        <option key={i.toString()} value={customerContactID}>{item.name}</option>
                                    );
                                }
                            });
                    }

                } else { //se existir processo selecionado
                    if (this.state.selectedProcess.requestedBy == item._id) { //com isto, ele so mete uma vez

                        contactsList.push(
                            <option key={i.toString()} selected value={item._id}>{item.name}</option>
                        );

                    }

                    if (this.state.selectedEntity != "") {
                        this
                            .state
                            .selectedEntity
                            .contacts
                            .forEach(contactCustomer => {
                                if (contactCustomer == item._id) {
                                    var contactIsNotified = false;

                                    this
                                        .state
                                        .selectedProcess
                                        .notify
                                        .forEach((notifyID, j) => {
                                            if (notifyID == contactCustomer) {
                                                contactIsNotified = true;
                                                contactsListForNotification.push(
                                                    <option
                                                        key={j.toString() + i.toString()}
                                                        selected="true"
                                                        value={contactCustomer}>{item.name}</option>
                                                );
                                            }
                                        });
                                    if (!contactIsNotified) {
                                        contactsListForNotification.push(
                                            <option key={i.toString()} value={contactCustomer}>{item.name}</option>
                                        );
                                    }
                                }
                            });
                    }

                }

            })

        if (this.state.processesStates[0] != undefined)
            this.state.processesStates.forEach((item, i) => {
                if (i == 0) {
                    processesStatesList.push(
                        <option disabled selected value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                processesStatesList.push(
                    <option key={i.toString()} value={item._id}>{item.stateName}</option>
                );
            })
        if (this.state.processesTypes[0] != undefined)
            this.state.processesTypes.forEach((item, i) => {
                if (i == 0) {
                    processesTypesList.push(
                        <option disabled selected value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }

                processesTypesList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })

        if (this.state.interventionTypes[0] != undefined)
            this.state.interventionTypes.forEach((item, i) => {
                if (i == 0) {
                    interventionTypesList.push(
                        <option disabled selected value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                interventionTypesList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })
        if (this.state.interventionMeans[0] != undefined)
            this.state.interventionMeans.forEach((item, i) => {
                if (i == 0) {
                    interventionMeansList.push(
                        <option disabled selected value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                interventionMeansList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })
        if (this.state.users[0] != undefined)
            this.state.users.forEach((item, i) => {
                if (i == 0) {
                    collaboratorList.push(
                        <option disabled selected value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                if (item.internalUser) {
                    collaboratorList.push(
                        <option key={i.toString()} value={item._id}>{item.name}</option>
                    );
                }

            })

        if (this.state.entities[0] != undefined)
            this.state.entities.forEach((item, i) => {

                if (i == 0) {
                    entitiesList.push(

                        <option disabled selected value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );

                }
                entitiesList.push(
                    <option
                        key={item
                            ._id
                            .toString()}
                        value={item._id}>{item.taxName}</option>
                );
            })

        processesList.push(
            <option id="processesList" disabled selected value key={-1}>
                -- Escolha uma opção --
            </option>
        );

        // if (this.state.processesOfContractsOrProjects.length != 0) {     for (var i =
        // 0; i < th.state.processesOfContractsOrProjects.length; i++) {         th
        // .state             .processesStates             .forEach(processState => { if
        // (processState._id == th.state.processesOfContractsOrProjects[i].state) { if
        // (!processState.endProcesses) {   processesList.push( <option
        // key={i.toString()}
        // value={th.state.processesOfContractsOrProjects[i]._id}>{th.state.processesOfC
        // o ntractsOrProjects[i].processNumber}</option>                         ); }
        //     }             });     } }
        if (this.state.processes.length != 0) {
            for (var i = 0; i < th.state.processes.length; i++) {
                th
                    .state
                    .processesStates
                    .forEach(processState => {
                        if (processState._id == th.state.processes[i].state) {
                            if (!processState.endProcesses) {
                                processesList.push(
                                    <option key={i.toString()} value={th.state.processes[i]._id}>{th.state.processes[i].processNumber}</option>
                                );
                            }
                        }
                    });

            }
        }

        projectOrContractList.push(
            <option id="projectOrContractList" selected value="" key={-1}>
                -- Escolha uma opção --
            </option>
        );

        if (this.state.contracts != null) {
            this
                .state
                .contracts
                .forEach((item, i) => {
                    this
                        .state
                        .contractTypes
                        .forEach(contractTp => {
                            if (contractTp._id == item.contractType) {
                                this
                                    .state
                                    .contractStates
                                    .forEach(ctState => {
                                        if (ctState._id == item.state && !ctState.close) {
                                            contractsGroup.push(
                                                <option key={i.toString()} value={item._id}>{contractTp.description + "  |  " + moment(item.beginDate).format("DD-MM-YYYY") + "  |  " + moment(item.endDate).format("DD-MM-YYYY") + "  |  " + item.availableHours + " (H. Disponiveis)"}</option>
                                            );
                                        }
                                        if (this.props.params.idContractProject == item._id) {
                                            $('#select2-select2-1-contractOrProject-container').text(contractTp.description + "  |  " + moment(item.beginDate).format("DD-MM-YYYY") + "  |  " + moment(item.endDate).format("DD-MM-YYYY") + "  |  " + item.availableHours + " (H. Disponiveis)");
                                            console.log(this.state.contractToCharge);
                                        }
                                    })

                            }
                        })
                })
        }

        if (this.state.projects != null) {

            this
                .state
                .projects
                .forEach((item, i) => {

                    this
                        .state
                        .projectTypes
                        .forEach(projectTp => {
                            if (projectTp._id == item.projectType) {
                                this
                                    .state
                                    .projectStates
                                    .forEach(prState => {
                                        if (prState._id == item.state && !prState.close) {
                                            projectsGroup.push(
                                                <option key={i.toString()} value={item._id}>{projectTp.description + "  |  " + moment(item.beginDate).format("DD-MM-YYYY") + "  |  " + (item.endDate
                                                    ? moment(item.endDate).format("DD-MM-YYYY")
                                                    : "Sem data") + "  |  " + this.calcDifBetweenA_B(item.hours, item.exhaustedHours) + " (H. Disponiveis)"}</option>
                                            );
                                        }

                                        if (this.props.params.idContractProject == item._id) {
                                            $('#select2-select2-1-contractOrProject-container').text(projectTp.description + "  |  " + moment(item.beginDate).format("DD-MM-YYYY") + "  |  " + (item.endDate
                                                ? moment(item.endDate).format("DD-MM-YYYY")
                                                : "Sem data") + "  |  " + this.calcDifBetweenA_B(item.hours, item.exhaustedHours) + " (H. Disponiveis)");
                                            console.log(this.state.projectToCharge);
                                        }
                                    })

                            }
                        })
                })
        }

        projectOrContractList.push(
            <optgroup label="Projetos">
                {projectsGroup}
            </optgroup>
        );
        projectOrContractList.push(
            <optgroup label="Contratos">
                {contractsGroup}
            </optgroup>
        );

        if (this.state.selectedEntity === "") {
            isCustomerSelected = true;
            isProcessSelected = true;
        }
        if (this.state.selectedProcess != "") {
            isProcessSelected = true;
        }

        if (this.state.processesOfContractsOrProjects.length == 0) {
            isContractProjectSelected = true;
        } else {
            isContractProjectSelected = false;
        }
        var opacityVar;
        if (this.state.addingNewProcess) {
            opacityVar = 0.5;
            if (document.getElementById('newProcessPopup') != null) {
                document
                    .getElementById('newProcessPopup')
                    .style
                    .visibility = "visible";
            }

        } else {
            opacityVar = 1;
            if (document.getElementById('newProcessPopup') != null) {
                document
                    .getElementById('newProcessPopup')
                    .style
                    .visibility = "hidden";
            }
        }
        if (!this.state.addingNewProcess) {
            if (this.state.addingNewContact) {
                opacityVar = 0.5;
                if (document.getElementById('newContactPopup') != null) {
                    document
                        .getElementById('newContactPopup')
                        .style
                        .visibility = "visible";
                }

            } else {
                opacityVar = 1;
                if (document.getElementById('newContactPopup') != null) {
                    document
                        .getElementById('newContactPopup')
                        .style
                        .visibility = "hidden";
                }

            }
        }
        if (this.props.params.idContractProject) {
            var disableContractOrProject = true;
        }
        if (this.props.params.idProcess) {

            $('#select2-select2-1-processes-container').text(this.state.selectedProcess.processNumber);
        }
        console.log("Process: " + this.state.selectedProcess);
        return (
            <ContentWrapper>
                <h3>Intervenções
                </h3>

                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}

                        <div
                            className="panel panel-default"
                            style={{
                                opacity: opacityVar
                            }}>

                            <div className="panel-heading">
                                <div className="panel-title">Adicionar uma nova intervenção ao processo {this.state.selectedProcess.processNumber}</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-4">
                                            <label className="control-label">Cliente</label>
                                            <FormControl
                                                type="text"
                                                name="hours"
                                                placeholder="0.0"
                                                required="required"
                                                disabled
                                                value={this.state.selectedEntity.taxName}
                                                className="form-control" />
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Processo</label>
                                            {/* <FormControl
                                                name="hours"
                                                disabled={this.props.params.idContractProject
                                                ? false
                                                : true}
                                                componentClass="select"
                                                placeholder="0.0"
                                                required="required"
                                                value={this.state.selectedProcess.processNumber}
                                                className="form-control">
                                                {processesList}
                                                </FormControl> */}
                                            <FormControl
                                                id="select2-1-processes"
                                                disabled={this.props.params.idContractProject
                                                    ? false
                                                    : true}
                                                componentClass="select"
                                                name="selectedProcess"
                                                className="form-control m-b"
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'selectedProcess')}>
                                                {processesList}

                                            </FormControl>

                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Cobrar a</label>

                                            <FormControl
                                                id="select2-1-contractOrProject"
                                                disabled={disableContractOrProject
                                                    ? disableContractOrProject
                                                    : isCustomerSelected}
                                                componentClass="select"
                                                name="toCharge"
                                                className="form-control m-b"
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'toCharge')}>
                                                {projectOrContractList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de intervenção*</label>
                                            <FormControl
                                                id="select2-1-interventionType"
                                                componentClass="select"
                                                name="selectedInterventionType"
                                                required="required"
                                                className="form-control m-b"
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'selectedProjectType')}>
                                                {interventionTypesList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Meio de intervenção</label>
                                            <FormControl
                                                id="select2-1-means"
                                                componentClass="select"
                                                name="selectedInterventionMean"
                                                className="form-control m-b"
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'selectedInterventionMean')}>
                                                {interventionMeansList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Técnico*</label>
                                            <FormControl
                                                id="select2-1-collaborator"
                                                componentClass="select"
                                                name="collaborator"
                                                required="required"
                                                className="form-control m-b"
                                                value={this.state.collaborator}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'collaborator')}>
                                                {collaboratorList}

                                            </FormControl>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Data*</label>
                                            <div id="datetimepicker" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.date}
                                                    onClick={this
                                                        .handleChange
                                                        .bind(this, 'date')} />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-3">
                                            <label className="control-label">Hora de início*</label>
                                            <div id="datetimepickertime1" className="input-group date">
                                                <input
                                                    name="beginHour"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.beginHour}
                                                    onClick={this
                                                        .handleChange
                                                        .bind(this, 'beginHour')} />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Hora de fim*</label>
                                            <div id="datetimepickertime2" className="input-group date">
                                                <input
                                                    name="endHour"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.endHour}
                                                    onClick={this
                                                        .handleChange
                                                        .bind(this, 'endHour')} />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Tempo total da Intervenção</label>
                                            <FormControl
                                                id="testeDeAlteracao"
                                                disabled
                                                type="text"
                                                name="totalInterventionTime"
                                                value={this.state.totalTime}
                                                required="required"
                                                className="form-control" />
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Tempo a cobrar*</label>
                                            <div id="datetimepickertime4" className="input-group date">
                                                <input
                                                    name="timeToCharge"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.timeToCharge} />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-5">
                                            <label className="control-label">Pedido por*</label>
                                            <FormControl
                                                id="select2-1-requestedBy"
                                                componentClass="select"
                                                disabled={isProcessSelected}
                                                required="required"
                                                name="collaborator"
                                                className="form-control m-b"
                                                value={this.state.requestedBy}>
                                                {contactsList}

                                            </FormControl>
                                        </div>
                                        <div
                                            className="col-sm-1"
                                            style={{
                                                paddingRight: "5%"
                                            }}>
                                            <button
                                                onClick={() => {
                                                    this.setState({ addingNewContact: true })
                                                }}
                                                type="button"
                                                id="addBtn"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                                <i className="icon-plus"></i>
                                            </button>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Notificar</label>
                                            <FormControl
                                                disabled={isCustomerSelected}
                                                id="select2-1-notify"
                                                multiple="multiple"
                                                componentClass="select"
                                                name="selectedContact"
                                                className="form-control m-b"
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'selectedContact')}>
                                                {contactsListForNotification}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-12">
                                            <label className="control-label">Relatório*</label>
                                            <textarea
                                                name="report"
                                                className="form-control"
                                                required="required"
                                                rows="2"
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, "report")} />

                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-3">
                                            <label className="control-label">Estado do Processo*</label>
                                            <FormControl
                                                id="select2-1-processesStates"
                                                componentClass="select"
                                                required="required"
                                                name="selectedPr"
                                                className="form-control m-b">
                                                {processesStatesList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-3">
                                            <label className="control-label">Validar mais tarde?</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        name="toBeValidated"
                                                        checked={this.state.toBeValidated}
                                                        disabled={this.state.contractToCharge == null && this.state.projectToCharge == null
                                                            ? true
                                                            : false}
                                                        onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'toBeValidated')} />
                                                    <em className="fa fa-check"></em>
                                                </label>
                                            </div>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Oferta</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        name="offer"
                                                        checked={this.state.offer}
                                                        onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'offer')} />
                                                    <em className="fa fa-check"></em>

                                                </label>
                                            </div>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Reclamação</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        name="complaint"
                                                        checked={this.state.complaint}
                                                        onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'complaint')} />
                                                    <em className="fa fa-check"></em>

                                                </label>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/processes/view/' + this.props.params.idEntity + '/' + this.props.params.idProcess)}
                                            type="button"
                                            className="btn ">Voltar ao processo</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
                {/*Popup de adição de processo*/}
                <Col
                    style={{
                        height: "100%",
                        width: "100%",
                        visibility: "hidden"
                    }}
                    id="newProcessPopup">

                    <form
                        onSubmit={this.handleSubmitProcess}
                        data-parsley-validate=""
                        noValidate
                        style={{
                            width: "70%"
                        }}>
                        {/* START panel */}
                        <div
                            className="panel panel-default"
                            style={{
                                position: "absolute",
                                top: "0",
                                width: "100%",
                                height: "100%",
                                border: 0,
                                justifyContent: "center",
                                alignItems: "center",
                                display: 'flex',
                                backgroundColor: "transparent"
                            }}>

                            <div
                                className="panel panel-default"
                                style={{
                                    width: '50%',
                                    height: "45%"
                                }}>
                                <div className="panel-heading">
                                    <div className="panel-title">Novo processo</div>
                                </div>
                                <div className="panel-body">

                                    <div className="form-group">
                                        <Row>
                                            <div className="col-sm-12">
                                                <label className="control-label">Tipo de Processo</label>
                                                <FormControl
                                                    id="select2-1-processType"
                                                    componentClass="select"
                                                    name="processType"
                                                    className="form-control m-b"
                                                    value={this.state.processType}>
                                                    {processesTypesList}

                                                </FormControl>
                                            </div>
                                        </Row>
                                    </div>
                                    <div className="form-group">
                                        <Row>
                                            <div className="col-sm-12">
                                                <label className="control-label">Descrição</label>
                                                <textarea
                                                    name="descriptionProcess"
                                                    className="form-control"
                                                    required="required"
                                                    rows="5"
                                                    onChange={this
                                                        .handleChange
                                                        .bind(this, "descriptionProcess")} />

                                            </div>
                                        </Row>
                                    </div>

                                </div>
                                <div className="form-group">
                                    <Row>
                                        <div
                                            className="col-sm-12"
                                            style={{
                                                textAlign: "center"
                                            }}>
                                            <button
                                                onClick={() => {
                                                    this.setState({ addingNewProcess: false, interventionToAssignToProcess: null })
                                                }}
                                                type="button"
                                                className="btn ">Voltar</button>
                                            <button
                                                type="submit"
                                                className="btn btn-primary"
                                                style={{
                                                    marginLeft: "10px"
                                                }}>Guardar</button>
                                        </div>
                                    </Row>
                                </div>

                            </div>

                        </div>
                    </form>
                </Col>
                {/*Popup de adição de contacto*/}
                <Col
                    style={{
                        height: "100%",
                        width: "100%",
                        visibility: "hidden"
                    }}
                    id="newContactPopup">

                    <form
                        onSubmit={this.handleSubmitNewContact}
                        data-parsley-validate=""
                        noValidate
                        style={{
                            width: "70%"
                        }}>
                        {/* START panel */}
                        <div
                            className="panel panel-default"
                            style={{
                                position: "absolute",
                                top: "0",
                                width: "100%",
                                height: "100%",
                                border: 0,
                                justifyContent: "center",
                                alignItems: "center",
                                display: 'flex',
                                backgroundColor: "transparent"
                            }}>

                            <div
                                className="panel panel-default"
                                style={{
                                    width: '50%',
                                    height: "45%"
                                }}>
                                <div className="panel-heading">
                                    <div className="panel-title">Novo contacto</div>
                                </div>
                                <div className="panel-body">

                                    <div className="form-group">
                                        <Row>
                                            <div className="col-sm-12">
                                                <label className="control-label">Nome*</label>
                                                <FormControl
                                                    type="text"
                                                    name="newContactName"
                                                    required="required"
                                                    className="form-control"
                                                    onChange={this
                                                        .handleChange
                                                        .bind(this, 'newContactName')} />
                                            </div>
                                        </Row>
                                    </div>
                                    <div className="form-group">
                                        <Row>
                                            <div className="col-sm-12">
                                                <label className="control-label">E-mail*</label>
                                                <FormControl
                                                    type="text"
                                                    name="newContactEmail"
                                                    required="required"
                                                    className="form-control"
                                                    onChange={this
                                                        .handleChange
                                                        .bind(this, 'newContactEmail')} />
                                            </div>
                                        </Row>
                                    </div>
                                    <div className="form-group">
                                        <Row>
                                            <div className="col-sm-6">
                                                <label className="control-label">Telemóvel</label>
                                                <FormControl
                                                    type="text"
                                                    name="newContactPhone"
                                                    className="form-control"
                                                    onChange={this
                                                        .handleChange
                                                        .bind(this, 'newContactPhone')} />
                                            </div>
                                            <div className="col-sm-6">
                                                <label className="control-label">Telefone</label>
                                                <FormControl
                                                    type="text"
                                                    name="newContactPhone2"
                                                    className="form-control"
                                                    onChange={this
                                                        .handleChange
                                                        .bind(this, 'newContactPhone2')} />
                                            </div>
                                        </Row>
                                    </div>

                                </div>
                                <div className="form-group">
                                    <Row>
                                        <div
                                            className="col-sm-12"
                                            style={{
                                                textAlign: "center"
                                            }}>
                                            <button
                                                type="button"
                                                className="btn "
                                                onClick={() => {
                                                    this.setState({ addingNewContact: false })
                                                }}>Voltar</button>
                                            <button
                                                type="submit"
                                                className="btn btn-primary"
                                                style={{
                                                    marginLeft: "10px"
                                                }}>Guardar</button>
                                        </div>
                                    </Row>
                                </div>

                            </div>

                        </div>
                    </form>
                </Col>

            </ContentWrapper>

        );
    }
}

export default AddIntervention;
