import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../../Layout/ContentWrapper';
import { Router, Route, Link, History, withRouter } from 'react-router';
import {browserHistory} from 'react-router';


import css from '../../../../styles/tableIcons.scss'

class AddInterventionMean extends React.Component {
    constructor() {
        super();
        this.state = {
            description: ""
                };

        this.handleChange = this
            .handleChange
            .bind(this);
        
        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(e) {
        this.setState({description: e.target.value});
    }


    handleSubmit(e) {
        const {description} = this.state;
        var self = this;
        var headers = new Headers({'Content-Type': 'application/json'});
        var newInterventionMean = JSON.parse(JSON.stringify({"description": description}));

    
        e.preventDefault();
        console.log(newInterventionMean);
        console.log(description);

        this.serverRequest = axios
            .post(__APIURL__ + '/interventionMeans/', newInterventionMean, {headers: this.headers})
            .then(function (response) {
               
                             self.props.router.push('/interventionMeans');

            })
            .catch(function (error) {
                console.log(error);
            });

    }


    render() {

        return (
            <ContentWrapper>
                <h3>Meios de Intervenção
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar um novo meio de intervenção</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <label className="control-label">Descrição*</label>
                                    <FormControl
                                        type="text"
                                        name="description"
                                        required="required"
                                        className="form-control"
                                        value={this.state.description}
                                        onChange={this.handleChange}/>
                                </div>
                               
                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <button type="submit" className="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }

}

export default AddInterventionMean;
