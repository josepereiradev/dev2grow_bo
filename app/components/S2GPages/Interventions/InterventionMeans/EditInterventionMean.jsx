import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import { Router, Route, Link, History, withRouter } from 'react-router';


import css from '../../../../styles/tableIcons.scss'

class EditInterventionMean extends React.Component {

 constructor() {
        super();
        this.state = {
            description: ""
                };

        this.handleChange = this
            .handleChange
            .bind(this);
        
        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(e) {
        this.setState({description: e.target.value});
    }


    handleSubmit(e) {
                var self = this;

        const {description} = this.state;
        var headers = new Headers({'Content-Type': 'application/json'});
        var updateInterventionMean = JSON.parse(JSON.stringify({"description": description}));
        var id = this.props.params.id;

    
        e.preventDefault();
        console.log(updateInterventionMean);
        console.log(description);
        this.serverRequest = axios
            .put(__APIURL__ + '/interventionMeans/'+id, updateInterventionMean, {headers: this.headers})
            .then(function (response) {
                                self.props.router.push('/interventionMeans');

            })
            .catch(function (error) {
                console.log(error);
            });

    }

componentWillMount() {

        var th = this;
        var id = this.props.params.id;
        this.serverRequest = axios
            .get(__APIURL__ + "/interventionMeans/" + id)
            .then(function (result) {
                th.setState({description: result.data.description});
            })
      
    }

    render() {

        return (
            <ContentWrapper>
                <h3>Meios de Intervenção
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar meio de Intervenção</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <label className="control-label">Descrição*</label>
                                    <FormControl
                                        type="text"
                                        required="required"
                                        className="form-control"
                                        value={this.state.description}
                                        onChange={this.handleChange}/>
                                </div>
                               
                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <button type="submit" className="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }

}

export default EditInterventionMean;
