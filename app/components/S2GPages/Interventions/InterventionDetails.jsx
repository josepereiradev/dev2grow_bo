import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Tab,
    Tabs,
    Table,
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class InterventionDetails extends React.Component {

    constructor() {
        super();
        this.state = {
            intervention: null,
            customer: "",
            process: "",
            contractOrProject: "",
            interventionType: "",
            interventionMean: "",
            collaborator: "",
            date: "",
            beginHour: "",
            endHour: "",
            totalTime: "",
            timeToCharge: "",
            requestedBy: "",
            notify: "",
            report: "",
            processState: "",
            toBeValidated: "",
            offer: "",
            complaint: ""
            // date: new Date().toLocaleDateString(), timeToBeCharged: "", realTime: "",
            // interventionNumber: "", selectedInterventionType: "Sem tipo de intervenção
            // atribuído", selectedInterventionMean: "Sem meio de intervenção atribuído",
            // internalReport: "Sem relatório interno", externalReport: "Sem relatório
            // externo", collaborator: "Sem colaborador", beginHour: new
            // Date().toLocaleTimeString(), endHour: new Date().toLocaleTimeString(), offer:
            // false, complaint: false, validated: false, terminateProcess: false,
            // attachments: [], projectToCharge: "", contractToCharge: "", users: [],
            // processData: [], entityData: [], toCharge: "Não atribuído"
        };
    }
    handleSelect(key) {
        this.setState({key});
    }

    componentWillMount() {
        var th = this;
        var id = this.props.params.id;
        var idEntity = this.props.params.idEntity;
        var idProcess = this.props.params.idProcess;

        this.serverRequest = axios
            .get(__APIURL__ + "/interventions/" + id)
            .then(function (result) {
                var interventionFromDataBase = result.data;
                th.setState({
                    intervention: result.data,
                    date: new Date(result.data.date).toLocaleDateString(),
                    timeToCharge: result.data.timeToBeCharged,
                    totalTime: result.data.realTime,
                    interventionNumber: result.data.interventionNumber,
                    // interventionType: result.data.interventionType, means: result.data.means,
                    internalReport: result.data.internalReport,
                    externalReport: result.data.externalReport,
                    // collaborator: result.data.collaborator,
                    beginHour: new Date(result.data.beginHour).toLocaleTimeString(),
                    endHour: new Date(result.data.endHour).toLocaleTimeString(),
                    offer: result.data.offer,
                    complaint: result.data.complaint,
                    validated: result.data.validated,
                    terminateProcess: result.data.terminateProcess,
                    attachments: result.data.attachments,

                    //entity: result.data.entity, process: result.data.process,
                });

                th.serverRequest = axios
                    .get(__APIURL__ + "/interventionTypes/" + result.data.interventionType)
                    .then(function (result) {
                        th.setState({interventionType: result.data.description})
                    })
                th.serverRequest = axios
                    .get(__APIURL__ + "/interventionMeans/" + result.data.means)
                    .then(function (result) {
                        th.setState({selectedInterventionMean: result.data.description})
                    })
                th.serverRequest = axios
                    .get(__APIURL__ + "/users/" + result.data.collaborator)
                    .then(function (result) {
                        th.setState({collaborator: result.data.name})
                    })
                th.serverRequest = axios
                    .get(__APIURL__ + "/contacts/")
                    .then(function (result) {
                        var notifyNames = "";
                        var requestedByName = "";

                        result
                            .data
                            .forEach(contact => {
                                if (contact._id == interventionFromDataBase.requestedBy) {
                                    requestedByName = contact.name;
                                }
                                interventionFromDataBase
                                    .notify
                                    .forEach(not => {
                                        if (not == contact._id) {
                                            notifyNames += contact.name + " ";
                                        }
                                    })
                            })

                        th.setState({requestedBy: requestedByName, notify: notifyNames})
                    })

                if (result.data.projectToCharge != null) {

                    th.serverRequest = axios
                        .get(__APIURL__ + "/projects/" + result.data.projectToCharge)
                        .then(function (projectResult) {
                            axios
                                .get(__APIURL__ + "/projectStates/" + projectResult.data.state)
                                .then(function (result) {

                                    th.setState({
                                        toCharge: result.data.stateName + "  |  " + moment(projectResult.data.beginDate).format("DD-MM-YYYY") + "  |  " + (projectResult.data.endDate
                                            ? moment(projectResult.data.endDate).format("DD-MM-YYYY")
                                            : "Sem data")
                                    })
                                })

                        })
                }
                if (result.data.contractToCharge != null) {

                    th.serverRequest = axios
                        .get(__APIURL__ + "/contracts/" + result.data.contractToCharge)
                        .then(function (contractResult) {

                            axios
                                .get(__APIURL__ + "/contractStates/" + contractResult.data.state)
                                .then(function (result) {
                                    console.log(result.data);
                                    th.setState({
                                        toCharge: result.data.stateName + "  |  " + moment(contractResult.data.beginDate).format("DD-MM-YYYY") + "  |  " + moment(contractResult.data.endDate).format("DD-MM-YYYY")
                                    })
                                })

                        });

                }

                th.serverRequest = axios
                    .get(__APIURL__ + "/processStates/" + result.data.processState)
                    .then(function (result) {
                        // {contractTp.description + "  |  " +
                        // moment(item.beginDate).format("DD-MM-YYYY") + "  |  " +
                        // moment(item.endDate).format("DD-MM-YYYY") + "  |  " + item.availableHours + "
                        // (H. Disponiveis)"}
                        console.log(result.data.stateName);
                        th.setState({processState: result.data.stateName})
                    })

                th.serverRequest = axios
                    .get(__APIURL__ + "/processes/")
                    .then(function (result) {
                        result
                            .data
                            .forEach(process => {
                                var processinterv = process.interventions;

                                processinterv.forEach(pInt => {
                                    if (pInt == th.props.params.id) {
                                        console.log(process.processNumber);
                                        th.setState({process: process.processNumber});
                                        th.serverRequest = axios
                                            .get(__APIURL__ + "/entities/" + process.entity)
                                            .then(function (result) {
                                     
                                                th.setState({customer: result.data.abrevName});
                                            })
                                    }
                                })

                            })

                    })

            })

    }

    render() {
        // var th = this; var offerSN = "",     complaintSN = "",     validatedSN = "",
        // terminateProcessSN = ""; this.state.offer == true     ? offerSN = 'Sim'  :
        // offerSN = 'Não'; this.state.complaint == true     ? complaintSN = 'Sim'  :
        // complaintSN = 'Não'; this.state.validated == true     ? validatedSN = 'Sim' :
        // validatedSN = 'Não'; this.state.terminateProcess == true     ?
        // terminateProcessSN = 'Sim'     : terminateProcessSN = 'Não'; var
        // timeToBeChargedSTR = ""; var realTimeSTR = ""; if
        // (this.state.timeToBeCharged.length <= 2) {     timeToBeChargedSTR = this
        // .state         .timeToBeCharged         .concat(" minutos") } else {
        // timeToBeChargedSTR = this.state.timeToBeCharged } if
        // (this.state.realTime.length <= 2) {     realTimeSTR = this         .state
        // .realTime         .concat(" minutos") } else {     realTimeSTR =
        // this.state.realTime }
        console.log(this.state.intervention)
        return (
            <ContentWrapper>
                <h3>Intervenção {this.state.interventionNumber}
                </h3>

                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}

                        <div className="panel panel-default">

                            <div className="panel-heading">
                                <div className="panel-title">Intervenção</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-4">
                                            <label className="control-label">Cliente*</label>

                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                disabled={true}
                                                value={this.state.customer
                                                ? this.state.customer
                                                : ""}
                                                className="form-control"/>

                                        </div>
                                        <div className="col-sm-4">
                                            <label className="control-label">Processo*</label>

                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                disabled={true}
                                                value={this.state.process
                                                ? this.state.process
                                                : ""}
                                                className="form-control"/>
                                        </div>
                                        <div className="col-sm-4">
                                            <label className="control-label">Cobrar a</label>

                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                value={this.state.toCharge}
                                                disabled={true}
                                                className="form-control"/>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de intervenção*</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                value={this.state.interventionType
                                                ? this.state.interventionType
                                                : ""}
                                                disabled={true}
                                                className="form-control"/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Meio de intervenção</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                value={this.state.interventionMean
                                                ? this.state.interventionMean
                                                : ""}
                                                disabled={true}
                                                className="form-control"/>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Técnico*</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                value={this.state.collaborator
                                                ? this.state.collaborator
                                                : ""}
                                                disabled={true}
                                                className="form-control"/>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Data*</label>
                                            <div id="datetimepicker" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    disabled={true}
                                                    value={this.state.intervention
                                                    ? moment(this.state.intervention.date).format("DD/MM/YYYY")
                                                    : ""}
                                                    className="form-control"/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-3">
                                            <label className="control-label">Hora de início*</label>
                                            <div id="datetimepickertime1" className="input-group date">
                                                <input
                                                    name="beginHour"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.intervention
                                                    ? moment(this.state.intervention.beginHour).format("HH:mm")
                                                    : ""}
                                                    disabled={true}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Hora de fim*</label>
                                            <div id="datetimepickertime2" className="input-group date">
                                                <input
                                                    name="endHour"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.intervention
                                                    ? moment(this.state.intervention.endHour).format("HH:mm")
                                                    : ""}
                                                    disabled={true}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Tempo total da Intervenção*</label>
                                            <FormControl
                                                id="testeDeAlteracao"
                                                disabled={true}
                                                type="text"
                                                value={this.state.intervention
                                                ? moment(this.state.intervention.totalTime).format("HH:mm")
                                                : ""}
                                                name="totalInterventionTime"
                                                required="required"
                                                className="form-control"/>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Tempo a cobrar*</label>
                                            <div id="datetimepickertime4" className="input-group date">
                                                <input
                                                    name="timeToCharge"
                                                    type="text"
                                                    disabled={true}
                                                    value={this.state.intervention
                                                    ? moment(this.state.intervention.timeToCharge).format("HH:mm")
                                                    : ""}
                                                    className="form-control"/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Pedido por*</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                value={this.state.requestedBy
                                                ? this.state.requestedBy
                                                : ""}
                                                disabled={true}
                                                className="form-control"/>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Notificar</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                value={this.state.notify
                                                ? this.state.notify
                                                : ""}
                                                disabled={true}
                                                className="form-control"/>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-12">
                                            <label className="control-label">Relatório*</label>
                                            <textarea
                                                name="report"
                                                className="form-control"
                                                required="required"
                                                value={this.state.intervention
                                                ? this.state.intervention.report
                                                : ""}
                                                rows="2"
                                                disabled={true}/>

                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-3">
                                            <label className="control-label">Estado do Processo*</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                value={this.state.processState
                                                ? this.state.processState
                                                : ""}
                                                disabled={true}
                                                className="form-control"/>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Validar mais tarde?</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        name="toBeValidated"
                                                        disabled={true}
                                                        checked={this.state.intervention
                                                        ? this.state.intervention.toBeValidated
                                                        : ""}/>
                                                    <em className="fa fa-check"></em>
                                                </label>
                                            </div>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Oferta</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        disabled={true}
                                                        name="offer"
                                                        checked={this.state.intervention
                                                        ? this.state.intervention.offer
                                                        : ""}/>
                                                    <em className="fa fa-check"></em>

                                                </label>
                                            </div>
                                        </div>

                                        <div className="col-sm-3">
                                            <label className="control-label">Reclamação</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        disabled={true}
                                                        name="complaint"
                                                        checked={this.state.intervention
                                                        ? this.state.intervention.complaint
                                                        : ""}/>
                                                    <em className="fa fa-check"></em>

                                                </label>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/interventions')}
                                            type="button"
                                            className="btn ">Voltar</button>
                                    </Col>
                                   
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>

            </ContentWrapper>

        // <ContentWrapper>     <h3>Intervenção de {this.state.entityData.taxName} do
        // processo #{this.state.processData.processNumber}     </h3 >     <Col> {/*
        // START panel */}         <div className="panel panel-default"> <div
        // className="panel-heading">                 <div
        // className="panel-title">Detalhes da intervenção</div>             </div> <div
        // className="panel-body">                 <Row> <Col lg={5}> <div
        // className="form-group">                 <label
        // className="control-label">Número da
        // intervenção:&nbsp;</label>{this.state.interventionNumber}     </div> <div
        // className="form-group">               <label
        // className="control-label">Meio:&nbsp;</label>{this.state.selectedIntervention
        // M ean}                         </div>                         <div
        // className="form-group">                             <label
        // className="control-label">Tipo de
        // intervenção:&nbsp;</label>{this.state.selectedInterventionType} </div>
        // <div className="form-group">   <label
        // className="control-label">Data:&nbsp;</label>{this.state.date}    </div>
        //     <div className="form-group">     <label className="control-label">Hora de
        // início:&nbsp;</label>{this.state.beginHour}                         </div>
        //                <div className="form-group"> <label
        // className="control-label">Hora de fim:&nbsp;</label>{this.state.endHour}
        //               </div>    <div className="form-group"> <label
        // className="control-label">Tempo real:&nbsp;</label>{realTimeSTR}      </div>
        //                        <div className="form-group">                <label
        // className="control-label">Tempo a ser
        // cobrado:&nbsp;</label>{timeToBeChargedSTR} </div>             </Col>
        //      <Col lg={5}>   <div className="form-group">            <label
        // className="control-label">A cobrar:&nbsp;</label>{this.state.toCharge} </div>
        // <div className="form-group">     <label
        // className="control-label">Colaborador:&nbsp;</label>{this.state.collaborator}
        //                         </div>                         <div
        // className="form-group">                             <label
        // className="control-label">Relatório
        // interno:&nbsp;</label>{this.state.internalReport} </div>    <div
        // className="form-group">           <label className="control-label">Relatório
        // externo:&nbsp;</label>{this.state.externalReport} </div>    <div
        // className="form-group">           <label
        // className="control-label">Oferta:&nbsp;</label>{offerSN} </div> <div
        // className="form-group">           <label
        // className="control-label">Reclamação:&nbsp;</label>{complaintSN} </div>
        // <div className="form-group">    <label
        // className="control-label">Validado:&nbsp;</label>{validatedSN}     </div>
        //         <div className="form-group">      <label
        // className="control-label">Terminar
        // processo:&nbsp;</label>{terminateProcessSN}                         </div>
        //       <div className="form-group"> <label
        // className="control-label">Anexos:&nbsp;</label>{this.state.attachments}
        // </div>                     </Col>                 </Row>     <Row>     <Col
        // lg={10}> <div className="form-group">    <button onClick={() =>
        // this.props.router.push('/processes/view/' + this.props.params.idEntity + '/'
        // + this.props.params.idProcess)}         className="btn ">Voltar ao
        // processo</button>             </div>               </Col> </Row>    </div>
        //       {/* END panel */} </div>     </Col> </ContentWrapper>

        );
    }
}

export default InterventionDetails;