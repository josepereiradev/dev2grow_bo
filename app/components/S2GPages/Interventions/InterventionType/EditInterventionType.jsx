import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';

import css from '../../../../styles/tableIcons.scss'

class EditInterventionType extends React.Component {

    constructor() {
        super();
        this.state = {
            description: "",
            minimumTime: null,
            nextsTimes: null
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(e) {
        this.setState({description: e.target.value});
    }

    handleSubmit(e) {
        var self = this;

        const {description, minimumTime, nextsTimes} = this.state;
        var headers = new Headers({'Content-Type': 'application/json'});
        var updateInterventionType = JSON.parse(JSON.stringify({"description": description, "minimumTime": minimumTime, "nextsTimes": nextsTimes}));
        var id = this.props.params.id;

        e.preventDefault();

        this.serverRequest = axios
            .put(__APIURL__ + '/interventionTypes/' + id, updateInterventionType, {headers: this.headers})
            .then(function (response) {
                self
                    .props
                    .router
                    .push('/interventionTypes');

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;
        this.serverRequest = axios
            .get(__APIURL__ + "/interventionTypes/" + id)
            .then(function (result) {
                th.setState({description: result.data.description, minimumTime: result.data.minimumTime, nextsTimes: result.data.nextsTimes});
            })

    }

    render() {

        return (
            <ContentWrapper>
                <h3>Tipos de Intervenção
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar tipo de Intervenção</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-4">
                                            <label className="control-label">Descrição*</label>
                                            <FormControl
                                                type="text"
                                                name="description"
                                                required="required"
                                                className="form-control"
                                                value={this.state.description}
                                                onChange={this.handleChange}/>
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Tempo minimo*</label>
                                            <div id="datetimepickerMinimum" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    className="form-control"
                                                    required="required"
                                                    value={moment(this.state.minimumTime).format("HH:mm")}
                                                    onClick={this
                                                    .handleChange
                                                    .bind(this, 'date')}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <label className="control-label">Periodos seguintes*</label>
                                            <div id="datetimepickerNexts" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    className="form-control"
                                                    required="required"
                                                    value={moment(this.state.nextsTimes).format("HH:mm")}
                                                    onClick={this
                                                    .handleChange
                                                    .bind(this, 'date')}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={1}>

                                        <button
                                            onClick={() => this.props.router.push('/interventionTypes')}
                                            type="button"
                                            className="btn ">Voltar</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }

    componentDidMount() {
        var that = this;
        if ($.fn.datetimepicker) {

            $('#datetimepickerMinimum')
                .datetimepicker({
                    format: "HH:mm",
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerMinimum')
                        .data("DateTimePicker")
                        .date();
                    that.setState({
                        minimumTime: moment(dateSelected).format()
                    });
                    console.log("MinimumTime: " + that.state.minimumTime);
                });

            $('#datetimepickerNexts')
                .datetimepicker({
                    format: "HH:mm",
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerNexts')
                        .data("DateTimePicker")
                        .date();
                    that.setState({
                        nextsTimes: moment(dateSelected).format()
                    });
                    console.log("NextTime: " + that.state.nextsTimes);
                });

        }

    }

}

export default EditInterventionType;
