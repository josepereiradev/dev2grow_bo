import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../../styles/tableIcons.scss'
import ContentWrapper from '../../../Layout/ContentWrapper';

class InterventionTypes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
    }

    getDataFromBD() {
        var th = this;
        axios
            .get(__APIURL__ + "/interventionTypes/")
            .then(function (result) {
                th.setState({data: result.data});
            })
    }

    create() {
        this
            .props
            .router
            .push('/interventionTypes/create');
    }

    edit(id) {
        this
            .props
            .router
            .push('/interventionTypes/edit/' + id);
    }

    delete(name, id) {
        var self = this;
        swal({
            title: "Tem a certeza que quer eliminar?",
            text: "Após eliminar não poderá recuperar o registo!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            closeOnConfirm: false
        }, function () {
            axios
                .delete(__APIURL__ + "/interventionTypes/" + id)
                .then(function (response) {
                    if (response.status == 200) {
                        swal("Eliminado!", name + " foi eliminado.", "success");
                        self.getDataFromBD();
                    } else {
                        // TODO: HANDLE IT
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    // TODO: HANDLE IT
                });
        });
    }

    render() {
        const css = css;

        return (
            <ContentWrapper>
                <h3>Tipos de Intervenção
                </h3>
                <style>
                    {
                        css
                    }</style>

                <button
                    onClick={() => this.create()}
                    type="button"
                    id="addBtn"
                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                    <i className="icon-plus"></i>
                </button>
                <Row>
                    <Col lg={12}>
                        <Panel>
                            <Table responsive hover>
                                <thead>
                                    <tr>
                                        <th>Tipos de Intervenção</th>
                                        <th>Tempo minimo</th>
                                        <th>Periodos seguintes</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.data.message != "NO_RESULTS"
                                        ? this
                                            .state
                                            .data
                                            .map(data => <tr key={data._id}>
                                                <td>{data.description}</td>
                                                <td>{moment(data.minimumTime).format("HH:mm")}</td>
                                                <td>{moment(data.nextsTimes).format("HH:mm")}</td>
                                                <td>
                                                    <button
                                                        onClick={() => this.edit(data._id)}
                                                        type="button"
                                                        id="lineBtns"
                                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                        <i className="icon-pencil"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    <button
                                                        onClick={() => this.delete(data.description, data._id)}
                                                        type="button"
                                                        id="lineBtns"
                                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                        <i className="icon-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>)
                                        : <tr></tr>}

                                </tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default InterventionTypes;
