import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../../Layout/ContentWrapper';
import {Router, Route, Link, History, withRouter} from 'react-router';
import {browserHistory} from 'react-router';

import css from '../../../../styles/tableIcons.scss'

class AddInterventionType extends React.Component {
    constructor() {
        super();
        this.state = {
            description: "",
            minimumTime: Date,
            nextsTimes: Date
        };

        this.handleChange = this.handleChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    handleSubmit(e) {
        const {description, minimumTime, nextsTimes} = this.state;
        var self = this;
        var headers = new Headers({'Content-Type': 'application/json'});
        var newInterventionType = JSON.parse(JSON.stringify({"description": description, "minimumTime": minimumTime, "nextsTimes": nextsTimes}));

        e.preventDefault();

        this.serverRequest = axios
            .post(__APIURL__ + '/interventionTypes/', newInterventionType, {headers: this.headers})
            .then(function (response) {

                self
                    .props
                    .router
                    .push('/interventionTypes');

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentDidMount() {
        var that = this;
        if ($.fn.datetimepicker) {

            $('#datetimepickerMinimum')
                .datetimepicker({
                    format: "HH:mm",
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerMinimum')
                        .data("DateTimePicker")
                        .date();
                    that.setState({
                        minimumTime: moment(dateSelected).format()
                    });
                    console.log("MinimumTime: " + that.state.minimumTime);
                });

            $('#datetimepickerNexts')
                .datetimepicker({
                    format: "HH:mm",
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerNexts')
                        .data("DateTimePicker")
                        .date();
                    that.setState({
                        nextsTimes: moment(dateSelected).format()
                    });
                    console.log("NextTime: " + that.state.nextsTimes);
                });
        }
    }

    render() {

        return (
            <ContentWrapper>
                <h3>Tipos de Intervenção
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar um novo tipo de intervenção</div>
                            </div>

                            <div className="panel-body">
                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-4">
                                            <label className="control-label">Descrição*</label>
                                            <FormControl
                                                type="text"
                                                name="description"
                                                required="required"
                                                className="form-control"
                                                value={this.state.description}
                                                onChange={this.handleChange}/>
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Tempo minimo*</label>
                                            <div id="datetimepickerMinimum" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    className="form-control"
                                                    required="required"
                                                    onClick={this.handleChange.bind(this, 'date')}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <label className="control-label">Periodos seguintes*</label>
                                            <div id="datetimepickerNexts" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    className="form-control"
                                                    required="required"
                                                    onClick={this
                                                    .handleChange
                                                    .bind(this, 'date')}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={1}>

                                        <button
                                            onClick={() => this.props.router.push('/interventionTypes')}
                                            type="button"
                                            className="btn ">Voltar</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
}

export default AddInterventionType;
