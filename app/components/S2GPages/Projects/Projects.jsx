import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';

class Projects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            projectType: [],
            projectState: [],
            entities: [],
            interventions: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/projectTypes/",)
            .then(function (result) {
                th.setState({projectType: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projectStates/",)
            .then(function (result) {
                th.setState({projectState: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/",)
            .then(function (result) {
                th.setState({entities: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/interventions/",)
            .then(function (result) {
                th.setState({interventions: result.data});
            })

    }

    getDataFromBD() {
        axios
            .get(__APIURL__ + "/projects/")
            .then(res => {
                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({data})
                    setTimeout(function () {
                        var dtProjects = $('#dtProjects').dataTable({
                            'pageLength': 100,
                            'retrieve': true, 'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'responsive': true, // https://datatables.net/extensions/responsive/examples/
                            // Text translation options Note the required keywords between underscores (e.g
                            // _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            // Datatable Buttons setup
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                {
                                    extend: 'copy',
                                    className: 'btn-sm',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'csv',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'excel',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'pdf',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'print',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs.keyup(function () {
                            dtProjects.fnFilter(this.value, columnInputs.index(this));
                        });
                    }.bind(this), 500);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    view(id, entityParent) {
        this
            .props
            .router
            .push('/projects/view/' + entityParent + '/' + id);
    }

    create() {
        this
            .props
            .router
            .push('/projects/create');
    }

    edit(id, entityParent) {
        this
            .props
            .router
            .push('/projects/edit/' + entityParent + '/' + id);
    }

    delete(name, id, idEntity) {
        var entityParentData;
        var canDelete = true;

        if(!this.state.interventions.error){
            this
            .state
            .interventions
            .forEach(interv => {
                if (interv.projectToCharge == id) {
                    canDelete = false;
                }
            })
        }
        
        if (canDelete) {
                    this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + idEntity)
            .then(function (result) {
                entityParentData = result.data;

                console.log("entityParentData", entityParentData)

                var headers = new Headers({'Content-Type': 'application/json'});
                swal({
                    title: "Tem a certeza que quer eliminar?",
                    text: "Após eliminar não poderá recuperar o registo!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    closeOnConfirm: false
                }, function () {
                    axios
                        .delete(__APIURL__ + "/projects/" + id)
                        .then(function (response) {
                            if (response.status == 200) {
                                entityParentData
                                    .projects
                                    .splice(entityParentData.projects.indexOf(id), 1);
                                var updateEntity = JSON.parse(JSON.stringify({
                                    "nif": entityParentData.nif,
                                    "qrCode": entityParentData.qrCode,
                                    "taxName": entityParentData.taxName,
                                    "abrevName": entityParentData.abrevName,
                                    "accountManager": entityParentData.accountManager,
                                    "contacts": entityParentData.contacts,
                                    "projects": entityParentData.projects,
                                    "processes": entityParentData.processes,
                                    "contracts": entityParentData.contracts,
                                    "associatedUsers": entityParentData.associatedUsers
                                }));

                                self.serverRequest = axios
                                    .put(__APIURL__ + '/entities/' + idEntity, updateEntity, {headers: self.headers})
                                    .then(function (response2) {})
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                console.log("elemento removido da entidade: ", entityParentData.projects);

                                if (response.status == 200) {
                                    swal("Eliminado!", name + " foi eliminado.", "success");
                                    self.getDataFromBD();
                                } else {
                                    // TODO: HANDLE IT
                                }
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                            // TODO: HANDLE IT
                        });
                });
            });

        } else {
              swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
        }
        var self = this;


    }

    render() {
        const css = css;
        var rows = [];
        var projectS;
        var blockExHours;
        // var notifyExHours;
        var projectType = "",
            entityParent = "",
            entityParentName = "";

        this
            .state
            .data
            .map((data, j) => {

                this
                    .state
                    .projectType
                    .forEach((obj, i) => {
                        if (data.projectType == obj._id) {
                            projectType = obj.description;
                        }
                    })

                this
                    .state
                    .projectState
                    .forEach((projectState, j) => {
                        //para esse contrato verifica o nome da descrição do tipo de contrato
                        if (data.state == projectState._id) {
                            projectS = <span className={projectState.stateLabelColor}>{projectState.stateName}</span>

                        }
                    })

                if (data.blockExhaustedHours == true) {
                    blockExHours = <em className="fa fa-check"></em>
                } else {
                    blockExHours = <em className="fa fa-close"></em>
                }
                // if (data.notifyExhaustedHours == true) {     notifyExHours = <em
                // className="fa fa-check"></em> } else {     notifyExHours = <em className="fa
                // fa-close"></em> }

                this
                    .state
                    .entities
                    .map((e, i) => {

                        if (e.projects == data._id) {
                            entityParent = e._id
                            entityParentName = e.abrevName;

                        }

                    })

                // o entityParentName é um apontador por isso acaba por ser sempre igual abaixo
                // com o de cima, mesmo que o valor esteja a ser actualizado
                var foo = JSON.stringify(entityParent);
                foo = JSON.parse(foo);

                rows.push(
                    <tr key={data._id}>
                        <td className="tdAlignCenter">{data.projectCode}</td>
                        <td className="tdAlignCenter">{projectType}</td>
                        <td className="tdAlignCenter">{entityParentName}</td>
                        <td
                            className="tdAlignCenter"
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(data.beginDate).toLocaleDateString()}</td>
                        <td
                            className="tdAlignCenter"
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{data.endDate != null
                                ? new Date(data.endDate).toLocaleDateString()
                                : "-"}</td>
                        <td className="tdAlignCenter">{data.hours}</td>
                        {/* <td className="tdAlignCenter">{data.availableHours}</td> */}
                        <td className="tdAlignCenter">{data.exhaustedHours}</td>
                        {/*<td>{notifyExHours}</td>*/}
                        {/*<td>{blockExHours}</td>*/}
                        <td className="tdAlignCenter">{projectS}</td>
                        <td className="tdAlignCenter">
                            <button
                                onClick={() => this.view(data._id, foo)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-eye"></i>
                            </button>
                        </td >
                        <td className="tdAlignCenter">
                            <button
                                onClick={() => this.edit(data._id, foo)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td className="tdAlignCenter">
                            <button
                                onClick={() => this.delete(data.projectCode, data._id, foo)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                )

                //  }  })
            })

        return (
            <ContentWrapper>
                <h3>Todos os projetos
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <Row>
                    <Col lg={12}>
                        <Panel>
                            <button
                                onClick={() => this.create()}
                                type="button"
                                id="addBtn"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                <i className="icon-plus"></i>
                            </button>
                            <Table id="dtProjects" responsive hover>
                                <thead>
                                    <tr>
                                        <th className="tdAlignCenter">Código do projeto</th>
                                        <th className="tdAlignCenter">Tipo de projeto</th>
                                        <th className="tdAlignCenter">Cliente</th>
                                        <th className="tdAlignCenter">Data de início</th>
                                        <th className="tdAlignCenter">Data de fim</th>
                                        <th className="tdAlignCenter">Horas</th>
                                        {/* <th className="tdAlignCenter">Horas disponív.</th> */}
                                        <th className="tdAlignCenter">Horas consum.</th>
                                        {/*<th>Notificar horas consumidas</th>*/}
                                        {/*<th>Bloquear horas consumidas</th>*/}
                                        <th className="tdAlignCenter">Estado</th>
                                        <th className="tdAlignCenter"></th>
                                        <th className="tdAlignCenter"></th>
                                        <th className="tdAlignCenter"></th>
                                    </tr>
                                </thead>
                                <tbody>{rows}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default Projects;
