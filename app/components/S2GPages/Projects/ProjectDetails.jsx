import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Tab,
    Tabs,
    Table,
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class ProjectDetails extends React.Component {

    constructor() {
        super();
        this.state = {
            project: null,
            beginDate: "",
            endDate: "",

            hours: "",
            notifyExhaustedHours: false,
            blockExhaustedHours: false,
            projectTypes: [],
            projectStates: [],
            selectedProjectType: "",
            selectedProjectState: "",
            selectedProjectTypeName: "",
            selectedProjectStateName: "",
            projectCode: "",
            availableHours: "",
            exhaustedHours: "",
            entityData: [],

            projectInterventions: [],
            interventions: [],
            interventionTypes: [],
            interventionMeans: [],
            users: [],
            contracts: [],
            projects: [],
            customers: [],
            processes: [],

            key: 1
        };
    }
    handleSelect(key) {
        this.setState({key});
    }
    calcDifBetweenA_B(ATime, BTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    view(id) {
        this
            .props
            .router
            .push('/interventions/view/' + id);
    }

    create() {
        this
            .props
            .router
            .push('/interventions/create/' + this.props.params.idEntity + '/' + this.props.params.id + "/1");
    }

    edit(customer, process, id) {
        this
            .props
            .router
            .push('/interventions/edit/' + customer + '/' + process + '/' + id);
    }

    delete(name, id) {
        var self = this;
        var contador = 0;
        swal({
            title: "Tem a certeza que quer eliminar?",
            text: "Após eliminar não poderá recuperar o registo!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            closeOnConfirm: false
        }, function () {

            axios
                .get(__APIURL__ + "/interventions/" + id)
                .then(function (intervResponse) {

                    axios
                        .delete(__APIURL__ + "/interventions/" + id)
                        .then(function (response) {
                            if (response.status == 200) {
                                if (!intervResponse.data.toBeValidated) {
                                    var exhaustedHours = self.calcDifBetweenA_B(self.state.project.exhaustedHours, moment(intervResponse.data.timeToCharge).format("HH:mm"));
                                    var updateProject = JSON.parse(JSON.stringify({
                                        "beginDate": (self.state.project.beginDate == null
                                            ? new Date()
                                            : self.state.project.beginDate),
                                        "endDate": (self.state.project.endDate == null
                                            ? null
                                            : self.state.project.endDate),
                                        "hours": self.state.project.hours,
                                        // "notifyExhaustedHours": notifyExhaustedHours,
                                        "blockExhaustedHours": self.state.project.blockExhaustedHours,
                                        "projectType": self.state.project.projectType,
                                        "state": self.state.project.state,
                                        "projectCode": self.state.project.projectCode,
                                        "availableHours": self.state.project.availableHours,
                                        "exhaustedHours": exhaustedHours
                                    }));
                                    self.state.project.exhaustedHours = exhaustedHours;
                                    axios
                                        .put(__APIURL__ + '/projects/' + intervResponse.data.projectToCharge, updateProject, {headers: self.headers})
                                        .then(function (response) {
                                            contador++;
                                            if (contador == 2) {
                                                swal("Eliminado!", name + " foi eliminado.", "success");
                                                var newInterv = [];
                                                self
                                                    .state
                                                    .projectInterventions
                                                    .forEach(contInterv => {
                                                        if (contInterv._id != id) {
                                                            newInterv.push(contInterv);
                                                        }
                                                    })
                                                self.setState({projectInterventions: newInterv})
                                            }

                                        })
                                } else {
                                    contador++;
                                    if (contador == 2) {
                                        swal("Eliminado!", name + " foi eliminado.", "success");
                                        var newInterv = [];
                                        self
                                            .state
                                            .projectInterventions
                                            .forEach(contInterv => {
                                                if (contInterv._id != id) {
                                                    newInterv.push(contInterv);
                                                }
                                            })
                                        self.setState({projectInterventions: newInterv})
                                    }
                                }

                            } else {
                                // TODO: HANDLE IT
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                            // TODO: HANDLE IT
                        });
                    if (!intervResponse.data.toBeValidated) {
                        self
                            .state
                            .processes
                            .forEach(proc => {
                                var interventionsOfProcess = proc.interventions;
                                interventionsOfProcess.forEach(inter => {
                                    if (inter == id) {
                                        var updateProcess = JSON.parse(JSON.stringify({
                                            "processNumber": proc.processNumber,
                                            "date": (proc.date == null
                                                ? moment().format()
                                                : proc.date),
                                            "description": proc.description,
                                            "entity": proc.entity,
                                            "createdBy": proc.createdBy,
                                            "requestedBy": proc.requestedBy,
                                            "collaborator": proc.collaborator,
                                            "notify": proc.notify,
                                            "processType": proc.processType,
                                            "state": proc.state,
                                            "interventions": proc.interventions,
                                            "duration": proc.duration
                                        }));
                                        console.log( updateProcess.duration + " "+ moment(intervResponse.data.timeToCharge).format("HH:mm"));
                                        updateProcess.duration = self.calcDifBetweenA_B(updateProcess.duration, moment(intervResponse.data.timeToCharge).format("HH:mm"));
                                        proc.duration = updateProcess.duration;

                                        console.log( updateProcess.duration + " " + moment(intervResponse.data.timeToCharge).format("HH:mm"));
                                        var newInterventionsList = [];
                                        
                                        updateProcess
                                            .interventions
                                            .forEach(interv => {
                                                if (interv != id) {
                                                    newInterventionsList.push(interv);
                                                }
                                            })

                        
                                        updateProcess.interventions = newInterventionsList;
                                        proc.interventions = newInterventionsList;
                                        axios
                                            .put(__APIURL__ + '/processes/' + proc._id, updateProcess, {headers: self.headers})
                                            .then(function (response) {
                                                contador++;
                                                if (contador == 2) {
                                                    swal("Eliminado!", name + " foi eliminado.", "success");
                                                    var newInterv = [];
                                                    self
                                                        .state
                                                        .projectInterventions
                                                        .forEach(contInterv => {
                                                            if (contInterv._id != id) {
                                                                newInterv.push(contInterv);
                                                            }
                                                        })
                                                    self.setState({projectInterventions: newInterv})
                                                }
                                            })
                                    }
                                })
                            })
                    } else {
                        contador++;
                        if (contador == 2) {
                            swal("Eliminado!", name + " foi eliminado.", "success");
                            var newInterv = [];
                            self
                                .state
                                .projectInterventions
                                .forEach(contInterv => {
                                    if (contInterv._id != id) {
                                        newInterv.push(contInterv);
                                    }
                                })
                            self.setState({projectInterventions: newInterv})
                        }
                    }

                })

        });
    }

    componentWillMount() {
        var th = this;
        var id = this.props.params.id;
        var idEntity = this.props.params.idEntity;
        this.serverRequest = axios
            .get(__APIURL__ + "/projects/" + id)
            .then(function (result) {

                th.setState({
                    project: result.data,
                    beginDate: new Date(result.data.beginDate).toLocaleDateString(),
                    endDate: new Date(result.data.endDate).toLocaleDateString(),
                    hours: ((result.data.hours != undefined)
                        ? result.data.hours
                        : "00:00"),
                    notifyExhaustedHours: ((result.data.notifyExhaustedHours != undefined)
                        ? result.data.notifyExhaustedHours
                        : false),
                    blockExhaustedHours: ((result.data.blockExhaustedHours != undefined)
                        ? result.data.blockExhaustedHours
                        : false),
                    selectedProjectType: result.data.projectType,
                    selectedProjectState: result.data.state,
                    availableHours: ((result.data.availableHours != undefined)
                        ? result.data.availableHours
                        : "Sem intervenções"),
                    exhaustedHours: ((result.data.exhaustedHours != undefined)
                        ? result.data.exhaustedHours
                        : "Sem intervenções"),
                    projectCode: ((result.data.projectCode != undefined)
                        ? result.data.projectCode
                        : "")
                });
                th.serverRequest = axios
                    .get(__APIURL__ + "/projectTypes/" + result.data.projectType)
                    .then(function (result) {
                        th.setState({selectedProjectTypeName: result.data.description})

                    })
                th.serverRequest = axios
                    .get(__APIURL__ + "/projectStates/" + result.data.state)
                    .then(function (result) {
                        th.setState({selectedProjectStateName: result.data.stateName})

                    })
            })

        //tratamento das intervenções:
        this.serverRequest = axios
            .get(__APIURL__ + "/interventions/")
            .then(function (result) {
                console.log(result.data)
                th.setState({interventions: result.data})

                console.log(th.state.interventions)
                console.log(th.state.interventions.length)
                for (var i = 0; i < th.state.interventions.length; i++) {
                    console.log(th.state.interventions[i], th.props.params.id)
                    if (th.state.interventions[i].projectToCharge == th.props.params.id) {
                        console.log(th.state.interventions[i])
                        th
                            .state
                            .projectInterventions
                            .push(th.state.interventions[i])

                        setTimeout(function () {
                            var dtInterventions = $('.dtInterventions').dataTable({
                                'retrieve': true, 'paging': true, // Table pagination
                                'ordering': true, // Column ordering
                                'info': true, // Bottom left status text
                                'responsive': true, // https://datatables.net/extensions/responsive/examples/
                                // Text translation options Note the required keywords between underscores (e.g
                                // _MENU_)
                                oLanguage: {
                                    sSearch: 'Search all columns:',
                                    sLengthMenu: '_MENU_ records per page',
                                    info: 'Showing page _PAGE_ of _PAGES_',
                                    zeroRecords: 'Nothing found - sorry',
                                    infoEmpty: 'No records available',
                                    infoFiltered: '(filtered from _MAX_ total records)'
                                }
                            });
                            var inputSearchClass = 'datatable_input_col_search';
                            var columnInputs = $('tfoot .' + inputSearchClass);

                            // On input keyup trigger filtering
                            columnInputs.keyup(function () {
                                dtInterventions.fnFilter(this.value, columnInputs.index(this));
                            });
                        })
                    }
                }
            });

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + idEntity)
            .then(function (result) {
                th.setState({entityData: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/")
            .then(function (result) {
                th.setState({customers: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/interventionTypes/")
            .then(function (result) {
                th.setState({interventionTypes: result.data})
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/interventionMeans/")
            .then(function (result) {
                th.setState({interventionMeans: result.data})
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({users: result.data})
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contracts/")
            .then(function (result) {
                th.setState({contracts: result.data})
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projects/")
            .then(function (result) {
                th.setState({projects: result.data})
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processes/")
            .then(function (result) {
                th.setState({processes: result.data})
            })

    }

    renderInterventionsTable() {
        var th = this;
        var interventionRows = []
        var interventionT = "";
        var interventionM = "";
        var collaboratorName = "";
        var offerSymbol;
        var complaintSymbol;
        var customer = "";
        var process = "";
        //   var validatedSymbol = <em className="fa fa-close"></em>;   var
        // terminateSymbol = <em className="fa fa-close"></em>; Popular a linha de
        // registos das intervenções
        this
            .state
            .projectInterventions
            .forEach((projectInterv, i) => {
                console.log(this, th)
                th
                    .state
                    .interventionTypes
                    .forEach((intervT, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (projectInterv.interventionType == intervT._id) {
                            console.log("intervT", intervT)
                            interventionT = intervT.description;
                        }
                    })
                th
                    .state
                    .interventionMeans
                    .forEach((intervM, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (projectInterv.means == intervM._id) {
                            console.log("intervM", intervM)
                            interventionM = intervM.description;
                        }
                    })
                th
                    .state
                    .users
                    .forEach((collaborator, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (projectInterv.collaborator == collaborator._id) {
                            console.log("collaborator", collaborator)
                            collaboratorName = collaborator.name;
                        }
                    })

                if (th.state.contracts.message != "NO_RESULTS") {
                    th
                        .state
                        .contracts
                        .forEach((c, j) => {
                            //para esse contacto verifica o nome da descrição do tipo de contacto
                            if (projectInterv.contractToCharge == c._id) {
                                th
                                    .state
                                    .customers
                                    .forEach(customerObj => {
                                        customerObj
                                            .contracts
                                            .forEach(contrID => {
                                                if (contrID == c._id) {
                                                    customer = customerObj.abrevName;
                                                }
                                            })
                                    })

                            }
                        })
                }

                if (th.state.projects.message != "NO_RESULTS") {
                    th
                        .state
                        .projects
                        .forEach((p, j) => {
                            //para esse contacto verifica o nome da descrição do tipo de contacto
                            if (projectInterv.projectToCharge == p._id) {
                                th
                                    .state
                                    .customers
                                    .forEach(customerObj => {
                                        customerObj
                                            .projects
                                            .forEach(projtID => {
                                                if (projtID == p._id) {
                                                    customer = customerObj.abrevName;
                                                }
                                            })
                                    })

                            }
                        })
                }

                th
                    .state
                    .processes
                    .forEach(proc => {
                        var intervs = proc.interventions;

                        intervs.forEach(intID => {
                            if (intID == projectInterv._id) {
                                process = proc;
                            }
                        })
                    })

                if (projectInterv.offer == true) {
                    offerSymbol = <em className="fa fa-check"></em>
                } else {
                    offerSymbol = <em className="fa fa-close"></em>
                }
                if (projectInterv.complaint == true) {
                    complaintSymbol = <em className="fa fa-check"></em>
                } else {
                    complaintSymbol = <em className="fa fa-close"></em>;
                }
                var toBeValidated;
                if (projectInterv.toBeValidated) {
                    toBeValidated = <em className="fa fa-close"></em>
                } else {
                    toBeValidated = <em className="fa fa-check"></em>
                }

                //para esse contacto faz o push para contact rows
                interventionRows.push(
                    <tr key={projectInterv._id} className="active">
                        <td>{projectInterv.interventionNumber}</td>
                        <td>{interventionT}</td>
                        <td>{customer}</td>
                        <td>{collaboratorName}</td>
                        <td>{moment(projectInterv.date).format("DD/MM/YYYY")}</td>
                        <td>{moment(projectInterv.beginHour).format("HH:mm")}</td>
                        <td>{moment(projectInterv.endHour).format("HH:mm")}</td>
                        <td>{moment(projectInterv.totalTime).format("HH:mm")}</td>
                        <td>{moment(projectInterv.timeToCharge).format("HH:mm")}</td>
                        <td>{toBeValidated}</td>
                        {/* <td>{offerSymbol}</td>*/}
                        {/* <td>{complaintSymbol}</td>*/}
                        <td>
                            <button
                                onClick={() => this.view(projectInterv._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-eye"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.edit(process.entity, process._id, projectInterv._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(projectInterv.interventionNumber, projectInterv._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                );

            })

        return interventionRows;
    };

    render() {

        const css = css;
        var tableInterventions = [];

        // tratamentos dos intervenções if (this.state.projectInterventions[0] != null) {
        tableInterventions.push(
            <Table className="dtInterventions">
                <thead>
                    <tr className="active">
                        <th>Número de intervenção</th>
                        <th>Tipo de intervenção</th>
                        <th>Cliente</th>
                        <th>Colaborador</th>
                        <th>Data</th>
                        <th>Hora de início</th>
                        <th>Hora de fim</th>
                        <th>Tempo total</th>
                        <th>Tempo cobrado</th>
                        <th>Validado</th>
                        {/*  <th>Oferta</th>*/}
                        {/*  <th>Reclamação</th>*/}
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderInterventionsTable()}
                </tbody>
            </Table>

        );

        //  else {     tableInterventions.push(         <Col lg={12}>
        // <b>Este contrato não tem intervenções</b>         </Col>     ) }

        this.state.notifyExhaustedHours == 1
            ? this.state.notifyExhaustedHours = 'Sim'
            : this.state.notifyExhaustedHours = 'Não';
        this.state.blockExhaustedHours == true
            ? this.state.blockExhaustedHours = 'Sim'
            : this.state.blockExhaustedHours = 'Não';

        console.log(this.state)

        return (
            <ContentWrapper>
                <h3>Projetos de {this.state.entityData.taxName}
                </h3 >
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Detalhes do projeto</div>
                            </div>
                            <div className="panel-body">
                                <Row>
                                    <Col lg={5}>
                                        <div className="form-group">
                                            <label className="control-label">Código do projeto:&nbsp;</label>{this.state.projectCode}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Estado:&nbsp;</label>{this.state.selectedProjectStateName}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Tipo de projeto:&nbsp;</label>{this.state.selectedProjectTypeName}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Data de início:&nbsp;</label>{this.state.beginDate}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Data de fim:&nbsp;</label>{this.state.endDate}
                                        </div>

                                    </Col>
                                    <Col lg={5}>

                                        <div className="form-group">
                                            <label className="control-label">Notificar horas consumidas:&nbsp;</label>{this.state.notifyExhaustedHours}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Bloquear horas consumidas:&nbsp;</label>{this.state.blockExhaustedHours}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Horas:&nbsp;</label>{this.state.hours}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Horas disponíveis:&nbsp;</label>{this.state.availableHours}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Horas consumidas:&nbsp;</label>{this.state.exhaustedHours}
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col lg={10}>
                                        <div className="form-group">
                                            <button
                                                onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/3")}
                                                className="btn ">Voltar ao cliente</button>
                                        </div>
                                    </Col>
                                </Row>
                            </div>

                            {/* END panel */}
                            {/* START tabs */}
                            <Tabs
                                activeKey={this.state.key}
                                onSelect={this
                                .handleSelect
                                .bind(this)}
                                justified
                                id="tabID">
                                <Tab eventKey={1} title="Intervenções">
                                    <button
                                        onClick={() => this.create()}
                                        type="button"
                                        id="addBtn"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                        <i className="icon-plus"></i>
                                    </button>
                                    <Row>

                                        {/* data table de adendas */}
                                        {tableInterventions}
                                    </Row>
                                </Tab>

                            </Tabs>
                            {/* END tabs */}

                        </div>
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
}

export default ProjectDetails;