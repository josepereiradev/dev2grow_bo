import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {Router, Route, Link, History, withRouter} from 'react-router';
import {browserHistory} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class AddProject extends React.Component {
    constructor() {
        super();
        this.state = {
            beginDate: new Date(),
            endDate: new Date(),
            hours: "",
            perMonth: false,
            // notifyExhaustedHours: "",
            blockExhaustedHours: false,
            projectTypes: [],
            projectStates: [],
            selectedProjectType: "593e7b847cebfc5f47556a80",
            selectedProjectState: "593e7bcc7cebfc5f47556a83",
            projectCode: "",
            //availableHours: "", exhaustedHours: "",

            entityData: []
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleCheckbox = this
            .handleCheckbox
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
        console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        var th = this;
        const {
            beginDate,
            endDate,
            hours,
            // notifyExhaustedHours,
            blockExhaustedHours,
            projectTypes,
            projectStates,
            selectedProjectState,
            selectedProjectType,
            projectCode,
            //availableHours, exhaustedHours,
            entityData
        } = this.state;
        console.log(this.state);
        var headers = new Headers({'Content-Type': 'application/json'});
        var newProject = JSON.parse(JSON.stringify({
            "beginDate": (beginDate == null
                ? new Date().toISOString()
                : new Date(beginDate).toISOString()),
            "endDate": (endDate != null
                ? new Date(endDate).toISOString()
                : null),
            "hours": hours + ":00",
            // "notifyExhaustedHours": notifyExhaustedHours,
            "blockExhaustedHours": blockExhaustedHours,
            "projectType": selectedProjectType,
            "state": selectedProjectState,
            "projectCode": projectCode,
            "exhaustedHours": "00:00"
        }));

        var id = this.props.params.idEntity;

        e.preventDefault();
        console.log(newProject);

        this.serverRequest = axios
            .post(__APIURL__ + '/projects/', newProject, {headers: this.headers})
            .then(function (response) {
                console.log(response);

                if (entityData.projects == null) {
                    entityData.projects = ["" + response.data.obj._id + ""]

                } else {
                    entityData
                        .projects
                        .push(response.data.obj._id);
                }

                var updateEntity = JSON.parse(JSON.stringify({
                    "nif": entityData.nif,
                    "qrCode": entityData.qrCode,
                    "taxName": entityData.taxName,
                    "abrevName": entityData.abrevName,
                    "accountManager": entityData.accountManager,
                    "contacts": entityData.contacts,
                    "projects": entityData.projects,
                    "processes": entityData.processes,
                    "contracts": entityData.contracts,
                    "associatedUsers": entityData.associatedUsers
                }));
                console.log(updateEntity);
                th.serverRequest = axios
                    .put(__APIURL__ + '/entities/' + id, updateEntity, {headers: th.headers})
                    .then(function (response) {
                        th
                            .props
                            .router
                            .push('/customers/view/' + id + "/" + 3);  //nmr da aba que vai abrir
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                console.log("contrato adicionado à entidade: ", th.state.entityData.projects);

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.idEntity;

        console.log(id)

        this.serverRequest = axios
            .get(__APIURL__ + "/projectTypes/")
            .then(function (result) {
                th.setState({projectTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projectStates/")
            .then(function (result) {
                th.setState({projectStates: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + id)
            .then(function (result) {
                th.setState({entityData: result.data});
            })
        this.setState({endDate: null});
    }

    render() {

        var projectTypeList = [];
        var projectStateList = [];

        if (this.state.projectTypes[0] != undefined) 
            this.state.projectTypes.forEach((item, i) => {
                if (i == 0) {
                    projectTypeList.push(
                        <option disabled defaultValue value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                projectTypeList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })
        if (this.state.projectStates[0] != undefined) 
            this.state.projectStates.forEach((item, i) => {
                if (i == 0) {
                    projectStateList.push(
                        <option disabled defaultValue value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                projectStateList.push(
                    <option key={i.toString()} value={item._id}>{item.stateName}</option>
                );
            })

        return (
            <ContentWrapper>
                <h3>Projeto de {this.state.entityData.taxName}
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar um novo projeto</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Código de projeto*</label>
                                            <FormControl
                                                type="text"
                                                name="projectCode"
                                                required="required"
                                                className="form-control"
                                                onChange={this.handleChange}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Horas*</label>
                                            <FormControl
                                                type="text"
                                                name="hours"
                                                required="required"
                                                className="form-control"
                                                onChange={this.handleChange}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Data de início*</label>
                                            <div id="datetimepickerBegin" className="input-group date">
                                                <input name="beginDate" type="text" className="form-control" />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Data de fim</label>
                                            <div id="datetimepickerEnd" className="input-group date">
                                                <input name="endDate" type="text" className="form-control" />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Estado*</label>
                                            <FormControl
                                                id="select2-1-state"
                                                componentClass="select"
                                                name="selectedProjectState"
                                                className="form-control m-b"
                                                value={this.state.selectedProjectState}>
                                                {projectStateList}

                                            </FormControl>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de projeto*</label>
                                            <FormControl
                                                id="select2-1-projectType"
                                                componentClass="select"
                                                name="selectedProjectType"
                                                className="form-control m-b"
                                                value={this.state.selectedProjectType}>
                                                {projectTypeList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        

                                            {/* <div className="col-sm-3">
                                                <label className="control-label">Notificar horas consumidas</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyExhaustedHours"
                                                            checked={this.state.notifyExhaustedHours}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'notifyExhaustedHours')}/>
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div> */}

                                            <div className="col-sm-3">
                                                <label className="control-label">Bloquear horas consumidas</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="blockExhaustedHours"
                                                            checked={this.state.blockExhaustedHours}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'blockExhaustedHours')}/>
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                           
                                        </div>

                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/3")}
                                            type="button"
                                            className="btn ">Voltar ao cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {

        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepickerBegin')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerBegin')
                        .data("DateTimePicker")
                        .date();
                    this.setState({beginDate: dateSelected});
                    console.log(this.state.beginDate);
                }.bind(this));;

            $('#datetimepickerEnd').datetimepicker({
                format: 'DD/MM/YYYY',
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerEnd')
                        .data("DateTimePicker")
                        .date();
                    console.log(this.state.endDate);
                    this.setState({endDate: dateSelected});
                }.bind(this));;

            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-state')
                .select2({theme: 'bootstrap'})
                .on('change', this.handleChange);
            $('#select2-1-projectType')
                .select2({theme: 'bootstrap'})
                .on('change', this.handleChange);

        }
    }
}

export default AddProject;
