import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../../styles/tableIcons.scss'
import ContentWrapper from '../../../Layout/ContentWrapper';

class ProjectStates extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            projects: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
    }

    getDataFromBD() {
        axios
            .get(__APIURL__ + "/projectStates/")
            .then(res => {
                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({data})
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        axios
            .get(__APIURL__ + '/projects/')
            .then(res => {

                this.setState({projects: res.data})

            })
    }

    create() {
        this
            .props
            .router
            .push('/projectStates/create');
    }

    edit(id) {
        this
            .props
            .router
            .push('/projectStates/edit/' + id);
    }

    delete(name, id) {
        var self = this;
        var confirmDelete = true;

        this
            .state
            .projects
            .forEach(project => {
                if (project.state == id) {
                    confirmDelete = false;
                }
            })
        if (confirmDelete) {
            swal({
                title: "Tem a certeza que quer eliminar?",
                text: "Após eliminar não poderá recuperar o registo!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                closeOnConfirm: false
            }, function () {
                axios
                    .delete(__APIURL__ + "/projectStates/" + id)
                    .then(function (response) {
                        if (response.status == 200) {
                            swal("Eliminado!", name + " foi eliminado.", "success");
                            self.getDataFromBD();
                        } else {
                            // TODO: HANDLE IT
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        // TODO: HANDLE IT
                    });
            });
        } else {
            swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
        }

    }

    render() {
        const css = css;
        console.log(this.state.data);
        return (
            <ContentWrapper>
                <h3>Estados de Projecto
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <button
                    onClick={() => this.create()}
                    type="button"
                    id="addBtn"
                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                    <i className="icon-plus"></i>
                </button>
                <Row>
                    <Col lg={12}>
                        <Panel>
                            <Table responsive hover>
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Pré-visualização com cor</th>
                                        <th>Fecha projetos</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this
                                        .state
                                        .data
                                        .map(data => {
                                            var closeProjectIcon;
                                            if (data.close) {
                                                closeProjectIcon = <em className="fa fa-check"></em>
                                            } else {
                                                closeProjectIcon = <em className="fa fa-close"></em>
                                            }

                                            return <tr key={data._id}>
                                                <td>{data.stateName}</td>
                                                <td>
                                                    <span className={data.stateLabelColor}>{data.stateName}</span>
                                                </td>
                                                <th>{closeProjectIcon}</th>
                                                <td>
                                                    <button
                                                        onClick={() => this.edit(data._id)}
                                                        type="button"
                                                        id="lineBtns"
                                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                        <i className="icon-pencil"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    <button
                                                        onClick={() => this.delete(data.stateName, data._id)}
                                                        type="button"
                                                        id="lineBtns"
                                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                        <i className="icon-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        
                                        })
}
                                </tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default ProjectStates;
