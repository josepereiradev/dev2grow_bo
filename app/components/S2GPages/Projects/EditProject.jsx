import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class EditProject extends React.Component {

    constructor() {
        super();
        this.state = {
            beginDate: new Date(),
            endDate: new Date(),
            hours: "",
            // notifyExhaustedHours: "",
            blockExhaustedHours: false,
            projectTypes: [],
            projectStates: [],
            selectedProjectType: "",
            selectedProjectState: "",
            projectCode: "",
            availableHours: "",
            exhaustedHours: "",

            entityData: []

        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
        console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        var th = this;

        const {
            beginDate, endDate, hours,
            // notifyExhaustedHours,
            blockExhaustedHours,
            projectTypes,
            projectStates,
            selectedProjectState,
            selectedProjectType,
            projectCode,
            availableHours,
            exhaustedHours
        } = this.state;

        var headers = new Headers({'Content-Type': 'application/json'});
        var updateProject = JSON.parse(JSON.stringify({
            "beginDate": (beginDate == null
                ? new Date()
                : moment(beginDate, "DD/MM/YYYY").format()),
            "endDate": (endDate == null
                ? new Date()
                : moment(endDate, "DD/MM/YYYY").format()),
            "hours": hours,
            // "notifyExhaustedHours": notifyExhaustedHours,
            "blockExhaustedHours": blockExhaustedHours,
            "projectType": selectedProjectType,
            "state": selectedProjectState,
            "projectCode": projectCode,
            "availableHours": availableHours,
            "exhaustedHours": exhaustedHours
        }));
        console.log(updateProject);

        var idEntity = this.props.params.idEntity;
        var id = this.props.params.id;

        e.preventDefault();
        console.log(updateProject);

        this.serverRequest = axios
            .put(__APIURL__ + '/projects/' + id, updateProject, {headers: this.headers})
            .then(function (response) {
                console.log(response);
                th
                    .props
                    .router
                    .push('/customers/view/' + idEntity+"/3");

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + this.props.params.idEntity)
            .then(function (result) {
                th.setState({entityData: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projectTypes/")
            .then(function (result) {
                th.setState({projectTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projectStates/")
            .then(function (result) {
                th.setState({projectStates: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projects/" + id)
            .then(function (result) {

                th.setState({
                    beginDate: new Date(result.data.beginDate).toLocaleDateString(),
                    endDate: new Date(result.data.endDate).toLocaleDateString(),
                    lastRenewal: new Date(result.data.lastRenewal).toLocaleDateString(),
                    notifyRenewal: ((result.data.notifyRenewal != undefined)
                        ? result.data.notifyRenewal
                        : false),
                    hours: ((result.data.hours != undefined)
                        ? result.data.hours
                        : "00:00"),
                    perMonth: ((result.data.perMonth != undefined)
                        ? result.data.perMonth
                        : false),
                    // notifyExhaustedHours: ((result.data.notifyExhaustedHours != undefined)     ?
                    // result.data.notifyExhaustedHours     : false),
                    blockExhaustedHours: ((result.data.blockExhaustedHours != undefined)
                        ? result.data.blockExhaustedHours
                        : false),
                    selectedProjectType: result.data.projectType,
                    selectedProjectState: result.data.state,
                    availableHours: ((result.data.availableHours != undefined)
                        ? result.data.availableHours
                        : "Sem intervenções"),
                    exhaustedHours: ((result.data.exhaustedHours != undefined)
                        ? result.data.exhaustedHours
                        : "Sem intervenções"),
                    projectCode: ((result.data.projectCode != undefined)
                        ? result.data.projectCode
                        : "")
                });
            })

    }
    render() {

        var projectTypeList = [];
        var projectStateList = [];

        if (this.state.projectTypes[0] != undefined) 
            this.state.projectTypes.forEach((item, i) => {
                projectTypeList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })
        if (this.state.projectStates[0] != undefined) 
            this.state.projectStates.forEach((item, i) => {
                projectStateList.push(
                    <option key={i.toString()} value={item._id}>{item.stateName}</option>
                );
            })

        return (
            <ContentWrapper>
                <h3>Projeto de {this.state.entityData.taxName}
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar projeto</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Código de projeto</label>
                                            <FormControl
                                                type="text"
                                                name="projectCode"
                                                required="required"
                                                className="form-control"
                                                value={this.state.projectCode}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'projectCode')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Horas</label>
                                            <FormControl
                                                type="text"
                                                name="hours"
                                                required="required"
                                                value={this.state.hours}
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'hours')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">
                                    <Row>
                                        {/* <div className="col-sm-3">
                                            <label className="control-label">Notificar horas consumidas</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        name="notifyExhaustedHours"
                                                        checked={this.state.notifyExhaustedHours}
                                                        onChange={this
                                                        .handleCheckbox
                                                        .bind(this, 'notifyExhaustedHours')}/>
                                                    <em className="fa fa-check"></em>

                                                </label>
                                            </div>
                                        </div> */}

                                        
                                        <div className="col-sm-3">
                                            <label className="control-label">Horas disponíveis</label>
                                            <FormControl
                                                disabled
                                                type="text"
                                                name="availableHours"
                                                required="required"
                                                className="form-control"
                                                value={this.state.availableHours}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'availableHours')}/>
                                        </div>
                                        <div className="col-sm-3">
                                            <label className="control-label">Horas consumidas</label>
                                            <FormControl
                                                disabled
                                                type="text"
                                                name="exhaustedHours"
                                                required="required"
                                                value={this.state.exhaustedHours}
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'exhaustedHours')}/>
                                        </div>
                                        <div className="col-sm-3">
                                            <label className="control-label">Bloquear horas consumidas</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        name="blockExhaustedHours"
                                                        checked={this.state.blockExhaustedHours}
                                                        onChange={this
                                                        .handleCheckbox
                                                        .bind(this, 'blockExhaustedHours')}/>
                                                    <em className="fa fa-check"></em>

                                                </label>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Data de início</label>
                                            <div id="datetimepickerBegin" className="input-group date">
                                                <input
                                                    name="beginDate"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.beginDate}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Data de fim</label>
                                            <div id="datetimepickerEnd" className="input-group date">
                                                <input
                                                    name="endDate"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.endDate}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Estado</label>
                                            <FormControl
                                                componentClass="select"
                                                name="selectedProjectState"
                                                className="form-control m-b"
                                                value={this.state.selectedProjectState}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedProjectState')}>
                                                {projectStateList}

                                            </FormControl>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de projeto</label>
                                            <FormControl
                                                componentClass="select"
                                                name="selectedProjectType"
                                                className="form-control m-b"
                                                value={this.state.selectedProjectType}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedProjectType')}>
                                                {projectTypeList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-12"></div>

                                    </Row>
                                </div>
                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/projects/')}
                                            type="button"
                                            className="btn ">Voltar aos Projetos</button>
                                    </Col>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/3")}
                                            type="button"
                                            className="btn ">Voltar ao cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {

        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepickerBegin')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerBegin')
                        .data("DateTimePicker")
                        .date();
                    console.log(dateSelected.format("DD/MM/YYYY"));
                    this.setState({
                        beginDate: dateSelected.format("DD/MM/YYYY")
                    });
                }.bind(this));

            $('#datetimepickerEnd')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerEnd')
                        .data("DateTimePicker")
                        .date();
                    console.log(dateSelected.format("DD/MM/YYYY"));
                    this.setState({
                        endDate: dateSelected.format("DD/MM/YYYY")
                    });
                }.bind(this));

            $('#datetimepickerLastRenewal')
                .datetimepicker({
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerLastRenewal')
                        .data("DateTimePicker")
                        .date();
                    this.setState({
                        beginDate: dateSelected.format("DD/MM/YYYY")
                    });
                }.bind(this));

            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-state').select2({theme: 'bootstrap'});
            $('#select2-1-country').select2({theme: 'bootstrap'});
            $('#select2-1-function').select2({theme: 'bootstrap'});
            //  $('#select2-2').select2({theme: 'bootstrap'});
            // $('#select2-3').select2({theme: 'bootstrap'});
            // $('#select2-4').select2({placeholder: 'Select a state', allowClear: true,
            // theme: 'bootstrap'});
        }
    }
}

export default EditProject;
