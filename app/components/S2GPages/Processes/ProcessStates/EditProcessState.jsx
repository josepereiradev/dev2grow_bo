import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';

import css from '../../../../styles/tableIcons.scss'

class EditProcessState extends React.Component {

    constructor() {
        super();
        this.state = {
            stateName: "",
            stateLabelColor: "",
            selectedColor: "",
            endProcesses: ""
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }
    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)
        if (name == "stateLabelColor") {
            switch (e.target.value) {
                case 'label label-danger':
                    this.setState({selectedColor: 'bg-danger'});
                    break;
                case 'label label-warning':
                    this.setState({selectedColor: 'bg-warning'});
                    break;
                case 'label label-info':
                    this.setState({selectedColor: 'bg-info'});
                    break;
                case 'label label-primary':
                    this.setState({selectedColor: 'bg-primary'});
                    break;
                case 'label label-success':
                    this.setState({selectedColor: 'bg-success'});
                    break;
                case 'label label-default':
                    this.setState({selectedColor: 'bg-default'});
                    break;

            }
        }

    }
    handleCheckbox(name, e) {
        console.log([e.target.name]);
        this.setState({
            [e.target.name]: e.target.checked
        });
        console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        var self = this;

        const {stateName, stateLabelColor, endProcesses} = this.state;
        var headers = new Headers({'Content-Type': 'application/json'});
        var updateProcessState = JSON.parse(JSON.stringify({"stateName": stateName, "stateLabelColor": stateLabelColor, "endProcesses": endProcesses}));
        var id = this.props.params.id;

        e.preventDefault();
        console.log(updateProcessState);
        this.serverRequest = axios
            .put(__APIURL__ + '/processStates/' + id, updateProcessState, {headers: this.headers})
            .then(function (response) {
                console.log(response);
                self
                    .props
                    .router
                    .push('/processStates');

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;
        this.serverRequest = axios
            .get(__APIURL__ + "/processStates/" + id)
            .then(function (result) {
                th.setState({stateName: result.data.stateName, stateLabelColor: result.data.stateLabelColor, endProcesses: result.data.endProcesses});
                switch (result.data.stateLabelColor) {
                    case 'label label-danger':
                        th.setState({selectedColor: 'bg-danger'});
                        break;
                    case 'label label-warning':
                        th.setState({selectedColor: 'bg-warning'});
                        break;
                    case 'label label-info':
                        th.setState({selectedColor: 'bg-info'});
                        break;
                    case 'label label-primary':
                        th.setState({selectedColor: 'bg-primary'});
                        break;
                    case 'label label-success':
                        th.setState({selectedColor: 'bg-success'});
                        break;
                    case 'label label-default':
                        th.setState({selectedColor: 'bg-default'});
                        break;

                }
            })

    }

    render() {
        console.log(this.state)

        return (
            <ContentWrapper>
                <h3>Estados de Processo
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar estado de Processo</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-4">
                                            <label className="control-label">Nome do estado*</label>
                                            <FormControl
                                                type="text"
                                                name="stateName"
                                                required="required"
                                                className="form-control"
                                                value={this.state.stateName}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'stateName')}/>
                                        </div>
                                        <div className="col-sm-4">
                                            <label className="control-label">Cor do estado</label>
                                            <FormControl
                                                componentClass="select"
                                                name="stateLabelColor"
                                                className="form-control m-b"
                                                className={this.state.selectedColor}
                                                value={this.state.stateLabelColor}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'stateLabelColor')}>
                                                //.label-default, .label-primary, .label-success, .label-info, .label-warning or
                                                .label-danger
                                                <option value="label label-default" className="bg-default">Escolhido</option>
                                                <option value="label label-primary" className="bg-primary">Primary</option>
                                                <option value="label label-success" className="bg-success">Success</option>
                                                <option value="label label-info" className="bg-info">Info</option>
                                                <option value="label label-warning" className="bg-warning">Warning</option>
                                                <option value="label label-danger" className="bg-danger">Danger</option>

                                            </FormControl>

                                        </div>
                                        <div className="col-sm-4">
                                            <label className="control-label">Fecha processos</label>
                                            <div className="checkbox c-checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        name="endProcesses"
                                                        checked={this.state.endProcesses}
                                                        onChange={this
                                                        .handleCheckbox
                                                        .bind(this, 'endProcesses')}/>
                                                    <em className="fa fa-check"></em>
                                                </label>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={1}>

                                        <button
                                            onClick={() => this.props.router.push('/processStates')}
                                            type="button"
                                            className="btn ">Voltar</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {
        var self = this;

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1').select2({theme: 'bootstrap'});

            $('#select2-1').on('change', function () {
                self.setState({
                    stateLabelColor: $(this).val()
                });
            });
        }
    }
}

export default EditProcessState;
