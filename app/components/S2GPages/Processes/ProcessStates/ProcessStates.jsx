import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../../styles/tableIcons.scss'
import ContentWrapper from '../../../Layout/ContentWrapper';

class ProcessStates extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            processes: [],
            interventions: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
    }

    getDataFromBD() {
        axios
            .get(__APIURL__ + "/processStates/")
            .then(res => {
                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({data})
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        axios
            .get(__APIURL__ + "/processes")
            .then(res => {
                this.setState({processes: res.data})
            })
        axios
            .get(__APIURL__ + "/interventions")
            .then(res => {
                this.setState({interventions: res.data})
            })
    }

    create() {
        this
            .props
            .router
            .push('/processStates/create');
    }

    edit(id) {
        this
            .props
            .router
            .push('/processStates/edit/' + id);
    }

    delete(name, id) {
        var self = this;
        var confirmDelete = true;
        this
            .state
            .processes
            .forEach(process => {
                if (process.state == id) {
                    confirmDelete = false;
                }
            })
            this.state.interventions.forEach(interv =>{
                if(interv.processState == id){
                     confirmDelete = false;
                }
            })
            if(confirmDelete){
                  swal({
            title: "Tem a certeza que quer eliminar?",
            text: "Após eliminar não poderá recuperar o registo!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            closeOnConfirm: false
        }, function () {
            axios
                .delete(__APIURL__ + "/processStates/" + id)
                .then(function (response) {
                    if (response.status == 200) {
                        swal("Eliminado!", name + " foi eliminado.", "success");
                        self.getDataFromBD();
                    } else {
                        // TODO: HANDLE IT
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    // TODO: HANDLE IT
                });
        });
            }else{
                  swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
            }
      
    }

    render() {
        const css = css;

        return (
            <ContentWrapper>
                <h3>Estados de Processo
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <button
                    onClick={() => this.create()}
                    type="button"
                    id="addBtn"
                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                    <i className="icon-plus"></i>
                </button>
                <Row>
                    <Col lg={12}>
                        <Panel>
                            <Table responsive hover>
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Pré-visualização com cor</th>
                                        <th>Fecha processos</th>

                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this
                                        .state
                                        .data
                                        .map(data => {
                                            var endProcess;
                                            if (data.endProcesses) {
                                                endProcess = <em className="fa fa-check"></em>
                                            } else {
                                                endProcess = <em className="fa fa-close"></em>
                                            }

                                            return <tr key={data._id}>
                                                <td>{data.stateName}</td>
                                                <td>
                                                    <span className={data.stateLabelColor}>{data.stateName}</span>
                                                </td>
                                                <td>
                                                    {endProcess}
                                                </td>
                                                <td>
                                                    <button
                                                        onClick={() => this.edit(data._id)}
                                                        type="button"
                                                        id="lineBtns"
                                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                        <i className="icon-pencil"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    <button
                                                        onClick={() => this.delete(data.stateName, data._id)}
                                                        type="button"
                                                        id="lineBtns"
                                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                        <i className="icon-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        })}
                                </tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default ProcessStates;
