import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class EditProcess extends React.Component {

    constructor() {
        super();
        this.state = {

            date: new Date(),
            description: "",
            selectedEntity: "",
            createdBy: "",
            requestedBy: "",
            collaborator: "",
            notify: [],
            selectedProcessType: "",
            selectedProcessState: "",
            selectedContract: null,
            selectedProject: null,
            processNumber: "",
            duration: "",
            interventions: [],

            entityData: [],
            entities: [],
            users: [],
            contacts: [],
            contracts: [],
            projects: [],
            processTypes: [],
            processStates: [],
            entityContacts: ""
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleSubmit(e) {
        var th = this;

        const {
            date,
            description,
            selectedEntity,
            createdBy,
            requestedBy,
            notify,
            selectedProcessType,
            selectedProcessState,
            collaborator,
            selectedContract,
            interventions,
            selectedProject,
            processNumber,
            duration
        } = this.state;

        var headers = new Headers({'Content-Type': 'application/json'});
        var updateProcess = JSON.parse(JSON.stringify({
            "processNumber": processNumber,
            "date": (date == null
                ? moment().format()
                : moment(date, "DD/MM/YYYY").format()),
            "description": description,
            "entity": th.props.params.idEntity,
            "createdBy": createdBy,
            "requestedBy": requestedBy,
            "collaborator": collaborator,
            "notify": notify,
            "contract": selectedContract,
            "project": selectedProject,
            "processType": selectedProcessType,
            "state": selectedProcessState,
            "interventions": interventions,
            "duration": duration
        }));

        var idEntity = this.props.params.idEntity;
        var id = this.props.params.id;
        console.log(JSON.stringify(updateProcess));

        e.preventDefault();

        this.serverRequest = axios
            .put(__APIURL__ + '/processes/' + id, updateProcess, {headers: this.headers})
            .then(function (response) {
                console.log(response)
                th
                    .props
                    .router
                    .push('/customers/view/' + idEntity + "/4");

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    updateProjectsAndContracts(e) {
        var entityId = e.target.value;
        var th = this;
        var contractsOfCustomer = [];
        var projectsOfCustomer = [];
        th.serverRequest = axios
            .get(__APIURL__ + "/entities/" + entityId)
            .then(function (result) {

                var contractsReceived = result.data.contracts;
                var projectsReceived = result.data.projects;

                //Filtrar os contratos que pertencem ao cliente
                contractsReceived.forEach(contractID => {

                    th
                        .state
                        .contracts
                        .forEach(contract => {

                            if (contractID == contract._id) {
                                contractsOfCustomer.push(contract);
                            }
                        });
                });

                projectsReceived.forEach(projectID => {

                    th
                        .state
                        .projects
                        .forEach(project => {

                            if (projectID == project._id) {
                                projectsOfCustomer.push(project);
                            }
                        });

                });

                th.setState({customerContracts: contractsOfCustomer, customerProjects: projectsOfCustomer, selectedEntity: entityId, entityData: result.data});

            });
    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;
        this.serverRequest = axios
            .get(__APIURL__ + "/processTypes/")
            .then(function (result) {
                th.setState({processTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processStates/")
            .then(function (result) {
                th.setState({processStates: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contacts/")
            .then(function (result) {
                th.setState({contacts: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contracts/")
            .then(function (result) {
                th.setState({contracts: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projects/")
            .then(function (result) {
                th.setState({projects: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({users: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + this.props.params.idEntity)
            .then(function (result) {
                th.setState({entityData: result.data, entityContacts: result.data.contacts});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/")
            .then(function (result) {
                th.setState({entities: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/processes/" + id)
            .then(function (result) {
                th.setState({
                    date: new Date(result.data.date).toLocaleDateString(),
                    description: ((result.data.description != "")
                        ? result.data.description
                        : "Sem assunto"),
                    selectedEntity: result.data.entity,
                    createdBy: result.data.createdBy,
                    requestedBy: result.data.requestedBy,
                    interventions: result.data.interventions,
                    notify: ((result.data.notify != undefined)
                        ? result.data.notify
                        : []),
                    selectedProcessType: result.data.processType,
                    selectedProcessState: result.data.state,
                    selectedContract: result.data.contract,
                    selectedProject: result.data.project,
                    collaborator: result.data.collaborator,
                    //entitydata
                    processNumber: ((result.data.processNumber != undefined)
                        ? result.data.processNumber
                        : "Sem número"),
                    duration: ((result.data.duration != undefined)
                        ? result.data.duration
                        : "00:00")
                });

            })

    }
    render() {

        var th = this;
        var processTypeList = [];
        var processStateList = [];
        var contactsList = [];
        var contactsListToNotify = [];
        var projectOrContractList = [];
        var usersList = [];
        var entitiesList = [];

        if (this.state.processTypes[0] != undefined) 
            this.state.processTypes.forEach((item, i) => {
                if (item._id == th.state.selectedProcessType) {
                    $('#select2-select2-1-processType-container').text(item.description);
                }
                processTypeList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })
        if (this.state.processStates[0] != undefined) 
            this.state.processStates.forEach((item, i) => {
                if (item._id == th.state.selectedProcessState) {
                    $('#select2-select2-1-state-container').text(item.stateName);
                }
                processStateList.push(
                    <option key={i.toString()} value={item._id}>{item.stateName}</option>
                );
            })
        if (this.state.contacts[0] != undefined) 
            this.state.contacts.forEach((item, i) => {
                if (item._id == th.state.requestedBy) {
                    $('#select2-select2-1-requestedBy-container').text(item.name);
                }
                for (var i = 0; i < th.state.entityContacts.length; i++) {
                    if (item._id == th.state.entityContacts[i]) {

                        contactsList.push(
                            <option key={i.toString()} selected="true" value={item._id}>{item.name}</option>
                        );

                        var contactIsNotified = false;
                        this
                            .state
                            .notify
                            .forEach((notifyID, j) => {
                                if (notifyID == item._id) {
                                    contactIsNotified = true;
                                    contactsListToNotify.push(
                                        <option key={j.toString() + i.toString()} selected="true" value={item._id}>{item.name}</option>
                                    );
                                }
                            });
                        if (!contactIsNotified) {
                            contactsListToNotify.push(
                                <option key={i.toString()} value={item._id}>{item.name}</option>
                            );
                        }

                    }

                }

            })

        if (this.state.users[0] != undefined) 
            this.state.users.forEach((item, i) => {
                if (item._id == th.state.createdBy) {
                    $('#select2-select2-1-createdBy-container').text(item.name);
                }
                if(item._id == th.state.collaborator){
                    $('#select2-select2-1-collaborator-container').text(item.name);
                }
                if (item.internalUser) {
                    usersList.push(
                        <option key={i.toString()} value={item._id}>{item.name}</option>
                    );
                }

            })
        if (this.state.entities[0] != undefined) 
            this.state.entities.forEach((item, i) => {
                if (item._id == th.state.selectedEntity) {
                    $('#select2-select2-1-entity-container').text(item.taxName);
                }
                entitiesList.push(
                    <option
                        key={item
                        ._id
                        .toString()}
                        value={item._id}>{item.taxName}</option>
                );
            });
        
        var contractsGroup = [];
        if (this.state.contracts.length != 0 && this.state.entityData.contracts !== undefined) {

            this
                .state
                .contracts
                .forEach((contract, i) => {
                    this
                        .state
                        .entityData
                        .contracts
                        .forEach(contractID => {

                            if (contract._id == contractID) {
                                contractsGroup.push(
                                    <option key={i} value={contractID}>{contract.contractCode}</option>
                                );
                            }
                        })
                    if (contract._id == this.state.selectedContract) {
                        $('#select2-select2-1-association-container').text(contract.contractCode);
                    }
                });
        }

        var projectsGroup = [];
        console.log(this.state.projects);
        if (this.state.projects.message != "NO_RESULTS" && this.state.entityData.projects !== undefined) {
            this
                .state
                .projects
                .forEach((project, i) => {
                    this
                        .state
                        .entityData
                        .projects
                        .forEach(projectID => {
                            if (project._id == projectID) {
                                projectsGroup.push(
                                    <option key={i} value={projectID}>{project.projectCode}</option>
                                );
                            }
                        })
                    if (project._id == this.state.selectedProject) {
                        $('#select2-select2-1-association-container').text(project.projectCode);
                    }
                });
        }

        projectOrContractList.push(
            <optgroup key={-2} label="Contratos">
                {contractsGroup}
            </optgroup>
        );
        projectOrContractList.push(
            <optgroup key={-3} label="Projetos">
                {projectsGroup}
            </optgroup>
        );

        /*  this
            .state
            .notify
            .forEach(notify => {
                this
                    .state
                    .contacts
                    .forEach((contact, i) => {
                        if (notify == contact._id) {

                            const element1 = <li class="select2-selection__choice" title="Contacto1">Contacto1</li>

                            // const element2 = <li className="select2-search select2-search--inline"><input
                            // className="select2-search__field" type="search" tabindex="0"
                            // autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                            // role="textbox" aria-autocomplete="list" placeholder=""  /></li>;
                            // ReactDOM.render(element1,
                            // document.getElementsByClassName('select2-selection__rendered')[6]); //fiz por
                            // tentativa e erro ate encontrar o elemento ReactDOM.render(element2,
                            // document.getElementsByClassName('select2-selection__rendered')[6]);
                        }

                    });

            });*/

        return (
            <ContentWrapper>
                <h3>Processo de {this.state.entityData.taxName}
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar processo</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Número de processo</label>
                                            <FormControl
                                                disabled
                                                type="text"
                                                name="processNumber"
                                                required="required"
                                                className="form-control"
                                                value={this.state.processNumber}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'processNumber')}/>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Duração</label>
                                            <FormControl
                                                disabled
                                                type="text"
                                                name="duration"
                                                required="required"
                                                className="form-control"
                                                value={this.state.duration}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'duration')}/>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Cliente*</label>
                                            <FormControl
                                                id="select2-1-entity"
                                                componentClass="select"
                                                name="selectedEntity"
                                                className="form-control m-b"
                                                required="required"
                                                value={this.state.selectedEntity}
                                                onChange={this
                                                .updateProjectsAndContracts
                                                .bind(this, 'gender')}>
                                                {entitiesList}

                                            </FormControl>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Associar a*</label>
                                            <FormControl
                                                id="select2-1-association"
                                                componentClass="select"
                                                name="association"
                                                className="form-control m-b"
                                                value={this.state.selectedContract || this.state.selectedProject}>
                                                {projectOrContractList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de Processo</label>
                                            <FormControl
                                                id="select2-1-processType"
                                                componentClass="select"
                                                name="selectedProcessType"
                                                className="form-control m-b"
                                                value={this.state.selectedProcessType}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedProcessType')}>
                                                {processTypeList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Estado do Processo</label>
                                            <FormControl
                                                id="select2-1-state"
                                                componentClass="select"
                                                name="selectedProcessState"
                                                className="form-control m-b"
                                                value={this.state.selectedProcessState}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedProcessState')}>
                                                {processStateList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Data</label>
                                            <div id="datetimepickerBegin" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.date}
                                                    onClick={this
                                                    .handleChange
                                                    .bind(this, 'date')}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Criado por</label>
                                            <FormControl
                                                id="select2-1-createdBy"
                                                componentClass="select"
                                                name="createdBy"
                                                className="form-control m-b"
                                                value={this.state.createdBy}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'createdBy')}>
                                                {usersList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Pedido por</label>
                                            <FormControl
                                                id="select2-1-requestedBy"
                                                componentClass="select"
                                                name="requestedBy"
                                                className="form-control m-b"
                                                value={this.state.requestedBy}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'requestedBy')}>
                                                {contactsList}

                                            </FormControl>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Notificar</label>
                                            <FormControl
                                                id="select2-1-notify"
                                                multiple="multiple"
                                                componentClass="select"
                                                name="selectedContact"
                                                className="form-control m-b"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedContact')}>
                                                {contactsListToNotify}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Técnico</label>
                                            <FormControl
                                                id="select2-1-collaborator"
                                                componentClass="select"
                                                name="collaborator"
                                                required="required"
                                                className="form-control m-b"
                                                value={this.state.collaborator}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'collaborator')}>
                                                {usersList}

                                            </FormControl>

                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Descrição</label>
                                            <textarea
                                                name="description"
                                                className="form-control"
                                                required="required"
                                                rows="2"
                                                value={this.state.description}
                                                onChange={this
                                                .handleChange
                                                .bind(this, "description")}/>

                                        </div>
                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/4")}
                                            type="button"
                                            className="btn ">Voltar ao cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {

        // CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepickerBegin').datetimepicker({
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            });
            $('#datetimepickerEnd').datetimepicker({
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            });
            $('#datetimepickerLastRenewal').datetimepicker({
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            });
            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-entity')
                .select2({theme: 'bootstrap'})
                .select2({theme: 'bootstrap'})
                .on('change', this.updateProjectsAndContracts);
            $('#select2-1-state').select2({theme: 'bootstrap'});
            $('#select2-1-createdBy').select2({theme: 'bootstrap'});
            $('#select2-1-requestedBy').select2({theme: 'bootstrap'});
            $('#select2-1-processType').select2({theme: 'bootstrap'});
            $('#select2-1-notify')
                .select2({theme: 'bootstrap'})
                .on('change', function () {
                    self.setState({
                        notify: $(this).val()
                    });
                    console.log("Notify: " + $(this).val());
                });
            $('#select2-1-state').on('change', function () {
                console.log($(this).val());
                self.setState({
                    selectedProcessState: $(this).val()
                });
            });
            $('#select2-1-createdBy').on('change', function () {
                self.setState({
                    createdBy: $(this).val()
                });
            });
            $('#select2-1-requestedBy').on('change', function () {
                self.setState({
                    requestedBy: $(this).val()
                });
            });
            /*      TEM DE SER MULTISELECT
           $('#select2-1-notify').on('change', function() {
                self.setState({ : $(this).val() });
            });*/
            $('#select2-1-processType').on('change', function () {
                self.setState({
                    selectedProcessType: $(this).val()
                });
            });
            $('#select2-1-collaborator')
                .select2({theme: 'bootstrap'})
                .on('change', function () {
                    self.setState({
                        collaborator: $(this).val()
                    });
                    console.log(self.state.collaborator);
                });
            $('#select2-1-association')
                .select2({theme: 'bootstrap'})
                .on('change', function () {
                    self
                        .state
                        .contracts
                        .forEach(contract => {
                            if (contract._id == $(this).val()) {
                                self.setState({selectedContract: contract._id, selectedProject: null});
                                console.log("Contracto:");
                                console.log(self.state);
                            }
                        });
                    if (self.state.selectedProject === null) {
                        self
                            .state
                            .projects
                            .forEach(project => {
                                if (project._id == $(this).val()) {
                                    self.setState({selectedProject: project._id, selectedContract: null});
                                    console.log("Projecto:");
                                    console.log(self.state);
                                }
                            });
                    }

                });

        }
    }
}

export default EditProcess;
