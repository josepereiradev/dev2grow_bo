import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';

class Processes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            processTypes: [],
            processStates: [],
            entities: [],
            users: [],
            contacts: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/processTypes/",)
            .then(function (result) {
                th.setState({processTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processStates/",)
            .then(function (result) {
                th.setState({processStates: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/",)
            .then(function (result) {
                th.setState({entities: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/users/",)
            .then(function (result) {
                th.setState({users: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contacts/",)
            .then(function (result) {
                th.setState({contacts: result.data});
            })

    }

    getDataFromBD() {
        axios
            .get(__APIURL__ + "/processes/")
            .then(res => {
                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({data})
                    setTimeout(function () {
                        var dtProcesses = $('#dtProcesses').dataTable({
                            'pageLength': 100,
                            'retrieve': true, 'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'responsive': true, // https://datatables.net/extensions/responsive/examples/
                            // Text translation options Note the required keywords between underscores (e.g
                            // _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            // Datatable Buttons setup
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                {
                                    extend: 'copy',
                                    className: 'btn-sm',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'csv',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'excel',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'pdf',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'print',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs.keyup(function () {
                            dtProcesses.fnFilter(this.value, columnInputs.index(this));
                        });
                    }.bind(this), 500);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    view(id, entityParent) {
        this
            .props
            .router
            .push('/processes/view/' + entityParent + '/' + id);
    }

    create() {
        this
            .props
            .router
            .push('/processes/create');
    }

    edit(id, entityParent) {
        console.log(entityParent, id)
        this
            .props
            .router
            .push('/processes/edit/' + entityParent + '/' + id);
    }

    delete(name, id, idEntity) {
        var entityParentData;

        var self = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + idEntity)
            .then(function (result) {
                entityParentData = result.data;

                console.log("entityParentData", entityParentData)

                var headers = new Headers({'Content-Type': 'application/json'});

                axios
                    .get(__APIURL__ + "/processes/" + id)
                    .then(function (response) {
                        var process = response.data;
                        if (response.data.interventions.length == 0) {

                            swal({
                                title: "Tem a certeza que quer eliminar?",
                                text: "Após eliminar não poderá recuperar o registo!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Sim",
                                closeOnConfirm: false
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    axios
                                        .delete(__APIURL__ + "/processes/" + id)
                                        .then(function (response) {
                                            if (response.status == 200) {
                                                entityParentData
                                                    .processes
                                                    .splice(entityParentData.processes.indexOf(id), 1);
                                                var updateEntity = JSON.parse(JSON.stringify({
                                                    "nif": entityParentData.nif,
                                                    "qrCode": entityParentData.qrCode,
                                                    "taxName": entityParentData.taxName,
                                                    "abrevName": entityParentData.abrevName,
                                                    "accountManager": entityParentData.accountManager,
                                                    "contacts": entityParentData.contacts,
                                                    "projects": entityParentData.projects,
                                                    "processes": entityParentData.processes,
                                                    "contracts": entityParentData.contracts,
                                                    "associatedUsers": entityParentData.associatedUsers
                                                }));

                                                self.serverRequest = axios
                                                    .put(__APIURL__ + '/entities/' + idEntity, updateEntity, {headers: self.headers})
                                                    .then(function (response2) {})
                                                    .catch(function (error) {
                                                        console.log(error);
                                                    });
                                                console.log("elemento removido da entidade: ", entityParentData.processes);

                                                if (response.status == 200) {
                                                    swal("Eliminado!", process.processNumber + " foi eliminado.", "success");
                                                    self.getDataFromBD();
                                                } else {
                                                    // TODO: HANDLE IT
                                                }
                                            }
                                        })
                                        .catch(function (error) {
                                            console.log(error);
                                            // TODO: HANDLE IT
                                        });
                                }

                            })

                        } else {
                            swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
                        }

                    });

            })
    }

    render() {
        const css = css;
        var rows = [];
        var processT = "";
        var processS;
        var processRequester = "";
        var processCreator = "";
        var entityParent = "";
        var entityParentName = "";

        var th = this;
        this
            .state
            .data
            .forEach((data, j) => {
                //trata de um processo só
                this
                    .state
                    .processTypes
                    .forEach((processType, j) => {
                        //para esse processo verifica o nome da descrição do tipo de contrato
                        if (data.processType == processType._id) {
                            processT = processType.description;
                        }
                    })
                this
                    .state
                    .processStates
                    .forEach((processState, j) => {
                        //para esse processo verifica o nome da descrição do tipo de contrato
                        if (data.state == processState._id) {
                            processS = <span className={processState.stateLabelColor}>{processState.stateName}</span>
                        }
                    })
                this
                    .state
                    .contacts
                    .forEach((requester, j) => {

                        //para esse processo verifica o nome da descrição do tipo de contrato
                        if (data.requestedBy == requester._id) {
                            processRequester = requester.name;
                        }
                    })
                this
                    .state
                    .users
                    .forEach((creator, j) => {

                        //para esse processo verifica o nome da descrição do tipo de contrato
                        if (data.createdBy == creator._id) {
                            processCreator = creator.name;
                        }
                    })

                this
                    .state
                    .entities
                    .forEach((e, i) => {
                        if (e.processes.indexOf(data._id) != -1) {
                            entityParentName = e.taxName;
                            entityParent = e._id;
                        }
                    })

                // o entityParentName é um apontador por isso acaba por ser sempre igual abaixo
                // com o de cima, mesmo que o valor esteja a ser actualizado
                var foo = JSON.stringify(entityParent);
                foo = JSON.parse(foo);

                rows.push(
                    <tr key={data._id}>
                        <td>{data.processNumber}</td>
                        <td>{processT}</td>
                        <td>{entityParentName}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(data.date).toLocaleDateString()}</td>
                        <td>{data.duration}</td>
                        <td>{processRequester}</td>
                        <td>{processCreator}</td>
                        <td>{processS}</td>
                        <td>
                            <button
                                onClick={() => this.view(data._id, foo)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-eye"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.edit(data._id, foo)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(data.processCode, data._id, foo)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                )

                //  }  })
            })

        return (
            <ContentWrapper>
                <h3>Todos os processos
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <Row>
                    <Col lg={12}>
                        <Panel>
                            <button
                                onClick={() => this.create()}
                                type="button"
                                id="addBtn"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                <i className="icon-plus"></i>
                            </button>
                            <Table id="dtProcesses" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Código do processo</th>
                                        <th>Tipo de processo</th>
                                        <th>Cliente</th>
                                        <th>Data</th>
                                        <th>Duração</th>
                                        <th>Pedido por</th>
                                        <th>Criado por</th>
                                        <th>Estado</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>{rows}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default Processes;
