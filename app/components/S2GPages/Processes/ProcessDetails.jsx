import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Tab,
    Tabs,
    Table,
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';
import moment from 'moment';

import css from '../../../styles/tableIcons.scss'

class ProcessDetails extends React.Component {

    constructor() {
        super();
        this.state = {
            process: null,

            date: new Date().toLocaleDateString(),
            description: "Sem descrição",
            createdBy: "",
            requestedBy: "",
            createdByName: "",
            requestedByName: "",
            notify: [""],
            notifyNames: [""],
            selectedProcessType: "",
            selectedProcessState: "",
            selectedProcessTypeName: "",
            selectedProcessStateName: "",
            processNumber: "Ainda não",
            entities: [],
            users: [],
            contacts: [],
            processTypes: [],
            processStates: [],
            entityData: [],

            interventions: [],
            processInterventions: [],
            interventionTypes: [],
            interventionMeans: [],
            contracts: [],
            projects: [],
            key: 1
        };
    }
    handleSelect(key) {
        this.setState({key});
    }

    calcUpdatedProcessTime(processTime, interventionTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(interventionTime.substring(0, interventionTime.indexOf(':')));
        var minutesInterv = parseInt(interventionTime.split(":").pop());

        var hoursProcess = parseInt(processTime.substring(0, processTime.indexOf(':')));
        var minutesProcess = parseInt(processTime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }
    calcSumBetweenA_B(ATime, BTime) {
        //ATime: '10:00' BTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesInterv + minutesProcess;
        var updatedHours = hoursInterv + hoursProcess;

        if (updatedMinutes >= 60) {
            updatedMinutes = updatedMinutes - 60;
            updatedHours++;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }
    calcDifBetweenA_B(ATime, BTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;
        var idEntity = this.props.params.idEntity;

        this.serverRequest = axios
            .get(__APIURL__ + "/processes/" + id)
            .then(function (processResult) {

                th.setState({
                    process: processResult.data,
                    date: new Date(processResult.data.date).toLocaleDateString(),
                    description: processResult.data.description,
                    //   createdBy: processResult.data.createdBy,  requestedBy:
                    // processResult.data.requestedBy,
                    notify: processResult.data.notify,
                    // selectedProcessType: processResult.data.processType, selectedProcessState:
                    // processResult.data.state,
                    processNumber: processResult.data.processNumber
                });

                th.serverRequest = axios
                    .get(__APIURL__ + "/processTypes/" + processResult.data.processType)
                    .then(function (result) {
                        th.setState({selectedProcessTypeName: result.data.description})
                    })
                th.serverRequest = axios
                    .get(__APIURL__ + "/processStates/" + processResult.data.state)
                    .then(function (result) {
                        th.setState({selectedProcessStateName: result.data.stateName})
                    })
                th.serverRequest = axios
                    .get(__APIURL__ + "/contacts/" + processResult.data.requestedBy)
                    .then(function (result) {
                        th.setState({requestedByName: result.data.name})
                    })
                th.serverRequest = axios
                    .get(__APIURL__ + "/contacts/")
                    .then(function (result) {
                        th.setState({contacts: result.data})
                    })
                th.serverRequest = axios
                    .get(__APIURL__ + "/users/" + processResult.data.createdBy)
                    .then(function (result) {
                        th.setState({createdByName: result.data.name})
                    })

                //tratamento das intervenções:
                th.serverRequest = axios
                    .get(__APIURL__ + "/interventions/")
                    .then(function (result) {

                        var intervOfProcess = []
                        processResult
                            .data
                            .interventions
                            .forEach(intervProcessID => {
                                result
                                    .data
                                    .forEach(interv => {
                                        if (intervProcessID == interv._id) {
                                            intervOfProcess.push(interv);
                                        }
                                    })

                            })
                        th.setState({interventions: result.data, processInterventions: intervOfProcess})
                        setTimeout(function () {
                            $.fn.dataTableExt.oSort["customProcess-desc"] = function(x, y) {
                                return moment(x, 'DD-MM-YYYY').valueOf() < moment(y, 'DD-MM-YYYY').valueOf();
                            };
                              
                            $.fn.dataTableExt.oSort["customProcess-asc"] = function(x, y) {
                                return moment(x, 'DD-MM-YYYY').valueOf() > moment(y, 'DD-MM-YYYY').valueOf();
                            }

                            var dtInterventions = $('.dtInterventions').dataTable({
                                'retrieve': true, 'paging': true, // Table pagination
                                'ordering': true, // Column ordering
                                'info': true, // Bottom left status text
                                'responsive': true, // https://datatables.net/extensions/responsive/examples/
                                // Text translation options Note the required keywords between underscores (e.g
                                // _MENU_)
                                oLanguage: {
                                    sSearch: 'Search all columns:',
                                    sLengthMenu: '_MENU_ records per page',
                                    info: 'Showing page _PAGE_ of _PAGES_',
                                    zeroRecords: 'Nothing found - sorry',
                                    infoEmpty: 'No records available',
                                    infoFiltered: '(filtered from _MAX_ total records)'
                                },
                                "aoColumnDefs": [{
                                    "sType": "customProcess",
                                    "bSortable": true,
                                    "aTargets": [3]
                                }],
                            });
                            var inputSearchClass = 'datatable_input_col_search';
                            var columnInputs = $('tfoot .' + inputSearchClass);

                            // On input keyup trigger filtering
                            columnInputs.keyup(function () {
                                dtInterventions.fnFilter(this.value, columnInputs.index(this));
                            });
                        })

                    });

            })

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + idEntity)
            .then(function (result) {
                th.setState({entityData: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({users: result.data})
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/interventionTypes/")
            .then(function (result) {
                th.setState({interventionTypes: result.data})
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/interventionMeans/")
            .then(function (result) {
                th.setState({interventionMeans: result.data})
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projects/")
            .then(function (result) {
                th.setState({projects: result.data})
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contracts/")
            .then(function (result) {
                th.setState({contracts: result.data})
            })
    }

    view(id) {
        this
            .props
            .router
            .push('/interventions/view/' + this.props.params.idEntity + '/' + this.props.params.id + '/' + id);
    }

    create() {
        this
            .props
            .router
            .push('/interventions/create/' + this.props.params.idEntity + '/' + this.props.params.id);
    }

    edit(id) {
        this
            .props
            .router
            .push('/interventions/edit/' + this.props.params.idEntity + '/' + this.props.params.id + '/' + id + '/1');
    }

    delete(name, id) {
        var self = this;
        swal({
            title: "Tem a certeza que quer eliminar?",
            text: "Após eliminar não poderá recuperar o registo!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            closeOnConfirm: true
        }, function () {

            axios
                .get(__APIURL__ + "/interventions/" + id)
                .then(function (intervention) {

                    self
                        .state
                        .process
                        .interventions
                        .forEach(intervID => {
                            if (intervID == id) {

                                var updateProcess = {
                                    processNumber: self.state.process.processNumber,
                                    date: (self.state.process.date == null
                                        ? moment().format()
                                        : self.state.process.date),
                                    description: self.state.process.description,
                                    entity: self.state.process.entity,
                                    createdBy: self.state.process.createdBy,
                                    requestedBy: self.state.process.requestedBy,
                                    notify: self.state.process.notify,
                                    //contract: process.contract, project: process.project,
                                    processType: self.state.process.processType,
                                    state: self.state.process.state,
                                    duration: self.state.process.duration,
                                    interventions: self.state.process.interventions
                                };

                                if (!intervention.data.toBeValidated) {
                                    updateProcess.duration = self.calcUpdatedProcessTime(self.state.process.duration, moment(intervention.data.timeToCharge).format("HH:mm"));
                                }

                                var newInterventionsList = [];

                                updateProcess
                                    .interventions
                                    .forEach(interv => {
                                        if (interv != id) {
                                            newInterventionsList.push(interv);
                                        }
                                    })
                                updateProcess.interventions = newInterventionsList;

                                axios
                                    .put(__APIURL__ + '/processes/' + self.state.process._id, updateProcess, {headers: self.headers})
                                    .then(function (response) {
                                        axios
                                            .delete(__APIURL__ + "/interventions/" + id)
                                            .then(function (response) {
                                                var interventionsLeft = self.state.processInterventions;

                                                // if () { caso nao tenha sido validada, nao deve descontar nos contratos
                                                self
                                                    .state
                                                    .processInterventions
                                                    .forEach(interv => {

                                                        if (interv._id == id) {
                                                            interventionsLeft.pop(interv);

                                                            if (interv.contractToCharge != null && !intervention.data.toBeValidated) {

                                                                self
                                                                    .state
                                                                    .contracts
                                                                    .forEach(contract => {

                                                                        if (contract._id == interv.contractToCharge) {

                                                                            // console.log(self.calcSumBetweenA_B(contract.availableHours,
                                                                            // intervention.data.timeToCharge));
                                                                            // console.log(self.calcDifBetweenA_B(contract.exhaustedHours,
                                                                            // intervention.data.timeToCharge));
                                                                            var updateContract = JSON.parse(JSON.stringify({
                                                                                "beginDate": (contract.beginDate == null
                                                                                    ? new Date()
                                                                                    : contract.beginDate),
                                                                                "endDate": (contract.endDate == null
                                                                                    ? new Date()
                                                                                    : contract.endDate),
                                                                                "lastRenewal": (contract.lastRenewal == null
                                                                                    ? null
                                                                                    : contract.lastRenewal),
                                                                                "notifyRenewal": contract.notifyRenewal,
                                                                                "hours": contract.hours,
                                                                                "perMonth": contract.perMonth,
                                                                                "notifyExhaustedHours": contract.notifyExhaustedHours,
                                                                                "blockExhaustedHours": contract.blockExhaustedHours,
                                                                                "contractType": contract.contractType,
                                                                                "state": contract.state,
                                                                                "addenda": contract.addenda,
                                                                                "contractCode": contract.contractCode,
                                                                                "availableHours": self.calcSumBetweenA_B(contract.availableHours, moment(intervention.data.timeToCharge).format("HH:mm")),
                                                                                "exhaustedHours": self.calcDifBetweenA_B(contract.exhaustedHours, moment(intervention.data.timeToCharge).format("HH:mm")),
                                                                                "observations": contract.observations
                                                                            }));

                                                                            updateProcess._id = self.state.process._id;
                                                                            axios
                                                                                .put(__APIURL__ + '/contracts/' + contract._id, updateContract, {headers: self.headers})
                                                                                .then(function (response) {
                                                                                    if (response.status == 200) {
                                                                                        swal("Eliminado!", "Intervenção eliminada.", "success");

                                                                                        self.setState({
                                                                                            processInterventions: interventionsLeft || [],
                                                                                            process: updateProcess,
                                                                                            key: 1
                                                                                        });

                                                                                    }
                                                                                })
                                                                                .catch(function (error) {
                                                                                    console.log(error);
                                                                                });

                                                                        }
                                                                    });

                                                            } else {
                                                                if (interv.projectToCharge != null && !intervention.data.toBeValidated) {

                                                                    self
                                                                        .state
                                                                        .projects
                                                                        .forEach(project => {
                                                                            if (project._id == interv.projectToCharge) {

                                                                                // console.log(self.calcSumBetweenA_B(contract.availableHours,
                                                                                // intervention.data.timeToCharge));
                                                                                // console.log(self.calcDifBetweenA_B(contract.exhaustedHours,
                                                                                // intervention.data.timeToCharge));
                                                                                var updateProject = JSON.parse(JSON.stringify({
                                                                                    "beginDate": (project.beginDate == null
                                                                                        ? new Date()
                                                                                        : project.beginDate),
                                                                                    "endDate": (project.endDate == null
                                                                                        ? null
                                                                                        : project.endDate),
                                                                                    "hours": project.hours,
                                                                                    "notifyExhaustedHours": project.notifyExhaustedHours,
                                                                                    "blockExhaustedHours": project.blockExhaustedHours,
                                                                                    "projectType": project.projectType,
                                                                                    "state": project.state,
                                                                                    "projectCode": project.projectCode,
                                                                                    "availableHours": project.availableHours,
                                                                                    "exhaustedHours": self.calcDifBetweenA_B(project.exhaustedHours, moment(intervention.data.timeToCharge).format("HH:mm"))
                                                                                }));

                                                                                axios
                                                                                    .put(__APIURL__ + '/projects/' + project._id, updateProject, {headers: self.headers})
                                                                                    .then(function (response) {
                                                                                        console.log(response)

                                                                                    })
                                                                                    .catch(function (error) {
                                                                                        console.log(error);
                                                                                    });

                                                                            }
                                                                        });

                                                                }
                                                            }

                                                        }
                                                    })
                                                updateProcess._id = self.state.process._id;
                                                self.setState({
                                                    processInterventions: interventionsLeft || [],
                                                    process: updateProcess
                                                });

                                                swal("Eliminado!", "Intervenção eliminada.", "success");

                                            })
                                            .catch(function (error) {
                                                console.log(error);
                                            });

                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });

                            }
                        });

                });

        });
    }

    renderInterventionsTable() {

        var th = this;
        var interventionRows = []
        var interventionT = "";
        var interventionM = "";
        var collaboratorName = "";
        var toCharge = "";
        var offerSymbol;
        var complaintSymbol;
        //   var validatedSymbol = <em className="fa fa-close"></em>;   var
        // terminateSymbol = <em className="fa fa-close"></em>; Popular a linha de
        // registos das intervenções
        this
            .state
            .processInterventions
            .forEach((processInterv, i) => {

                th
                    .state
                    .interventionTypes
                    .forEach((intervT, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (processInterv.interventionType == intervT._id) {

                            interventionT = intervT.description;
                        }
                    })
                th
                    .state
                    .interventionMeans
                    .forEach((intervM, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (processInterv.means == intervM._id) {

                            interventionM = intervM.description;
                        }
                    })
                th
                    .state
                    .users
                    .forEach((collaborator, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (processInterv.collaborator == collaborator._id) {

                            collaboratorName = collaborator.name;
                        }
                    })

                th
                    .state
                    .contracts
                    .forEach((c, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (processInterv.contractToCharge == c._id) {

                            toCharge = c.contractCode;
                        }
                    })
                th
                    .state
                    .projects
                    .forEach((p, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (processInterv.projectToCharge == p._id) {

                            toCharge = p.projectCode;
                        }
                    })

                if (processInterv.offer == true) {
                    offerSymbol = <em className="fa fa-check"></em>
                } else {
                    offerSymbol = <em className="fa fa-close"></em>
                }
                if (processInterv.complaint == true) {
                    complaintSymbol = <em className="fa fa-check"></em>
                } else {
                    complaintSymbol = <em className="fa fa-close"></em>;
                }
                var toBeValidated;
                if (processInterv.toBeValidated) {
                    toBeValidated = <em className="fa fa-close"></em>
                } else {
                    toBeValidated = <em className="fa fa-check"></em>
                }

                //para esse contacto faz o push para contact rows
                interventionRows.push(
                    <tr key={processInterv._id} className="active">
                        <td>{processInterv.interventionNumber}</td>
                        <td>{interventionT}</td>
                        <td>{collaboratorName}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{moment.utc(processInterv.date).format('DD-MM-YYYY')}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(processInterv.beginHour)
                                .toLocaleTimeString()
                                .substr(0, 5)}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(processInterv.endHour)
                                .toLocaleTimeString()
                                .substr(0, 5)}</td>
                        <td>{moment(processInterv.totalTime).format("HH:mm")}</td>

                        <td>{moment(processInterv.timeToCharge).format("HH:mm")}</td>
                        <td>{toBeValidated}</td>
                        {/* <td>{offerSymbol}</td>*/}
                        {/* <td>{complaintSymbol}</td>*/}
                        <td>
                            <button
                                onClick={() => this.view(processInterv._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-eye"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.edit(processInterv._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(processInterv.interventionNumber, processInterv._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                );

            })

        return interventionRows;
    };

    render() {

        var notifyNames = "";

        this
            .state
            .notify
            .forEach((notify, i) => {

                this
                    .state
                    .contacts
                    .forEach(contact => {
                        if (contact._id == notify) {

                            if (i + 1 == this.state.notify.length) {
                                notifyNames = notifyNames + contact.name;
                            } else {
                                notifyNames = notifyNames + contact.name + ", ";
                            }

                        }
                    });

            });
        var tableInterventions = null;

        // tratamentos dos intervenções if (this.state.processInterventions.length != 0)
        // {
        tableInterventions = <Table id="tableToRemove" className="dtInterventions">
            <thead>
                <tr className="active">
                    <th>Número de intervenção</th>
                    <th>Tipo de intervenção</th>
                    <th>Colaborador</th>
                    <th>Data</th>
                    <th>Hora de início</th>
                    <th>Hora de fim</th>
                    <th>Tempo real</th>
                    <th>Tempo cobrado</th>
                    <th>Validado</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {this.renderInterventionsTable()}
            </tbody>
        </Table>
        // } else {     tableInterventions.push(         <Col lg={12}> <b>Este processo
        // não tem intervenções</b>         </Col>     ) }

        return (
            <ContentWrapper>
                <h3>Processo de {this.state.entityData.taxName}
                </h3 >
                <Col>
                    {/* START panel */}
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <div className="panel-title">Detalhes do processo</div>
                        </div>
                        <div className="panel-body">
                            <Row>
                                <Col lg={5}>
                                    <div className="form-group">
                                        <label className="control-label">Número do processo:&nbsp;</label>{this.state.processNumber}
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Estado:&nbsp;</label>{this.state.selectedProcessStateName}
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Tipo de processo:&nbsp;</label>{this.state.selectedProcessTypeName}
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Data:&nbsp;</label>{this.state.date}
                                    </div>

                                </Col>
                                <Col lg={5}>
                                    <div className="form-group">
                                        <label className="control-label">Descrição:&nbsp;</label>{this.state.description}
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Pedido por:&nbsp;</label>{this.state.requestedByName}
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Criado por:&nbsp;</label>{this.state.createdByName}
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Contactos a serem notificados:&nbsp;</label>{notifyNames}
                                    </div>

                                </Col>
                            </Row>
                            <Row>
                                <Col lg={10}>
                                    <div className="form-group">
                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/4")}
                                            className="btn ">Voltar ao cliente</button>
                                    </div>
                                </Col>
                            </Row>
                        </div>

                        {/* END panel */}
                        {/* START tabs */}
                        <Tabs
                            activeKey={this.state.key}
                            onSelect={this
                            .handleSelect
                            .bind(this)}
                            justified
                            id="tabID">

                            <Tab eventKey={1} title="Intervenções">
                                {/* data table de intervenções */}
                                <button
                                    onClick={() => this.create()}
                                    type="button"
                                    id="addBtn"
                                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                    <i className="icon-plus"></i>
                                </button>
                                <Row>
                                    {tableInterventions}
                                </Row>
                            </Tab>

                        </Tabs>
                        {/* END tabs */}

                    </div>
                </Col>
            </ContentWrapper>

        );
    }
}

export default ProcessDetails;