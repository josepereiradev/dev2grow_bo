import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import Moment from 'moment';
import { Router, Route, Link, History, withRouter } from 'react-router';
import { browserHistory } from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class AddProcessFromList extends React.Component {
    constructor() {
        super();
        console.log(this)
        this.state = {
            date: new Date(),
            subject: "",
            description: "",
            collaborator: null,
            // selectedEntity: "593e80efecf41060c1bd0365",
            createdBy: sessionStorage.getItem('user')
                ? JSON
                    .parse(sessionStorage.getItem('user'))
                    .id
                : '',
            requestedBy: "",
            notify: [],
            selectedProcessType: "",
            selectedProcessState: "",
            entities: [],
            selectedEntity: "", //[object]
            users: [],
            contacts: [],
            contracts: [],
            customerContracts: [],
            //selectedContract: null,
            projects: [],
            customerProjects: [],
            //selectedProject: null,
            processTypes: [],
            processStates: [],
            entityData: []
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);

        this.updateProjectsAndContracts = this
            .updateProjectsAndContracts
            .bind(this);
    }

    handleChange(name, e) {

        console.log("name: ", [e.target.name], "value: " + e.target.value)
        var th = this;
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit(e) {

        var th = this;
        const {
            // date, subject, description, selectedEntity, createdBy, requestedBy, notify,
            // selectedProcessType, selectedProcessState, selectedContract, attachments,
            // entityData
            collaborator,
            date,
            description,
            selectedEntity,
            createdBy,
            requestedBy,
            notify,
            selectedProcessType,
            selectedProcessState,
            //selectedContract, selectedProject,
            entityData
        } = this.state;

        var headers = new Headers({ 'Content-Type': 'application/json' });
        var newProcess = JSON.parse(JSON.stringify({
            // "date": (date == null     ? new Date().toISOString()     : new
            // Date(date).toISOString()), "subject": subject, "description": description,
            // "entity": entityData._id, "createdBy": createdBy, "requestedBy": requestedBy,
            // "attachments": attachments, "notify": notify, //  "contract":
            // selectedContract, "processType": selectedProcessType, "state":
            // selectedProcessState
            "collaborator": collaborator,
            "date": (date == null
                ? new Date().toISOString()
                : new Date(date).toISOString()),
            "description": description,
            "entity": entityData._id,
            "createdBy": createdBy,
            "requestedBy": requestedBy,
            "notify": notify,
            //"contract": selectedContract, "project": selectedProject,
            "processType": selectedProcessType,
            "state": selectedProcessState,
            "interventions": []
        }));

        var id = this.props.params.idEntity;

        e.preventDefault();

        this.serverRequest = axios
            .post(__APIURL__ + '/processes/', newProcess, { headers: this.headers })
            .then(function (response) {

                if (entityData.processes == null) {
                    entityData.processes = ["" + response.data.obj._id + ""]

                } else {
                    entityData
                        .processes
                        .push(response.data.obj._id);
                }

                var updateEntity = JSON.parse(JSON.stringify({
                    "nif": entityData.nif,
                    "qrCode": entityData.qrCode,
                    "taxName": entityData.taxName,
                    "abrevName": entityData.abrevName,
                    "accountManager": entityData.accountManager,
                    "contacts": entityData.contacts,
                    "projects": entityData.projects,
                    "processes": entityData.processes,
                    "contracts": entityData.contracts,
                    "associatedUsers": entityData.associatedUsers
                }));

                console.log(updateEntity);

                th.serverRequest = axios
                    .put(__APIURL__ + '/entities/' + selectedEntity._id, updateEntity, { headers: th.headers })
                    .then(function (response) {
                        th
                            .props
                            .router
                            .push('/customers/view/' + selectedEntity._id + "/4");
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                console.log("processo adicionado à entidade: ", th.state.entityData.process);

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    componentWillMount() {
        var th = this;
        var id = this.props.params.idEntity;

        this.serverRequest = axios
            .get(__APIURL__ + "/processTypes/")
            .then(function (result) {
                th.setState({ processTypes: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processStates/")
            .then(function (result) {
                th.setState({ processStates: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contacts/")
            .then(function (result) {
                th.setState({ contacts: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contracts/")
            .then(function (result) {
                th.setState({ contracts: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/projects/")
            .then(function (result) {
                th.setState({ projects: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({ users: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/")
            .then(function (result) {
                th.setState({ entities: result.data });
            })
    }

    componentDidMount() {

        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider)
            $('[data-ui-slider]').bootstrapSlider();

        // CHOSEN -----------------------------------

        if ($.fn.chosen)
            $('.chosen-select').chosen();

        // MASKED -----------------------------------

        if ($.fn.inputmask)
            $('[data-masked]').inputmask();

        // FILESTYLE -----------------------------------

        if ($.fn.filestyle)
            $('.filestyle').filestyle();

        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg)
            $('.wysiwyg').wysiwyg();

        // Tags -----------------------------------
        if ($.fn.tagsinput)
            $("[data-role='tagsinput']").tagsinput()

        // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepickerBegin')
                .datetimepicker({
                    defaultDate: new Date(),
                    format: "DD/MM/YYYY HH:mm",
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })

                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerBegin')
                        .data("DateTimePicker")
                        .date();
                    this.setState({ date: dateSelected });
                    console.log("Date: " + dateSelected);
                }.bind(this));
        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-state')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        selectedProcessState: $(this).val()
                    });
                    console.log(self.state.selectedProcessState);
                });
            $('#select2-1-createdBy')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        createdBy: $(this).val()
                    });
                });
            $('#select2-1-collaborator')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {

                    if ($(this).val() == "") {
                        self.setState({ collaborator: null });
                        console.log(self.state.collaborator);
                    } else {
                        self.setState({
                            collaborator: $(this).val()
                        });
                        console.log(self.state.collaborator);
                    }

                });
            $('#select2-1-requestedBy')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        requestedBy: $(this).val()
                    });
                });
            $('#select2-1-processType')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        selectedProcessType: $(this).val()
                    });
                    console.log(self.state.selectedProcessType);
                });

            $('#select2-1-entity')
                .select2({ theme: 'bootstrap' })
                .on('change', this.updateProjectsAndContracts);
            /*  $('#select2-1-entity')
                .select2({theme: 'bootstrap'})
                .on('change', function () {
                    self.serverRequest = axios
                        .get(__APIURL__ + "/entities/" + $(this).val())
                        .then(function (result) {
                            self.setState({entityData: result.data});
                            console.log(result.data);
                            self.updateProjectsAndContracts();
 
                        })
                });*/
            $('#select2-1-notify')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        notify: $(this).val()
                    });
                    console.log("Notify: " + $(this).val());
                    console.log(self.state);
                });

            $('#select2-1-requestedBy')
            /*      TEM DE SER MULTISELECT
           $('#select2-1-notify').on('change', function() {
                self.setState({ : $(this).val() });
            });*/
        }
    }

    updateProjectsAndContracts(e) {
        var entityId = e.target.value;
        var th = this;
        var contractsOfCustomer = [];
        var projectsOfCustomer = [];
        th.serverRequest = axios
            .get(__APIURL__ + "/entities/" + entityId)
            .then(function (result) {

                var contractsReceived = result.data.contracts;
                var projectsReceived = result.data.projects;

                //Filtrar os contratos que pertencem ao cliente
                contractsReceived.forEach(contractID => {

                    th
                        .state
                        .contracts
                        .forEach(contract => {

                            if (contractID == contract._id) {
                                contractsOfCustomer.push(contract);
                            }
                        });
                });

                projectsReceived.forEach(projectID => {

                    th
                        .state
                        .projects
                        .forEach(project => {

                            if (projectID == project._id) {
                                projectsOfCustomer.push(project);
                            }
                        });

                });

                th.setState({ customerContracts: contractsOfCustomer, customerProjects: projectsOfCustomer, selectedEntity: result.data, entityData: result.data });
                $('#select2-1-association').off();
                $('#select2-1-association')
                    .select2({ theme: 'bootstrap' })
                    .on('change', function () {

                        th
                            .state
                            .contracts
                            .forEach(contract => {
                                if (contract._id == $(this).val()) {
                                    th.setState({ selectedContract: contract._id, selectedProject: null });
                                }
                            });
                        if (th.state.selectedProject === null) {
                            if (th.state.projects.message != "NO_RESULTS") {
                                th
                                    .state
                                    .projects
                                    .forEach(project => {
                                        if (project._id == $(this).val()) {
                                            th.setState({ selectedProject: project._id, selectedContract: null });
                                        }
                                    });
                            }

                        }
                    });

            });
    }

    render() {

        var processTypeList = [];
        var processStateList = [];
        var contactsList = [];
        var projectOrContractList = [];
        //    var contractsList = [];
        var usersList = [];
        var collaboratorList = [];
        var entitiesList = [];

        if (this.state.processTypes[0] != undefined)
            this.state.processTypes.forEach((item, i) => {
                if (i == 0) {
                    processTypeList.push(
                        <option disabled defaultValue value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                processTypeList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })
        if (this.state.processStates[0] != undefined)
            this.state.processStates.forEach((item, i) => {
                if (i == 0) {
                    processStateList.push(
                        <option disabled defaultValue value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                processStateList.push(
                    <option key={i.toString()} value={item._id}>{item.stateName}</option>
                );
            })
        if (this.state.contacts[0] != undefined)
            this.state.contacts.forEach((item, i) => {

                if (i == 0) {
                    contactsList.push(
                        <option disabled defaultValue key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                if (this.state.selectedEntity != "") {
                    this
                        .state
                        .selectedEntity
                        .contacts
                        .forEach((customerContact, j) => {
                            if (customerContact == item._id) {
                                contactsList.push(
                                    <option key={i.toString() + j.toString()} value={item._id}>{item.name}</option>
                                );
                            }
                        });
                }

            })
        /*        this
            .state
            .contracts
            .forEach((item, i) => {
                contractsList.push(
                    <option key={i.toString()} value={item._id}>{item.contractCode}</option>
                );
            })
        */
        if (this.state.users[0] != undefined)
            this.state.users.forEach((item, i) => {

                if (i == 0) {
                    usersList.push(
                        <option disabled defaultValue value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                    collaboratorList.push(
                        <option value={""} key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                if (item.internalUser) {
                    usersList.push(
                        <option key={i.toString()} value={item._id}>{item.name}</option>
                    );
                    collaboratorList.push(
                        <option key={i.toString()} value={item._id}>{item.name}</option>
                    );
                }

            })

        if (this.state.entities[0] != undefined)
            this.state.entities.forEach((item, i) => {

                if (i == 0) {
                    entitiesList.push(
                        <option disabled defaultValue value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }

                entitiesList.push(
                    <option
                        key={item
                            ._id
                            .toString()}
                        value={item._id}>{item.taxName}</option>
                );
            });

        var contractsGroup = [];
        for (var i = 0; i < this.state.customerContracts.length; i++) {
            contractsGroup.push(
                <option key={i} value={this.state.customerContracts[i]._id}>{this.state.customerContracts[i].contractCode}</option>
            );
        }

        var projectsGroup = [];
        for (var i = 0; i < this.state.customerProjects.length; i++) {
            projectsGroup.push(
                <option key={i} value={this.state.customerProjects[i]._id}>{this.state.customerProjects[i].projectCode}</option>
            );
        }

        //value ou defaultValue nao funcionam
        projectOrContractList.push(
            <option disabled selected key={-1}>
                --Escolha uma opção--
            </option>
        );
        projectOrContractList.push(
            <optgroup key={-2} label="Contratos">
                {contractsGroup}
            </optgroup>
        );
        projectOrContractList.push(
            <optgroup key={-3} label="Projetos">
                {projectsGroup}
            </optgroup>
        );

        if (this.state.selectedEntity === "") {
            var isNothingSelected = true;
        } else {
            var isNothingSelected = false;
        }

        return (
            <ContentWrapper>
                <h3>Processo
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar um novo processo</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Cliente*</label>
                                            <FormControl
                                                id="select2-1-entity"
                                                componentClass="select"
                                                name="selectedEntity"
                                                className="form-control m-b"
                                                required="required"
                                                value={this.state.selectedEntity}
                                                onChange={this
                                                    .updateProjectsAndContracts
                                                    .bind(this, 'gender')}>
                                                {entitiesList}

                                            </FormControl>
                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de Processo*</label>
                                            <FormControl
                                                id="select2-1-processType"
                                                componentClass="select"
                                                name="selectedProcessType"
                                                className="form-control m-b"
                                                required="required"
                                                value={this.state.selectedProcessType}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'selectedProcessType')}>
                                                {processTypeList}

                                            </FormControl>
                                        </div>
                                        {/*<div className="col-sm-6">
                                            <label className="control-label">Associar a*</label>
                                            <FormControl
                                                disabled={isNothingSelected}
                                                id="select2-1-association"
                                                componentClass="select"
                                                name="association"
                                                className="form-control m-b"
                                                required="required">
                                                {projectOrContractList}

                                            </FormControl>
                                        </div>*/}

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Estado do Processo*</label>
                                            <FormControl
                                                id="select2-1-state"
                                                componentClass="select"
                                                name="selectedProcessState"
                                                className="form-control m-b"
                                                required="required"
                                                value={this.state.selectedProcessState}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'selectedProcessState')}>
                                                {processStateList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Data*</label>
                                            <div id="datetimepickerBegin" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    className="form-control"
                                                    required="required"
                                                    onClick={this
                                                        .handleChange
                                                        .bind(this, 'date')} />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Criado por*</label>
                                            <FormControl
                                                id="select2-1-createdBy"
                                                componentClass="select"
                                                name="createdBy"
                                                required="required"
                                                className="form-control m-b"
                                                value={this.state.createdBy}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'createdBy')}>
                                                {usersList}

                                            </FormControl>

                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Pedido por*</label>
                                            <FormControl
                                                id="select2-1-requestedBy"
                                                componentClass="select"
                                                name="requestedBy"
                                                className="form-control m-b"
                                                required="required"
                                                value={this.state.requestedBy}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'requestedBy')}>
                                                {contactsList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Técnico</label>
                                            <FormControl
                                                id="select2-1-collaborator"
                                                componentClass="select"
                                                name="collaborator"
                                                required="required"
                                                className="form-control m-b"
                                                value={this.state.collaborator}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'collaborator')}>
                                                {collaboratorList}

                                            </FormControl>

                                        </div>

                                        <div className="col-sm-6">
                                            <label className="control-label">Notificar</label>
                                            <FormControl
                                                id="select2-1-notify"
                                                multiple="multiple"
                                                componentClass="select"
                                                name="selectedContact"
                                                className="form-control m-b"
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, 'selectedContact')}>
                                                {contactsList}

                                            </FormControl>
                                        </div>

                                    </Row>
                                </div>
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Descrição*</label>
                                            <textarea
                                                name="description"
                                                className="form-control"
                                                required="required"
                                                rows="2"
                                                value={this.state.description}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, "description")} />

                                        </div>
                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/processes')}
                                            type="button"
                                            className="btn ">Voltar aos processos</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
}

export default AddProcessFromList;
