import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    Table
} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';

class Templates extends React.Component {
    constructor() {
        super();
        this.state = {
            data: [],
            showPanel: false
        }
    }

    componentDidMount() {
        // this.getDataFromBD();
        setTimeout(function () {
            if (this.refs.listPanel) {
                this.setState({showPanel: true});
                $('#loader').hide();
            }
        }.bind(this), 1500);
    }

    componentWillMount() {
        this.getDataFromBD();
    }

    componentWillUnmount() {
        var destroy = {
            destroy: true
        };
        $('#dtTemplates').dataTable(destroy);
    }

    getDataFromBD() {
        var self = this;

        axios
            .get(__APIURL__ + "/templates/")
            .then(function (result) {

                self.setState({data: result.data});

                setTimeout(function () {
                    //
                    // Filtering by Columns
                    //
                    var dtTemplates = $('#dtTemplates').dataTable({
                        'retrieve': true, 'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'responsive': true, // https://datatables.net/extensions/responsive/examples/
                        // Text translation options Note the required keywords between underscores (e.g
                        // _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs.keyup(function () {
                        dtTemplates.fnFilter(this.value, columnInputs.index(this));
                    });
                }.bind(this), 500);
            })
    }

    edit(id) {
        this
            .props
            .router
            .push('/templates/edit/' + id);
    }

    render() {
        const css = css;

        var rows = [];
        this
            .state
            .data
            .forEach((item, i) => {
                rows.push(
                    <tr className="gradeA" key={i.toString()}>
                        <td>{item.name}</td>
                        <td>{item.subject}</td>
                        <td>
                            <button
                                onClick={() => this.edit(item._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>

                        </td>
                    </tr>
                );
            });

        return (
            <ContentWrapper>
                <h3>Templates de Email
                </h3>

                <Panel
                    ref="listPanel"
                    className={this.state.showPanel
                    ? 'show-panel'
                    : 'hide-panel'}>
                    <style>
                        {
                            css
                        }</style>

                    <Table id="dtTemplates" responsive striped hover>
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Assunto</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {rows}
                        </tbody>
                    </Table>
                </Panel>

            </ContentWrapper>

        );
    }

}

export default Templates;
