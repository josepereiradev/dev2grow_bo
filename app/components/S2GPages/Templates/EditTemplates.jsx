import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import { Router, Route, Link, History, withRouter } from 'react-router';


import css from '../../../styles/tableIcons.scss'

class EditTemplate extends React.Component {
    constructor() {
        super();

        this.state = {
            name: '',
            subject: '',
            text: ''

        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
		$('#loader').hide();
        var self = this;

        axios
            .get(__APIURL__ + "/templates/"+this.props.params.id)
            .then(function (result) {
                console.log(result.data);
                        self.setState({
                            name: result.data.name,
                            subject: result.data.subject,
                            text: result.data.text

                        });

                        if($.fn.wysiwyg) {
                            $('.wysiwyg').wysiwyg();
                            $('.wysiwyg').html(result.data.html)

                        }
               
        });
    }

    handleChange(event) {
        console.log(event.target.name);
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();

        var self = this;
        const {subject, text} = this.state;
        var id = this.props.params.id;

        var headers = new Headers({'Content-Type': 'application/json'});
        var updateTemplate = JSON.parse(JSON.stringify({"subject": subject, "text": text, "html": $('.wysiwyg').html() }));

        axios
            .put(__APIURL__ + '/templates/' + id, updateTemplate, {headers: this.headers})
            .then(function (response) {
                self.props.router.push('/templates');
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <ContentWrapper>
                <h3>Template</h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editing: {this.state.name}</div>
                            </div>
                            <div className="panel-body">
                                <Row>
                                    <div className="col-sm-12">
                                        <label className="control-label">Assunto</label>
                                        <input type="text" name="subject" className="form-control" value={this.state.subject} onChange={this.handleChange} />
                                    </div>
                                </Row>
                                <br />
                                <Row>
                                    <div className="col-sm-12">
                                        <label className="control-label">Texto</label>
                                        <textarea rows="10" className="form-control" name="text" value={this.state.text} onChange={this.handleChange}></textarea>
                                    </div>
                                </Row>
                                <br />
                                <Row>
                                    <div className="col-sm-12">
                                        <label className="control-label">Html</label>
                                        <Col>
                                            <div data-role="editor-toolbar" data-target="#editor" className="btn-toolbar btn-editor">
                                                <div className="btn-group dropdown">
                                                    <a data-toggle="dropdown" title="Font" className="btn btn-default">
                                                        <em className="fa fa-font"></em><b className="caret"></b>
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li><a href="" data-edit="fontName Arial" style={ {    fontFamily: 'Arial'} }>Arial</a>
                                                        </li>
                                                        <li><a href="" data-edit="fontName Sans" style={ {    fontFamily: 'Sans'} }>Sans</a>
                                                        </li>
                                                        <li><a href="" data-edit="fontName Serif" style={ {    fontFamily: 'Serif'} }>Serif</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="btn-group dropdown">
                                                    <a data-toggle="dropdown" title="Font Size" className="btn btn-default">
                                                        <em className="fa fa-text-height"></em>&nbsp;<b className="caret"></b>
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li><a href="" data-edit="fontSize 5" style={ {    fontSize: "24px"} }>Huge</a>
                                                        </li>
                                                        <li><a href="" data-edit="fontSize 3" style={ {    fontSize: "18px"} }>Normal</a>
                                                        </li>
                                                        <li><a href="" data-edit="fontSize 1" style={ {    fontSize: "14px"} }>Small</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="btn-group">
                                                    <a data-edit="bold" data-toggle="tooltip" title="Bold (Ctrl/Cmd+B)" className="btn btn-default">
                                                        <em className="fa fa-bold"></em>
                                                    </a>
                                                    <a data-edit="italic" data-toggle="tooltip" title="Italic (Ctrl/Cmd+I)" className="btn btn-default">
                                                        <em className="fa fa-italic"></em>
                                                    </a>
                                                    <a data-edit="strikethrough" data-toggle="tooltip" title="Strikethrough" className="btn btn-default">
                                                        <em className="fa fa-strikethrough"></em>
                                                    </a>
                                                    <a data-edit="underline" data-toggle="tooltip" title="Underline (Ctrl/Cmd+U)" className="btn btn-default">
                                                        <em className="fa fa-underline"></em>
                                                    </a>
                                                </div>
                                                <div className="btn-group">
                                                    <a data-edit="insertunorderedlist" data-toggle="tooltip" title="Bullet list" className="btn btn-default">
                                                        <em className="fa fa-list-ul"></em>
                                                    </a>
                                                    <a data-edit="insertorderedlist" data-toggle="tooltip" title="Number list" className="btn btn-default">
                                                        <em className="fa fa-list-ol"></em>
                                                    </a>
                                                    <a data-edit="outdent" data-toggle="tooltip" title="Reduce indent (Shift+Tab)" className="btn btn-default">
                                                        <em className="fa fa-dedent"></em>
                                                    </a>
                                                    <a data-edit="indent" data-toggle="tooltip" title="Indent (Tab)" className="btn btn-default">
                                                        <em className="fa fa-indent"></em>
                                                    </a>
                                                </div>
                                                <div className="btn-group">
                                                    <a data-edit="justifyleft" data-toggle="tooltip" title="Align Left (Ctrl/Cmd+L)" className="btn btn-default">
                                                        <em className="fa fa-align-left"></em>
                                                    </a>
                                                    <a data-edit="justifycenter" data-toggle="tooltip" title="Center (Ctrl/Cmd+E)" className="btn btn-default">
                                                        <em className="fa fa-align-center"></em>
                                                    </a>
                                                    <a data-edit="justifyright" data-toggle="tooltip" title="Align Right (Ctrl/Cmd+R)" className="btn btn-default">
                                                        <em className="fa fa-align-right"></em>
                                                    </a>
                                                    <a data-edit="justifyfull" data-toggle="tooltip" title="Justify (Ctrl/Cmd+J)" className="btn btn-default">
                                                        <em className="fa fa-align-justify"></em>
                                                    </a>
                                                </div>
                                                <div className="btn-group dropdown">
                                                    <a data-toggle="dropdown" title="Hyperlink" className="btn btn-default">
                                                        <em className="fa fa-link"></em>
                                                    </a>
                                                    <div className="dropdown-menu">
                                                        <div className="input-group ml-xs mr-xs">
                                                            <input id="LinkInput" placeholder="URL" type="text" data-edit="createLink" className="form-control input-sm" />
                                                            <div className="input-group-btn">
                                                                <button type="button" className="btn btn-sm btn-default">Add</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a data-edit="unlink" data-toggle="tooltip" title="Remove Hyperlink" className="btn btn-default">
                                                        <em className="fa fa-cut"></em>
                                                    </a>
                                                </div>
                                                <div className="btn-group">
                                                    <a id="pictureBtn" data-toggle="tooltip" title="Insert picture (or just drag &amp; drop)" className="btn btn-default">
                                                        <em className="fa fa-picture-o"></em>
                                                    </a>
                                                    <input type="file" data-edit="insertImage" style={ {    position: "absolute",    opacity: "0",    width: "41px",    height: "34px"} } />
                                                </div>
                                                <div className="btn-group pull-right">
                                                    <a data-edit="undo" data-toggle="tooltip" title="Undo (Ctrl/Cmd+Z)" className="btn btn-default">
                                                        <em className="fa fa-undo"></em>
                                                    </a>
                                                    <a data-edit="redo" data-toggle="tooltip" title="Redo (Ctrl/Cmd+Y)" className="btn btn-default">
                                                        <em className="fa fa-repeat"></em>
                                                    </a>
                                                </div>
                                            </div>
                                            <div style={ { overflow: "scroll", height: "250px", maxHeight: "250px"} } className="form-control wysiwyg mt-lg"></div>
                                        </Col>
                                    </div>
                                    
                                </Row>
                            </div>
                            <div className="panel-footer">
                                <button type="submit" className="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </Col>
            </ContentWrapper>
        );
    }

}

export default EditTemplate;
