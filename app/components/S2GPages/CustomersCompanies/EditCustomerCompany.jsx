import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {Router, Route, Link, History, withRouter} from 'react-router';
import {browserHistory} from 'react-router';

import css from '../../../styles/tableIcons.scss'

class AddCustomerCompany extends React.Component {
    constructor() {
        super();
        this.state = {
            companies: [],
            custCompany: "",

            customer: "",
            selectedCompany: "",
            primaveraCode: ""
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleSubmit(e) {
        const {
            custCompany, selectedCompany, primaveraCode/*, selectedEntity*/
        } = this.state;

        var headers = new Headers({'Content-Type': 'application/json'});
        var newCustomerCompany = JSON.parse(JSON.stringify({"entity": custCompany.entity, "primaveraCode": primaveraCode, "company": selectedCompany}));
        var self = this;

        e.preventDefault();

        this.serverRequest = axios
            .put(__APIURL__ + '/entityAndCompanies/' + custCompany._id, newCustomerCompany, {headers: this.headers})
            .then(function (response) {
                console.log(response);

                self
                    .props
                    .router
                    .push('/customers/view/' + custCompany.entity + '/5')

                //    })
            })
            .catch(function (error) {
                console.log(error);
            });

    }
    componentWillMount() {

        var th = this;
        this.serverRequest = axios
            .get(__APIURL__ + "/companies/")
            .then(function (result) {
                th.setState({companies: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/entityAndCompanies/")
            .then(function (result) {

                result
                    .data
                    .forEach(custComp => {
                        if (custComp._id = th.props.params.id) {

                            th.setState({custCompany: custComp, selectedCompany: custComp.company, customer: custComp.entity});
                        }
                    });

            })

    }
    render() {
        var companiesList = [];

        this
            .state
            .companies
            .forEach((item, i) => {
                if (i == 0) {
                    companiesList.push(
                        <option disabled value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }

                companiesList.push(
                    <option key={i.toString()} value={item._id}>{item.name}</option >
                );
            })

        return (
            <ContentWrapper>
                <h3>Empresas
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar associação a uma empresa</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Empresa*</label>
                                            <FormControl
                                                id="select2-1-company"
                                                componentClass="select"
                                                name="selectedCompany"
                                                className="form-control m-b"
                                                value={this.state.selectedCompany}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedCompany')}>
                                                {companiesList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Web Service Primavera*</label>
                                            <FormControl
                                                type="text"
                                                name="primaveraCode"
                                                required="required"
                                                className="form-control"
                                                value={this.state.primaveraCode}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'primaveraCode')}/>
                                        </div>
                                    </Row>
                                </div>

                                {/*  <div className="form-group">

                                    <label className="control-label">Entidade correspondente</label>
                                    <FormControl
                                        required
                                        componentClass="select"
                                        name="selectedEntity"
                                        className="form-control m-b"
                                        value={this.state.selectedEntity}
                                        onChange={this
                                        .handleChange
                                        .bind(this, 'selectedEntity')}>
                                        {entitiesList}

                                    </FormControl>
                                </div>
*/}
                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.id + '/5')}
                                            type="button"
                                            className="btn ">Voltar ao cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }

    componentDidMount() {
        var self = this;

    }
}

export default AddCustomerCompany;
