import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';

class Companies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            entityAndCompaniesData: [],
            entities: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
    }

    getDataFromBD() {
        axios
            .get(__APIURL__ + "/companies/")
            .then(res => {

                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({data})
                    setTimeout(function () {
                        var dtCompanies = $('#dtCompanies').dataTable({
                            'retrieve': true, 'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'responsive': true, // https://datatables.net/extensions/responsive/examples/
                            // Text translation options Note the required keywords between underscores (e.g
                            // _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            // Datatable Buttons setup
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                {
                                    extend: 'copy',
                                    className: 'btn-sm',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'csv',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'excel',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'pdf',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'print',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs.keyup(function () {
                            dtCompanies.fnFilter(this.value, columnInputs.index(this));
                        });
                    }.bind(this), 500);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        var th = this;
        axios
            .get(__APIURL__ + "/entityAndCompanies/",)
            .then(function (result) {
                th.setState({entityAndCompaniesData: result.data});
            })

        axios
            .get(__APIURL__ + "/entities/",)
            .then(function (result) {
                th.setState({entities: result.data});
            })

    }

    create() {
        this
            .props
            .router
            .push('/companies/create');
    }

    edit(id) {
        this
            .props
            .router
            .push('/companies/edit/' + id);
    }

    delete(name, id) {
        var self = this;
        var confirmDelete = true;
        this
            .state
            .entityAndCompaniesData
            .forEach(entityC => {
                if (entityC.company == id) {
                    confirmDelete = false;

                }
            });
        if (confirmDelete) {
            swal({
                title: "Tem a certeza que quer eliminar?",
                text: "Após eliminar não poderá recuperar o registo!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                closeOnConfirm: false
            }, function () {
                axios
                    .delete(__APIURL__ + "/companies/" + id)
                    .then(function (response) {
                        if (response.status == 200) {
                            /*   axios
                            .get(__APIURL__ + "/entityAndCompanies/")
                            .then(function (result) {
                                for (var i = 0; i < result.data.length; i++) {
                                    console.log(result.data[i])
                                    if (result.data[i].company == id) {

                                        axios
                                            .delete(__APIURL__ + "/entityAndCompanies/" + result.data[i]._id)
                                            .then(function (response) {
                                                if (response.status == 200) {*/

                            swal("Eliminado!", name + " foi eliminado.", "success");
                            self.getDataFromBD();
                        } else {
                            // TODO: HANDLE IT
                        }
                        // })    }  } })     }
                    })
                    .catch(function (error) {
                        console.log(error);
                        // TODO: HANDLE IT
                    });
            });
        } else {
            swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
        }

    }

    render() {
        const css = css;
        var companiesRows = [];
        var companyName = "",
            entityName = "",
            webServicePrimavera = "";

        this
            .state
            .data
            .map((data, j) => {
                /*this
                    .state
                    .entities
                    .map((entities, i) => {
                        //for (var x = 0; x < this.state.entityAndCompaniesData.length; i++) {
                            //if (this.state.entityAndCompaniesData[x].company == data._id && this.state.entityAndCompaniesData[x].entity == entities._id)
                          //      console.log(this.state.entityAndCompaniesData[x]);
                            */
                /*  this
            .state
            .entityAndCompaniesData
            .map((ecs, i) => {
                this
                    .state
                    .data
                    .forEach((obj, i) => {
                        if (ecs.company == obj._id) {
                            companyName = obj.name;
                            webServicePrimavera = obj.webServicePrimavera;
                        }
                    })

                this
                    .state
                    .entities
                    .forEach((obj, i) => {
                        if (ecs.entity == obj._id) {
                            entityName = obj.taxName;
                        }
                    })
*/
                companiesRows.push(
                    <tr key={data._id}>
                        <td>{data.name}</td>
                        {/* <td>{data.webServicePrimavera}</td> */}
                        {/*<td>{entityName}</td>*/}
                        <td>
                            <button
                                onClick={() => this.edit(data._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(data.name, data._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                )
                // }
            })

        return (
            <ContentWrapper>
                <h3>Empresas
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <button
                    onClick={() => this.create()}
                    type="button"
                    id="addBtn"
                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                    <i className="icon-plus"></i>
                </button>
                <Row>
                    <Col lg={12}>
                        <Panel>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        {/* <th>Web Service Primavera</th> */}
                                        {/*<th>Entidade associada</th>*/}

                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {companiesRows}

                                </tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default Companies;
