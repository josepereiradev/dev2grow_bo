import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {Router, Route, Link, History, withRouter} from 'react-router';
import {browserHistory} from 'react-router';

import css from '../../../styles/tableIcons.scss'

class AddCompany extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "",
            webServicePrimavera: ""
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleSubmit(e) {
        const {
            name,
            webServicePrimavera
        } = this.state;
        var headers = new Headers({'Content-Type': 'application/json'});
        var newCompany = JSON.parse(JSON.stringify({
            "name": name, "webServicePrimavera": webServicePrimavera
        }));
        var self = this;

        e.preventDefault();

        this.serverRequest = axios
            .post(__APIURL__ + '/companies/', newCompany, {headers: this.headers})
            .then(function (response) {

                /*  var newCompanyEntityRelation = JSON.parse(JSON.stringify({"entity": selectedEntity, "company": response.data.obj._id}));
                console.log(newCompanyEntityRelation);
                axios
                    .post(__APIURL__ + '/entityAndCompanies/', newCompanyEntityRelation, {headers: self.headers})
                    .then(function (response) {
                         console.log(response);
                  */
                self
                    .props
                    .router
                    .push('/companies');

                //    })
            })
            .catch(function (error) {
                console.log(error);
            });

    }
    componentWillMount() {

        var th = this;

        /*   this.serverRequest = axios
            .get(__APIURL__ + "/entities/")
            .then(function (result) {
                th.setState({entities: result.data});
            })
    */
    }
    render() {
        /* var entitiesList = [];

        this
            .state
            .entities
            .forEach((item, i) => {
                entitiesList.push(
                    <option key={i.toString()} value={item._id}>{item.taxName}</option>
                );
            })*/

        return (
            <ContentWrapper>
                <h3>Empresas
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar nova empresa</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <label className="control-label">Nome*</label>
                                    <FormControl
                                        type="text"
                                        name="name"
                                        required="required"
                                        className="form-control"
                                        value={this.state.name}
                                        onChange={this
                                        .handleChange
                                        .bind(this, 'name')}/>
                                </div>
                                <div className="form-group">
                                    <label className="control-label">Web Service Primavera*</label>
                                    <FormControl
                                        type="text"
                                        name="webServicePrimavera"
                                        required="required"
                                        className="form-control"
                                        value={this.state.webServicePrimavera}
                                        onChange={this
                                        .handleChange
                                        .bind(this, 'webServicePrimavera')}/>
                                </div>

                                {/*     <div className="form-group">

                                    <label className="control-label">Entidade correspondente</label>
                                    <FormControl
                                        required
                                        componentClass="select"
                                        name="selectedEntity"
                                        className="form-control m-b"
                                        value={this.state.selectedEntity}
                                        onChange={this
                                        .handleChange
                                        .bind(this, 'selectedEntity')}>
                                        {entitiesList}

                                    </FormControl>
                                </div>
*/}
                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/companies')}
                                            type="button"
                                            className="btn ">Voltar às empresas</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }

}

export default AddCompany;
