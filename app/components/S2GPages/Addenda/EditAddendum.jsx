import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class EditAddendum extends React.Component {

    constructor() {
        super();
        this.state = {
            hours: "",
            date: "",
            observations: "",
            contractData: [],


            addendaObj: null
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        var th = this;
        const {date, hours, observations,addendaObj, contractData} = this.state;

        var decimalTime = parseFloat(hours);
        decimalTime = decimalTime * 60 * 60;
        var hours2 = Math.floor((decimalTime / (60 * 60)));
        decimalTime = decimalTime - (hours2 * 60 * 60);
        var minutes = Math.floor((decimalTime / 60));
        decimalTime = decimalTime - (minutes * 60);
        if (hours2 < 10) {
            hours2 = "0" + hours2;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        var hoursOnRightFormat = hours2 + ":" + minutes; //horas atualizadas

        var headers = new Headers({'Content-Type': 'application/json'});
        var updateAddendum = JSON.parse(JSON.stringify({
            "hours": hoursOnRightFormat,
            "date": moment(date, "DD/MM/YYYY"),
            "observations": observations
        }));

        var idContract = this.props.params.idContract;
        var idEntity = this.props.params.idEntity;

        var id = this.props.params.id;

        e.preventDefault();

        this.serverRequest = axios
            .put(__APIURL__ + '/addenda/' + id, updateAddendum, {headers: this.headers})
            .then(function (response) {

               /* th.setState({
                    [contractData.addenda]: contractData
                        .addenda
                        .push(response.data.obj._id)
                });*/
                
                var newAvailableHours = th.calcSumBetweenA_B(th.calcDifBetweenA_B(contractData.availableHours, addendaObj.hours),updateAddendum.hours);  //ESTE TEM O VALOR CERTO

                var updateContract = JSON.parse(JSON.stringify({
                    "beginDate": (contractData.beginDate == null
                        ? new Date()
                        : new Date(contractData.beginDate).toISOString()),
                    "endDate": (contractData.endDate == null
                        ? new Date()
                        : new Date(contractData.endDate).toISOString()),
                    "lastRenewal": (contractData.lastRenewal == null
                        ? new Date()
                        : new Date(contractData.lastRenewal).toISOString()),
                    "notifyRenewal": contractData.notifyRenewal,
                    "hours": contractData.hours,
                    "perMonth": contractData.perMonth,
                    "notifyExhaustedHours": contractData.notifyExhaustedHours,
                    "blockExhaustedHours": contractData.blockExhaustedHours,
                    "contractType": contractData.contractType,
                    "state": contractData.state,
                    "addenda": contractData.addenda,
                    "contractCode": contractData.contractCode,
                    "availableHours": newAvailableHours,
                    "exhaustedHours": contractData.exhaustedHours
                }));

       
                th.serverRequest = axios
                    .put(__APIURL__ + '/contracts/' + contractData._id, updateContract, {headers: th.headers})
                    .then(function (response) {
                        th
                            .props
                            .router
                            .push('/contracts/view/' + th.props.params.idEntity + '/' + contractData._id);

                    })
                    .catch(function (error) {
                        console.log(error);
                    });


                /* th
                    .props
                    .router
                    .push('/contracts/view/' + idEntity + '/' + idContract);*/

            })
            .catch(function (error) {
                console.log(error);
            });

    }
    calcSumBetweenA_B(ATime, BTime) {
        //ATime: '10:00' BTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesInterv + minutesProcess;
        var updatedHours = hoursInterv + hoursProcess;

        if (updatedMinutes >= 60) {
            updatedMinutes = updatedMinutes - 60;
            updatedHours++;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }
    calcDifBetweenA_B(ATime, BTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    componentWillMount() {

        var th = this;
        var idContract = this.props.params.idContract;
        var id = this.props.params.id;
        
        this.serverRequest = axios
            .get(__APIURL__ + "/addenda/" + id)
            .then(function (result) {

                var hours = parseFloat(parseInt(result.data.hours.split(':')[0], 10) + '.' + parseInt((result.data.hours.split(':')[1]/6)*10, 10));

                th.setState({
                    hours: hours,
                    observations: result.data.observations,
                    date: moment(result.data.date).format("DD/MM/YYYY"),
                    addendaObj: result.data,
                });
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/contracts/" + idContract)
            .then(function (result) {
                th.setState({contractData: result.data});
            })

    }
    render() {

        return (
            <ContentWrapper>

                <h3>Adenda
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar uma nova adenda</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Número de Horas</label>
                                            <FormControl
                                                type="text"
                                                name="hours"
                                                required="required"
                                                value={this.state.hours}
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'hours')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Data</label>
                                            <div id="datetimepicker" className="input-group date">
                                                <input
                                                    name="date"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.date}
                                                    onClick={this
                                                    .handleChange
                                                    .bind(this, 'date')}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-12">
                                            <label className="control-label">Observações</label>
                                            <textarea
                                                id="textAreaObservations"
                                                name="observations"
                                                className="form-control"
                                                rows="3"
                                                value={this.state.observations}
                                                onChange={this
                                                .handleChange
                                                .bind(this, "observations")}/>

                                        </div>
                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/contracts/view/' + this.props.params.idEntity + '/' + this.props.params.idContract)}
                                            type="button"
                                            className="btn ">Voltar ao contrato</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {

        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepicker').datetimepicker({
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            });
            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-state').select2({theme: 'bootstrap'});
            $('#select2-1-country').select2({theme: 'bootstrap'});
            $('#select2-1-function').select2({theme: 'bootstrap'});
            //  $('#select2-2').select2({theme: 'bootstrap'});
            // $('#select2-3').select2({theme: 'bootstrap'});
            // $('#select2-4').select2({placeholder: 'Select a state', allowClear: true,
            // theme: 'bootstrap'});
        }
    }

}

export default EditAddendum;
