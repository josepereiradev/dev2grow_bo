import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../../Layout/ContentWrapper';
import { Router, Route, Link, History, withRouter } from 'react-router';
import {browserHistory} from 'react-router';


import css from '../../../../styles/tableIcons.scss'

class AddFunction extends React.Component {
    constructor() {
        super();
        this.state = {
            description: ""
                };

        this.handleChange = this
            .handleChange
            .bind(this);
        
        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(e) {
        this.setState({description: e.target.value});
    }


    handleSubmit(e) {
        const {description} = this.state;
        var self = this;
        var headers = new Headers({'Content-Type': 'application/json'});
        var newUserFunction = JSON.parse(JSON.stringify({"description": description}));

    
        e.preventDefault();
        console.log(newUserFunction);
        console.log(description);

        this.serverRequest = axios
            .post(__APIURL__ + '/functions/', newUserFunction, {headers: this.headers})
            .then(function (response) {
               
                             self.props.router.push('/functions');

            })
            .catch(function (error) {
                console.log(error);
            });

    }


    render() {

        return (
            <ContentWrapper>
                <h3>Tipo de Notícia
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar uma função de utilizador</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <label className="control-label">Descrição*</label>
                                    <FormControl
                                        type="text"
                                        name="description"
                                        required="required"
                                        className="form-control"
                                        value={this.state.description}
                                        onChange={this.handleChange}/>
                                </div>
                               
                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
<Row>
                                    <Col lg={1}>

                                        <button  onClick={() => this.props.router.push('/functions')} type="button" className="btn ">Voltar</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>                               </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }

}

export default AddFunction;
