import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import { Row, Col, Panel, Button, Table } from 'react-bootstrap';
import { Router, Route, Link, History, withRouter } from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';

class Users extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            userFunctions: [],
            countries: [],
            interventions: [],
            processes: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/functions/")
            .then(function (result) {
                th.setState({ userFunctions: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/countries/")
            .then(function (result) {
                th.setState({ countries: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processes/")
            .then(function (result) {
                console.log("MY RESULT", result);
                th.setState({ processes: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/interventions/")
            .then(function (result) {
                th.setState({ interventions: result.data });
            })

    }

    getDataFromBD() {
        axios
            .get(__APIURL__ + "/users/")
            .then(res => {
                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({ data })
                    setTimeout(function () {
                        var dtUsers = $('#dtUsers').dataTable({
                            'pageLength': 100,
                            'retrieve': true,
                            'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'responsive': true, // https://datatables.net/extensions/responsive/examples/
                            // Text translation options Note the required keywords between underscores (e.g
                            // _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            // Datatable Buttons setup
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                {
                                    extend: 'copy',
                                    className: 'btn-sm',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'csv',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'excel',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'pdf',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'print',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Entidades',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs.keyup(function () {
                            dtUsers.fnFilter(this.value, columnInputs.index(this));
                        });
                    }.bind(this), 500);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    create() {
        this
            .props
            .router
            .push('/users/create');
    }

    edit(id) {
        this
            .props
            .router
            .push('/users/edit/' + id);
    }

    delete(name, id) {
        var self = this;

        var confirmDelete = true;
        console.log("I'M HERE", this.state.processes);
        if (Array.isArray(this.state.processes)) {
            this.state.processes.forEach(process => {
                if (process.collaborator == id) {
                    confirmDelete = false;
                }
            })
        }
        if (Array.isArray(this.state.interventions)) {
            this
                .state
                .interventions
                .forEach(interv => {
                    if (interv.collaborator == id) {
                        confirmDelete = false;
                    }
                })
        }
        if (confirmDelete) {
            swal({
                title: "Tem a certeza que quer eliminar?",
                text: "Após eliminar não poderá recuperar o registo!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                closeOnConfirm: false
            }, function () {
                axios
                    .delete(__APIURL__ + "/users/" + id)
                    .then(function (response) {
                        if (response.status == 200) {
                            swal("Eliminado!", name + " foi eliminado.", "success");
                            self.getDataFromBD();
                        } else {
                            // TODO: HANDLE IT
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        // TODO: HANDLE IT
                    });
            });
        } else {
            swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
        }

    }

    render() {
        const css = css;
        var rows = [];
        var gender = "",
            company = "",
            userCountry = "",
            userfunc = "";
        this
            .state
            .data
            .map((data, j) => {

                //company
                if (data.company == undefined) {
                    company = ''
                } else {
                    company = data.company
                }
                //gender
                if (data.gender == 'female') {
                    gender = 'F'
                } else if (data.gender == 'male') {
                    gender = 'M'
                }
                if (this.state.userFunctions[0] != undefined)
                    this.state.userFunctions.forEach((func, i) => {
                        if (data.userFunction == func._id) {
                            console.log(data.name + ": " + func.description);
                            userfunc = func.description
                        }
                    })
                if (this.state.countries[0] != undefined)
                    this.state.countries.forEach((c, i) => {
                        if (data.country != null)
                            if (data.country.id == c._id) {
                                userCountry = c
                                    .code
                                    .toString();
                            }

                    })
                var photoLink = null;
                if (data.photo != null) {
                    if (data.photo.indexOf("fbcdn") == -1) {
                        photoLink = data.photo + "?time=" + moment().valueOf();
                    } else {
                        photoLink = data.photo + "&time=" + moment().valueOf()
                    }
                }


                rows.push(
                    <tr key={data._id}>
                        {/*<td>{data._id}</td>*/}
                        <td>
                            <div className="media">
                                <img
                                    src={photoLink != null
                                        ? photoLink
                                        : "http://54.171.236.54/assets/img/profile/unknown.png"}
                                    alt="Image"
                                    className="img-responsive img-circle" />
                            </div>
                        </td>
                        <td>{data.name}</td>
                        <td>{company}</td>
                        <td>{data.email}</td>
                        <td>{data.phone}</td>
                        <td>{userfunc}</td>
                        {/*<td>{func.description}</td>*/}
                        <td>{new Date(data.birthdate).toLocaleDateString()}</td>
                        <td>{gender}</td>
                        <td>{userCountry}</td>
                        {/*   <td>{new Date(data._modified).toLocaleString()}</td>*/}
                        {/*  <td>{new Date(data._created).toLocaleString()}</td>*/}
                        <td>
                            <button
                                onClick={() => this.edit(data._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(data.name, data._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                )
                userfunc = "";
                //  }  })
            })

        return (
            <ContentWrapper>
                <h3>Utilizadores
                </h3>
                <style>
                    {
                        css
                    }
                </style>

                <button
                    onClick={() => this.create()}
                    type="button"
                    id="addBtn"
                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                    <i className="icon-plus"></i>
                </button>
                <Row>
                    <Col lg={12}>
                        <Panel>
                            <Table id="dtUsers" responsive hover>
                                <thead>
                                    <tr>
                                        {/* <th>ID do utilizador</th>*/}
                                        <th>Foto</th>
                                        <th>Nome</th>
                                        <th>Empresa</th>
                                        <th>E-mail</th>
                                        <th>Telefone</th>
                                        <th>Função</th>
                                        <th>Data de nascimento</th>
                                        <th>Sexo</th>
                                        <th>País</th>
                                        {/*  <th>Última actualização</th>*/}
                                        {/*  <th>Criado a</th>*/}

                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>{rows}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default Users;
