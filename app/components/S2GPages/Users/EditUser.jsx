import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class EditUser extends React.Component {

    constructor() {
        super();
        this.state = {
            photo: "",
            name: "",
            email: "",
            birthdate: new Date(),
            gender: "male",
            phone: "",
            company: "",
            selectedCountry: "",
            selectedFunction: "",
            countries: [],
            functs: [],
            isManager: false
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);

        this.handleCheckbox = this
            .handleCheckbox
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
        console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        const {
            photo,
            name,
            email,
            birthdate,
            gender,
            phone,
            company,
            selectedCountry,
            selectedFunction,
            isManager
        } = this.state;

        var headers = new Headers({'Content-Type': 'application/json'});
        var updateUser = JSON.parse(JSON.stringify({
            "photo": photo,
            "name": name,
            "phone": phone,
            "email": email,
            "birthdate": moment(this.state.birthdate, "DD/MM/YYYY").format(), 
            "company": company,
            "gender": gender,
            "country": selectedCountry,
            "userFunction": selectedFunction,
            "internalUser": isManager
        }));

        var id = this.props.params.id;

        var self = this;

        e.preventDefault();
        console.log(updateUser);
        this.serverRequest = axios
            .put(__APIURL__ + '/users/' + id, updateUser, {headers: this.headers})
            .then(function (response) {
                self
                    .props
                    .router
                    .push('/users');

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;

        this.serverRequest = axios
            .get(__APIURL__ + "/users/" + id)
            .then(function (result) {
                console.log("Pais:");
                console.log(result.data.country);

                th.setState({
                    photo: result.data.photo,
                    name: result.data.name,
                    email: result.data.email,
                    gender: result.data.gender,
                    birthdate: new Date(result.data.birthdate).toLocaleDateString(),
                    phone: result.data.phone,
                    company: result.data.company,
                    selectedCountry: result.data.country != null ? result.data.country._id : null,
                    selectedFunction: result.data.userFunction,
                    isManager: result.data.internalUser
                });
                            console.log(result.data.country);
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/countries/")
            .then(function (result) {
                th.setState({countries: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/functions/")
            .then(function (result) {
                th.setState({functs: result.data});
            })
    }

    render() {
        console.log("show stuff:", this.state);

        var functionsList = [];
        var countriesList = [];

        if (this.state.countries[0] != undefined) 
            this.state.countries.forEach((item, i) => {
                countriesList.push(
                    <option key={i.toString()} value={item._id}>{item.name}</option>
                );
            })

        if (this.state.functs[0] != undefined) 
            this.state.functs.forEach((item, i) => {
                functionsList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })

        const preview = {
            display: 'block',
            width: '200px',
            padding: '10px',
            margin: '0 auto'
        };
        const relative = {
            position: 'relative'
        };

        let photoDisplay = (typeof this.state.photo !== 'undefined' && this.state.photo != '')
            ? <img style={preview} src={this.state.photo} alt="Foto de perfil"/>
            : '';

        return (
            <ContentWrapper>
                <h3>Utilizadores
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar utilizador</div>
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Nome</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                className="form-control"
                                                value={this.state.name}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'name')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">E-mail</label>
                                            <FormControl
                                                disabled
                                                type="text"
                                                name="email"
                                                required="required"
                                                className="form-control"
                                                value={this.state.email}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'email')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Empresa</label>
                                            <FormControl
                                                type="text"
                                                name="company"
                                                required="required"
                                                className="form-control"
                                                value={this.state.company}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'company')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Telefone</label>
                                            <FormControl
                                                type="text"
                                                name="phone"
                                                required="required"
                                                className="form-control"
                                                value={this.state.phone}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'phone')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Sexo</label>
                                            <FormControl
                                                id="select2-1-gender"
                                                componentClass="select"
                                                name="gender"
                                                className="form-control m-b"
                                                value={this.state.gender}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'gender')}>
                                                <option value='male'>Masculino</option>
                                                <option value='female'>Feminino</option>

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Função</label>
                                            <FormControl
                                                id="select2-1-function"
                                                componentClass="select"
                                                name="selectedFunction"
                                                className="form-control m-b"
                                                value={this.state.selectedFunction}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedFunction')}>
                                                {functionsList}

                                            </FormControl>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">País</label>
                                            <FormControl
                                                id="select2-1-country"
                                                componentClass="select"
                                                name="selectedCountry"
                                                className="form-control m-b"
                                                value={this.state.selectedCountry}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedCountry')}>
                                                {countriesList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Data de nascimento</label>
                                            <div id="datetimepicker1" className="input-group date">
                                                <input
                                                    name="birthdate"
                                                    type="text"
                                                    className="form-control" 
                                                    value={this.state.birthdate} />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label
                                                className="control-label"
                                                style={{
                                                display: 'inline-block'
                                            }}>Utilizador Backoffice</label>
                                            <div
                                                className="checkbox c-checkbox"
                                                style={{
                                                display: 'inline-block',
                                                margin: '10px'
                                            }}>
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        name="isManager"
                                                        checked={this.state.isManager}
                                                        onChange={this
                                                        .handleCheckbox
                                                        .bind(this, 'isManager')}/>
                                                    <em className="fa fa-check"></em>

                                                </label>
                                            </div>

                                        </div>
                                    </Row>

                                    <div className="mb-lg">
                                        <input
                                            id="photo"
                                            type="file"
                                            name="photo"
                                            data-input="false"
                                            data-button-name="btn btn-info"
                                            data-button-text="Carregar foto de perfil"
                                            data-icon-name="fa fa-upload mr"
                                            className="form-control filestyle"/>

                                        <div style={relative}>
                                            {photoDisplay}
                                        </div>
                                    </div>
                                    {/*<label className="control-label">URL da foto</label>
                                <FormControl
                                    type="text"
                                    name="photo"
                                    required="required"
                                    className="form-control"
                                    value={this.state.photo}
                                    onChange={this
                                    .handleChange
                                    .bind(this, 'photo')}/>
*/}

                                </div>
                                <div className="required">* Campo obrigatório</div>
                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={1}>

                                        <button
                                            onClick={() => this.props.router.push('/users')}
                                            type="button"
                                            className="btn ">Voltar</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {
        // FILESTYLE
        var self = this;

        $('.filestyle').filestyle();

        document
            .getElementById('photo')
            .onchange = function () {
            var data = new FormData();
            data.append('photo', document.getElementById('photo').files[0]);
            var config = {
                onUploadProgress: function (progressEvent) {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                }
            };
            console.log(document.getElementById('photo').files[0])
            axios.post(__APIURL__ + '/users/photo/' + self.props.params.id, data) //, config)
                .then(function (res) {
                    console.log(res)
                    console.log(data)
                    console.log(self.props.params.id);
                    self.setState({
                        photo: 'https://dev2grow.ddns.net:8443/assets/img/profile/' + self.props.params.id + '.jpg'
                    })
                    console.log(self.state.photo)
                })
                .catch(function (err) {
                    console.log(err);
                    // TODO: HANDLE IT
                });
        };
        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {
            
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY',
                date: this.state.birthdate,
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            }).on('dp.change', function(e){
                let dateSelected = $('#datetimepicker1').data("DateTimePicker").date();
                console.log(dateSelected.format("DD/MM/YYYY"));
                this.setState({birthdate: dateSelected.format("DD/MM/YYYY")});

            }.bind(this));
            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-gender').select2({theme: 'bootstrap'});
            $('#select2-1-country').select2({theme: 'bootstrap'});
            $('#select2-1-function').select2({theme: 'bootstrap'});
            $('#select2-1-gender').on('change', function () {
                self.setState({
                    gender: $(this).val()
                });
            });
            $('#select2-1-country').on('change', function () {
                self.setState({
                    selectedCountry: $(this).val()
                });
            });
            $('#select2-1-function').on('change', function () {
                self.setState({
                    selectedFunction: $(this).val()
                });
            });
        }
    }

}

export default EditUser;
