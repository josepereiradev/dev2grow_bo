import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {Router, Route, Link, History, withRouter} from 'react-router';
import {browserHistory} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class AddContact extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "",
            telephone: "",
            mobilePhone: "",
            email: "",
            active: true,
            notifyProcesses: false,
            contactTypes: [],
            selectedContactType: "5923145b9248c53d640191c2",
            entityData: []
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        var th = this;
        const {
            name,
            telephone,
            mobilePhone,
            email,
            active,
            notifyProcesses,
            selectedContactType,

            entityData
        } = this.state;
        var headers = new Headers({'Content-Type': 'application/json'});
        var newContact = JSON.parse(JSON.stringify({
            "name": name,
            "contactType": selectedContactType,
            "active": active,
            "notifyProcesses": notifyProcesses,
            "telephone": telephone,
            "mobilePhone": mobilePhone,
            "email": email
        }));

        var id = this.props.params.idEntity;

        e.preventDefault();

        this.serverRequest = axios
            .post(__APIURL__ + '/contacts/', newContact, {headers: this.headers})
            .then(function (response) {
                th
                    .state
                    .entityData
                    .contacts
                    .push(response.data.obj._id);
                var updateEntity = JSON.parse(JSON.stringify({
                    "nif": entityData.nif,
                    "qrCode": entityData.qrCode,
                    "taxName": entityData.taxName,
                    "abrevName": entityData.abrevName,
                    "accountManager": entityData.accountManager,
                    "contacts": entityData.contacts,
                    "projects": entityData.projects,
                    "processes": entityData.processes,
                    "contracts": entityData.contracts,
                    "associatedUsers": entityData.associatedUsers
                }));

                th.serverRequest = axios
                    .put(__APIURL__ + '/entities/' + id, updateEntity, {headers: th.headers})
                    .then(function (response) {
                        th
                            .props
                            .router
                            .push('/customers/view/' + id + "/1"); //nmr da aba que vai abrir
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
               

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.idEntity;

        this.serverRequest = axios
            .get(__APIURL__ + "/contactTypes/")
            .then(function (result) {
                th.setState({contactTypes: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + id)
            .then(function (result) {
                th.setState({entityData: result.data});
            })

    }

    render() {
        var contactTypeList = [];

        if (this.state.contactTypes[0] != undefined) 
            this.state.contactTypes.forEach((item, i) => {

                if (i == 0) {
                    contactTypeList.push(
                        <option disabled defaultValue value key={i.toString()}>
                            -- Escolha uma opção --
                        </option>
                    );
                }

                contactTypeList.push(
                    <option
                        key={item
                        ._id
                        .toString()}
                        value={item._id}>{item.description}</option>
                );
            })

        return (

            <ContentWrapper>

                <h3>Contacto
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar um novo contacto</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Nome</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'name')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">E-mail</label>
                                            <FormControl
                                                type="text"
                                                name="email"
                                                required="required"
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'email')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Telemóvel</label>
                                            <FormControl
                                                type="text"
                                                name="mobilePhone"
                                                required="required"
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'mobilePhone')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Telefone</label>
                                            <FormControl
                                                type="text"
                                                name="telephone"
                                                required="required"
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'telephone')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de contacto</label>
                                            <FormControl
                                                id="select2-1"
                                                componentClass="select"
                                                name="selectedContactType"
                                                className="form-control m-b"
                                                value={this.state.selectedContactType}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedContactType')}>
                                                {contactTypeList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">

                                            <div className="col-sm-3">
                                                <label className="control-label">Activo</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="active"
                                                            checked={this.state.active}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'active')}/>
                                                        <em className="fa fa-check"></em>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-sm-4">
                                                <label className="control-label">Notificar processos</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyProcesses"
                                                            checked={this.state.notifyProcesses}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'notifyProcesses')}/>
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity+ "/1")}
                                            type="button"
                                            className="btn ">Voltar ao cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {
        var self = this;

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1').select2({theme: 'bootstrap'});

            $('#select2-1').on('change', function () {
                self.setState({
                    selectedContactType: $(this).val()
                });
            });
        }
    }
}

export default AddContact;
