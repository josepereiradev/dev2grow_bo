import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';

import css from '../../../styles/tableIcons.scss'

class EditContact extends React.Component {

    constructor() {
        super();
        this.state = {
            name: "",
            telephone: "",
            mobilePhone: "",
            email: "",
            active: false,
            notifyProcesses: false,
            contactTypes: [],
            selectedContactType: ""
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        var th = this;
        var idEntity = this.props.params.idEntity;

        const {
            name,
            telephone,
            mobilePhone,
            email,
            active,
            notifyProcesses,
            selectedContactType
        } = this.state;
        var headers = new Headers({'Content-Type': 'application/json'});
        var updateContact = JSON.parse(JSON.stringify({
            "name": name,
            "contactType": selectedContactType,
            "active": active,
            "notifyProcesses": notifyProcesses,
            "telephone": telephone,
            "mobilePhone": mobilePhone,
            "email": email
        }));

        var id = this.props.params.id;

        e.preventDefault();

        this.serverRequest = axios
            .put(__APIURL__ + '/contacts/' + id, updateContact, {headers: this.headers})
            .then(function (response) {

                th
                    .props
                    .router
                    .push('/customers/view/' + idEntity + "/1");

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;

        this.serverRequest = axios
            .get(__APIURL__ + "/contactTypes/")
            .then(function (result) {
                th.setState({contactTypes: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/contacts/" + id)
            .then(function (result) {

                th.setState({
                    name: ((result.data.name != undefined)
                        ? result.data.name
                        : ""),
                    telephone: ((result.data.telephone != undefined)
                        ? result.data.telephone
                        : ""),
                    mobilePhone: ((result.data.mobilePhone != undefined)
                        ? result.data.mobilePhone
                        : ""),
                    email: ((result.data.email != undefined)
                        ? result.data.email
                        : ""),
                    active: ((result.data.active != undefined)
                        ? result.data.active
                        : ""),
                    notifyProcesses: ((result.data.notifyProcesses != undefined)
                        ? result.data.notifyProcesses
                        : ""),
                    selectedContactType: ((result.data.contactType != undefined)
                        ? result.data.contactType
                        : "5923145b9248c53d640191c2")
                });
            })

    }
    render() {

        var contactTypeList = [];

                                          if(this.state.contactTypes[0] != undefined)
 this
            .state
            .contactTypes
            .forEach((item, i) => {
                
                contactTypeList.push(
                    <option key={i.toString()} value={item._id}>{item.description}</option>
                );
            })

        return (
            <ContentWrapper>
                <h3>Contacto
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar contacto</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Nome</label>
                                            <FormControl
                                                type="text"
                                                name="name"
                                                required="required"
                                                className="form-control"
                                                value={this.state.name}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'name')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">E-mail</label>
                                            <FormControl
                                                type="text"
                                                name="email"
                                                required="required"
                                                className="form-control"
                                                value={this.state.email}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'email')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Telemóvel</label>
                                            <FormControl
                                                type="text"
                                                name="mobilePhone"
                                                required="required"
                                                className="form-control"
                                                value={this.state.mobilePhone}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'mobilePhone')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Telefone</label>
                                            <FormControl
                                                type="text"
                                                name="telephone"
                                                required="required"
                                                className="form-control"
                                                value={this.state.telephone}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'telephone')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de contacto</label>
                                            <FormControl id="select2-1"
                                                componentClass="select"
                                                name="selectedContactType"
                                                className="form-control m-b"
                                                value={this.state.selectedContactType}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedContactType')}>
                                                {contactTypeList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">

                                            <div className="col-sm-3">
                                                <label className="control-label">Activo</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="active"
                                                            checked={this.state.active}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'active')}/>
                                                        <em className="fa fa-check"></em>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-sm-4">
                                                <label className="control-label">Notificar processos</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyProcesses"
                                                            checked={this.state.notifyProcesses}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'notifyProcesses')}/>
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/1")}
                                            type="button"
                                            className="btn ">Voltar ao cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
  componentDidMount() {
        var self = this;

        // Select 2

        var self = this;         if ($.fn.select2) {
            $('#select2-1').select2({theme: 'bootstrap'});
           
            $('#select2-1').on('change', function() {
                self.setState({ selectedContactType: $(this).val() });
            });
        }
    }
}

export default EditContact;
