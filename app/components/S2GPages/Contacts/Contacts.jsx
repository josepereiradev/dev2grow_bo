import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';

class Contacts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            contactType: [],
            entities: [],
            processes: [],
            interventions: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/contactTypes/",)
            .then(function (result) {
                th.setState({contactType: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/",)
            .then(function (result) {
                th.setState({entities: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processes/",)
            .then(function (result) {
                th.setState({processes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/interventions/",)
            .then(function (result) {
                th.setState({interventions: result.data});
            })

    }

    getDataFromBD() {
        axios
            .get(__APIURL__ + "/contacts/")
            .then(res => {
                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({data})
                    setTimeout(function () {
                        var dtContacts = $('#dtContacts').dataTable({
                            'pageLength': 100,
                            'retrieve': true, 'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'responsive': true, // https://datatables.net/extensions/responsive/examples/
                            // Text translation options Note the required keywords between underscores (e.g
                            // _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            // Datatable Buttons setup
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                {
                                    extend: 'copy',
                                    className: 'btn-sm',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'csv',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contactos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'excel',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contactos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'pdf',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contactos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'print',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contactos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs.keyup(function () {
                            dtContacts.fnFilter(this.value, columnInputs.index(this));
                        });
                    }.bind(this), 500);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    create() {
        this
            .props
            .router
            .push('/contacts/create');
    }

    edit(id, entityParent) {
        this
            .props
            .router
            .push('/contacts/edit/' + entityParent + '/' + id);
    }

    delete(name, id, idEntity) {
        var entityParentData;
        var self = this;

        var confirmDelete = true;
   
        if (!this.state.processes.error) {
            this
                .state
                .processes
                .forEach(process => {
                    if (process.requestedBy == id) {
                        confirmDelete = false;
                    } else {
                        process
                            .notify
                            .forEach(contactToNot => {
                                if (contactToNot == id) {
                                    confirmDelete = false;
                                }
                            })
                    }
                })
        }

        if (!this.state.interventions.error) {
            this
                .state
                .interventions
                .forEach(interv => {
                    if (interv.requestedBy == id) {
                        confirmDelete = false;
                    } else {
                        interv
                            .notify
                            .forEach(contactToNot => {
                                if (contactToNot == id) {
                                    confirmDelete = false;
                                }
                            })
                    }
                })
        }

        if (confirmDelete) {
            this.serverRequest = axios
                .get(__APIURL__ + "/entities/" + idEntity)
                .then(function (result) {
                    entityParentData = result.data;

                    var headers = new Headers({'Content-Type': 'application/json'});

                    swal({
                        title: "Tem a certeza que quer eliminar?",
                        text: "Após eliminar não poderá recuperar o registo!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Sim",
                        closeOnConfirm: false
                    }, function () {
                        axios
                            .delete(__APIURL__ + "/contacts/" + id)
                            .then(function (response) {
                                if (response.status == 200) {
                                    entityParentData
                                        .contacts
                                        .splice(entityParentData.contacts.indexOf(id), 1);
                                    var updateEntity = JSON.parse(JSON.stringify({
                                        "nif": entityParentData.nif,
                                        "qrCode": entityParentData.qrCode,
                                        "taxName": entityParentData.taxName,
                                        "abrevName": entityParentData.abrevName,
                                        "accountManager": entityParentData.accountManager,
                                        "contacts": entityParentData.contacts,
                                        "projects": entityParentData.projects,
                                        "processes": entityParentData.processes,
                                        "contracts": entityParentData.contracts,
                                        "associatedUsers": entityParentData.associatedUsers
                                    }));

                                    self.serverRequest = axios
                                        .put(__APIURL__ + '/entities/' + idEntity, updateEntity, {headers: self.headers})
                                        .then(function (response2) {})
                                        .catch(function (error) {
                                            console.log(error);
                                        });

                                    if (response.status == 200) {

                                        swal("Eliminado!", name + " foi eliminado.", "success");
                                        self.getDataFromBD();
                                    } else {
                                        // TODO: HANDLE IT
                                    }
                                }
                            })
                            .catch(function (error) {
                                console.log(error);
                                // TODO: HANDLE IT
                            });
                    });

                })
        } else {
            swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
        }

    }

    render() {
        const css = css;
        var rows = [];
        var notifyState;
        var activeState;
        var contactType = "";
        var entityParent = "";

        this
            .state
            .data
            .map(contact => {

                this
                    .state
                    .contactType
                    .forEach((obj, i) => {
                        if (contact.contactType == obj._id) {
                            contactType = obj.description;
                        }
                    })

                this
                    .state
                    .entities
                    .map((e, i) => {
                        for (var i = 0; i < this.state.entities.length; i++) {
                            for (var j = 0; j < e.contacts.length; j++) {
                                if (e.contacts[j] == contact._id) 
                                    entityParent = e;
                                }
                            }
                    })

                // o entityParentName é um apontador por isso acaba por ser sempre igual abaixo
                // com o de cima, mesmo que o valor esteja a ser actualizado
                var foo = JSON.stringify(entityParent);
                foo = JSON.parse(foo);

                if (contact.notifyProcesses == true) {
                    notifyState = <em className="fa fa-check"></em>
                } else {
                    notifyState = <em className="fa fa-close"></em>
                }
                if (contact.active == true) {
                    activeState = <span className="label label-success">Activo</span>
                } else {
                    activeState = <span className="label label-default">Inactivo</span>
                }

                rows.push(
                    <tr key={contact._id}>
                        <td>{contact.name}</td>
                        <td>{contactType}</td>
                        <td>{entityParent.abrevName}</td>
                        <td>{activeState}</td>
                        <td>{contact.telephone}</td>
                        <td>{contact.mobilePhone}</td>
                        <td>{contact.email}</td>
                        <td>
                            <button
                                onClick={() => this.edit(contact._id, foo._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(contact.name, contact._id, foo._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                )

                //  }  })
            })

        return (
            <ContentWrapper>
                <h3>Todos os contactos
                </h3>
                <style>
                    {
                        css
                    }
</style>
                {/*<button
                    onClick={() => this.create()}
                    type="button"
                    id="addBtn"
                    className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                    <i className="icon-plus"></i>
                </button>*/}
                <Row>
                    <Col lg={12}>
                        <Panel>
                            <Table id="dtContacts" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Tipo de Contacto</th>
                                        <th>Cliente</th>
                                        <th>Activo</th>
                                        <th>Telefone</th>
                                        <th>Telemóvel</th>
                                        <th>Email</th>

                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>{rows}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default Contacts;
