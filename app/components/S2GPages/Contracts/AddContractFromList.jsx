import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {Router, Route, Link, History, withRouter} from 'react-router';
import {browserHistory} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class AddContractFromList extends React.Component {
    constructor() {
        super();
        this.state = {
            beginDate: new Date(),
            endDate: moment()
                .add('years', 1)
                .subtract(1, "days"),
            lastRenewal: new Date(),
            notifyRenewal: false,
            hours: "",
            // perMonth: false,
            notifyExhaustedHours: false,
            blockExhaustedHours: false,
            contractTypes: [],
            contractStates: [],
            selectedContractType: "",
            selectedEntity: "",
            selectedContractState: "",
            addenda: [],
            contractCode: "",
            observations: "",
            //availableHours: "", exhaustedHours: "",

            entities: []
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
        //console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        var th = this;
        var hours = this.state.hours;
        const {
            beginDate, endDate, lastRenewal, notifyRenewal,
            // perMonth,
            notifyExhaustedHours,
            blockExhaustedHours,
            contractTypes,
            contractStates,
            selectedContractState,
            selectedContractType,
            addenda,
            contractCode,
            entityData,
            selectedEntity,
            observations
        } = this.state;
        hours = parseFloat(hours.replace(',', '.').replace('.', '.'));
        var decimalTime = parseFloat(hours);
        decimalTime = decimalTime * 60 * 60;
        var hours2 = Math.floor((decimalTime / (60 * 60)));
        decimalTime = decimalTime - (hours2 * 60 * 60);
        var minutes = Math.floor((decimalTime / 60));
        decimalTime = decimalTime - (minutes * 60);
        if (hours2 < 10) {
            hours2 = "0" + hours2;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        var hoursOnRightFormat = hours2 + ":" + minutes;

        var headers = new Headers({'Content-Type': 'application/json'});
        var newContract = JSON.parse(JSON.stringify({
            "beginDate": (beginDate == null || beginDate == ""
                ? new Date().toISOString()
                : new Date(beginDate).toISOString()),
            "endDate": (endDate == null || endDate == ""
                ? new Date().toISOString()
                : new Date(endDate).toISOString()),
            "lastRenewal": null,
            "notifyRenewal": notifyRenewal,
            "hours": hoursOnRightFormat,
            "availableHours": hoursOnRightFormat,
            "exhaustedHours": "00:00",
            // "perMonth": perMonth,
            "notifyExhaustedHours": notifyExhaustedHours,
            "blockExhaustedHours": blockExhaustedHours,
            "contractType": selectedContractType,
            "state": selectedContractState,
            "addenda": addenda,
            "contractCode": contractCode,
            "observations": observations
            //"availableHours": availableHours, "exhaustedHours": exhaustedHours,
        }));

        e.preventDefault();

        this.serverRequest = axios
            .post(__APIURL__ + '/contracts/', newContract, {headers: this.headers})
            .then(function (response) {

                th
                    .state
                    .entities
                    .forEach((entityData, i) => {
                        if (entityData._id == selectedEntity) {

                            if (entityData.contracts == null) {
                                entityData.contracts = ["" + response.data.obj._id + ""]

                            } else {
                                entityData
                                    .contracts
                                    .push(response.data.obj._id);
                            }

                            var updateEntity = JSON.parse(JSON.stringify({
                                "nif": entityData.nif,
                                "qrCode": entityData.qrCode,
                                "taxName": entityData.taxName,
                                "abrevName": entityData.abrevName,
                                "accountManager": entityData.accountManager,
                                "contacts": entityData.contacts,
                                "projects": entityData.projects,
                                "processes": entityData.processes,
                                "contracts": entityData.contracts,
                                "associatedUsers": entityData.associatedUsers
                            }));

                            th.serverRequest = axios
                                .put(__APIURL__ + '/entities/' + entityData._id, updateEntity, {headers: th.headers})
                                .then(function (response) {
                                    th
                                        .props
                                        .router
                                        .push('/customers/view/' + entityData._id + "/2");
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });

                        }
                    })
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.idEntity;

        this.serverRequest = axios
            .get(__APIURL__ + "/contractTypes/")
            .then(function (result) {
                th.setState({contractTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contractStates/")
            .then(function (result) {
                th.setState({contractStates: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/")
            .then(function (result) {
                th.setState({entities: result.data});
            })

    }

    render() {

        var contractTypeList = [];
        var contractStateList = [];
        var entitiesList = [];

        if (this.state.contractTypes[0] != undefined) 
            this.state.contractTypes.forEach((item, i) => {
                if (i == 0) {
                    contractTypeList.push(
                        <option disabled selected value key={i.toString()}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                contractTypeList.push(
                    <option
                        key={item
                        ._id
                        .toString()}
                        value={item._id}>{item.description}</option>
                );
            })
        if (this.state.contractStates[0] != undefined) 
            this.state.contractStates.forEach((item, i) => {
                if (i == 0) {
                    contractStateList.push(
                        <option disabled selected value key={i.toString()}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                contractStateList.push(
                    <option
                        key={item
                        ._id
                        .toString()}
                        value={item._id}>{item.stateName}</option>
                );
            })
        if (this.state.entities[0] != undefined) 
            this.state.entities.forEach((item, i) => {
                if (i == 0) {
                    entitiesList.push(
                        <option disabled selected value key={i.toString()}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                entitiesList.push(
                    <option
                        key={item
                        ._id
                        .toString()}
                        value={item._id}>{item.taxName}</option>
                );
            })

        return (
            <ContentWrapper>
                <h3>Contrato
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Adicionar um novo contrato</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-4">
                                            <label className="control-label">Código de contrato</label>
                                            <FormControl
                                                type="text"
                                                name="contractCode"
                                                required="required"
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'contractCode')}/>
                                        </div>
                                        <div className="col-sm-4">
                                            <label className="control-label">Horas</label>
                                            <FormControl
                                                type="text"
                                                name="hours"
                                                required="required"
                                                placeholder="0.0"
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'hours')}/>
                                        </div>
                                        <div className="col-sm-4">
                                            <label className="control-label">Entidade</label>
                                            <FormControl
                                                id="select2-1-entity"
                                                componentClass="select"
                                                name="selectedEntity"
                                                className="form-control m-b"
                                                value={this.state.selectedEntity}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedEntity')}>
                                                {entitiesList}

                                            </FormControl>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-4">
                                            <label className="control-label">Tipo de contrato</label>
                                            <FormControl
                                                id="select2-1-contractType"
                                                componentClass="select"
                                                name="selectedContractType"
                                                className="form-control m-b"
                                                value={this.state.selectedContractType}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedContractType')}>
                                                {contractTypeList}

                                            </FormControl>
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Data de início</label>
                                            <div id="datetimepickerBegin" className="input-group date">
                                                <input
                                                    name="beginDate"
                                                    type="text"
                                                    className="form-control"
                                                    value={moment(this.state.beginDate).format("DD/MM/YYYY")}
                                                    onClick={this
                                                    .handleChange
                                                    .bind(this, 'beginDate')}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Data de fim</label>
                                            <div id="datetimepickerEnd" className="input-group date">
                                                <input
                                                    name="endDate"
                                                    type="text"
                                                    className="form-control"
                                                    value={moment(this.state.endDate).format("DD/MM/YYYY")}
                                                    onClick={this
                                                    .handleChange
                                                    .bind(this, 'endDate')}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Estado</label>
                                            <FormControl
                                                id="select2-1-state"
                                                componentClass="select"
                                                name="selectedContractState"
                                                className="form-control m-b"
                                                value={this.state.selectedContractState}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedContractState')}>
                                                {contractStateList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Observações</label>
                                            <textarea
                                                id="textAreaObservations"
                                                name="observations"
                                                className="form-control"
                                                rows="3"
                                                value={this.state.observations}
                                                onChange={this
                                                .handleChange
                                                .bind(this, "observations")}/>

                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-12">

                                            <div className="col-sm-3">
                                                <label className="control-label">Notificar renovação</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyRenewal"
                                                            checked={this.state.notifyRenewal}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'notifyRenewal')}/>
                                                        <em className="fa fa-check"></em>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <label className="control-label">Notificar horas consumidas</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyExhaustedHours"
                                                            checked={this.state.notifyExhaustedHours}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'notifyExhaustedHours')}/>
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div>

                                            {/* <div className="col-sm-3">
                                                <label className="control-label">Por mês</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="perMonth"
                                                            checked={this.state.perMonth}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'perMonth')}/>
                                                        <em className="fa fa-check"></em>
                                                    </label>
                                                </div>
                                            </div> */}
                                            <div className="col-sm-3">
                                                <label className="control-label">Bloquear horas consumidas</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="blockExhaustedHours"
                                                            checked={this.state.blockExhaustedHours}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'blockExhaustedHours')}/>
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/2")}
                                            type="button"
                                            className="btn ">Voltar ao cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {

        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------
        var that = this;
        if ($.fn.datetimepicker) {

            $('#datetimepickerBegin')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function () {
                    let dateSelected = $('#datetimepickerBegin')
                        .data("DateTimePicker")
                        .date();
                    console.log(moment(dateSelected).add('years', 1));

                    that.setState({
                        beginDate: dateSelected,
                        endDate: moment(dateSelected)
                            .add('years', 1)
                            .subtract(1, "days")
                    });
                });

            $('#datetimepickerEnd')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function () {
                    let dateSelected = $('#datetimepickerEnd')
                        .data("DateTimePicker")
                        .date();
                    that.setState({endDate: dateSelected});
                });

            $('#datetimepickerLastRenewal')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function () {
                    let dateSelected = $('#datetimepickerLastRenewal')
                        .data("DateTimePicker")
                        .date();
                    that.setState({lastRenewal: dateSelected});
                });
            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-state').select2({theme: 'bootstrap'});
            $('#select2-1-contractType').select2({theme: 'bootstrap'});
            $('#select2-1-entity').select2({theme: 'bootstrap'});
            $('#select2-1-state').on('change', function () {
                self.setState({
                    selectedContractState: $(this).val()
                });
            });
            $('#select2-1-contractType').on('change', function () {
                self.setState({
                    selectedContractType: $(this).val()
                });
            });
            $('#select2-1-entity').on('change', function () {
                self.setState({
                    selectedEntity: $(this).val()
                });
            });
        }
    }
}

export default AddContractFromList;
