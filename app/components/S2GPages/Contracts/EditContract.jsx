import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class EditContract extends React.Component {

    constructor() {
        super();
        this.state = {
            beginDate: new Date(),
            endDate: new Date(),
            lastRenewal: new Date(),
            notifyRenewal: "",
            hours: "",
            // perMonth: false,
            notifyExhaustedHours: "",
            blockExhaustedHours: false,
            contractTypes: [],
            contractStates: [],
            selectedContractType: "",
            selectedContractState: "",
            addenda: [],
            contractCode: "",
            availableHours: "",
            exhaustedHours: "",
            observations: ""
        };

        this.handleChange = this
            .handleChange
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }
    calcDifBetweenA_B(ATime, BTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log("name: ", [e.target.name], "value: " + e.target.value)

    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
        console.log("name: ", [e.target.name], "value: " + e.target.checked)

    }

    handleSubmit(e) {
        var th = this;
        var hours = this.state.hours.toString();
        const {
            beginDate, endDate, lastRenewal, notifyRenewal,
            // perMonth,
            notifyExhaustedHours,
            blockExhaustedHours,
            contractTypes,
            contractStates,
            selectedContractState,
            selectedContractType,
            addenda,
            contractCode,
            availableHours,
            exhaustedHours,
            observations
        } = this.state;
      
        hours = parseFloat(hours.replace(',', '.').replace('.', '.'));
       
        var decimalTime = parseFloat(hours);
        decimalTime = decimalTime * 60 * 60;
        var hours2 = Math.floor((decimalTime / (60 * 60)));
        decimalTime = decimalTime - (hours2 * 60 * 60);
        var minutes = Math.floor((decimalTime / 60));
        decimalTime = decimalTime - (minutes * 60);
        if (hours2 < 10) {
            hours2 = "0" + hours2;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        var hoursOnRightFormat = hours2 + ":" + minutes;

        var headers = new Headers({'Content-Type': 'application/json'});
        var updateContract = JSON.parse(JSON.stringify({
            "beginDate": (beginDate == null
                ? new Date()
                : moment(beginDate, "DD/MM/YYYY").format()),
            "endDate": (endDate == null
                ? new Date()
                : moment(endDate, "DD/MM/YYYY").format()),
            "lastRenewal": (lastRenewal == "-"
                ? null
                : moment(lastRenewal, "DD/MM/YYYY").format()),
            "notifyRenewal": notifyRenewal,
            "hours": hoursOnRightFormat,
            // "perMonth": perMonth,
            "notifyExhaustedHours": notifyExhaustedHours,
            "blockExhaustedHours": blockExhaustedHours,
            "contractType": selectedContractType,
            "state": selectedContractState,
            "addenda": addenda,
            "contractCode": contractCode,
            "availableHours": availableHours,
            "exhaustedHours": exhaustedHours,
            "observations": observations
        }));

        updateContract.availableHours = this.calcDifBetweenA_B(hoursOnRightFormat, updateContract.exhaustedHours);

        var idEntity = this.props.params.idEntity;
        var id = this.props.params.id;

        e.preventDefault();
        console.log(updateContract);

        this.serverRequest = axios
            .put(__APIURL__ + '/contracts/' + id, updateContract, {headers: this.headers})
            .then(function (response) {
                console.log(response)
                th
                    .props
                    .router
                    .push('/customers/view/' + idEntity + "/2");

            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.id;

        this.serverRequest = axios
            .get(__APIURL__ + "/contractTypes/")
            .then(function (result) {
                th.setState({contractTypes: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contractStates/")
            .then(function (result) {
                th.setState({contractStates: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contracts/" + id)
            .then(function (result) {
                console.log(new Date(result.data.beginDate).toLocaleDateString());

                var hours = parseFloat(parseInt(result.data.hours.split(':')[0], 10) + '.' + parseInt((result.data.hours.split(':')[1] / 6) * 10, 10)); //transform a HH:mm into decimal
                th.setState({
                    beginDate: new Date(result.data.beginDate).toLocaleDateString(),
                    endDate: new Date(result.data.endDate).toLocaleDateString(),
                    lastRenewal: result.data.lastRenewal
                        ? new Date(result.data.lastRenewal).toLocaleDateString()
                        : "-",
                    notifyRenewal: ((result.data.notifyRenewal != undefined)
                        ? result.data.notifyRenewal
                        : false),
                    hours: ((result.data.hours != undefined)
                        ? hours
                        : "00:00"),
                    // perMonth: ((result.data.perMonth != undefined)     ? result.data.perMonth :
                    // false),
                    notifyExhaustedHours: ((result.data.notifyExhaustedHours != undefined)
                        ? result.data.notifyExhaustedHours
                        : false),
                    blockExhaustedHours: ((result.data.blockExhaustedHours != undefined)
                        ? result.data.blockExhaustedHours
                        : false),
                    selectedContractType: result.data.contractType,
                    selectedContractState: result.data.state,
                    availableHours: ((result.data.availableHours != undefined)
                        ? result.data.availableHours
                        : "Sem intervenções"),
                    exhaustedHours: ((result.data.exhaustedHours != undefined)
                        ? result.data.exhaustedHours
                        : "Sem intervenções"),
                    contractCode: ((result.data.contractCode != undefined)
                        ? result.data.contractCode
                        : ""),
                    observations: result.data.observations
                });
            })

    }

    render() {

        var contractTypeList = [];
        var contractStateList = [];

        if (this.state.contractTypes[0] != undefined) 
            this.state.contractTypes.forEach((item, i) => {
                if (i == 0 && this.state.selectedContractType == null) {
                    console.log(item.description)
                    this.setState({selectedContractType: item._id})
                }

                if(item._id == this.state.selectedContractType){
                    $('#select2-select2-1-contractType-container').text(item.description)
                }

                contractTypeList.push(
                    <option
                        key={item
                        ._id
                        .toString()}
                        value={item._id}>{item.description}</option>
                );
            })
        if (this.state.contractStates[0] != undefined) 
            this.state.contractStates.forEach((item, i) => {
                if (i == 0 && this.state.selectedContractState == null) {
                    console.log(item.stateName)
                    this.setState({selectedContractState: item._id})
                }

                if(item._id == this.state.selectedContractState){
                    $('#select2-select2-1-state-container').text(item.stateName)
                }

                contractStateList.push(
                    <option
                        key={item
                        ._id
                        .toString()}
                        value={item._id}>{item.stateName}</option>
                );
            })

        return (
            <ContentWrapper>
                <h3>Contrato
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Editar contrato</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Código de contrato</label>
                                            <FormControl
                                                type="text"
                                                name="contractCode"
                                                required="required"
                                                className="form-control"
                                                value={this.state.contractCode}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'contractCode')}/>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Horas</label>
                                            <FormControl
                                                type="text"
                                                name="hours"
                                                required="required"
                                                value={this.state.hours}
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'hours')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Tipo de contrato</label>
                                            <FormControl
                                                id="select2-1-contractType"
                                                componentClass="select"
                                                name="selectedContractType"
                                                className="form-control m-b"
                                                value={this.state.selectedContractType}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedContractType')}>
                                                {contractTypeList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-3">
                                            <label className="control-label">Horas disponíveis</label>
                                            <FormControl
                                                disabled
                                                type="text"
                                                name="availableHours"
                                                required="required"
                                                className="form-control"
                                                value={this.state.availableHours}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'availableHours')}/>
                                        </div>
                                        <div className="col-sm-3">
                                            <label className="control-label">Horas consumidas</label>
                                            <FormControl
                                                disabled
                                                type="text"
                                                name="exhaustedHours"
                                                required="required"
                                                value={this.state.exhaustedHours}
                                                className="form-control"
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'exhaustedHours')}/>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-4">
                                            <label className="control-label">Data de início</label>
                                            <div id="datetimepickerBegin" className="input-group date">
                                                <input
                                                    name="beginDate"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.beginDate}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Data de fim</label>
                                            <div id="datetimepickerEnd" className="input-group date">
                                                <input
                                                    name="endDate"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.endDate}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Data da última renovação</label>
                                            <div id="datetimepickerLastRenewal" className="input-group date">
                                                <input
                                                    disabled
                                                    name="lastRenewal"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.lastRenewal}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Estado</label>
                                            <FormControl
                                                id="select2-1-state"
                                                componentClass="select"
                                                name="selectedContractState"
                                                className="form-control m-b"
                                                value={this.state.selectedContractState}
                                                onChange={this
                                                .handleChange
                                                .bind(this, 'selectedContractState')}>
                                                {contractStateList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Observações</label>
                                            <textarea
                                                id="textAreaObservations"
                                                name="observations"
                                                className="form-control"
                                                rows="3"
                                                value={this.state.observations}
                                                onChange={this
                                                .handleChange
                                                .bind(this, "observations")}/>

                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-12">

                                            <div className="col-sm-3">
                                                <label className="control-label">Notificar renovação</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyRenewal"
                                                            checked={this.state.notifyRenewal}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'notifyRenewal')}/>
                                                        <em className="fa fa-check"></em>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <label className="control-label">Notificar horas consumidas</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyExhaustedHours"
                                                            checked={this.state.notifyExhaustedHours}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'notifyExhaustedHours')}/>
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div>

                                            {/* <div className="col-sm-3">
                                                <label className="control-label">Por mês</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="perMonth"
                                                            checked={this.state.perMonth}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'perMonth')}/>
                                                        <em className="fa fa-check"></em>
                                                    </label>
                                                </div>
                                            </div> */}
                                            <div className="col-sm-3">
                                                <label className="control-label">Bloquear horas consumidas</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="blockExhaustedHours"
                                                            checked={this.state.blockExhaustedHours}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'blockExhaustedHours')}/>
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </Row>
                                </div>
                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/contracts/')}
                                            type="button"
                                            className="btn ">Voltar para os contratos</button>
                                    </Col>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/2")}
                                            type="button"
                                            className="btn ">Voltar/Ir para o cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
    componentDidMount() {

        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider) 
            $('[data-ui-slider]').bootstrapSlider();
        
        // CHOSEN -----------------------------------

        if ($.fn.chosen) 
            $('.chosen-select').chosen();
        
        // MASKED -----------------------------------

        if ($.fn.inputmask) 
            $('[data-masked]').inputmask();
        
        // FILESTYLE -----------------------------------

        if ($.fn.filestyle) 
            $('.filestyle').filestyle();
        
        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg) 
            $('.wysiwyg').wysiwyg();
        
        // Tags -----------------------------------
        if ($.fn.tagsinput) 
            $("[data-role='tagsinput']").tagsinput()

            // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {
            $('#datetimepickerBegin')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerBegin')
                        .data("DateTimePicker")
                        .date();
                    this.setState({
                        beginDate: dateSelected.format('DD/MM/YYYY')
                    });
                }.bind(this));

            $('#datetimepickerEnd')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerEnd')
                        .data("DateTimePicker")
                        .date();
                    this.setState({
                        endDate: dateSelected.format('DD/MM/YYYY')
                    });
                }.bind(this));

            $('#datetimepickerLastRenewal')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function (e) {
                    let dateSelected = $('#datetimepickerLastRenewal')
                        .data("DateTimePicker")
                        .date();
                    this.setState({
                        lastRenewal: dateSelected.format('DD/MM/YYYY')
                    });
                }.bind(this));
            // only time
            $('#datetimepicker2').datetimepicker({format: 'LT'});

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-state').select2({theme: 'bootstrap'});
            $('#select2-1-contractType').select2({theme: 'bootstrap'});
            $('#select2-1-state').on('change', function () {
                self.setState({
                    selectedContractState: $(this).val()
                });
            });
            $('#select2-1-contractType').on('change', function () {
                self.setState({
                    selectedContractType: $(this).val()
                });
            });
        }
    }
}

export default EditContract;
