import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Tab,
    Tabs,
    Table,
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import {browserHistory} from 'react-router';
import {Router, Route, Link, History, withRouter} from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class ContractDetails extends React.Component {

    constructor() {
        super();
        this.state = {
            contract: null,
            beginDate: "",
            endDate: "",
            lastRenewal: "",
            notifyRenewal: false,
            hours: "",
            totalHoursAddendas: "00:00",
            perMonth: false,
            notifyExhaustedHours: false,
            blockExhaustedHours: false,
            contractTypes: [],
            contractStates: [],
            selectedContractType: "",
            selectedContractState: "",
            selectedContractTypeName: "",
            selectedContractStateName: "",
            addenda: [],
            addendaContract: [],
            contractCode: "",
            availableHours: "",
            exhaustedHours: "",
            entityData: [],

            contractInterventions: [],
            interventions: [],
            processes: [],
            interventionTypes: [],
            interventionMeans: [],
            allAddendas: [],
            users: [],
            key: 1
        };
    }

    handleSelect(key) {
        this.setState({key});
    }

    componentWillMount() {
        var th = this;
        var id = this.props.params.id;
        var idEntity = this.props.params.idEntity;
        var ttlHourAddenda = this.state.totalHoursAddendas;

        //parte de tratamento das addendas
        this.serverRequest = axios
            .get(__APIURL__ + "/addenda/",)
            .then(function (result1) {

                //  th.setState({addenda: result1.data});

                th.serverRequest = axios
                    .get(__APIURL__ + "/contracts/" + id)
                    .then(function (result2) {
                        th.setState({
                            addenda: result1.data,
                            contract: result2.data,
                            beginDate: new Date(result2.data.beginDate).toLocaleDateString(),
                            endDate: new Date(result2.data.endDate).toLocaleDateString(),
                            lastRenewal: result2.data.lastRenewal
                                ? new Date(result2.data.lastRenewal).toLocaleDateString()
                                : null,
                            notifyRenewal: ((result2.data.notifyRenewal != undefined)
                                ? result2.data.notifyRenewal
                                : false),
                            hours: ((result2.data.hours != undefined)
                                ? result2.data.hours
                                : "00:00"),
                            perMonth: ((result2.data.perMonth != undefined)
                                ? result2.data.perMonth
                                : false),
                            notifyExhaustedHours: ((result2.data.notifyExhaustedHours != undefined)
                                ? result2.data.notifyExhaustedHours
                                : false),
                            blockExhaustedHours: ((result2.data.blockExhaustedHours != undefined)
                                ? result2.data.blockExhaustedHours
                                : false),
                            addendaContract: result2.data.addenda,
                            selectedContractType: result2.data.contractType,
                            selectedContractState: result2.data.state,
                            availableHours: ((result2.data.availableHours != undefined)
                                ? result2.data.availableHours
                                : "Sem intervenções"),
                            exhaustedHours: ((result2.data.exhaustedHours != undefined)
                                ? result2.data.exhaustedHours
                                : "Sem intervenções"),
                            contractCode: ((result2.data.contractCode != undefined)
                                ? result2.data.contractCode
                                : "")
                        });
                        th.serverRequest = axios
                            .get(__APIURL__ + "/contractTypes/" + result2.data.contractType)
                            .then(function (result2) {
                                th.setState({selectedContractTypeName: result2.data.description})

                            })
                        th.serverRequest = axios
                            .get(__APIURL__ + "/contractStates/" + result2.data.state)
                            .then(function (result2) {
                                th.setState({selectedContractStateName: result2.data.stateName})

                            })
                        if (!th.state.addenda.error) {
                            th
                                .state
                                .addenda
                                .forEach(addenda => {

                                    th
                                        .state
                                        .addendaContract
                                        .forEach(addContract => {
                                            if (addenda._id == addContract) {

                                                ttlHourAddenda = th.calcSumBetweenA_B(ttlHourAddenda, addenda.hours);
                                            }
                                        });
                                });

                            th.setState({totalHoursAddendas: ttlHourAddenda});
                        }

                    })

                setTimeout(function () {
                    var dtAddenda = $('.dtAddenda').dataTable({
                        'retrieve': true, 'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'responsive': true, // https://datatables.net/extensions/responsive/examples/
                        // Text translation options Note the required keywords between underscores (e.g
                        // _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs.keyup(function () {
                        dtAddenda.fnFilter(this.value, columnInputs.index(this));
                    });
                })
            })

        //tratamento das intervenções:
        this.serverRequest = axios
            .get(__APIURL__ + "/interventions/")
            .then(function (result) {

                th.setState({interventions: result.data})

                for (var i = 0; i < th.state.interventions.length; i++) {

                    if (th.state.interventions[i].contractToCharge == th.props.params.id) {

                        th
                            .state
                            .contractInterventions
                            .push(th.state.interventions[i])

                        setTimeout(function () {
                            var dtInterventions = $('.dtInterventions').dataTable({
                                'retrieve': true, 'paging': true, // Table pagination
                                'ordering': true, // Column ordering
                                'info': true, // Bottom left status text
                                'responsive': true, // https://datatables.net/extensions/responsive/examples/
                                // Text translation options Note the required keywords between underscores (e.g
                                // _MENU_)
                                oLanguage: {
                                    sSearch: 'Search all columns:',
                                    sLengthMenu: '_MENU_ records per page',
                                    info: 'Showing page _PAGE_ of _PAGES_',
                                    zeroRecords: 'Nothing found - sorry',
                                    infoEmpty: 'No records available',
                                    infoFiltered: '(filtered from _MAX_ total records)'
                                }
                            });
                            var inputSearchClass = 'datatable_input_col_search';
                            var columnInputs = $('tfoot .' + inputSearchClass);

                            // On input keyup trigger filtering
                            columnInputs.keyup(function () {
                                dtInterventions.fnFilter(this.value, columnInputs.index(this));
                            });
                        })
                    }
                }
            });

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + idEntity)
            .then(function (result) {
                th.setState({entityData: result.data});
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/interventionTypes/")
            .then(function (result) {
                th.setState({interventionTypes: result.data})
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/interventionMeans/")
            .then(function (result) {
                th.setState({interventionMeans: result.data})
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/users/")
            .then(function (result) {
                th.setState({users: result.data})
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/processes/")
            .then(function (result) {
                th.setState({processes: result.data})
            })

    }

    calcSumBetweenA_B(ATime, BTime) {
        //ATime: '10:00' BTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesInterv + minutesProcess;
        var updatedHours = hoursInterv + hoursProcess;

        if (updatedMinutes >= 60) {
            updatedMinutes = updatedMinutes - 60;
            updatedHours++;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    calcDifBetweenA_B(ATime, BTime) {
        //processTime: '10:00' interventionTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesProcess - minutesInterv;
        var updatedHours = hoursProcess - hoursInterv;
        if (updatedMinutes < 0) {
            updatedMinutes = 60 + updatedMinutes;
            updatedHours--;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    createAddenda() {
        this
            .props
            .router
            .push('/addenda/create/' + this.props.params.idEntity + '/' + this.props.params.id);
    }

    editAddenda(id) {
        this
            .props
            .router
            .push('/addenda/edit/' + this.props.params.idEntity + '/' + this.props.params.id + '/' + id);
    }

    deleteAddenda(id) {
        var self = this;
        swal({
            title: "Tem a certeza que quer eliminar a adenda seleccionada?",
            text: "Após eliminar não poderá recuperar o registo!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            closeOnConfirm: true
        }, function () {
            axios
                .delete(__APIURL__ + "/addenda/" + id)
                .then(function (response) {
                    if (response.status == 200) {
                        var newAddendaList = [];
                        var allAddendas = [];
                        var newHoursAvailable = self.state.availableHours;
                        var newTtlHoursAdd = self.state.totalHoursAddendas;
                        var addnContract = self.state.addendaContract;

                        self
                            .state
                            .addenda
                            .forEach(objAddenda => {

                                if (objAddenda._id == id) {
                                    newTtlHoursAdd = self.calcDifBetweenA_B(newTtlHoursAdd, objAddenda.hours); //calculo das horas totais addendas
                                    newHoursAvailable = self.calcDifBetweenA_B(self.calcSumBetweenA_B(newTtlHoursAdd, self.state.hours), self.state.exhaustedHours); //calculo das horas disponiveis

                                } else {
                                    allAddendas.push(objAddenda);
                                    self
                                        .state
                                        .contract
                                        .addenda
                                        .forEach(addendaID => {
                                            if (addendaID == objAddenda._id) {
                                                newAddendaList.push(objAddenda._id);
                                            }
                                        });

                                }
                            });

                        var updateContract = JSON.parse(JSON.stringify({
                            "beginDate": (self.state.beginDate == null
                                ? new Date()
                                : moment(self.state.beginDate, "DD/MM/YYYY").format()),
                            "endDate": (self.state.endDate == null
                                ? new Date()
                                : moment(self.state.endDate, "DD/MM/YYYY").format()),
                            "lastRenewal": (self.state.lastRenewal == null
                                ? null
                                : moment(self.state.lastRenewal, "DD/MM/YYYY").format()),
                            "notifyRenewal": self.state.notifyRenewal,
                            "hours": self.state.hours,
                            "perMonth": self.state.perMonth,
                            "notifyExhaustedHours": self.state.notifyExhaustedHours,
                            "blockExhaustedHours": self.state.blockExhaustedHours,
                            "contractType": self.state.selectedContractType,
                            "state": self.state.selectedContractState,
                            "addenda": newAddendaList,
                            "contractCode": self.state.contractCode,
                            "availableHours": newHoursAvailable,
                            "exhaustedHours": self.state.exhaustedHours,
                            "observations": self.state.contract.observations
                        }));

                        axios
                            .put(__APIURL__ + '/contracts/' + self.props.params.id, updateContract, {headers: self.headers})
                            .then(function (response) {

                                self.setState({addenda: allAddendas, addendaContract: newAddendaList, availableHours: newHoursAvailable, totalHoursAddendas: newTtlHoursAdd});
                                swal("Eliminado!", "Adenda eliminada.", "success");

                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    // TODO: HANDLE IT
                });
        });
    }

    view(id) {
        this
            .props
            .router
            .push('/interventions/view/' + id);
    }

    create() {

        this
            .props
            .router
            .push('/interventions/create/' + this.props.params.idEntity + '/' + this.state.contract._id + "/1");
    }

    edit(customer, process, id) {
        this
            .props
            .router
            .push('/interventions/edit/' + customer + '/' + process + '/' + id);
    }

    delete(name, id) {
        var self = this;
        var contador = 0;
        swal({
            title: "Tem a certeza que quer eliminar?",
            text: "Após eliminar não poderá recuperar o registo!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            closeOnConfirm: false
        }, function () {

            axios
                .get(__APIURL__ + "/interventions/" + id)
                .then(function (intervResponse) {

                    axios
                        .delete(__APIURL__ + "/interventions/" + id)
                        .then(function (response) {
                            if (response.status == 200) {
                                if (!intervResponse.data.toBeValidated) {
                                    var availableHours = self.calcSumBetweenA_B(self.state.contract.availableHours, moment(intervResponse.data.timeToCharge).format("HH:mm"));
                                    var exhaustedHours = self.calcDifBetweenA_B(self.state.contract.exhaustedHours, moment(intervResponse.data.timeToCharge).format("HH:mm"));
                                    var updateContract = JSON.parse(JSON.stringify({
                                        "beginDate": (self.state.contract.beginDate == null
                                            ? new Date()
                                            : self.state.contract.beginDate),
                                        "endDate": (self.state.contract.endDate == null
                                            ? new Date()
                                            : self.state.contract.endDate),
                                        "lastRenewal": (self.state.contract.lastRenewal == null
                                            ? null
                                            : self.state.contract.lastRenewal),
                                        "notifyRenewal": self.state.contract.notifyRenewal,
                                        "hours": self.state.contract.hours,
                                        "perMonth": self.state.contract.perMonth,
                                        "notifyExhaustedHours": self.state.contract.notifyExhaustedHours,
                                        "blockExhaustedHours": self.state.contract.blockExhaustedHours,
                                        "contractType": self.state.contract.contractType,
                                        "state": self.state.contract.state,
                                        "addenda": self.state.contract.addenda,
                                        "contractCode": self.state.contract.contractCode,
                                        "availableHours": availableHours,
                                        "exhaustedHours": exhaustedHours,
                                        "observations": self.state.contract.observations
                                    }));
                                    self.state.contract.availableHours = availableHours;
                                    self.state.contract.exhaustedHours = exhaustedHours;

                                    axios
                                        .put(__APIURL__ + '/contracts/' + intervResponse.data.contractToCharge, updateContract, {headers: self.headers})
                                        .then(function (response) {
                                            contador++;
                                            if (contador == 2) {
                                                swal("Eliminado!", name + " foi eliminado.", "success");
                                                var newInterv = [];
                                                self
                                                    .state
                                                    .contractInterventions
                                                    .forEach(contInterv => {
                                                        if (contInterv._id != id) {
                                                            newInterv.push(contInterv);
                                                        }
                                                    })
                                                self.setState({contractInterventions: newInterv})
                                            }

                                        })
                                } else {
                                    contador++;
                                    if (contador == 2) {
                                        swal("Eliminado!", name + " foi eliminado.", "success");
                                        var newInterv = [];
                                        self
                                            .state
                                            .contractInterventions
                                            .forEach(contInterv => {
                                                if (contInterv._id != id) {
                                                    newInterv.push(contInterv);
                                                }
                                            })
                                        self.setState({contractInterventions: newInterv})
                                    }
                                }

                            } else {
                                // TODO: HANDLE IT
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                            // TODO: HANDLE IT
                        });
                    if (!intervResponse.data.toBeValidated) {
                        self
                            .state
                            .processes
                            .forEach(proc => {
                                var interventionsOfProcess = proc.interventions;
                                interventionsOfProcess.forEach(inter => {
                                    if (inter == id) {
                                        var updateProcess = JSON.parse(JSON.stringify({
                                            "processNumber": proc.processNumber,
                                            "date": (proc.date == null
                                                ? moment().format()
                                                : proc.date),
                                            "description": proc.description,
                                            "entity": proc.entity,
                                            "createdBy": proc.createdBy,
                                            "requestedBy": proc.requestedBy,
                                            "collaborator": proc.collaborator,
                                            "notify": proc.notify,
                                            "processType": proc.processType,
                                            "state": proc.state,
                                            "interventions": proc.interventions,
                                            "duration": proc.duration
                                        }));
                                        updateProcess.duration = self.calcDifBetweenA_B(updateProcess.duration, moment(intervResponse.data.timeToCharge).format("HH:mm"));
                                        var newInterventionsList = [];
                                        proc.duration = updateProcess.duration;
                                        updateProcess
                                            .interventions
                                            .forEach(interv => {
                                                if (interv != id) {
                                                    newInterventionsList.push(interv);
                                                }
                                            })
                                        updateProcess.interventions = newInterventionsList;
                                        proc.interventions = newInterventionsList;

                                        axios
                                            .put(__APIURL__ + '/processes/' + proc._id, updateProcess, {headers: self.headers})
                                            .then(function (response) {
                                                contador++;
                                                if (contador == 2) {
                                                    swal("Eliminado!", name + " foi eliminado.", "success");
                                                    var newInterv = [];
                                                    self
                                                        .state
                                                        .contractInterventions
                                                        .forEach(contInterv => {
                                                            if (contInterv._id != id) {
                                                                newInterv.push(contInterv);
                                                            }
                                                        })
                                                    self.setState({contractInterventions: newInterv})
                                                }
                                            })
                                    }
                                })
                            })
                    } else {
                        contador++;
                        if (contador == 2) {
                            swal("Eliminado!", name + " foi eliminado.", "success");
                            var newInterv = [];
                            self
                                .state
                                .contractInterventions
                                .forEach(contInterv => {
                                    if (contInterv._id != id) {
                                        newInterv.push(contInterv);
                                    }
                                })
                            self.setState({contractInterventions: newInterv})
                        }
                    }

                })

        });
    }

    renderAddendaTable() {

        var addendaRows = []

        //Popular a linha de registos das adendas
        if (!this.state.addenda.error) {

            this
                .state
                .addenda
                .map(addendum => {

                    this
                        .state
                        .addendaContract
                        .forEach((ac, i) => {
                            if (addendum._id == ac) {
                                //para essa adenda faz o push para addenda rows
                                console.log(addendum.hours);
                                addendaRows.push(
                                    <tr key={addendum._id} className="active">
                                        <td>{addendum.hours}</td>
                                        <td>{new Date(addendum.date).toLocaleDateString()}</td>
                                        <td>
                                            <button
                                                onClick={() => this.editAddenda(addendum._id, 'addenda')}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-pencil"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <button
                                                onClick={() => this.deleteAddenda(addendum._id)}
                                                type="button"
                                                id="lineBtns"
                                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                                <i className="icon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                );
                            }
                        })
                })
        }

        return addendaRows;
    };

    renderInterventionsTable() {
        var th = this;
        var interventionRows = []
        var interventionT = "";
        var interventionM = "";
        var collaboratorName = "";
        var customer = "";
        var offerSymbol;
        var complaintSymbol;
        var process = "";
        //   var validatedSymbol = <em className="fa fa-close"></em>;   var
        // terminateSymbol = <em className="fa fa-close"></em>; Popular a linha de
        // registos das intervenções
        this
            .state
            .contractInterventions
            .forEach((contractInterv, i) => {

                th
                    .state
                    .interventionTypes
                    .forEach((intervT, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (contractInterv.interventionType == intervT._id) {

                            interventionT = intervT.description;
                        }
                    })
                th
                    .state
                    .interventionMeans
                    .forEach((intervM, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (contractInterv.means == intervM._id) {

                            interventionM = intervM.description;
                        }
                    })
                th
                    .state
                    .users
                    .forEach((collaborator, j) => {
                        //para esse contacto verifica o nome da descrição do tipo de contacto
                        if (contractInterv.collaborator == collaborator._id) {

                            collaboratorName = collaborator.name;
                        }
                    })
                th
                    .state
                    .processes
                    .forEach(proc => {
                        var intervOfProcess = proc.interventions;

                        intervOfProcess.forEach(interP => {
                            if (interP == contractInterv._id) {
                                process = proc;
                            }
                        })
                    })

                if (contractInterv.offer == true) {
                    offerSymbol = <em className="fa fa-check"></em>
                } else {
                    offerSymbol = <em className="fa fa-close"></em>
                }
                if (contractInterv.complaint == true) {
                    complaintSymbol = <em className="fa fa-check"></em>
                } else {
                    complaintSymbol = <em className="fa fa-close"></em>;
                }
                var toBeValidated;
                if (contractInterv.toBeValidated) {
                    toBeValidated = <em className="fa fa-close"></em>
                } else {
                    toBeValidated = <em className="fa fa-check"></em>
                }
                //para esse contacto faz o push para contact rows
                interventionRows.push(
                    <tr key={contractInterv._id} className="active">
                        <td>{contractInterv.interventionNumber}</td>
                        <td>{interventionT}</td>
                        <td>{collaboratorName}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{moment(contractInterv.date).format("DD/MM/YYYY")}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{moment(contractInterv.beginHour).format("HH:mm")}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{moment(contractInterv.endHour).format("HH:mm")}</td>
                        <td>{moment(contractInterv.totalTime).format("HH:mm")}</td>
                        <td>{moment(contractInterv.timeToCharge).format("HH:mm")}</td>
                        <td>{toBeValidated}</td>
                        <td>
                            <button
                                onClick={() => this.view(contractInterv._id, contractInterv.entity, contractInterv.process)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-eye"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.edit(process.entity, process._id, contractInterv._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(contractInterv.interventionNumber, contractInterv._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                );

            })

        return interventionRows;
    };

    renewContract(){
        this.props.router.push('/contracts/create/' + this.props.params.idEntity + "/" + this.state.contract._id);
    }

    render() {

        const css = css;
        var tableAddenda = [];
        var tableInterventions = [];

        //tratamentos dos contactos
        if (this.state.addendaContract[0] != null) {
            tableAddenda.push(
                <Table className="dtAddenda">
                    <thead>
                        <tr className="active">
                            <th>Horas</th>
                            <th>Data</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderAddendaTable()}
                    </tbody>
                </Table>

            );

        } else {
            tableAddenda.push(
                <Col lg={12}>
                    <b>Este contrato não tem adendas</b>
                </Col>
            )
        }

        //tratamentos dos intervenções

        tableInterventions.push(
            <Table className="dtInterventions">
                <thead>
                    <tr className="active">
                        <th>Número de intervenção</th>
                        <th>Tipo de intervenção</th>
                        <th>Colaborador</th>
                        <th>Data</th>
                        <th>Hora de início</th>
                        <th>Hora de fim</th>
                        <th>Tempo real</th>
                        <th>Tempo cobrado</th>
                        <th>Validado</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderInterventionsTable()}
                </tbody>
            </Table>

        );

        var notifyRenewalSN = "",
            notifyExhaustedHoursSN = "",
            perMonthSN = "",
            blockExhaustedHoursSN = "";

        //validar os números 1 e 0 e booleans para sim ou não
        this.state.notifyRenewal == 1
            ? notifyRenewalSN = 'Sim'
            : notifyRenewalSN = 'Não';
        this.state.notifyExhaustedHours == 1
            ? notifyExhaustedHoursSN = 'Sim'
            : notifyExhaustedHoursSN = 'Não';
        this.state.perMonth == true
            ? perMonthSN = 'Sim'
            : perMonthSN = 'Não';
        this.state.blockExhaustedHours == true
            ? blockExhaustedHoursSN = 'Sim'
            : blockExhaustedHoursSN = 'Não';

        var a = this
            .state
            .hours
            .split(':');
        var minutesContractHours = (+ a[0]) * 60 + (+ a[1]);

        var b = this
            .state
            .availableHours
            .split(':');
        var minutesAvailableContractHours = (+ b[0]) * 60 + (+ b[1]);

        var newContractStyle = { marginLeft: "20px" };
        var availableHoursLabel = {};

        if (minutesContractHours * 0.2 > minutesAvailableContractHours) {
            availableHoursLabel = { fontWeight: "bold", color: "red" };
            newContractStyle.backgroundColor = "#DAE047";
        }

        return (
            <ContentWrapper>
                <h3>Contrato de {this.state.entityData.taxName}
                </h3 >
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">Detalhes do contrato</div>
                            </div>
                            <div className="panel-body">
                                <Row>
                                    <Col lg={5}>
                                        <div className="form-group">
                                            <label className="control-label">Código do contrato:&nbsp;</label>{this.state.contractCode}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Estado:&nbsp;</label>{this.state.selectedContractStateName}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Tipo de contrato:&nbsp;</label>{this.state.selectedContractTypeName}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Data de início:&nbsp;</label>{this.state.beginDate}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Data de fim:&nbsp;</label>{this.state.endDate}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Última renovação:&nbsp;</label>{this.state.lastRenewal
                                                ? this.state.lastRenewal
                                                : "-"}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Notificar renovação:&nbsp;</label>{notifyRenewalSN}
                                        </div>
                                    </Col>
                                    <Col lg={5}>

                                        <div className="form-group">
                                            <label className="control-label">Notificar horas consumidas:&nbsp;</label>{notifyExhaustedHoursSN}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Bloquear horas consumidas:&nbsp;</label>{blockExhaustedHoursSN}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Por mês:&nbsp;</label>{perMonthSN}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Horas contratadas:&nbsp;</label>{this.state.hours}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Horas totais adendas:&nbsp;</label>{this.state.totalHoursAddendas}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Horas consumidas:&nbsp;</label>{this.state.exhaustedHours}
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Horas disponíveis:&nbsp;</label>
                                            <span style={availableHoursLabel}>{this.state.availableHours}</span>
                                        </div>

                                    </Col>
                                </Row>
                                <Row>
                                    <Col lg={10}>
                                        <div className="form-group">
                                            <button
                                                type="button"
                                                onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/2")}
                                                className="btn ">Voltar ao cliente</button>
                                            <button
                                                style={newContractStyle}
                                                onClick={this.renewContract.bind(this)}
                                                className="btn " type="button" >Renovar contrato</button>
                                        </div>
                                    </Col>
                                </Row>
                            </div>

                            {/* END panel */}
                            {/* START tabs */}
                            <Tabs
                                activeKey={this.state.key}
                                onSelect={this
                                .handleSelect
                                .bind(this)}
                                justified
                                id="tabID">
                                <Tab eventKey={1} title="Adendas">
                                    <button
                                        onClick={() => this.createAddenda()}
                                        type="button"
                                        id="addBtn"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                        <i className="icon-plus"></i>
                                    </button>
                                    <Row>

                                        {/* data table de adendas */}
                                        {tableAddenda}
                                    </Row>

                                </Tab>
                                <Tab eventKey={2} title="Intervenções">
                                    <button
                                        onClick={() => this.create()}
                                        type="button"
                                        id="addBtn"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                        <i className="icon-plus"></i>
                                    </button>
                                    <Row>

                                        {/* data table de adendas */}
                                        {tableInterventions}
                                    </Row>
                                </Tab>

                            </Tabs>
                            {/* END tabs */}

                        </div>
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
}

export default ContractDetails;