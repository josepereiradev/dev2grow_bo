import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import ContentWrapper from '../../Layout/ContentWrapper';
import { Router, Route, Link, History, withRouter } from 'react-router';
import { browserHistory } from 'react-router';
import * as CONST from '../../Common/constants';

import css from '../../../styles/tableIcons.scss'

class AddContract extends React.Component {
    constructor() {
        super();
        this.state = {

            contractToRenew: null,

            beginDate: new Date(),
            endDate: moment().add('years', 1).subtract(1, "days"),
            lastRenewal: new Date(),
            notifyRenewal: false,
            hours: "",
            // perMonth: false,
            notifyExhaustedHours: false,
            blockExhaustedHours: false,
            contractTypes: [],
            contractStates: [],
            selectedContractType: "",
            selectedContractState: "",
            addenda: [],
            contractCode: "",
            observations: "",
            //availableHours: "", exhaustedHours: "",

            entityData: []
        };

        this.headers = new Headers({ 'Content-Type': 'application/json' });

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(name, e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleCheckbox(name, e) {
        this.setState({
            [e.target.name]: e.target.checked
        });
    }

    dateToString(date) {
        if (date == null || date == "") {
            return new Date().toISOString();
        }
        return new Date(date).toISOString();
    }

    createNewContract(newContract, entityData, isRenew) {
        var id = this.props.params.idEntity;
        
        axios
            .post(__APIURL__ + '/contracts/', newContract, { headers: this.headers })
            .then(response => {
                let contractId = JSON.parse(JSON.stringify(response.data.id));
                if (entityData.contracts) {
                    entityData.contracts.push(contractId);
                } else {
                    entityData.contracts = [contractId];
                }

                var updateEntity = JSON.parse(JSON.stringify({
                    "nif": entityData.nif,
                    "qrCode": entityData.qrCode,
                    "taxName": entityData.taxName,
                    "abrevName": entityData.abrevName,
                    "accountManager": entityData.accountManager,
                    "contacts": entityData.contacts,
                    "projects": entityData.projects,
                    "processes": entityData.processes,
                    "contracts": entityData.contracts,
                    "associatedUsers": entityData.associatedUsers
                }));

                axios
                    .put(__APIURL__ + '/entities/' + id, updateEntity, { headers: this.headers })
                    .then(response => {

                        if(isRenew){
                            let contractToRenew = this.state.contractToRenew;
                            let renewState = JSON.parse(JSON.stringify(this.state.contractStates[1]._id));
                            let oldContract = {
                                beginDate: this.dateToString(contractToRenew.beginDate),
                                endDate: this.dateToString(contractToRenew.endDate),
                                lastRenewal: this.state.beginDate,
                                notifyRenewal: contractToRenew.notifyRenewal,
                                hours: contractToRenew.hours,
                                availableHours: contractToRenew.availableHours,
                                exhaustedHours: contractToRenew.exhaustedHours,
                                // perMonth: contractToRenew.perMonth,
                                notifyExhaustedHours: contractToRenew.notifyExhaustedHours,
                                blockExhaustedHours: contractToRenew.blockExhaustedHours,
                                contractType: contractToRenew.contractType,
                                state: renewState,
                                addenda: contractToRenew.addenda,
                                contractCode: contractToRenew.contractCode,
                                observations: contractToRenew.observations
                            };
    
                            axios
                                .put(__APIURL__ + '/contracts/' + this.state.contractToRenew._id, oldContract, { headers: this.headers })
                                .then(() => {
                                    this.props.router.push('/customers/view/' + id + "/2");
                                })
                                .catch(function (error) {
                                    console.log('Deu bot');
                                    console.log(error);
                                });
                        } else {
                            this.props.router.push('/customers/view/' + id + "/2");
                        }
                    })
                    .catch(function (error) {
                        console.log('Deu bot');
                        console.log(error);
                    });
            })
    }

    handleSubmit(e) {

        var th = this;
        var hours = this.state.hours.toString();
        const {
            beginDate,
            endDate,
            lastRenewal,
            notifyRenewal,
            // perMonth,
            notifyExhaustedHours,
            blockExhaustedHours,
            contractTypes,
            contractStates,
            selectedContractState,
            selectedContractType,
            addenda,
            contractCode,
            entityData,
            observations
        } = this.state;

        hours = parseFloat(hours.replace(',', '.').replace('.', '.'));

        var decimalTime = parseFloat(hours);
        decimalTime = decimalTime * 60 * 60;
        var hours2 = Math.floor((decimalTime / (60 * 60)));
        decimalTime = decimalTime - (hours2 * 60 * 60);
        var minutes = Math.floor((decimalTime / 60));
        decimalTime = decimalTime - (minutes * 60);
        if (hours2 < 10) {
            hours2 = "0" + hours2;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        var hoursOnRightFormat = hours2 + ":" + minutes;

        // var headers = new Headers({ 'Content-Type': 'application/json' });

        let newState = JSON.parse(JSON.stringify(this.state.contractStates[0]._id));
        let newContract = {
            beginDate: this.dateToString(beginDate),
            endDate: this.dateToString(endDate),
            lastRenewal: null,
            notifyRenewal: notifyRenewal,
            hours: hoursOnRightFormat,
            availableHours: hoursOnRightFormat,
            exhaustedHours: '00:00',
            // "perMonth": perMonth,
            notifyExhaustedHours: notifyExhaustedHours,
            blockExhaustedHours: blockExhaustedHours,
            contractType: selectedContractType,
            state: newState,
            addenda: addenda,
            contractCode: contractCode,
            observations: observations
        }

        var id = this.props.params.idEntity;

        e.preventDefault();

        //Renovar Contrato
        if (this.props.params.idContract) {

            var leftAvaialableMilliseconds = moment.duration(th.state.contractToRenew.availableHours, "HH:mm").asMilliseconds();
            var newAddendum;
            if (leftAvaialableMilliseconds != 0) {
                newAddendum = {
                    hours: this.state.contractToRenew.availableHours,
                    date: this.state.beginDate,
                    observations: "Horas transitadas"
                };
            }

            swal({
                title: "Contrato com horas ainda disponiveis!",
                text: "Deseja manter após a renovação as horas em excesso?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, manter horas em excesso",
                cancelButtonText: "Não",
                closeOnConfirm: true
            }, (isConfirm) => {

                if (isConfirm) {
                    axios
                        .post(__APIURL__ + '/addenda/', newAddendum, { headers: this.headers })
                        .then(response => {
                            let addendaId = JSON.parse(JSON.stringify(response.data.id));
                            newContract.addenda.push(addendaId);
                            newContract.availableHours = this.calcSumBetweenA_B(hoursOnRightFormat, this.state.contractToRenew.availableHours);

                            this.createNewContract(newContract, entityData, true);
                        })
                } else {
                    this.createNewContract(newContract, entityData, true);
                }
            })
        } else {
            this.createNewContract(newContract, entityData, false);
        }
    }

    componentWillMount() {

        var th = this;
        var id = this.props.params.idEntity;
        if (this.props.params.idContract) {
            this.serverRequest = axios
                .get(__APIURL__ + "/contracts/" + th.props.params.idContract)
                .then(function (result) {

                    var hoursDecimal = parseFloat(parseInt(result.data.hours.split(':')[0], 10) + '.' + parseInt((result.data.hours.split(':')[1] / 6) * 10, 10));

                    th.setState({
                        contractToRenew: result.data,
                        beginDate: moment(result.data.endDate).add('days', 1), //contracto começa um dia depois de expirar
                        endDate: moment(result.data.endDate).add('years', 1), //contracto dura 1 ano
                        lastRenewal: result.data.lastRenewal,
                        notifyRenewal: result.data.notifyRenewal,
                        hours: hoursDecimal,
                        // perMonth: result.data.perMonth,
                        notifyExhaustedHours: result.data.notifyExhaustedHours,
                        blockExhaustedHours: result.data.blockExhaustedHours,
                        selectedContractType: result.data.contractType,
                        selectedContractState: result.data.state,
                        contractCode: result.data.contractCode
                    });
                })
        }
        this.serverRequest = axios
            .get(__APIURL__ + "/contractTypes/")
            .then(function (result) {

                th.setState({ contractTypes: result.data });
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contractStates/")
            .then(function (result) {

                th.setState({ contractStates: result.data });
            })

        this.serverRequest = axios
            .get(__APIURL__ + "/entities/" + id)
            .then(function (result) {
                th.setState({ entityData: result.data });
            })
    }

    componentDidMount() {
        var that = this;
        // BOOTSTRAP SLIDER CTRL -----------------------------------

        if ($.fn.bootstrapSlider)
            $('[data-ui-slider]').bootstrapSlider();

        // CHOSEN -----------------------------------

        if ($.fn.chosen)
            $('.chosen-select').chosen();

        // MASKED -----------------------------------

        if ($.fn.inputmask)
            $('[data-masked]').inputmask();

        // FILESTYLE -----------------------------------

        if ($.fn.filestyle)
            $('.filestyle').filestyle();

        // WYSIWYG -----------------------------------

        if ($.fn.wysiwyg)
            $('.wysiwyg').wysiwyg();

        // Tags -----------------------------------
        if ($.fn.tagsinput)
            $("[data-role='tagsinput']").tagsinput()

        // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepickerBegin')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function () {
                    let dateSelected = $('#datetimepickerBegin')
                        .data("DateTimePicker")
                        .date();
                    that.setState({ beginDate: dateSelected, endDate: moment(dateSelected).add('years', 1).subtract(1, "days") });
                });

            $('#datetimepickerEnd')
                .datetimepicker({
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                })
                .on('dp.change', function () {
                    let dateSelected = $('#datetimepickerEnd')
                        .data("DateTimePicker")
                        .date();
                    that.setState({ endDate: dateSelected });
                });

            /*$('#datetimepickerLastRenewal').datetimepicker({
                format: 'DD/MM/YYYY',
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-crosshairs',
                    clear: 'fa fa-trash'
                }
            })
                .on('dp.change', function () {
                    let dateSelected = $('#datetimepickerLastRenewal')
                        .data("DateTimePicker")
                        .date();
                        that.setState({lastRenewal: dateSelected});
                });*/

            // only time
            $('#datetimepicker2').datetimepicker({ format: 'LT' });

        }

        if ($.fn.colorpicker) {

            $('.demo-colorpicker').colorpicker();

            $('#demo_selectors').colorpicker({
                colorSelectors: {
                    'default': '#777777',
                    'primary': CONST.APP_COLORS['primary'],
                    'success': CONST.APP_COLORS['success'],
                    'info': CONST.APP_COLORS['info'],
                    'warning': CONST.APP_COLORS['warning'],
                    'danger': CONST.APP_COLORS['danger']
                }
            });
        }

        // Select 2

        var self = this;
        if ($.fn.select2) {
            $('#select2-1-state')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        selectedContractState: $(this).val()
                    });

                });
            $('#select2-1-contractType')
                .select2({ theme: 'bootstrap' })
                .on('change', function () {
                    self.setState({
                        selectedContractType: $(this).val()
                    });

                });
            /*   $('#select2-1-state').on('change', function() {
                self.setState({ selectedContractState: $(this).val() });
            });
 
            $('#select2-1-contractType').on('change', function() {
                self.setState({ selectedContractType: $(this).val() });
            });*/
        }
    }

    calcSumBetweenA_B(ATime, BTime) {
        //ATime: '10:00' BTime: '05:00'

        var hoursInterv = parseInt(BTime.substring(0, BTime.indexOf(':')));
        var minutesInterv = parseInt(BTime.split(":").pop());

        var hoursProcess = parseInt(ATime.substring(0, ATime.indexOf(':')));
        var minutesProcess = parseInt(ATime.split(":").pop());

        var updatedMinutes = minutesInterv + minutesProcess;
        var updatedHours = hoursInterv + hoursProcess;

        if (updatedMinutes >= 60) {
            updatedMinutes = updatedMinutes - 60;
            updatedHours++;
        }

        if (updatedHours.toString().length == 1) {
            updatedHours = "0" + updatedHours;
        }
        if (updatedMinutes.toString().length == 1) {
            updatedMinutes = "0" + updatedMinutes;
        }

        var finalTimeToCharge = updatedHours + ":" + updatedMinutes;

        return finalTimeToCharge;
    }

    render() {

        var contractTypeList = [];
        var contractStateList = [];

        if (this.state.contractTypes[0] != undefined)
            this.state.contractTypes.forEach((item, i) => {
                if (i == 0) {
                    contractTypeList.push(
                        <option disabled defaultValue value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }
                contractTypeList.push(
                    <option
                        key={item
                            ._id
                            .toString()}
                        value={item._id}>{item.description}</option>
                );
            })
        if (this.state.contractStates[0] != undefined)
            this.state.contractStates.forEach((item, i) => {
                if (i == 0) {
                    contractStateList.push(
                        <option disabled defaultValue value key={-1}>
                            -- Escolha uma opção --
                        </option>
                    );
                }

                contractStateList.push(
                    <option
                        key={item
                            ._id
                            .toString()}
                        value={item._id}>{item.stateName}</option>
                );
            })
        var titleOfPage = "";

        if (this.props.params.idContract) {
            titleOfPage = "Renovar contrato";
        } else {
            titleOfPage = "Adicionar um novo contrato";
        }

        return (
            <ContentWrapper>
                <h3>Contrato
                </h3>
                <Col>
                    <form onSubmit={this.handleSubmit} data-parsley-validate="" noValidate>
                        {/* START panel */}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <div className="panel-title">{titleOfPage}</div>
                            </div>
                            <div className="panel-body">

                                <div className="form-group">
                                    <Row>
                                        <div className="col-sm-6">
                                            <label className="control-label">Código de contrato</label>
                                            <FormControl
                                                type="text"
                                                name="contractCode"
                                                required="required"
                                                className="form-control"
                                                value={this.state.contractCode}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, "contractCode")} />
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Horas</label>
                                            <FormControl
                                                type="text"
                                                name="hours"
                                                required="required"
                                                placeholder="0.0"
                                                value={this.state.hours}
                                                className="form-control"
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, "hours")} />
                                        </div>
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-4">
                                            <label className="control-label">Tipo de contrato</label>
                                            <FormControl
                                                id="select2-1-contractType"
                                                componentClass="select"
                                                required="required"
                                                name="selectedContractType"
                                                className="form-control m-b"
                                                value={this.state.selectedContractType}>
                                                {contractTypeList}

                                            </FormControl>
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Data de início</label>
                                            <div id="datetimepickerBegin" className="input-group date">
                                                <input
                                                    name="beginDate"
                                                    type="text"
                                                    required="required"
                                                    className="form-control"
                                                    value={moment(this.state.beginDate).format("DD/MM/YYYY")}
                                                    onClick={this.handleChange} />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-4">
                                            <label className="control-label">Data de fim</label>
                                            <div id="datetimepickerEnd" className="input-group date">
                                                <input
                                                    name="endDate"
                                                    type="text"
                                                    required="required"
                                                    className="form-control"
                                                    value={moment(this.state.endDate).format("DD/MM/YYYY")}
                                                    onClick={this.handleChange} />

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        {/* <div className="col-sm-4">
                                            <label className="control-label">Data da última renovação</label>
                                            <div id="datetimepickerLastRenewal" className="input-group date">
                                                <input
                                                    name="lastRenewal"
                                                    type="text"
                                                    className="form-control"
                                                    onClick={this.handleChange}/>

                                                <span className="input-group-addon">
                                                    <span className="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>*/}
                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>

                                        <div className="col-sm-6">
                                            <label className="control-label">Estado</label>
                                            <FormControl
                                                id="select2-1-state"
                                                componentClass="select"
                                                required="required"
                                                name="selectedContractState"
                                                className="form-control m-b"
                                                value={this.state.selectedContractState}>
                                                {contractStateList}

                                            </FormControl>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className="control-label">Observações</label>
                                            <textarea
                                                id="textAreaObservations"
                                                name="observations"
                                                required="required"
                                                className="form-control"
                                                rows="3"
                                                value={this.state.observations}
                                                onChange={this
                                                    .handleChange
                                                    .bind(this, "observations")} />

                                        </div>

                                    </Row>
                                </div>

                                <div className="form-group">

                                    <Row>
                                        <div className="col-sm-12">

                                            <div className="col-sm-3">
                                                <label className="control-label">Notificar renovação</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyRenewal"
                                                            checked={this.state.notifyRenewal}
                                                            onChange={this
                                                                .handleCheckbox
                                                                .bind(this, 'notifyRenewal')} />
                                                        <em className="fa fa-check"></em>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <label className="control-label">Notificar horas consumidas</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="notifyExhaustedHours"
                                                            checked={this.state.notifyExhaustedHours}
                                                            onChange={this
                                                                .handleCheckbox
                                                                .bind(this, 'notifyExhaustedHours')} />
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div>

                                            {/* <div className="col-sm-3">
                                                <label className="control-label">Por mês</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="perMonth"
                                                            checked={this.state.perMonth}
                                                            onChange={this
                                                            .handleCheckbox
                                                            .bind(this, 'perMonth')}/>
                                                        <em className="fa fa-check"></em>
                                                    </label>
                                                </div>
                                            </div> */}
                                            <div className="col-sm-3">
                                                <label className="control-label">Bloquear horas consumidas</label>
                                                <div className="checkbox c-checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            name="blockExhaustedHours"
                                                            checked={this.state.blockExhaustedHours}
                                                            onChange={this
                                                                .handleCheckbox
                                                                .bind(this, 'blockExhaustedHours')} />
                                                        <em className="fa fa-check"></em>

                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </Row>
                                </div>

                                <div className="required">* Campo obrigatório</div>

                            </div>
                            <div className="panel-footer">
                                <Row>
                                    <Col lg={2}>

                                        <button
                                            onClick={() => this.props.router.push('/customers/view/' + this.props.params.idEntity + "/2")}
                                            type="button"
                                            className="btn ">Voltar ao cliente</button>
                                    </Col>
                                    <Col lg={1}>

                                        <button type="submit" className="btn btn-primary">Guardar</button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        {/* END panel */}
                    </form>
                </Col>
            </ContentWrapper>

        );
    }
}

export default AddContract;
