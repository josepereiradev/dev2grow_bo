import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'node_modules/axios';
import {Row, Col, Panel, Button, Table} from 'react-bootstrap';
import {Router, Route, Link, History, withRouter} from 'react-router';
import css from '../../../styles/tableIcons.scss'
import ContentWrapper from '../../Layout/ContentWrapper';
import * as CONST from '../../Common/constants';

class Contracts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            contractType: [],
            contractStates: [],
            entities: [],
            interventions: [],
            addendas: []
        }
    }

    componentDidMount() {
        this.getDataFromBD();
        var th = this;

        this.serverRequest = axios
            .get(__APIURL__ + "/contractTypes/",)
            .then(function (result) {
                th.setState({contractType: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/contractStates/",)
            .then(function (result) {
                th.setState({contractStates: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/entities/",)
            .then(function (result) {
                th.setState({entities: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/interventions/",)
            .then(function (result) {
                th.setState({interventions: result.data});
            })
        this.serverRequest = axios
            .get(__APIURL__ + "/addenda/",)
            .then(function (result) {
                th.setState({addendas: result.data});
            })

    }

    getDataFromBD() {
        axios
            .get(__APIURL__ + "/contracts/")
            .then(res => {

                if (res.data.message != "NO_RESULTS") {
                    const data = res
                        .data
                        .map(obj => obj);
                    this.setState({data})
                    setTimeout(function () {
                        var dtContracts = $('#dtContracts').dataTable({
                            'pageLength': 100,
                            'retrieve': true, 'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            'responsive': true, // https://datatables.net/extensions/responsive/examples/
                            // Text translation options Note the required keywords between underscores (e.g
                            // _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            // Datatable Buttons setup
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                {
                                    extend: 'copy',
                                    className: 'btn-sm',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'csv',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'excel',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'pdf',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }, {
                                    extend: 'print',
                                    className: 'btn-sm',
                                    title: 'Dev2Grow - Contratos',
                                    exportOptions: {
                                        columns: [
                                            0,
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7
                                        ]
                                    }
                                }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs.keyup(function () {
                            dtContracts.fnFilter(this.value, columnInputs.index(this));
                        });
                    }.bind(this), 500);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    view(id) {
        var customerID = "";
        this
            .state
            .entities
            .forEach(custObj => {
                custObj
                    .contracts
                    .forEach(contractID => {
                        if (contractID == id) {
                            customerID = custObj._id;
                        }
                    })
            })
        this
            .props
            .router
            .push('/contracts/view/' + customerID + '/' + id);
    }

    create() {
        this
            .props
            .router
            .push('/contracts/create');
    }

    edit(id) {
        var customerID = "";
        this
            .state
            .entities
            .forEach(custObj => {
                custObj
                    .contracts
                    .forEach(contractID => {
                        if (contractID == id) {
                            customerID = custObj._id;
                        }
                    })
            })
        this
            .props
            .router
            .push('/contracts/edit/' + customerID + '/' + id);
    }

    delete(name, id) {

        var customerID = "";
        var contractToDelete = "";
        var canDelete = true;

        this
            .state
            .entities
            .forEach(custObj => {
                custObj
                    .contracts
                    .forEach(contractID => {
                        if (contractID == id) {
                            customerID = custObj._id;

                        }
                    })
            })

        this
            .state
            .data
            .forEach(contract => {
                if (contract._id == id) {
                    contractToDelete = contract;
                    if (contract.addenda.length != 0) {
                        canDelete = false;
                    }
                }
            })
        if (!this.state.interventions.error) {
            this
                .state
                .interventions
                .forEach(interv => {
                    if (contractToDelete._id == interv.contractToCharge) {
                        canDelete = false;
                    }
                })
        }

        var entityParentData;
        var self = this;
        if (canDelete) {
            this.serverRequest = axios
                .get(__APIURL__ + "/entities/" + customerID)
                .then(function (result) {
                    entityParentData = result.data;

                    var headers = new Headers({'Content-Type': 'application/json'});

                    swal({
                        title: "Tem a certeza que quer eliminar?",
                        text: "Após eliminar não poderá recuperar o registo!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Sim",
                        closeOnConfirm: false
                    }, function () {

                        axios
                            .delete(__APIURL__ + "/contracts/" + id)
                            .then(function (response) {
                                if (response.status == 200) {
                                    entityParentData
                                        .contracts
                                        .splice(entityParentData.contracts.indexOf(id), 1);
                                    var updateEntity = JSON.parse(JSON.stringify({
                                        "nif": entityParentData.nif,
                                        "qrCode": entityParentData.qrCode,
                                        "taxName": entityParentData.taxName,
                                        "abrevName": entityParentData.abrevName,
                                        "accountManager": entityParentData.accountManager,
                                        "contacts": entityParentData.contacts,
                                        "projects": entityParentData.projects,
                                        "processes": entityParentData.processes,
                                        "contracts": entityParentData.contracts,
                                        "associatedUsers": entityParentData.associatedUsers
                                    }));

                                    self.serverRequest = axios
                                        .put(__APIURL__ + '/entities/' + customerID, updateEntity, {headers: self.headers})
                                        .then(function (response2) {})
                                        .catch(function (error) {
                                            console.log(error);
                                        });

                                    if (response.status == 200) {
                                        swal("Eliminado!", name + " foi eliminado.", "success");
                                        self.getDataFromBD();
                                    } else {
                                        // TODO: HANDLE IT
                                    }
                                }
                            })
                            .catch(function (error) {
                                console.log(error);
                                // TODO: HANDLE IT
                            });

                    });

                });
        } else {
            swal('Não é possivel eliminar o registo', "Existem dependencias associadas a este registo", "error");
        }

    }

    render() {

        const css = css;
        var rows = [];
        var contractType = "",
            entityParent = "",
            entityParentName = "",
            contractS = "";

        this
            .state
            .data
            .forEach(cust => {

                var nameOfCustomer = "";
                var contractType = "";
                var contractState = "";

                this
                    .state
                    .entities
                    .forEach(entit => {
                        entit
                            .contracts
                            .forEach(contractID => {
                                if (contractID == cust._id) {
                                    nameOfCustomer = entit.abrevName;
                                }
                            })
                    })

                this
                    .state
                    .contractType
                    .forEach(contrType => {

                        if (contrType._id == cust.contractType) {
                            contractType = contrType.description;
                        }
                    })

                this
                    .state
                    .contractStates
                    .forEach(contrState => {
                        if (contrState._id == cust.state) {
                            console.log("111")
                            contractState = <span className={contrState.stateLabelColor}>{contrState.stateName}</span>
                        }
                    })

                rows.push(
                    <tr key={cust._id}>
                        <td>{cust.contractCode}</td>
                        <td>{contractType}</td>
                        <td>{nameOfCustomer}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(cust.beginDate).toLocaleDateString()}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{new Date(cust.endDate).toLocaleDateString()}</td>
                        <td
                            style={{
                            whiteSpace: 'nowrap'
                        }}>{cust.lastRenewal
                                ? new Date(cust.lastRenewal).toLocaleDateString()
                                : "-"}</td>
                        <td>{cust.hours}</td>
                        <td>{cust.availableHours}</td>
                        <td>{cust.exhaustedHours}</td>
                        <td>{contractState}</td>
                        <td>
                            <button
                                onClick={() => this.view(cust._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-eye"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.edit(cust._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>
                        <td>
                            <button
                                onClick={() => this.delete(cust.contractCode, cust._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                )
            });

        return (
            <ContentWrapper>
                <h3>Todos os contratos
                </h3>
                <style>
                    {
                        css
                    }
</style>

                <Row>
                    <Col lg={12}>
                        <Panel>
                            <button
                                onClick={() => this.create()}
                                type="button"
                                id="addBtn"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless pull-right">
                                <i className="icon-plus"></i>
                            </button>
                            <Table id="dtContracts" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Código do contrato</th>
                                        <th>Tipo de contrato</th>
                                        <th>Cliente</th>
                                        <th>Data de início</th>
                                        <th>Data de fim</th>
                                        <th>Última renovação</th>
                                        <th>Horas</th>
                                        <th>Horas disponív.</th>
                                        <th>Horas consum.</th>
                                        <th>Estado</th>

                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>{rows}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default Contracts;
