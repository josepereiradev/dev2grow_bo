import React from 'react';
import axios from 'node_modules/axios';
import ReactDom from 'react-dom';
import ContentWrapper from '../Layout/ContentWrapper';
import { Row, Col, Panel, Button, Table, Accordion, InputGroup } from 'react-bootstrap';
import { DateRange, defaultRanges } from 'react-date-range';
// import { Grid, Row, Col, Dropdown, MenuItem, Tabs, Tab, Panel } from
// 'react-bootstrap';
import DashboardRun from './Dashboard.run';
import _ from 'node_modules/underscore';
import moment from "moment";

class Dashboard extends React.Component {

    constructor() {
        super();
        this.state = {
            //old Dashboard
            processesOpen: [],
            interventionsToValidate: [],
            interventions: [],
            contractsExhaustedHours: [],
            contractsToRenew: [],

            allProcessesOpenBackup: [],

            customers: [],
            contacts: [],
            interventionTypes: [],
            processTypes: [],
            // processStates: [],
            // contractTypes: [],
            contractStates: [],
            users: [],
            processes: [],
            mineSelected: true,

            //new Dashboard
            rangeStart: 0,
            rangeEnd: 0,
            selectedRange: '',
            contractTypes: [],
            clients: [],
            selectedClients: [],
            selectedClientsText: [],
            contracts: [],
            selectedContracts: [],
            selectedContractsText: [],
            processStates: [],
            selectedProcessStates: [],
            selectedProcessStatesText: [],

            clientsState: 'Todos',
            contractsState: 'Todos',
            procState: 'Todos',

            rendered: false,
        }
    }

    componentDidMount() {

        console.log('Update')
        //   this.filterOwnProcesses(true);
        $('#select2-clients').select2({ theme: 'bootstrap', placeholder: "Select one or more clients" }).off('select2:select select2:unselect').on('select2:select select2:unselect', this.handleMultipleClients.bind(this));
        $('#select2-contracts').select2({ theme: 'bootstrap', placeholder: "Select one or more contracts" }).off('select2:select select2:unselect').on('select2:select select2:unselect', this.handleMultipleContracts.bind(this));
        $('#select2-processes').select2({ theme: 'bootstrap', placeholder: "Select one or more contracts" }).off('select2:select select2:unselect').on('select2:select select2:unselect', this.handleMultipleProcessStates.bind(this));
    }

    componentWillMount() {

        var contador = 0;
        var services = 12;
        axios
            .get(__APIURL__ + '/interventions/')
            .then(result => {
                if (!result.data.error) {
                    var interventionsFromDB = [];
                    var interventions = [];

                    result
                        .data
                        .forEach(intervention => {
                            if (intervention.toBeValidated) {
                                interventionsFromDB.push(intervention);
                            }
                            interventions.push(intervention);
                        })

                    this.state.interventionsToValidate = interventionsFromDB;
                    this.state.interventions = interventions;
                    contador++;

                    if (contador === services) {
                        this.setStateNow();
                    }
                } else {
                    contador++;

                    if (contador === services) {
                        this.setStateNow();
                    }
                }

            })
        axios
            .get(__APIURL__ + '/processes/')
            .then(result => {

                axios
                    .get(__APIURL__ + '/processStates/')
                    .then(result2 => {

                        if (!result.data.error && !result2.data.error) {
                            var processesFromDB = [];
                            result
                                .data
                                .forEach(process => {
                                    result2
                                        .data
                                        .forEach(processState => {
                                            if (!processState.endProcesses && process.state == processState._id) {
                                                processesFromDB.push(process);
                                            }
                                        })
                                })
                            contador++;

                            this.state.processesOpen = processesFromDB;
                            if (contador === services) {
                                this.setStateNow();
                            }

                        } else {
                            contador++;

                            if (contador === services) {
                                this.setStateNow();
                            }

                        }
                    })
            })

        axios
            .get(__APIURL__ + '/contracts/')
            .then(result => {
                if (!result.data.error) {
                    var contracts = [];
                    var contractsExFromDB = [];
                    var contractsRenewFromDB = [];
                    result
                        .data
                        .forEach(contract => {
                            contracts.push(contract);

                            var a = contract
                                .hours
                                .split(':');
                            var minutesContractHours = (+ a[0]) * 60 + (+ a[1]);

                            var b = contract
                                .availableHours
                                .split(':');
                            var minutesAvailableContractHours = (+ b[0]) * 60 + (+ b[1]);

                            if (minutesAvailableContractHours > 0 && minutesContractHours * 0.1 >= minutesAvailableContractHours) {
                                contractsExFromDB.push(contract);
                            } else if (minutesAvailableContractHours <= 0) {
                                contractsRenewFromDB.push(contract);
                            } else if (moment(contract.endDate).diff(moment(), 'days') <= 30) {
                                contractsRenewFromDB.push(contract);
                            }

                        });
                    contador++;
                    this.state.contracts = contracts;
                    this.state.contractsToRenew = contractsRenewFromDB;
                    this.state.contractsExhaustedHours = contractsExFromDB;

                    if (contador === services) {
                        this.setStateNow();
                    }
                } else {
                    contador++;

                    if (contador === services) {
                        this.setStateNow();
                    }
                }

            })

        axios
            .get(__APIURL__ + '/entities/')
            .then(result => {
                this.state.customers = result.data;
                this.state.clients = result.data;
                contador++;

                if (contador === services) {
                    this.setStateNow();
                }
            })
        axios
            .get(__APIURL__ + '/contacts/')
            .then(result => {
                this.state.contacts = result.data;
                contador++;

                if (contador === services) {
                    this.setStateNow();
                }
            })
        axios
            .get(__APIURL__ + '/interventionTypes/')
            .then(result => {
                this.state.interventionTypes = result.data;
                contador++;

                if (contador === services) {
                    this.setStateNow();
                }
            })
        axios
            .get(__APIURL__ + '/processTypes/')
            .then(result => {
                this.state.processTypes = result.data;
                contador++;

                if (contador === services) {
                    this.setStateNow();
                }
            })
        axios
            .get(__APIURL__ + '/processStates/')
            .then(result => {
                this.state.processStates = result.data;
                contador++;

                if (contador === services) {
                    setStateNow();
                }
            })
        axios
            .get(__APIURL__ + '/contractTypes/')
            .then(result => {
                this.state.contractTypes = result.data;
                contador++;

                if (contador === services) {
                    this.setStateNow();
                }
            })
        axios
            .get(__APIURL__ + '/contractStates/')
            .then(result => {
                this.state.contractStates = result.data;
                contador++;

                if (contador === services) {
                    this.setStateNow();
                }
            })
        axios
            .get(__APIURL__ + '/users/')
            .then(result => {
                this.state.users = result.data;
                contador++;

                if (contador === services) {
                    this.setStateNow();
                }
            })
        axios
            .get(__APIURL__ + '/processes/')
            .then(result => {
                this.state.processes = result.data;
                contador++;

                if (contador === services) {
                    this.setStateNow();
                }
            })
    }

    setStateNow() {
        this.setState({ rendered: false }, () => {
            window.addEventListener("resize", this.updateDimensions.bind(this));
            this.setDashboard();
        })
    }

    setDashboard() {
        let columns = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

        var dtDashboard = $('#dtDashboard').dataTable({
            'pageLength': 100,
            'retrieve': true,
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ Records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            },
            // Datatable Buttons setup
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {
                    extend: 'copy',
                    className: 'btn-sm',
                    exportOptions: {
                        columns: columns
                    }
                }, {
                    extend: 'csv',
                    className: 'btn-sm',
                    title: 'Dev2Grow - Dashboard',
                    exportOptions: {
                        columns: columns
                    }
                }, {
                    extend: 'excel',
                    className: 'btn-sm',
                    title: 'Dev2Grow - Dashboard',
                    exportOptions: {
                        columns: columns
                    }
                }, {
                    extend: 'pdf',
                    className: 'btn-sm',
                    title: 'Dev2Grow - Dashboard',
                    exportOptions: {
                        columns: columns
                    }
                }, {
                    extend: 'print',
                    className: 'btn-sm',
                    title: 'Dev2Grow - Dashboard',
                    exportOptions: {
                        columns: columns
                    }
                }
            ]
        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs.keyup(function () {
            dtDashboard.fnFilter(this.value, columnInputs.index(this));
        });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
        $('#dtDashboard').dataTable({ destroy: true });
    }

    updateDimensions() {
        this.forceUpdate();
    }

    handleDateSelect(range) {
        if (this.state.rendered) {
            $("tbody").empty();
            $('#dtDashboard').dataTable().fnDestroy();
        }
        this.setState({
            rangeStart: range['startDate'],
            rangeEnd: range['endDate'],
            selectedRange: range['startDate'].format('DD/MM/YYYY').toString() + ' - ' + range['endDate'].format('DD/MM/YYYY').toString()
        }, () => {
            if (this.state.rendered) {
                this.setDashboard();
            }
        })
    }


    filterOwnProcesses(e) {

        if (e.target.checked) {
            this.setState({ mineSelected: true })

        } else {
            this.setState({ mineSelected: false })
        }

    }

    renderProcessesTable() {

        var rows = [];
        if (!this.state.mineSelected) {
            this
                .state
                .processesOpen
                .forEach(process => {
                    console.log(process);
                    var customer = this.state.customers.length != 0
                        ? this
                            .state
                            .customers
                            .filter(customer => customer._id == process.entity)[0]
                        : "";
                    rows.push(
                        <tr key={process._id} className="active">
                            <td className="displayTableCell">{process.processNumber}</td>
                            <td className="displayTableCell">{this.state.processTypes.length != 0
                                ? this
                                    .state
                                    .processTypes
                                    .filter(procType => procType._id == process.processType)[0]
                                    .description
                                : ""}</td>
                            <td className="displayTableCell">{customer.abrevName}</td>
                            <td className="displayTableCell">{moment(process.date).format("DD/MM/YYYY")}</td>
                            <td className="displayTableCell">{process.duration}</td>
                            <td className="displayTableCell">{this.state.contacts.length != 0
                                ? this
                                    .state
                                    .contacts
                                    .filter(contact => contact._id == process.requestedBy)[0]
                                    .name
                                : ""}</td>

                            <td className="displayTableCell">{this.state.users.length != 0 && process.collaborator
                                ? this
                                    .state
                                    .users
                                    .filter(user => user._id == process.collaborator)[0]
                                    .name
                                : ""}</td>

                            <td className="displayTableCell">{this.state.processStates.length != 0
                                ? this
                                    .state
                                    .processStates
                                    .filter(procSt => procSt._id == process.state)[0]
                                    .stateName
                                : ""}</td>
                            <td className="displayTableCell">
                                <button
                                    onClick={() => this.props.router.push('processes/view/' + customer._id + '/' + process._id)}
                                    type="button"
                                    id="lineBtns"
                                    className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                    <i className="icon-eye"></i>
                                </button>
                            </td>

                        </tr>
                    )

                })
        } else {
            var actualCollab = sessionStorage.getItem('user')
                ? JSON
                    .parse(sessionStorage.getItem('user'))
                    .id
                : '';

            this
                .state
                .processesOpen
                .forEach(process => {
                    if (process.collaborator == actualCollab) {
                        var customer = this.state.customers.length != 0
                            ? this
                                .state
                                .customers
                                .filter(customer => customer._id == process.entity)[0]
                            : "";
                        rows.push(
                            <tr key={process._id} className="active">
                                <td className="displayTableCell">{process.processNumber}</td>
                                <td className="displayTableCell">{this.state.processTypes.length != 0
                                    ? this
                                        .state
                                        .processTypes
                                        .filter(procType => procType._id == process.processType)[0]
                                        .description
                                    : ""}</td>
                                <td className="displayTableCell">{customer.abrevName}</td>
                                <td className="displayTableCell">{moment(process.date).format("DD/MM/YYYY")}</td>
                                <td className="displayTableCell">{process.duration}</td>
                                <td className="displayTableCell">{this.state.contacts.length != 0
                                    ? this
                                        .state
                                        .contacts
                                        .filter(contact => contact._id == process.requestedBy)[0]
                                        .name
                                    : ""}</td>
                                <td className="displayTableCell">{this.state.users.length != 0
                                    ? this
                                        .state
                                        .users
                                        .filter(user => user._id == process.collaborator)[0]
                                        .name
                                    : ""}</td>
                                <td className="displayTableCell">{this.state.processStates.length != 0
                                    ? this
                                        .state
                                        .processStates
                                        .filter(procSt => procSt._id == process.state)[0]
                                        .stateName
                                    : ""}</td>
                                <td className="displayTableCell">
                                    <button
                                        onClick={() => this.props.router.push('processes/view/' + customer._id + '/' + process._id)}
                                        type="button"
                                        id="lineBtns"
                                        className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                        <i className="icon-eye"></i>
                                    </button>
                                </td>

                            </tr>
                        )
                    }

                })
        }

        return rows;

    }

    renderInterventionsToValidate() {

        var rows = [];

        this
            .state
            .interventionsToValidate
            .forEach(intervention => {
                var idOfContact = this.state.contacts
                    ? this
                        .state
                        .contacts
                        .filter(contType => contType._id == intervention.requestedBy)[0]
                        ._id
                    : "";
                var userName = "";
                var process = null;
                this
                    .state
                    .customers
                    .map((cust, index) => {
                        if (cust.contacts.indexOf(idOfContact) >= 0) {

                            userName = cust.abrevName;
                            return;
                        }
                    });
                this
                    .state
                    .processes
                    .map(proc => {
                        if (proc.interventions.indexOf(intervention._id) >= 0) {
                            process = proc;
                            return;
                        }
                    })

                rows.push(
                    <tr key={intervention._id} className="active">
                        <td className="displayTableCell">{intervention.interventionNumber}</td>
                        <td className="displayTableCell">{this.state.interventionTypes.length != 0
                            ? this
                                .state
                                .interventionTypes
                                .filter(intType => intType._id == intervention.interventionType)[0]
                                .description
                            : ""}</td>
                        <td className="displayTableCell">{userName}</td>
                        <td className="displayTableCell">{this.state.users.length != 0
                            ? this
                                .state
                                .users
                                .filter(userTp => userTp._id == intervention.collaborator)[0]
                                .name
                            : ""}</td>
                        <td className="displayTableCell">{moment(intervention.date).format("DD/MM/YYYY")}</td>
                        <td className="displayTableCell">{moment(intervention.beginHour).format("HH:mm")}</td>
                        <td className="displayTableCell">{moment(intervention.endHour).format("HH:mm")}</td>
                        <td className="displayTableCell">{moment(intervention.totalTime).format("HH:mm")}</td>
                        <td className="displayTableCell">{moment(intervention.timeToCharge).format("HH:mm")}</td>
                        <td className="displayTableCell">{intervention.toBeValidated}</td>
                        <td className="displayTableCell">
                            <button
                                onClick={() => this.props.router.push('interventions/edit/' + process.entity + "/" + process._id + "/" + intervention._id)}
                                type="button"
                                id="lineBtns"
                                className="btn btn-default btn-icon btn-borderless btn-shadowless">
                                <i className="icon-pencil"></i>
                            </button>
                        </td>

                    </tr>
                )

            })
        return rows;

    }

    renderContractsTable(contractTableToLoad) {

        var rows = [];
        var table = null;
        var hoursTd;
        if (contractTableToLoad == "contractsExhaustedHours") {
            table = this.state.contractsExhaustedHours;
            hoursTd = {
                color: "red",
                fontWeight: "bold"
            }
        } else if (contractTableToLoad == "contractsToRenew") {
            table = this.state.contractsToRenew;

        }

        table.forEach(contract => {

            var custObj = "";
            var paintRed = false;
            this
                .state
                .customers
                .map((cust, index) => {
                    if (cust.contracts.indexOf(contract._id) >= 0) {

                        custObj = cust;
                        return;
                    }
                });


            rows.push(
                <tr key={contract._id} className="active">
                    <td className="displayTableCell">{contract.contractCode}</td>
                    <td className="displayTableCell">{this.state.contractTypes.length != 0
                        ? this
                            .state
                            .contractTypes
                            .filter(contT => contT._id == contract.contractType)[0]
                            .description
                        : ""}</td>
                    <td className="displayTableCell">{custObj.abrevName}</td>
                    <td className="displayTableCell">{moment(contract.beginDate).format("DD/MM/YYYY")}</td>
                    <td className="displayTableCell">{moment(contract.endDate).format("DD/MM/YYYY")}</td>
                    <td className="displayTableCell">{contract.lastRenewal
                        ? moment(contract.lastRenewal).format("DD/MM/YYYY")
                        : "-"}</td>
                    <td className="displayTableCell">{contract.hours}</td>
                    <td className="displayTableCell" style={hoursTd}>{contract.availableHours}</td>
                    <td className="displayTableCell">{contract.exhaustedHours}</td>
                    <td className="displayTableCell">{this.state.contractStates.length != 0
                        ? this
                            .state
                            .contractStates
                            .filter(contSt => contSt._id == contract.state)[0]
                            .stateName
                        : ""}</td>
                    <td className="displayTableCell">
                        <button
                            onClick={() => this.props.router.push('contracts/view/' + custObj._id + '/' + contract._id)}
                            type="button"
                            id="lineBtns"
                            className="btn btn-default btn-icon btn-borderless btn-shadowless">
                            <i className="icon-eye"></i>
                        </button>
                    </td>

                </tr>
            )

        })
        return rows;
    }


    //old
    /*render() {
        return (
            <ContentWrapper>
                <div className="content-heading">
                    Dev2Grow
                    <small data-localize="dashboard.WELCOME">Bem vindo à Dev2Grow!</small>
                </div>

                <Row>
                    <Col lg={12}>
                        <Panel>
                            <h4
                                style={{
                                    display: "inline-block",
                                    margin: 0
                                }}>Processos em aberto</h4>
                            <div
                                className="checkbox c-checkbox"
                                style={{
                                    display: "inline-block",
                                    float: "right",
                                    margin: 0
                                }}>
                                <label>
                                    <input
                                        type="checkbox"
                                        name="mineSelected"
                                        checked={this.state.mineSelected}
                                        onChange={this
                                            .filterOwnProcesses
                                            .bind(this)} />
                                    <em className="fa fa-check"></em>
                                </label>
                            </div>
                            <h4
                                style={{
                                    display: "inline-block",
                                    float: "right",
                                    margin: 0,
                                    marginRight: "5px"
                                }}>Meus</h4>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Código do processo</th>
                                        <th>Tipo de processo</th>
                                        <th>Cliente</th>
                                        <th>Data</th>
                                        <th>Duração</th>
                                        <th>Pedido por</th>
                                        <th>Técnico atribuido</th>
                                        <th>Estado</th>
                                        <th></th>

                                    </tr>

                                </thead>
                                <tbody>
                                    {this.renderProcessesTable()}
                                </tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
                <Row>

                    <Col lg={12}>
                        <Panel>
                            <h4>Intervenções por validar</h4>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Número de intervenção</th>
                                        <th>Tipo de intervenção</th>
                                        <th>Cliente</th>
                                        <th>Colaborador</th>
                                        <th>Data</th>
                                        <th>Hora de início</th>
                                        <th>Hora de fim</th>
                                        <th>Tempo real</th>
                                        <th>Tempo cobrado</th>
                                        <th>Validado</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>{this.renderInterventionsToValidate()}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
                <Row>

                    <Col lg={12}>
                        <Panel>
                            <h4>Contratos com horas a esgotar</h4>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Código do contrato</th>
                                        <th>Tipo de contrato</th>
                                        <th>Cliente</th>
                                        <th>Data de início</th>
                                        <th>Data de fim</th>
                                        <th>Última renovação</th>
                                        <th>Horas</th>
                                        <th>Horas disponíveis</th>
                                        <th>Horas consumidas</th>
                                        <th>Estado</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>{this.renderContractsTable("contractsExhaustedHours")}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
                <Row>

                    <Col lg={12}>
                        <Panel>
                            <h4>Contratos a renovar</h4>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Código do contrato</th>
                                        <th>Tipo de contrato</th>
                                        <th>Cliente</th>
                                        <th>Data de início</th>
                                        <th>Data de fim</th>
                                        <th>Última renovação</th>
                                        <th>Horas</th>
                                        <th>Horas disponíveis</th>
                                        <th>Horas consumidas</th>
                                        <th>Estado</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>{this.renderContractsTable("contractsToRenew")}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>

            </ContentWrapper>
        );
    }*/

    handleMultipleClients(e) {
        let clientsArr = this.state.selectedClients;
        let clientsTextArr = this.state.selectedClientsText;
        if (this.state.rendered) {
            $("tbody").empty();
            $('#dtDashboard').dataTable().fnDestroy();
        }
        if (e.type == 'select2:unselect') {
            let contracts = this.renderContractsText().filter(c => c.client != e.params.data.id);
            this.state.selectedClients = _.reject(clientsArr, s => s == e.params.data.id);
            this.state.selectedClientsText = _.reject(clientsTextArr, s => s == e.params.data.text);
            this.setState({
                contractState: contracts.length == 0 ? 'Todos' : this.state.contractsState,
                selectedContracts: contracts.map(c => c._id),
                selectedContractsText: contracts.map(c => c.contract),
            }, () => {
                this.setDashboard();
            });
        } else if (e.type == 'select2:select') {
            clientsArr.push(e.params.data.id);
            clientsTextArr.push(e.params.data.text);
            this.setState({
                selectedClients: _.uniq(clientsArr),
                selectedClientsText: _.uniq(clientsTextArr)
            }, () => {
                this.setDashboard();
            });
        }
    }

    handleMultipleContracts(e) {
        let contractsArr = this.state.selectedContracts;
        let contractsTextArr = this.state.selectedContractsText;
        if (this.state.rendered) {
            $("tbody").empty();
            $('#dtDashboard').dataTable().fnDestroy();
        }
        if (e.type == 'select2:unselect') {
            this.setState({
                selectedContracts: _.reject(contractsArr, s => s == e.params.data.id),
                selectedContractsText: _.reject(contractsTextArr, s => s == e.params.data.text)
            }, () => {
                this.setDashboard();
            });
        } else if (e.type == 'select2:select') {
            contractsArr.push(e.params.data.id);
            contractsTextArr.push(e.params.data.text);
            this.setState({
                selectedContracts: _.uniq(contractsArr),
                selectedContractsText: _.uniq(contractsTextArr)
            }, () => {
                this.setDashboard();
            });
        }
    }

    handleMultipleProcessStates(e) {
        let processStatesArr = this.state.selectedProcessStates;
        let processStatesTextArr = this.state.selectedProcessStatesText;
        if (this.state.rendered) {
            $("tbody").empty();
            $('#dtDashboard').dataTable().fnDestroy();
        }
        if (e.type == 'select2:unselect') {
            this.setState({
                selectedProcessStates: _.reject(processStatesArr, s => s == e.params.data.id),
                selectedProcessStatesText: _.reject(processStatesTextArr, s => s == e.params.data.text)
            }, () => {
                this.setDashboard();
            });
        } else if (e.type == 'select2:select') {
            processStatesArr.push(e.params.data.id);
            processStatesTextArr.push(e.params.data.text);
            this.setState({
                selectedProcessStates: _.uniq(processStatesArr),
                selectedProcessStatesText: _.uniq(processStatesTextArr)
            }, () => {
                this.setDashboard();
            });
        }
    }

    changeClientsState() {
        console.log("MY CLIENTS", this.state.clients);
        this.setState({
            clientsState: this.state.clientsState == 'Todos' ? 'Nenhum' : 'Todos',
            contractsState: this.state.clientsState == 'Todos' ? this.state.contractsState : 'Todos',
            selectedClients: this.state.clientsState == 'Todos' && Array.isArray(this.state.clients) ? this.state.clients.map(cl => JSON.parse(JSON.stringify(cl._id))) : [],
            selectedClientsText: this.state.clientsState == 'Todos' && Array.isArray(this.state.clients) ? this.state.clients.map(cl => cl.abrevName) : [],
            selectedContracts: this.state.clientsState == 'Todos' ? this.state.selectedContracts : [],
            selectedContractsText: this.state.clientsState == 'Todos' ? this.state.selectedContractsText : [],
        }, () => {
            $('#select2-clients').select2({ theme: 'bootstrap', placeholder: "Select one or more clients" }).off('select2:select select2:unselect').on('select2:select select2:unselect', this.handleMultipleClients.bind(this));
        });
    }

    changeContractState() {
        let contracts = [];

        if (this.state.contractsState == 'Todos') {
            contracts = this.renderContractsText();
        }

        this.setState({
            contractsState: this.state.contractsState == 'Todos' ? 'Nenhum' : 'Todos',
            selectedContracts: this.state.contractsState == 'Todos' ? contracts.map(c => c._id) : [],
            selectedContractsText: this.state.contractsState == 'Todos' ? contracts.map(c => c.contract) : [],
        }, () => {
            $('#select2-contracts').select2({ theme: 'bootstrap', placeholder: "Select one or more contracts" }).off('select2:select select2:unselect').on('select2:select select2:unselect', this.handleMultipleContracts.bind(this));
        });
    }

    changeProcState() {
        this.setState({
            procState: this.state.procState == 'Todos' ? 'Nenhum' : 'Todos',
            selectedProcessStates: this.state.procState == 'Todos' ? this.state.processStates.map(ps => ps._id) : [],
            selectedProcessStatesText: this.state.procState == 'Todos' ? this.state.processStates.map(ps => ps.stateName) : [],
        }, () => {
            $('#select2-processes').select2({ theme: 'bootstrap', placeholder: "Select one or more contracts" }).off('select2:select select2:unselect').on('select2:select select2:unselect', this.handleMultipleProcessStates.bind(this));
        });
    }

    renderContractsText() {
        let contracts = [];
        if (this.state.selectedClients.length == 0) {
            return contracts;
        }

        let selectedClients = this.state.clients.filter(cl => {
            return this.state.selectedClients.indexOf(JSON.parse(JSON.stringify(cl._id))) != -1;
        });

        selectedClients.forEach(client => {
            let selectedClientName = client.abrevName;
            let clientsContracts = this.state.contracts.filter(contract => client.contracts.indexOf(JSON.parse(JSON.stringify(contract._id))) != -1);
            clientsContracts = clientsContracts.map(contract => {
                let type = this.state.contractTypes.filter(type => contract.contractType == type._id)[0].description;
                let beginDate = moment(contract.beginDate).format("DD/MM/YYYY");
                let endDate = moment(contract.endDate).format("DD/MM/YYYY");
                return {
                    _id: contract._id,
                    client: client._id,
                    contract: `${selectedClientName} - ${type} | ${beginDate} | ${endDate}`,
                };
            });

            contracts = contracts.concat(clientsContracts);
        });

        return contracts;
    }

    renderContracts() {
        // Este método tem de ir ao array dos clientes selecionados,
        // ir buscar os contratos de cada um, e depois com essa informação
        // filtrar o array de contratos

        return this.renderContractsText().map(contract => {
            return <option key={contract._id} value={contract._id}>{contract.contract}</option>
        })
    }

    filterIntervention(intervention) {
        let isValid = true;
        let date = moment(intervention.dateHour, "DD/MM/YYYY HH:mm");

        //filter by date
        if (date.isBefore(this.state.rangeStart, 'day') || date.isAfter(this.state.rangeEnd, 'day')) {
            isValid = false;
        }

        //filter by client
        let selectedClients = this.state.selectedClientsText;
        if (selectedClients.length > 0 && selectedClients.indexOf(intervention.client) == -1) {
            isValid = false;
        }

        //filter by contracts
        let selectedContracts = this.state.selectedContracts;
        if (selectedContracts.length > 0 && selectedContracts.indexOf(intervention.contract) == -1) {
            isValid = false;
        }

        //filter by process state
        let selectedStates = this.state.selectedProcessStatesText;
        if (selectedStates.length > 0 && selectedStates.indexOf(intervention.processState) == -1) {
            isValid = false;
        }

        return isValid;
    }

    renderInterventions() {

        let interventionsArr = [];
        let clientsProcesses = [];
        if (Array.isArray(this.state.clients)) {
            this.state.clients.forEach(client => {
                let processes = [];
                client.processes.forEach(p => {
                    processes.push({
                        client: client.abrevName,
                        process: p,
                    })
                })

                clientsProcesses = clientsProcesses.concat(processes);
            })
        }

        console.log("HERE")
        if (Array.isArray(this.state.processes)) {
            console.log("MY PROCESSES", this.state.processes)
            this.state.processes.forEach(p => {
                let interventions = p.interventions.map(int => {
                    let intervention = this.state.interventions.filter(i => i._id == int)[0];
                    if (intervention) {
                        let type = this.state.interventionTypes.filter(type => type._id == intervention.interventionType)[0].description;
                        let processState = this.state.processStates.filter(proc => proc._id == p.state)[0].stateName;
                        let state = this.state.processStates.filter(proc => proc._id == intervention.processState)[0].stateName;
                        let duration = moment(intervention.timeToCharge).format("HH:mm");
                        let dateHourProcess = moment(p.date).format("DD/MM/YYYY");
                        let dateIntervention = moment(intervention.beginHour).format("DD/MM/YYYY HH:mm");
                        // console.log(int)
                        let client = clientsProcesses.filter(cl => cl.process == p._id)[0].client;

                        return {
                            _id: int,
                            client: client,
                            contract: intervention.contractToCharge,
                            dateHour: dateHourProcess,
                            process: p._id,
                            processNumber: p.processNumber,
                            interventionNumber: intervention.interventionNumber,
                            interventionDate: dateIntervention,
                            report: intervention.report,
                            type: type,
                            processState: processState,
                            state: state,
                            duration: duration,
                            valid: intervention.toBeValidated.toString()
                        }
                    }
                }).filter(i => i);
                interventionsArr = interventionsArr.concat(interventions);
            });
        }


        interventionsArr = _.uniq(interventionsArr, (item, key, a) => item._id);
        // a partir daqui, depois de ter todas as intervenções, vou começar a fazer as respetivas filtragens;

        interventionsArr = interventionsArr.filter(this.filterIntervention.bind(this));

        if (interventionsArr.length == 0) {
            return null;
        }

        this.state.rendered = true;

        // return interventionsArr;
        return interventionsArr.map(int => {
            return (
                <tr key={int._id}>
                    <th>{int.client}</th>
                    <th>{int.dateHour}</th>
                    <th>{int.processNumber}</th>
                    <th>{int.processState}</th>
                    <th>{int.interventionNumber}</th>
                    <th>{int.interventionDate}</th>
                    <th>{int.state}</th>
                    <th>{int.report}</th>
                    <th>{int.duration}</th>
                    <th>{int.type}</th>
                    <th>{int.valid}</th>
                </tr>
            );
        });
    }

    //new
    render() {

        return (
            <ContentWrapper>
                <div className="content-heading">
                    Dev2Grow
                    <small data-localize="dashboard.WELCOME">Bem vindo à Dev2Grow!</small>
                </div>
                <Row>
                    <Col lg={12}>
                        <Panel>
                            <h4
                                style={{
                                    display: "inline-block",
                                    margin: 0
                                }}>Processos em aberto</h4>
                            <div
                                className="checkbox c-checkbox"
                                style={{
                                    display: "inline-block",
                                    float: "right",
                                    margin: 0
                                }}>
                                <label>
                                    <input
                                        type="checkbox"
                                        name="mineSelected"
                                        checked={this.state.mineSelected}
                                        onChange={this
                                            .filterOwnProcesses
                                            .bind(this)} />
                                    <em className="fa fa-check"></em>
                                </label>
                            </div>
                            <h4
                                style={{
                                    display: "inline-block",
                                    float: "right",
                                    margin: 0,
                                    marginRight: "5px"
                                }}>Meus</h4>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Código do processo</th>
                                        <th>Tipo de processo</th>
                                        <th>Cliente</th>
                                        <th>Data</th>
                                        <th>Duração</th>
                                        <th>Pedido por</th>
                                        <th>Técnico atribuido</th>
                                        <th>Estado</th>
                                        <th></th>

                                    </tr>

                                </thead>
                                <tbody>
                                    {this.renderProcessesTable()}
                                </tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
                <Row>

                    <Col lg={12}>
                        <Panel>
                            <h4>Intervenções por validar</h4>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Número de intervenção</th>
                                        <th>Tipo de intervenção</th>
                                        <th>Cliente</th>
                                        <th>Colaborador</th>
                                        <th>Data</th>
                                        <th>Hora de início</th>
                                        <th>Hora de fim</th>
                                        <th>Tempo real</th>
                                        <th>Tempo cobrado</th>
                                        <th>Validado</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>{this.renderInterventionsToValidate()}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
                <Row>

                    <Col lg={12}>
                        <Panel>
                            <h4>Contratos com horas a esgotar</h4>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Código do contrato</th>
                                        <th>Tipo de contrato</th>
                                        <th>Cliente</th>
                                        <th>Data de início</th>
                                        <th>Data de fim</th>
                                        <th>Última renovação</th>
                                        <th>Horas</th>
                                        <th>Horas disponíveis</th>
                                        <th>Horas consumidas</th>
                                        <th>Estado</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>{this.renderContractsTable("contractsExhaustedHours")}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
                <Row>

                    <Col lg={12}>
                        <Panel>
                            <h4>Contratos a renovar</h4>
                            <Table id="dtCompanies" responsive hover>
                                <thead>
                                    <tr>
                                        <th>Código do contrato</th>
                                        <th>Tipo de contrato</th>
                                        <th>Cliente</th>
                                        <th>Data de início</th>
                                        <th>Data de fim</th>
                                        <th>Última renovação</th>
                                        <th>Horas</th>
                                        <th>Horas disponíveis</th>
                                        <th>Horas consumidas</th>
                                        <th>Estado</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>{this.renderContractsTable("contractsToRenew")}</tbody>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <Panel header="Filtros">
                            <Accordion style={{ cursor: 'pointer' }}>
                                <Panel className="accordion-panel-header" header={this.state.selectedRange} eventKey="1">
                                    <DateRange linkedCalendars={true} ranges={defaultRanges} startDate={now => now.subtract(30, 'day')} endDate={now => now} onInit={this.handleDateSelect.bind(this)} onChange={this.handleDateSelect.bind(this)} />
                                </Panel>
                                <Panel className="accordion-panel-header" header={this.state.selectedClientsText.toString().split(',').join(', ') || 'Clientes'} eventKey="2">
                                    <Row>
                                        <Col sm={12}>
                                            <InputGroup>
                                                <InputGroup.Button><Button style={{ backgroundColor: "#DAE047" }} className="btn-custom" onClick={this.changeClientsState.bind(this)}>{this.state.clientsState}</Button></InputGroup.Button>
                                                <select id="select2-clients" name="selectedClients" className="form-control" multiple="multiple" value={this.state.selectedClients} onChange={this.handleMultipleClients.bind(this)} style={{ width: '100%' }}>
                                                    <option></option>
                                                    {
                                                        console.log("MY CLIENTS", Array.isArray(this.state.clients)) &&
                                                        this.state.clients && Array.isArray(this.state.clients) && this.state.clients.map(client => {
                                                            return <option key={client._id} value={client._id}>{client.abrevName}</option>
                                                        })
                                                    }
                                                </select>
                                            </InputGroup>
                                        </Col>
                                    </Row>
                                </Panel>
                                <Panel className="accordion-panel-header" header={this.state.selectedContractsText.toString().split(',').join(', ') || 'Contratos'} eventKey="3">
                                    <Row>
                                        <Col sm={12}>
                                            <InputGroup>
                                                <InputGroup.Button><Button style={{ backgroundColor: "#DAE047" }} className="btn-custom" onClick={this.changeContractState.bind(this)}>{this.state.contractsState}</Button></InputGroup.Button>
                                                <select id="select2-contracts" name="selectedContracts" className="form-control" multiple="multiple" value={this.state.selectedContracts} onChange={this.handleMultipleContracts.bind(this)} style={{ width: '100%' }}>
                                                    <option></option>
                                                    {this.renderContracts()}
                                                </select>
                                            </InputGroup>
                                        </Col>
                                    </Row>
                                </Panel>
                                <Panel className="accordion-panel-header" header={this.state.selectedProcessStatesText.toString().split(',').join(', ') || 'Estados Processos'} eventKey="4">
                                    <Row>
                                        <Col sm={12}>
                                            <InputGroup>
                                                <InputGroup.Button><Button style={{ backgroundColor: "#DAE047" }} className="btn-custom" onClick={this.changeProcState.bind(this)}>{this.state.procState}</Button></InputGroup.Button>
                                                <select id="select2-processes" name="selectedProcessStates" className="form-control" multiple="multiple" value={this.state.selectedProcessStates} onChange={this.handleMultipleProcessStates.bind(this)} style={{ width: '100%' }}>
                                                    <option></option>
                                                    {
                                                        this.state.processStates.map(state => {
                                                            return <option key={state._id} value={state._id}>{state.stateName}</option>
                                                        })
                                                    }
                                                </select>
                                            </InputGroup>
                                        </Col>
                                    </Row>
                                </Panel>
                            </Accordion>
                        </Panel>
                        <Panel header="Intervenções">
                            <Table id="dtDashboard" responsive striped hover>
                                <thead>
                                    <tr>
                                        <th>Cliente</th>
                                        <th>Data</th>
                                        <th>Nº Processo</th>
                                        <th>Estado Processo</th>
                                        <th>Nº Intervenção</th>
                                        <th>Data Intervenção</th>
                                        <th>Estado Intervenção</th>
                                        <th>Relatório Interno</th>
                                        <th>Duração</th>
                                        <th>Tipo de Intervenção</th>
                                        <th>Validada</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.renderInterventions()
                                    }
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>
                                            <input type="text" name="filter_client" placeholder="Filter Client" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_date" placeholder="Filter Date" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_process_number" placeholder="Filter Process Number" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_process_state" placeholder="Filter Process State" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_intervention_number" placeholder="Filter Intervention Number" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_intervention_date" placeholder="Filter Intervention Date" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_intervention_state" placeholder="Filter Intervention State" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_internal_report" placeholder="Filter Internal Report" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_duration" placeholder="Filter Duration" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_type" placeholder="Filter Type" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                        <th>
                                            <input type="text" name="filter_valid" placeholder="Filter Valid" className="form-control input-sm datatable_input_col_search" />
                                        </th>
                                    </tr>
                                </tfoot>
                            </Table>
                        </Panel>
                    </Col>
                </Row>
            </ContentWrapper>
        );
    }

}

export default Dashboard;
