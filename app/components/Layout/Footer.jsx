import React from 'react';

class Footer extends React.Component {

    render() {
        return (
            <footer>
                <span>&copy;Dev2Grow 2017</span>
            </footer>
        );
    }

}

export default Footer;
