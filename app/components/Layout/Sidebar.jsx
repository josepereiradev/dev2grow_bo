import React from 'react';
import { Router, Route, Link, History, withRouter } from 'react-router';
import axios from 'node_modules/axios';


import pubsub from 'pubsub-js';
import { Collapse } from 'react-bootstrap';
import SidebarRun from './Sidebar.run';
import {browserHistory} from 'react-router';

class Sidebar extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            userBlockCollapse: false,
            collapse: {
                   dashboard: this.routeActive(['dashboard']),
                   news: this.routeActive(['news', 'newsTypes']),
                   pages: false,
                   layouts: false
            },
            name: sessionStorage.getItem('user') ? JSON.parse(sessionStorage.getItem('user')).name : '',
            id: sessionStorage.getItem('user') ? JSON.parse(sessionStorage.getItem('user')).id : '',
            //photo: sessionStorage.getItem('user') ? JSON.parse(sessionStorage.getItem('user')).photo : '',
            user: []       
        };
        this.pubsub_token = pubsub.subscribe('toggleUserblock', () => {
            this.setState({
                userBlockCollapse: !this.state.userBlockCollapse
            });
        });
        		this.logout = this.logout.bind(this);

    };

    componentDidMount() {
        // pass navigator to access router api
        SidebarRun(this.navigator.bind(this));
    }
    componentWillMount() {
        var th = this;
          this.serverRequest = axios
            .get(__APIURL__ + "/users/"+ this.state.id)
            .then(function (result) {
                th.setState({user: result.data});
            })
    }

    navigator(route) {
        this.props.router.push(route)
    }

    componentWillUnmount() {
        // React removed me from the DOM, I have to unsubscribe from the pubsub using my token
        pubsub.unsubscribe(this.pubsub_token);
    }

    routeActive(paths) {
        paths = Array.isArray(paths) ? paths : [paths];
        for (let p in paths) {
            if (this.props.router.isActive(paths[p]) === true)
                return true;
        }
        return false;
    }

    toggleItemCollapse(stateName) {
        var newCollapseState = {};
        for (let c in this.state.collapse) {
            if (this.state.collapse[c] === true && c !== stateName)
                this.state.collapse[c] = false;
        }
        this.setState({
            collapse: {
                [stateName]: !this.state.collapse[stateName]
            }
        });
    }
    logout() {
        this.navigator('/login');
    }

    render() {
        var photoLink = null;
                if(this.state.user.photo != null){
                    if (this.state.user.photo.indexOf("fbcdn") == -1){
                    photoLink = this.state.user.photo+"?time="+moment().valueOf();
                    }else{
                        photoLink = this.state.user.photo+"&time="+moment().valueOf()
                    }
                }
        return (
            <aside className='aside'>
                { /* START Sidebar (left) */}
                <div className="aside-inner">
                    <nav data-sidebar-anyclick-close="" className="sidebar">
                        { /* START sidebar nav */}
                        <ul className="nav">
                            { /* START user info */}
                            <li className="has-user-block">
                                <Collapse id="user-block" in={this.state.userBlockCollapse}>
                                    <div>
                                        <div className="item user-block">
                                            { /* User picture */}
                                            <div className="user-block-picture">
                                                <div className="user-block-status">
                                                    <img src={photoLink != null ? photoLink : "http://54.171.236.54/assets/img/profile/unknown.png"} alt="Avatar" width="60" height="60" className="img-thumbnail img-circle" />
                                                    <div className="circle circle-success circle-lg"></div>
                                                </div>
                                            </div>
                                            { /* Name and Job */}
                                            <div className="user-block-info">
                                                <span className="user-block-name">Olá, { this.state.name }</span>
                                                <span className="user-block-role">{ this.state.userFunction != null ? this.state.userFunction : '' }</span>
                                            </div>
                                            <ul className="user-block-info nav">
                                                <li className='active'>
                                                    {/*onClick=link to end session*/}
                                                    <div className="nav-item" title="Logout" onClick={this.logout}>
                                                        <em className="icon-logout"></em>
                                                        {/*For now let's redirect to Login page*/}
                                                        <span data-localize="sidebar.nav.pages.LOGIN">&nbsp;&nbsp;&nbsp;&nbsp;Terminar sessão</span>
                                                    </div>                                                  
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </Collapse>
                            </li>
                            { /* END user info */}
                            { /* Iterates over all sidebar items */}
                            
                            <li className={ this.routeActive('dashboard') ? 'active' : '' }>
                                <Link to="/" className="nav-item" title="Dashboard">
                                   
                                    <em className="fa fa-home"></em>
                                    <span data-localize="sidebar.nav.DASHBOARD">Início</span>
                                </Link>
                                    
                            </li>

                           

                            {/*Tabelas da S2G*/}
                            <li className={ this.routeActive(['news', 'newsTypes']) ? 'active' : '' }>
                                <Collapse in={ this.state.collapse.news } timeout={ 100 }>
                                    <ul id="news" className="nav sidebar-subnav">
                                        <li className="sidebar-subnav-header">Notícias</li>
                                        <li className={ this.routeActive('news') ? 'active' : '' }>
                                            <Link to="news" title="Notícias">
                                            <span>Notícias</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('newsTypes') ? 'active' : '' }>
                                            <Link to="newsTypes" title="Tipos de Notícias">
                                            <span>Tipos de Notícias</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </Collapse>
                            </li>

                            <li className={ this.routeActive(['users', 'functions']) ? 'active' : '' }>
                                <Link to="users" className="nav-item" title="Utilizadores" onClick={ this.toggleItemCollapse.bind(this, 'users') }>
                                    <em className="fa fa-user"></em>
                                    <span data-localize="sidebar.nav.USERS">Utilizadores</span>
                                </Link>
                                {/*<div className="nav-item" onClick={ this.toggleItemCollapse.bind(this, 'users') }>
                                    <em className="fa fa-user"></em>
                                    <span data-localize="sidebar.nav.USERS">Utilizadores</span>
                                </div>*/}
                                <Collapse in={ this.state.collapse.users } timeout={ 100 }>
                                    <ul id="users" className="nav sidebar-subnav">
                                        <li className="sidebar-subnav-header">Utilizadores</li>
                                        <li className={ this.routeActive('users') ? 'active' : '' }>
                                            <Link to="users" title="Utilizadores">
                                            <span>Utilizadores</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('functions') ? 'active' : '' }>
                                            <Link to="functions" title="Funções">
                                            <span>Funções</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </Collapse>
                            </li>

                            <li className={ this.routeActive('companies') ? 'active' : '' }>
                                <Link to="companies" title="Empresas" onClick={ this.toggleItemCollapse.bind(this, 'companies') }>     
                                    <em className="fa fa-users"></em>
                                    <span data-localize="sidebar.nav.COMPANY">Empresas</span>
                                </Link>
                                {/*<div className="nav-item" onClick={ this.toggleItemCollapse.bind(this, 'companies') }>
                                       <em className="fa fa-users"></em>
                                    <span data-localize="sidebar.nav.COMPANY">Empresas</span>
                                </div>*/}
                            </li>
                            
                            <li className={ this.routeActive('entities') ? 'active' : '' }>
                                <Link to="customers" className="nav-item" title="Clientes">
                                    <em className="fa fa-briefcase"></em>
                                    <span /*data-localize="sidebar.nav.ENTITIES"*/>Clientes</span>
                                </Link>
                                    
                            </li>



                            <li className={ this.routeActive(['contacts', 'contactTypes']) ? 'active' : '' }>
                                <Link to="contacts" className="nav-item" title="Contactos" onClick={ this.toggleItemCollapse.bind(this, 'contacts') }>
                                    <em className="fa fa-phone"></em>
                                    <span data-localize="sidebar.nav.CONTACTS">Contactos</span>
                                </Link>
                                {/*<div className="nav-item" onClick={ this.toggleItemCollapse.bind(this, 'contacts') }>
                                       <em className="fa fa-phone"></em>
                                    <span data-localize="sidebar.nav.CONTACTS">Contactos</span>
                                </div>*/}
                                <Collapse in={ this.state.collapse.contacts } timeout={ 100 }>
                                    <ul id="contacts" className="nav sidebar-subnav">
                                        <li className="sidebar-subnav-header">Contactos</li>
                                        <li className={ this.routeActive('contacts') ? 'active' : '' }>
                                            <Link to="contacts" title="Contactos">
                                            <span>Contactos</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('contactTypes') ? 'active' : '' }>
                                            <Link to="contactTypes" title="Tipos de Contactos">
                                            <span>Tipos de Contactos</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </Collapse>
                            </li>

                            <li className={ this.routeActive(['contracts', 'contractTypes']) ? 'active' : '' }>
                                <Link to="contracts" className="nav-item" title="Contratos" onClick={ this.toggleItemCollapse.bind(this, 'contracts') }>
                                    <em className="fa fa-file-text-o"></em>
                                    <span data-localize="sidebar.nav.CONTRACTS">Contratos</span>
                                </Link>
                                {/*<div className="nav-item" onClick={ this.toggleItemCollapse.bind(this, 'contracts') }>
                                       <em className="fa fa-file-text-o"></em>
                                    <span data-localize="sidebar.nav.CONTRACTS">Contratos</span>
                                </div>*/}
                                <Collapse in={ this.state.collapse.contracts } timeout={ 100 }>
                                    <ul id="contracts" className="nav sidebar-subnav">
                                        <li className="sidebar-subnav-header">Contratos</li>
                                        <li className={ this.routeActive('contracts') ? 'active' : '' }>
                                            <Link to="contracts" title="Contratos">
                                            <span>Contratos</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('contractTypes') ? 'active' : '' }>
                                            <Link to="contractTypes" title="Tipos de Contratos">
                                            <span>Tipos de Contratos</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('contractStates') ? 'active' : '' }>
                                            <Link to="contractStates" title="Estados de Contratos">
                                            <span>Estados de Contratos</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </Collapse>
                            </li>

                            <li className={ this.routeActive(['projects', 'projectTypes']) ? 'active' : '' }>
                                <Link to="projects" className="nav-item" title="Projetos" onClick={ this.toggleItemCollapse.bind(this, 'projects') }>
                                    <em className="fa fa-tasks"></em>
                                    <span data-localize="sidebar.nav.PROJECTS">Projetos</span>
                                </Link>
                               {/* <div className="nav-item" onClick={ this.toggleItemCollapse.bind(this, 'projects') }>
                                       <em className="fa fa-tasks"></em>
                                    <span data-localize="sidebar.nav.PROJECTS">Projetos</span>
                                </div>*/}
                                <Collapse in={ this.state.collapse.projects } timeout={ 100 }>
                                    <ul id="projects" className="nav sidebar-subnav">
                                        <li className="sidebar-subnav-header">Projetos</li>
                                        <li className={ this.routeActive('projects') ? 'active' : '' }>
                                            <Link to="projects" title="Projetos">
                                            <span>Projetos</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('projectTypes') ? 'active' : '' }>
                                            <Link to="projectTypes" title="Tipos de Projetos">
                                            <span>Tipos de Projetos</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('projectStates') ? 'active' : '' }>
                                            <Link to="projectStates" title="Estados de Projetos">
                                            <span>Estados de Projetos</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </Collapse>
                            </li>

                            <li className={ this.routeActive(['processes', 'processTypes']) ? 'active' : '' }>
                                <Link to="processes" className="nav-item" title="Processos" onClick={ this.toggleItemCollapse.bind(this, 'processes') }>
                                    <em className="fa fa-spinner"></em>
                                    <span data-localize="sidebar.nav.PROCESSES">Processos</span>
                                </Link>
                                {/*<div className="nav-item" onClick={ this.toggleItemCollapse.bind(this, 'processes') }>
                                       <em className="fa fa-spinner"></em>
                                    <span data-localize="sidebar.nav.PROCESSES">Processos</span>
                                </div>*/}
                                <Collapse in={ this.state.collapse.processes } timeout={ 100 }>
                                    <ul id="processes" className="nav sidebar-subnav">
                                        <li className="sidebar-subnav-header">Processos</li>
                                        <li className={ this.routeActive('processes') ? 'active' : '' }>
                                            <Link to="processes" title="Processos">
                                            <span>Processos</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('processTypes') ? 'active' : '' }>
                                            <Link to="processTypes" title="Tipos de Processo">
                                            <span>Tipos de Processo</span>
                                            </Link>
                                        </li>
                                         <li className={ this.routeActive('processStates') ? 'active' : '' }>
                                            <Link to="processStates" title="Estados de Processo">
                                            <span>Estados de Processo</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </Collapse>
                            </li>
                            
                            <li className={ this.routeActive(['interventions', 'interventionTypes']) ? 'active' : '' }>
                                <Link to="interventions" className="nav-item" title="Intervenções" onClick={ this.toggleItemCollapse.bind(this, 'interventions') }>
                                    <em className="fa fa-comments-o"></em>
                                    <span data-localize="sidebar.nav.INTERVENTIONS">Intervenções</span>
                                </Link>
                              {/*  <div className="nav-item" onClick={ this.toggleItemCollapse.bind(this, 'interventions') }>
                                       <em className="fa fa-comments-o"></em>
                                    <span data-localize="sidebar.nav.INTERVENTIONS">Intervenções</span>
                                </div>*/}
                                <Collapse in={ this.state.collapse.interventions } timeout={ 100 }>
                                    <ul id="interventions" className="nav sidebar-subnav">
                                        <li className="sidebar-subnav-header">Intervenções</li>
                                        <li className={ this.routeActive('interventions') ? 'active' : '' }>
                                            <Link to="interventions" title="Intervenções">
                                            <span>Intervenções</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('interventionTypes') ? 'active' : '' }>
                                            <Link to="interventionTypes" title="Tipos de Intervenção">
                                            <span>Tipos de Intervenção</span>
                                            </Link>
                                        </li>
                                        <li className={ this.routeActive('interventionMeans') ? 'active' : '' }>
                                            <Link to="interventionMeans" title="Meios de Intervenção">
                                            <span>Meios de Intervenção</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </Collapse>
                            </li>
                            <li className={this.routeActive(['templates', 'templates']) ? 'active' : ''}>
                                <Link to="templates" className="nav-item" title="Templates">
                                    <em className="fa fa-file-code-o"></em>
                                        <span data-localize="sidebar.nav.element.Templates">Email Templates</span>
                                </Link>
                            </li>

                        </ul>
                        { /* END sidebar nav */}
                    </nav>
                </div>
                { /* END Sidebar (left) */}
            </aside>
        );
    }

}

export default withRouter(Sidebar);

