import React from 'react';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import { Router, Route, Link, History } from 'react-router';
import axios from 'node_modules/axios';
import myStyles from '../../styles/myStyles.scss'

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputEmail: '',
            inputPassword: ''
        };

        this.handleChange = this
            .handleChange
            .bind(this);
        this.handleSubmit = this
            .handleSubmit
            .bind(this);

        sessionStorage.removeItem('user');
    }

    componentDidMount() {
        $('#loader').hide();
        sessionStorage.removeItem('user');
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });

    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(__APIURL__);
        var self = this;

        if (this.state.inputEmail != '' && this.state.inputPassword != '') {
            axios
                .post(__APIURL__ + '/auth/login', {
                    email: this.state.inputEmail,
                    password: this.state.inputPassword,
                    device: 'BO'
                })
                .then(function (response) {
                    if (response.status == 200) {
                        //  if ( response.data['role'].indexOf('admin') !== -1 ) {
                        sessionStorage.setItem('user', JSON.stringify(response.data));

                        const location = self.props.location
                        if (location.state && location.state.nextPathname) {
                            self
                                .props
                                .router
                                .push(location.state.nextPathname);
                        } else {
                            self
                                .props
                                .router
                                .push('/');
                        }
                        //  } else {
                        // TODO: HANDLE IT }
                    } else {
                        // TODO: HANDLE IT
                    }
                })
                .catch(function (error) {
                    console.log('error', error);
                    if (error.response) {
                        // The request was made and the server responded with a status code that falls
                        // out of the range of 2xx
                        console.log(error.response.data);
                        console.log(error.response.status);
                        console.log(error.response.headers);
                    } else if (error.request) {
                        // The request was made but no response was received `error.request` is an
                        // instance of XMLHttpRequest in the browser and an instance of
                        // http.ClientRequest in node.js
                        console.log(error.request);
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        console.log('Error', error.message);
                    }
                    console.log(error.config);

                    // TODO: HANDLE IT
                });
        }
    }

    render() {

        return (
            <div>
                <div id="loginWallpaper"></div>
                <div id="loginContent">
                    <div className="block-center wd-xl">
                        {/* START panel */}
                        <div id="whitePanelLogin" className="panel panel-dark panel-flat">
                            <div className="panel-heading text-center" style={{ background: "0" }}>
                                <a href="#">
                                    <img
                                        src="img/Dev2grow_colored.png"
                                        alt="Image"
                                        className="block-center img-responsive"
                                        width='180px' />
                                </a>
                            </div>
                            <div className="panel-body">
                                <form
                                    role="form"
                                    data-parsley-validate=""
                                    noValidate
                                    className="mb-lg"
                                    onSubmit={this.handleSubmit}>
                                    <div className="form-group has-feedback">
                                        <input
                                            id="inputEmail"
                                            name="inputEmail"
                                            type="email"
                                            placeholder="Introduza o email"
                                            autoComplete="off"
                                            required="required"
                                            className="form-control inputLoginText"
                                            value={this.state.inputEmail}
                                            onChange={this.handleChange} />
                                        <span className="fa fa-envelope form-control-feedback text-muted"></span>
                                    </div>
                                    <div className="form-group has-feedback">
                                        <input
                                            id="inputPassword"
                                            name="inputPassword"
                                            type="password"
                                            placeholder="Password"
                                            required="required"
                                            className="form-control inputLoginText"
                                            value={this.state.inputPassword}
                                            onChange={this.handleChange} />
                                        <span className="fa fa-lock form-control-feedback text-muted"></span>
                                    </div>
                          
                                    <button style={{ background: "#404042" }} type="submit" className="btn btn-block btn-primary mt-lg"><span style={{ color: "white", fontWeight: "bold" }}>Iniciar sessão</span></button>
                                </form>
                                {/* <p className="pt-lg text-center">Não está registado?</p>
                        <Link to="register" className="btn btn-block btn-default">Efectuar registo</Link>*/}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }

}

export default Login;
