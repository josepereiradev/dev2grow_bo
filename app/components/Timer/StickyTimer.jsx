import React from 'react';
import pubsub from 'pubsub-js';
import {
    Grid,
    Row,
    Col,
    Panel,
    Button,
    FormControl
} from 'react-bootstrap';
import {StickyContainer, Sticky} from 'react-sticky';
import {Router, Route, Link, History, withRouter} from 'react-router';
import {browserHistory} from 'react-router';
import moment from 'moment';
require("moment-duration-format");

const formattedSeconds = (time) => ((Math.floor(time / 3600) % 60 < 10)
    ? ('0' + Math.floor(time / 3600) % 60)
    : (Math.floor(time / 3600) % 60)) + ':' + ((Math.floor(time / 60) % 60 < 10)
    ? ('0' + Math.floor(time / 60) % 60)
    : (Math.floor(time / 60) % 60)) + ':' + ((time % 60 < 10)
    ? ('0' + time % 60)
    : (time % 60))

class StickyTimer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            secondsElapsed: 0,
            lastClearedIncrementer: null
        };
        this.incrementer = null;

    }

    componentWillMount() {
        console.log("LOCALSTORAGE:", localStorage.getItem('secondsElapsed'), localStorage.getItem('exitTime'), localStorage.getItem('paused'))

        if (localStorage.getItem('stopped') == "false") {

            if (localStorage.getItem('secondsElapsed') != null) {
                this.setState({
                    secondsElapsed: parseInt(JSON.parse(localStorage.getItem('secondsElapsed')))
                })

            } else {
                this.setState({secondsElapsed: 0})
            }
            if (localStorage.getItem('paused') == "false") {
                this.handleStartClick();

                if (localStorage.getItem('exitTime') != null) {
                    console.log(((Date.now() / 1000).toFixed(0) - localStorage.getItem('exitTime')))
                    this.setState({
                        secondsElapsed: parseInt(localStorage.getItem('secondsElapsed')) + (((Date.now() / 1000).toFixed(0) - localStorage.getItem('exitTime')))
                    })
                }
            }
        }
        if (localStorage.getItem('stopped') == "true") {
            this.setState({lastClearedIncrementer: 0});
            this.setState({secondsElapsed: 0});

        }
    }

    handleStartClick() {
        if (localStorage.getItem('paused') == "true") {

            localStorage.setItem('paused', false);
            localStorage.setItem('stopped', false);

            this.incrementer = setInterval(() => {
                this.setState({
                    secondsElapsed: parseInt(this.state.secondsElapsed) + 1
                }),
                console.log("seconds elapsed - ", this.state.secondsElapsed)
                console.log("now - ", (Date.now() / 1000).toFixed(0))
                localStorage.setItem('exitTime', (Date.now() / 1000).toFixed(0));
                localStorage.setItem('secondsElapsed', this.state.secondsElapsed);
            }, 1000);

        } else {

            localStorage.setItem('paused', false);
            localStorage.setItem('stopped', false);
            localStorage.removeItem('interventionStart');
            localStorage.setItem('interventionStart', moment().format()); //send in ISO String format

            this.setState({secondsElapsed: "1"});
            this.incrementer = setInterval(() => {
                this.setState({
                    secondsElapsed: parseInt(this.state.secondsElapsed) + 1
                }),
                //console.log("seconds elapsed - ", this.state.secondsElapsed)
                console.log("now - ", (Date.now() / 1000).toFixed(0))
                localStorage.setItem('exitTime', (Date.now() / 1000).toFixed(0));
                localStorage.setItem('secondsElapsed', this.state.secondsElapsed);
            }, 1000);
        }
    }

    handlePauseClick() {
        // localStorage.setItem('secondsElapsed', this.secondsElapsed);
        console.log("LOCALSTORAGE:", localStorage.getItem('secondsElapsed'), localStorage.getItem('exitTime'), localStorage.getItem('paused'))
        localStorage.setItem('paused', true);

        clearInterval(this.incrementer);
        this.setState({lastClearedIncrementer: this.incrementer});
        // console.log(this.incrementer) console.log("seconds now! ",
        // this.state.secondsElapsed, localStorage.getItem('actualTime'))
    }

    handleStopClick() {
        var beginTime;
        // if (localStorage.getItem('stopped') == "true") {
        //     alert("entrou")
        //     beginTime = moment();
        //     // localStorage.setItem('interventionStart', beginTime.format());
        // } else {
            beginTime = moment(localStorage.getItem('interventionStart'));
      
        // }

        var endTime = moment();
        var difTime = moment(endTime - beginTime);

        var timeToCharge = moment(this.state.secondsElapsed * 1000);

        clearInterval(this.incrementer);
        this.setState({secondsElapsed: 0, laps: []});
        localStorage.removeItem('actualTime');
        localStorage.removeItem('secondsElapsed');
        localStorage.setItem('interventionEnd', endTime.format()); //send in ISO string format
        localStorage.setItem('stopped', true);
        localStorage.setItem('totalInterventionTime', difTime.format());
        localStorage.setItem('timetoCharge', timeToCharge.format());

        this.setState({secondsElapsed: 0})
        localStorage.setItem('secondsElapsed', this.state.secondsElapsed);

        setTimeout(() => {
            //console.log(window.location.href, self.location.href)
            if (window.location.href != "http://localhost:3000/#/interventions/create") {
                window
                    .location
                    .assign("../#/interventions/create");
            } else {
                //console.log("reload")
                window
                    .location
                    .reload()
            }
        }, 2000);
        localStorage.setItem('secondsElapsed', 0);
    }

    /* handleLabClick() {
        this.setState({
            laps: this
                .state
                .laps
                .concat([this.state.secondsElapsed])
        })
    }*/

    render() {
        return (
            <StickyContainer >
                <Sticky >

                    {({
                        style,

                        // the following are also available but unused in this example
                        isSticky,
                        wasSticky,
                        distanceFromTop,
                        distanceFromBottom,
                        calculatedHeight
                    }) => {
                        return (
                            <div
                                style={{
                                display: "block",
                                position: "fixed",
                                backgroundColor: "#e15810 ",
                                color: "white",
                                top: "0",
                                left: "70%",
                                right: "auto",
                                height: "90px",
                                zIndex: "1500",
                                borderBottomLeftRadius: "5px",
                                borderBottomRightRadius: "5px"
                            }}>
                                <div
                                    className="stopwatch"
                                    style={{
                                    "marginTop": "10px",
                                    "backgroundColor": "white",
                                    "color": "black",
                                    "borderBottomLeftRadius": "5px",
                                    "borderBottomRightRadius": "5px",
                                    "borderTopLeftRadius": "5px",
                                    "borderTopRightRadius": "5px",
                                    "width": "160px",
                                    "height": "40px",
                                    "lineHeight": "40px",
                                    "fontSize": "36px",
                                    "textAlign": "center",
                                    "margin": "5px"
                                }}>

                                    <div className="stopwatch-timer">{formattedSeconds(parseInt(this.state.secondsElapsed))}</div>

                                    {/* https://codepen.io/seoh/pen/PPZYQy */}
                                    {(this.state.secondsElapsed === 0 || this.incrementer === this.state.lastClearedIncrementer
                                        ? <button
                                                className="btn btn-default"
                                                style={{
                                                fontSize: "9px",
                                                marginLeft: "3px",
                                                marginRight: "3px"
                                            }}
                                                onClick={this
                                                .handleStartClick
                                                .bind(this)}><em className="fa fa-play"/></button>
                                        : <button
                                            className="btn btn-default"
                                            style={{
                                            fontSize: "9px",
                                            marginLeft: "3px",
                                            marginRight: "3px"
                                        }}
                                            onClick={this
                                            .handlePauseClick
                                            .bind(this)}><em className="fa fa-pause"/></button>)}

                                    <button
                                        className="btn btn-default"
                                        style={{
                                        fontSize: "9px",
                                        marginLeft: "3px",
                                        marginRight: "3px"
                                    }}
                                        onClick={this
                                        .handleStopClick
                                        .bind(this)}><em className="fa fa-stop"/></button>

                                </div>

                            </div>
                        )
                    }
}
                </Sticky>
            </StickyContainer>
        );
    }

}

export default StickyTimer;
